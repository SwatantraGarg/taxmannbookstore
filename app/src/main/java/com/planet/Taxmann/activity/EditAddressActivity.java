package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.Addresses;
import com.planet.Taxmann.model.Country;
import com.planet.Taxmann.model.CountryCodeData;
import com.planet.Taxmann.model.CountryCodeResponse;
import com.planet.Taxmann.model.DeliveryCheckResponse;
import com.planet.Taxmann.model.Designation;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.GetCountryStateResponse;
import com.planet.Taxmann.model.GetDesignationListResponse;
import com.planet.Taxmann.model.GetStateCityResponse;
import com.planet.Taxmann.model.PostEmailMobile;
import com.planet.Taxmann.model.PostShippingAddress;
import com.planet.Taxmann.model.RegistrationRequest;
import com.planet.Taxmann.model.StateCity;
import com.planet.Taxmann.model.States;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditAddressActivity extends AppCompatActivity {
    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.country_layout)
    RelativeLayout countryLayout;

    @BindView(R.id.state_layout)
    RelativeLayout stateLayout;

    @BindView(R.id.country_view)
    View countryView;

    @BindView(R.id.state_view)
    View stateView;

    @BindView(R.id.user_name)
    EditText userName;

    @BindView(R.id.mobileEditText)
    EditText mobileNoEdit;

    @BindView(R.id.spinner_desig)
    SearchableSpinner spinnerDesign;

    @BindView(R.id.spinner_country)
    SearchableSpinner spinnerCountry;

    @BindView(R.id.spinner_state)
    SearchableSpinner spinnerState;

    @BindView(R.id.spinner)
    SearchableSpinner spinner;

    @BindView(R.id.pincode_no)
    EditText pinCode;

    @BindView(R.id.company_name)
    EditText companyName;

    @BindView(R.id.Flat_no)
    EditText flatNo;

    @BindView(R.id.street_name)
    EditText streetName;

    @BindView(R.id.radioGroup_ship)
    RadioGroup radioGroup;

    @BindView(R.id.ship_radio_home)
    RadioButton radioHome;

    @BindView(R.id.ship_radio_office)
    RadioButton radioOffice;

    @BindView(R.id.checkbox)
    CheckBox checkBoxDefault;

    @BindView(R.id.city_name)
    EditText cityName;

    @BindView(R.id.submit_btn)
    TextView submitBtn;

    @BindView(R.id.locality_name)
    EditText localityName;

    UserData userData;
    String statevalue,countryvalue,designvalue,countryCodevalue;
    List<Country> countryList;
    List<CountryCodeData> countryCodeData;
    Intent intent;
    String type, bill;
    int addID=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(EditAddressActivity.this,"user_data", UserData.class);

        getDesignationListResponse();
        getStateCountryListResponse();
        getCountryCodeResponse();
        intent = getIntent();

        if(intent!=null){
            List<Addresses> addressesList = intent.getParcelableArrayListExtra("address_info");
            int pos = intent.getIntExtra("position",-1);
            type = intent.getStringExtra("type");
            bill = intent.getStringExtra("bill");

        if(addressesList!=null && addressesList.size()>0){
            addID = addressesList.get(pos).getAddressID();
            userName.setText(addressesList.get(pos).getName());
            mobileNoEdit.setText(addressesList.get(pos).getUserMobile());
            companyName.setText(addressesList.get(pos).getCompanyName());
            flatNo.setText(addressesList.get(pos).getAddress1());
            streetName.setText(addressesList.get(pos).getAddress2());
            localityName.setText(addressesList.get(pos).getAreaLocality());
            pinCode.setText(addressesList.get(pos).getPinCode());
            cityName.setText(addressesList.get(pos).getCity());
            statevalue= addressesList.get(pos).getState();
            designvalue= addressesList.get(pos).getDesignation();
            countryCodevalue= addressesList.get(pos).getCountrycode();
            countryvalue= addressesList.get(pos).getCountry();
            if(addressesList.get(pos).getAddType()==1){
                radioHome.setChecked(true);
            }
            else if(addressesList.get(pos).getAddType()==2){
                radioOffice.setChecked(true);
            }
            else {
                radioHome.setChecked(true);
            }

            if(addressesList.get(pos).getIsDefault()==1){
                checkBoxDefault.setChecked(true);
            }else {
                checkBoxDefault.setChecked(false);
            }

        }
        if(type!=null && type.equalsIgnoreCase("edit")){
            stateLayout.setVisibility(View.VISIBLE);
            countryLayout.setVisibility(View.VISIBLE);
            stateView.setVisibility(View.VISIBLE);
            countryView.setVisibility(View.VISIBLE);
        }

        }
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditAddressActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });
        pinCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if(s.length()==6){
                    String pin_code = pinCode.getText().toString();
                    getStateCityResponse(countryList,pin_code);
                }
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(s.length()==6){
                String pin_code = pinCode.getText().toString();
                getStateCityResponse(countryList,pin_code);
            }

            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userName.getText().toString().isEmpty()){
                    Snackbar.make(userName,"Please enter your name",Snackbar.LENGTH_SHORT).show();
                }

                else if(mobileNoEdit.getText().toString().isEmpty()){
                    Snackbar.make(mobileNoEdit,"Please enter your mobile no",Snackbar.LENGTH_SHORT).show();
                }
                else if(mobileNoEdit.getText().toString().length()<6){
                    Snackbar.make(mobileNoEdit,"Please enter valid mobile no",Snackbar.LENGTH_SHORT).show();
                }
                else if(spinnerCountry.getSelectedItem()==null || spinnerCountry.getSelectedItem().toString().isEmpty()) {
                    Snackbar.make(spinnerCountry,"Please enter your country",Snackbar.LENGTH_SHORT).show();
                }
                else if(flatNo.getText().toString().isEmpty()){
                    Snackbar.make(flatNo,"Please enter your flat no.",Snackbar.LENGTH_SHORT).show();
                }
                else  if(cityName.getText().toString().isEmpty()){
                    Snackbar.make(cityName,"Please enter your city name",Snackbar.LENGTH_SHORT).show();
                }
                else  if(streetName.getText().toString().isEmpty()){
                    Snackbar.make(streetName,"Please enter your street name",Snackbar.LENGTH_SHORT).show();
                }
                else  if(pinCode.getText().toString().isEmpty()){
                    Snackbar.make(pinCode,"Please enter your pincode",Snackbar.LENGTH_SHORT).show();
                }
                else if(pinCode.getText().toString().length()<4){
                    Snackbar.make(pinCode,"Please enter valid pincode",Snackbar.LENGTH_SHORT).show();
                }
                else if(spinnerState.getSelectedItem()==null || spinnerState.getSelectedItem().toString().isEmpty()){
                    Snackbar.make(spinnerState,"Please enter your state name",Snackbar.LENGTH_SHORT).show();
                }
                else {
                    String user_name = userName.getText().toString();
                    String design;
                    if(spinnerDesign.getSelectedItem().toString().equalsIgnoreCase("Select designation")){
                        design = "";
                    }
                    else {
                         design = spinnerDesign.getSelectedItem().toString();
                    }

                    String flat = flatNo.getText().toString();
                    String locality = localityName.getText().toString();
                    String country = spinnerCountry.getSelectedItem().toString();
                    String state = spinnerState.getSelectedItem().toString();
                    String city = cityName.getText().toString();
                    String pincode = pinCode.getText().toString();
                    String street = streetName.getText().toString();
                    String company = companyName.getText().toString();
                    String mobile = mobileNoEdit.getText().toString();
                    int add_type = radioGroup.getCheckedRadioButtonId();
                    int pos = spinner.getSelectedItemPosition();
                    String country_code = countryCodeData.get(pos).getDialCode();

                    RadioButton radioButton = (RadioButton)

                            radioGroup.findViewById(add_type);

                    String selectedtext = (String) radioButton.getText();
                    int addType= 0;
                    if(selectedtext.equalsIgnoreCase("Home")){
                       addType = 1;
                    }
                    else {
                        addType = 2;
                    }

                    int isDefault= 0;
                   if(checkBoxDefault.isChecked()){
                       isDefault= 1;

                   }else {
                       isDefault=0;
                   }

                   PostShippingAddress postShippingAddress = new PostShippingAddress();
                   postShippingAddress.setAddType(addType);
                   postShippingAddress.setAddress1(flat);
                   postShippingAddress.setAddress2(street);
                   if(bill!=null && bill.equalsIgnoreCase("bill")){
                       postShippingAddress.setAddressType(1);
                   }
                   else {
                       postShippingAddress.setAddressType(2);
                   }

                   if(addID>0){
                       postShippingAddress.setAddressID(addID);
                   }
                   else {
                       postShippingAddress.setAddressID(0);
                   }

                   postShippingAddress.setAddressTitle("");
                   postShippingAddress.setAreaLocality(locality);
                   postShippingAddress.setCity(city);
                   postShippingAddress.setCompanyName(company);
                   postShippingAddress.setCountry(country);
                   postShippingAddress.setCountrycode(country_code);
                   postShippingAddress.setCurrentUser(0);
                   postShippingAddress.setDesignation(design);
                   postShippingAddress.setFullAddress("");
                   postShippingAddress.setFullName(user_name);
                   postShippingAddress.setIsDefault(isDefault);
                   postShippingAddress.setLandmark("");
                   postShippingAddress.setName("");
                   postShippingAddress.setNickName("");
                   postShippingAddress.setPinCode(pincode);
                   postShippingAddress.setSalutation("");
                   postShippingAddress.setState(state);
                   postShippingAddress.setUserMobile(mobile);
                   submitShippingAddressResponse(postShippingAddress);
                }
            }
        });
    }

    public void getDesignationListResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GetDesignationListResponse> call= apiService.getDesignationListResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(EditAddressActivity.this,"Please wait");
        call.enqueue(new Callback<GetDesignationListResponse>() {
            @Override
            public void onResponse(Call<GetDesignationListResponse> call, Response<GetDesignationListResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(EditAddressActivity.this);
                int statusCode = response.code();
                GetDesignationListResponse getDesignationListResponse =response.body();

                if(getDesignationListResponse!=null){
                    List<Designation> designationList = getDesignationListResponse.getData();
                    List<String> designations = new ArrayList<>();
                    designations.add("Select designation");
                    for (int i = 0; i <designationList.size() ; i++) {
                        String name = designationList.get(i).getName().toLowerCase();
                        designations.add(name);
                    }

                    // Creating adapter for spinner
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(EditAddressActivity.this, android.R.layout.simple_spinner_item, designations);

                    // Drop down layout style - list view with radio button
                    dataAdapter.setDropDownViewResource(R.layout.spinner_item);

                    // attaching data adapter to spinner
                    spinnerDesign.setAdapter(dataAdapter);
                    spinnerDesign.setTitle("Select designation");

                    if(designvalue!=null){
                        spinnerDesign.setSelection(dataAdapter.getPosition(designvalue));

                    }

                }

            }

            @Override
            public void onFailure(Call<GetDesignationListResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(EditAddressActivity.this);
                Toast.makeText(EditAddressActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void getStateCountryListResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GetCountryStateResponse> call= apiService.getCountryStateListresponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(EditAddressActivity.this,"Please wait");
        call.enqueue(new Callback<GetCountryStateResponse>() {
            @Override
            public void onResponse(Call<GetCountryStateResponse> call, Response<GetCountryStateResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(EditAddressActivity.this);
                int statusCode = response.code();
                GetCountryStateResponse getCountryStateResponse =response.body();
                if(getCountryStateResponse!=null){
                    countryList = getCountryStateResponse.getData();
                    List<String> contries = new ArrayList<>();
                    for (int i = 0; i <countryList.size(); i++) {
                        String name = countryList.get(i).getName();
                        contries.add(name);



                    }

                    // Creating adapter for spinner
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(EditAddressActivity.this, android.R.layout.simple_spinner_item, contries);

                    // Drop down layout style - list view with radio button
                    dataAdapter.setDropDownViewResource(R.layout.spinner_item);


                    // attaching data adapter to spinner
                    spinnerCountry.setAdapter(dataAdapter);


                    spinnerCountry.setTitle("Select Country");
                    setStateList(countryList,96);

                    spinnerCountry.setSelection(96);
                    if(countryvalue!= null){
                        spinnerCountry.setSelection(dataAdapter.getPosition(countryvalue));
                    }

                spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        int pos = spinnerCountry.getSelectedItemPosition();
                        setStateList(countryList,pos);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                       /* String myString = "India"; //the value you want the position for

                        //  dataAdapter = (ArrayAdapter) spinnerCountry.getAdapter(); //cast to an ArrayAdapter

                        int spinnerPosition = dataAdapter.getPosition(myString);


                        //set the default according to value
                        spinnerCountry.setSelection(0,true);*/
                    }
                });

                }

            }

            @Override
            public void onFailure(Call<GetCountryStateResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(EditAddressActivity.this);
                Toast.makeText(EditAddressActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void setStateList(List<Country> countryList, int pos){
        List<States> statesList = countryList.get(pos).getStatesList();
        List<String> states = new ArrayList<>();
        for (int i = 0; i <statesList.size() ; i++) {
            String name = statesList.get(i).getName().toLowerCase();

            states.add(name);
        }

        // Creating adapter for spinner
        ArrayAdapter<String>  dataAdapter1 = new ArrayAdapter<String>(EditAddressActivity.this, android.R.layout.simple_spinner_item, states);

        // Drop down layout style - list view with radio button
        dataAdapter1.setDropDownViewResource(R.layout.spinner_item);

        // attaching data adapter to spinner
        spinnerState.setAdapter(dataAdapter1);
        spinnerState.setTitle("Select state");
         //the value you want the position for

        //  dataAdapter = (ArrayAdapter) spinnerCountry.getAdapter(); //cast to an ArrayAdapter

        if (statevalue != null) {
            String name = statevalue.toLowerCase();
            spinnerState.setSelection(dataAdapter1.getPosition(name));
        }
    }

    public void getStateCityResponse(List<Country> list, String pin_code){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GetStateCityResponse> call= apiSerivce.getStateCityResponse(pin_code,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(EditAddressActivity.this,"Please wait");
        call.enqueue(new Callback<GetStateCityResponse>() {
            @Override
            public void onResponse(Call<GetStateCityResponse> call, Response<GetStateCityResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(EditAddressActivity.this);
                int statusCode = response.code();
                GetStateCityResponse getStateCityResponse = response.body();
                if(getStateCityResponse.getData()!=null){
                    if(getStateCityResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                        StateCity stateCity = getStateCityResponse.getData();

                        if(stateCity!=null){
                            stateLayout.setVisibility(View.VISIBLE);
                            countryLayout.setVisibility(View.VISIBLE);
                            stateView.setVisibility(View.VISIBLE);
                            countryView.setVisibility(View.VISIBLE);
                            cityName.setText(stateCity.getCityName());
                             statevalue = stateCity.getStateName();
                            setStateList(list,96);
                            cityName.requestFocus();
                            cityName.setSelection(cityName.length());

                            //spinnerState.setSelection(105);

                        }
                    }
                    else {
                        Toast.makeText(EditAddressActivity.this, getStateCityResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                    }

                }

            }

            @Override
            public void onFailure(Call<GetStateCityResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(EditAddressActivity.this);
                Toast.makeText(EditAddressActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void submitShippingAddressResponse(PostShippingAddress postShippingAddress){
        RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call;
        if(bill!=null && bill.equalsIgnoreCase("bill")){
           call = apiService.postBillingAddressResponse(postShippingAddress,userData.getUser_token());
        }
        else {
             call= apiService.postShippingAddressResponse(postShippingAddress,userData.getUser_token());
        }

        final KProgressHUD kProgressHUD=Constants.ShowProgress(EditAddressActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    Toast.makeText(EditAddressActivity.this, response.body().getStatusMsg(), Toast.LENGTH_LONG).show();
                    finish();
                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(EditAddressActivity.this, response.body().getStatusMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(EditAddressActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    public boolean isValidWord(String word) {
        return word.matches("[A-Za-z][^.]*");
    }
    public void getCountryCodeResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<CountryCodeResponse> call= apiService.getCountryCodeResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(EditAddressActivity.this,"Please wait");
        call.enqueue(new Callback<CountryCodeResponse>() {
            @Override
            public void onResponse(Call<CountryCodeResponse> call, Response<CountryCodeResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(EditAddressActivity.this);
                int statusCode = response.code();
                CountryCodeResponse countryCodeResponse = response.body();
                if(countryCodeResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    countryCodeData = countryCodeResponse.getData();
                    List<String> country_list = new ArrayList<>();
                    if(countryCodeData!=null && countryCodeData.size()>0){
                        for (int i = 0; i < countryCodeData.size(); i++) {
                            country_list.add(countryCodeData.get(i).getName());
                        }
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditAddressActivity.this, android.R.layout.simple_list_item_1,country_list);
                    spinner.setAdapter(adapter);
                    if(countryCodevalue!=null){
                        int pos = 0;
                        for (int i = 0; i <countryCodeData.size() ; i++) {
                            if(countryCodevalue.equalsIgnoreCase(countryCodeData.get(i).getDialCode())){
                                pos = i;
                            }
                            spinner.setSelection(pos);


                        }
                    }

                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(EditAddressActivity.this,response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CountryCodeResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(EditAddressActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

}
