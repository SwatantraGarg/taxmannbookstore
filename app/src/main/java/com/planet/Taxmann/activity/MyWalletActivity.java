package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.TransactionAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.Designation;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.GetDesignationListResponse;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import java.util.ArrayList;
import java.util.List;

public class MyWalletActivity extends AppCompatActivity {
    @BindView(R.id.money)
    TextView money;

    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.trasaction_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.no_transaction_layout)
    LinearLayout noTransLayout;
    UserData userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(MyWalletActivity.this,"user_data", UserData.class);
        getLoyalityPointResponse();

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyWalletActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void getLoyalityPointResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.getLoyalityPointResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(MyWalletActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(MyWalletActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse =response.body();
                if(generalResponse.getData()!=null){
                    double amount = (double) generalResponse.getData();
                    money.setText("₹"+Math.round(amount));
                    if(amount==0){
                        noTransLayout.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }
                    else {
                        noTransLayout.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        TransactionAdapter transactionAdapter = new TransactionAdapter();
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MyWalletActivity.this,LinearLayoutManager.VERTICAL,false);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        recyclerView.setAdapter(transactionAdapter);
                        transactionAdapter.notifyDataSetChanged();
                        recyclerView.setNestedScrollingEnabled(false);
                    }



                }

            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                //Constants.hideSoftKeyboard(MyWalletActivity.this);
                Toast.makeText(MyWalletActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
