package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.ShipAddressAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.Addresses;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.PersonalInfo;
import com.planet.Taxmann.model.PostShippingAddress;
import com.planet.Taxmann.model.USerInfo;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.model.UserInfoResponse;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageAddressActivity extends AppCompatActivity {
    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.add_new_btn)
    TextView addNewBtn;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.no_address_layout)
    LinearLayout noAddressLayout;

    @BindView(R.id.add_new_add)
    TextView addNewAddBtn;

    @BindView(R.id.visible_add_layout)
    LinearLayout visibleAddLayout;

    UserData userData;


    ShipAddressAdapter shipAddressAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_address);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(ManageAddressActivity.this,"user_data", UserData.class);
        getPersonalInfoResponse();
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageAddressActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });
        addNewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageAddressActivity.this, EditAddressActivity.class);
                startActivity(intent);
            }
        });

        addNewAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageAddressActivity.this, EditAddressActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getPersonalInfoResponse();
    }

    public void getPersonalInfoResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<UserInfoResponse> call= apiService.getUserInfoResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(ManageAddressActivity.this,"Please wait");
        call.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(ManageAddressActivity.this);
                int statusCode = response.code();
                UserInfoResponse userInfoResponse=response.body();
                if(userInfoResponse!=null) {
                    USerInfo userInfo = userInfoResponse.getData();
                    if(userInfo!=null){
                        if(userInfo.getAddress()!=null && userInfo.getAddress().size()>0){
                            List<Addresses> addressList = userInfo.getAddress();

                            if(addressList!=null && addressList.size()>0){
                                visibleAddLayout.setVisibility(View.VISIBLE);
                                noAddressLayout.setVisibility(View.GONE);
                                 shipAddressAdapter= new ShipAddressAdapter(ManageAddressActivity.this, addressList,"", new ShipAddressAdapter.ButtonClick() {
                                    @Override
                                    public void clicked(View view, int pos) {
                                        if(view.getId()==R.id.remove_btn){
                                            long add_id = addressList.get(pos).getAddressID();
                                            deleteAddressResponse(add_id);
                                            shipAddressAdapter.removeAt(pos);
                                        }
                                        else if(view.getId()==R.id.edit_btn){
                                            Intent intent = new Intent(ManageAddressActivity.this, EditAddressActivity.class);
                                            intent.putParcelableArrayListExtra("address_info", (ArrayList<? extends Parcelable>) addressList);
                                            intent.putExtra("position",pos);
                                            intent.putExtra("type","edit");
                                            startActivity(intent);
                                        }

                                    }
                                });
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ManageAddressActivity.this,LinearLayoutManager.VERTICAL,false);
                                recyclerView.setLayoutManager(linearLayoutManager);
                                recyclerView.setAdapter(shipAddressAdapter);

                            }else {
                                visibleAddLayout.setVisibility(View.GONE);
                                noAddressLayout.setVisibility(View.VISIBLE);
                            }
                        }
                        else {
                            visibleAddLayout.setVisibility(View.GONE);
                            noAddressLayout.setVisibility(View.VISIBLE);
                        }
                    }


                    }


                }


            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(ManageAddressActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void deleteAddressResponse(long add_id){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("addressId",add_id);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.deleteUserAddressResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD=Constants.ShowProgress(ManageAddressActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    getPersonalInfoResponse();
                    Toast.makeText(ManageAddressActivity.this, response.body().getStatusMsg(), Toast.LENGTH_LONG).show();

                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(ManageAddressActivity.this, response.body().getStatusMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(ManageAddressActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

}
