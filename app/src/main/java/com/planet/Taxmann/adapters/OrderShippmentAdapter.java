package com.planet.Taxmann.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.planet.Taxmann.R;
import com.planet.Taxmann.activity.TrackingOrderActivity;
import com.planet.Taxmann.model.Item;
import com.planet.Taxmann.model.Shippment;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderShippmentAdapter extends RecyclerView.Adapter<OrderShippmentAdapter.ViewHolder> {
    List<Shippment> shippmentList;
    List<Item> itemList;
    Context context;

    public OrderShippmentAdapter(List<Shippment> shippmentList,List<Item> itemList, Context context) {
        this.shippmentList = shippmentList;
        this.itemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OrderShippmentAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.order_shippment_layout,null));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(shippmentList!=null && shippmentList.size()>0){
            OrderItemAdapter orderItemAdapter = new OrderItemAdapter(shippmentList.get(position).getItems(),context);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
            holder.recyclerView.setLayoutManager(linearLayoutManager);
            holder.recyclerView.setAdapter(orderItemAdapter);
        }
        holder.shippTxt.setText("Shipment "+String.valueOf(position+1));

        holder.trackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TrackingOrderActivity.class);
                intent.putExtra("curiur_no",shippmentList.get(position).getCuriurNo());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return shippmentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.recycler_view_items)
        RecyclerView recyclerView;
        @BindView(R.id.shippment_txt)
        TextView shippTxt;
        @BindView(R.id.track)
        TextView trackBtn;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
