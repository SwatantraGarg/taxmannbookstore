package com.planet.Taxmann.model;

import com.google.gson.annotations.SerializedName;

public class PostEmailMobile {
    @SerializedName("mobile")
    private String mobile;

    @SerializedName("countrycode")
    private String countrycode;

    @SerializedName("email")
    private String email;

    @SerializedName("type")
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }



    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

}
