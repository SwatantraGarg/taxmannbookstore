package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RatingData {
    @SerializedName("productsRatingDetailsData")
    @Expose
    private ProductsRatingDetailsData productsRatingDetailsData;
    @SerializedName("reviewList")
    @Expose
    private List<ReviewList> reviewList = null;

    public ProductsRatingDetailsData getProductsRatingDetailsData() {
        return productsRatingDetailsData;
    }

    public void setProductsRatingDetailsData(ProductsRatingDetailsData productsRatingDetailsData) {
        this.productsRatingDetailsData = productsRatingDetailsData;
    }

    public List<ReviewList> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<ReviewList> reviewList) {
        this.reviewList = reviewList;
    }
}
