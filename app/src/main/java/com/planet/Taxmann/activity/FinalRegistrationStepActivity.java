package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.text.format.Formatter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.CheckboxAdapter;
import com.planet.Taxmann.adapters.IntrestCheckBoxAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.AoiList;
import com.planet.Taxmann.model.Data;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.LoginResponse;
import com.planet.Taxmann.model.LoginUserData;
import com.planet.Taxmann.model.PostLoginEmailUser;
import com.planet.Taxmann.model.PostLoginSocialUser;
import com.planet.Taxmann.model.PostStateProfessionInterst;
import com.planet.Taxmann.model.ProfessionList;
import com.planet.Taxmann.model.RegistrationRequest;
import com.planet.Taxmann.model.StateProfessionInterestResponse;
import com.planet.Taxmann.model.StatesList;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FinalRegistrationStepActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.other_text_edit)
    EditText otherTextEdit;
    @BindView(R.id.checkbox_other)
    CheckBox checkBox;
    @BindView(R.id.checkbox_select_all)
    CheckBox checkBoxSelect;
    @BindView(R.id.profession_list)
    RecyclerView professionList;
    @BindView(R.id.area_of_interest_list)
    RecyclerView area_of_intrestList;
    @BindView(R.id.next)
    TextView nextBtn;
    CheckboxAdapter checkboxAdapter;
    IntrestCheckBoxAdapter intrestCheckBoxAdapter;
    GeneralResponse generalResponse;
    LoginResponse loginResponse;

    StateProfessionInterestResponse stateProfessionInterestResponse;
    List<StatesList> statesList= new ArrayList<>();
    List<ProfessionList> professionsList= new ArrayList<>();
    List<AoiList> areaOfInterstList= new ArrayList<>();
    public static boolean IS_SELECT=false;
    UserData userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_registration_step);
        ButterKnife.bind(this);

        userData= DroidPrefs.get(FinalRegistrationStepActivity.this,"user_data",UserData.class);
        nextBtn.setOnClickListener(this);


        getResponse();
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
            otherTextEdit.setVisibility(View.VISIBLE);
        }
        else {
            otherTextEdit.setVisibility(View.GONE);
        }

    }
});

        checkBoxSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    intrestCheckBoxAdapter = new IntrestCheckBoxAdapter(areaOfInterstList,FinalRegistrationStepActivity.this,true);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    area_of_intrestList.setLayoutManager(layoutManager);
                    area_of_intrestList.setItemAnimator(new DefaultItemAnimator());
                    area_of_intrestList.setAdapter(intrestCheckBoxAdapter);
                    intrestCheckBoxAdapter.notifyDataSetChanged();
                }
                else {
                    intrestCheckBoxAdapter = new IntrestCheckBoxAdapter(areaOfInterstList,FinalRegistrationStepActivity.this,false);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    area_of_intrestList.setLayoutManager(layoutManager);
                    area_of_intrestList.setItemAnimator(new DefaultItemAnimator());
                    area_of_intrestList.setAdapter(intrestCheckBoxAdapter);
                    intrestCheckBoxAdapter.notifyDataSetChanged();
                }
            }
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Spinner Drop down elements
    }

   public void getResponse(){
       final  RetrofitEasyApi apiService = ApiClient.getClient().create(RetrofitEasyApi.class);
       Call<StateProfessionInterestResponse> call=apiService.getStateProfessionInterestResponse();
       call.enqueue(new Callback<StateProfessionInterestResponse>() {
           @Override
           public void onResponse(Call<StateProfessionInterestResponse> call, Response<StateProfessionInterestResponse> response) {
               int statusCode = response.code();
               stateProfessionInterestResponse=response.body();
               statesList = stateProfessionInterestResponse.getData().getStatesList();
               professionsList = stateProfessionInterestResponse.getData().getProfessionList();
               areaOfInterstList = stateProfessionInterestResponse.getData().getAoiList();

               if(statesList!=null && statesList.size()>0){
                   List<String> state= new ArrayList<>();
                   for (int i = 0; i <statesList.size() ; i++) {
                     String states= statesList.get(i).getName();
                     state.add(states);

                   }

                   // Creating adapter for spinner
                   ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(FinalRegistrationStepActivity.this, android.R.layout.simple_spinner_item, state);

                   // Drop down layout style - list view with radio button
                   dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                   // attaching data adapter to spinner
                   spinner.setAdapter(dataAdapter);

                   //profession list
                   checkboxAdapter = new CheckboxAdapter(professionsList,FinalRegistrationStepActivity.this);
                   RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                   professionList.setLayoutManager(mLayoutManager);
                   professionList.setItemAnimator(new DefaultItemAnimator());
                   professionList.setAdapter(checkboxAdapter);

                   //Area of Interest list

                   intrestCheckBoxAdapter = new IntrestCheckBoxAdapter(areaOfInterstList,FinalRegistrationStepActivity.this,false);
                   RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                   area_of_intrestList.setLayoutManager(layoutManager);
                   area_of_intrestList.setItemAnimator(new DefaultItemAnimator());
                   area_of_intrestList.setAdapter(intrestCheckBoxAdapter);
                   intrestCheckBoxAdapter.notifyDataSetChanged();

               }
           }
           @Override
           public void onFailure(Call<StateProfessionInterestResponse> call, Throwable t) {
               Toast.makeText(FinalRegistrationStepActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
           }
       });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next:
                if (CheckboxAdapter.professionCount == null || CheckboxAdapter.professionCount.size() == 0 && otherTextEdit.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please select atleast one profession", Toast.LENGTH_SHORT).show();

                } else if (IntrestCheckBoxAdapter.intrestCount == null || IntrestCheckBoxAdapter.intrestCount.size() == 0) {
                    Toast.makeText(this, "Please select atleast one of your area of interest", Toast.LENGTH_SHORT).show();

                } else {
                    PostStateProfessionInterst postStateProfessionInterst = new PostStateProfessionInterst();
                    postStateProfessionInterst.setProfession(CheckboxAdapter.professionCount);
                    postStateProfessionInterst.setInterest(IntrestCheckBoxAdapter.intrestCount);
                    int pos = spinner.getSelectedItemPosition();
                    String state_id = statesList.get(pos).getId();
                    postStateProfessionInterst.setState(state_id);
                    postStateProfessionInterst.setEmail(userData.getEmail());

                    if (otherTextEdit.getText().toString().trim().isEmpty()) {
                        postStateProfessionInterst.setProfessionOther("");
                    } else {
                        postStateProfessionInterst.setProfessionOther(otherTextEdit.getText().toString().trim());
                    }
                    postStateProfessionInterest(postStateProfessionInterst);
                }

        }

    }
    public void postStateProfessionInterest(PostStateProfessionInterst postStateProfessionInterst){
        final  RetrofitEasyApi apiService = ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call=apiService.postStateProfessionInterest(postStateProfessionInterst);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(FinalRegistrationStepActivity.this,"Please wait...");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                int statusCode = response.code();
                kProgressHUD.dismiss();

                Constants.hideSoftKeyboard(FinalRegistrationStepActivity.this);
                generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    if(userData!=null ){
                        if(userData.getProviderName()!=null && !userData.getProviderName().equalsIgnoreCase("")){
                            PostLoginSocialUser postLoginSocialUser= new PostLoginSocialUser();
                            postLoginSocialUser.setEmail(userData.getEmail());
                            postLoginSocialUser.setProvider_name(userData.getProviderName());
                            postLoginSocialUser.setProvider_userId(userData.getProviderUserId());
                            WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
                            String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                            String os1 = Build.BRAND;
                            String os2 = Build.DEVICE;
                            PackageManager manager = getApplicationContext().getPackageManager();
                            PackageInfo info = null;
                            try {
                                info = manager.getPackageInfo(
                                        getApplicationContext().getPackageName(), 0);
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                            int sdkVersion = android.os.Build.VERSION.SDK_INT;
                            String version = info.versionName;
                            postLoginSocialUser.setOs("Android");
                            postLoginSocialUser.setOsVersion(String.valueOf(sdkVersion));
                            postLoginSocialUser.setDevice(os1+" "+os2);
                            postLoginSocialUser.setDeviceType("MOBILE");
                            postLoginSocialUser.setBrowser("Taxmann BookStore");
                            postLoginSocialUser.setBrowserVersion(version);
                            postLoginSocialUser.setUserAgent(os1+os2+"/"+version+"/"+sdkVersion);
                            postLoginSocialUser.setIpAddress(ip);
                            loginUserBySocial(postLoginSocialUser);
                        }

                        else {
                            PostLoginEmailUser postLoginEmailUser= new PostLoginEmailUser();
                            postLoginEmailUser.setEmail(userData.getEmail());
                            postLoginEmailUser.setPassword(userData.getPassword());
                            WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
                            String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                            String os1 = Build.BRAND;
                            String os2 = Build.DEVICE;
                            PackageManager manager = getApplicationContext().getPackageManager();
                            PackageInfo info = null;
                            try {
                                info = manager.getPackageInfo(
                                        getApplicationContext().getPackageName(), 0);
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                            int sdkVersion = android.os.Build.VERSION.SDK_INT;
                            String version = info.versionName;
                            postLoginEmailUser.setOs("Android");
                            postLoginEmailUser.setOsVersion(String.valueOf(sdkVersion));
                            postLoginEmailUser.setDevice(os1+" "+os2);
                            postLoginEmailUser.setDeviceType("MOBILE");
                            postLoginEmailUser.setBrowser("Taxmann BookStore");
                            postLoginEmailUser.setBrowserVersion(version);
                            postLoginEmailUser.setUserAgent(os1+os2+"/"+version+"/"+sdkVersion);
                            postLoginEmailUser.setIpAddress(ip);
                            loginUserByEmail(postLoginEmailUser);
                        }
                    }
                    Toast.makeText(FinalRegistrationStepActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();



                }
                else {
                    Toast.makeText(FinalRegistrationStepActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(FinalRegistrationStepActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void loginUserBySocial(PostLoginSocialUser postLoginSocialUser){

        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<LoginResponse> call= apiService.getLoginResponseOfSocialUser(postLoginSocialUser);
        final KProgressHUD kProgressHUD=Constants.ShowProgress(FinalRegistrationStepActivity.this,"Please wait");
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                loginResponse=response.body();
                if(loginResponse.getResponseType().equalsIgnoreCase("SUCCESS")){

                    if(loginResponse.getData()!=null){
                        LoginUserData loginUserData= loginResponse.getData();
                        String user_token=loginUserData.getLoginToken();
                        String user_name=loginUserData.getUserName();
                        String user_email=loginUserData.getUserEmail();

                        UserData userData= new UserData();
                        userData.setUser_token(user_token);
                        userData.setName(user_name);
                        userData.setEmail(user_email);
                        DroidPrefs.apply(FinalRegistrationStepActivity.this,"user_data", userData);
                        startActivity(new Intent(FinalRegistrationStepActivity.this,MainActivity.class));
                        finish();
                    }
                }
                else if(loginResponse.getResponseType().equalsIgnoreCase("OTP_NOT_VERIFIED")){

                    startActivity(new Intent(FinalRegistrationStepActivity.this,MobileRegistrationActivity.class));
                }

                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(FinalRegistrationStepActivity.this, response.body().getStatusMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(FinalRegistrationStepActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void loginUserByEmail(PostLoginEmailUser postLoginEmailUser){

        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<LoginResponse> call= apiService.getLoginResponseOfEmailUser(postLoginEmailUser);
        final KProgressHUD kProgressHUD=Constants.ShowProgress(FinalRegistrationStepActivity.this,"Please wait");
        call.enqueue(new Callback<LoginResponse>() {

            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                loginResponse=response.body();
                if(loginResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    if(loginResponse.getData()!=null){
                        LoginUserData loginUserData= loginResponse.getData();
                        String user_token=loginUserData.getLoginToken();
                        String user_name=loginUserData.getUserName();
                        String user_email=loginUserData.getUserEmail();

                        UserData userData= new UserData();
                        userData.setUser_token(user_token);
                        userData.setName(user_name);
                        userData.setEmail(user_email);
                        DroidPrefs.apply(FinalRegistrationStepActivity.this,"user_data", userData);
                        startActivity(new Intent(FinalRegistrationStepActivity.this,MainActivity.class));
                        finish();
                    }
                }
                else if(loginResponse.getResponseType().equalsIgnoreCase("OTP_NOT_VERIFIED")){
                    startActivity(new Intent(FinalRegistrationStepActivity.this,MobileRegistrationActivity.class));
                }

                else {

                    kProgressHUD.dismiss();
                    Toast.makeText(FinalRegistrationStepActivity.this, response.body().getStatusMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                kProgressHUD.dismiss();

                Toast.makeText(FinalRegistrationStepActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

}
