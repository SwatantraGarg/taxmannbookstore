package com.planet.Taxmann.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GstInfo  implements Parcelable {
    @SerializedName("refId")
    @Expose
    private String refId;
    @SerializedName("LegalName")
    @Expose
    private String legalName;
    @SerializedName("GSTINofBusiness")
    @Expose
    private String gSTINofBusiness;
    @SerializedName("TypeOfEntity")
    @Expose
    private String typeOfEntity;
    @SerializedName("PANofBusiness")
    @Expose
    private String pANofBusiness;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("stateid")
    @Expose
    private String stateid;
    @SerializedName("gstinFilePath")
    @Expose
    private Object gstinFilePath;

    protected GstInfo(Parcel in) {
        refId = in.readString();
        legalName = in.readString();
        gSTINofBusiness = in.readString();
        typeOfEntity = in.readString();
        name = in.readString();
        address = in.readString();
        stateid = in.readString();
    }

    public static final Creator<GstInfo> CREATOR = new Creator<GstInfo>() {
        @Override
        public GstInfo createFromParcel(Parcel in) {
            return new GstInfo(in);
        }

        @Override
        public GstInfo[] newArray(int size) {
            return new GstInfo[size];
        }
    };

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getGSTINofBusiness() {
        return gSTINofBusiness;
    }

    public void setGSTINofBusiness(String gSTINofBusiness) {
        this.gSTINofBusiness = gSTINofBusiness;
    }

    public String getTypeOfEntity() {
        return typeOfEntity;
    }

    public void setTypeOfEntity(String typeOfEntity) {
        this.typeOfEntity = typeOfEntity;
    }

    public String getPANofBusiness() {
        return pANofBusiness;
    }

    public void setPANofBusiness(String pANofBusiness) {
        this.pANofBusiness = pANofBusiness;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStateid() {
        return stateid;
    }

    public void setStateid(String stateid) {
        this.stateid = stateid;
    }

    public Object getGstinFilePath() {
        return gstinFilePath;
    }

    public void setGstinFilePath(Object gstinFilePath) {
        this.gstinFilePath = gstinFilePath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(refId);
        dest.writeString(legalName);
        dest.writeString(gSTINofBusiness);
        dest.writeString(typeOfEntity);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(stateid);
    }
}
