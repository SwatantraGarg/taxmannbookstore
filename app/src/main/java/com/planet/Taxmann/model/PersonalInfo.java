package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PersonalInfo {
    @SerializedName("UserID")
    @Expose
    private Integer userID;
    @SerializedName("Salutation")
    @Expose
    private String salutation;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("MiddleName")
    @Expose
    private String middleName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("EmailID")
    @Expose
    private String emailID;
    @SerializedName("AlternateEmailID")
    @Expose
    private String alternateEmailID;
    @SerializedName("CompanyName")
    @Expose
    private String companyName;
    @SerializedName("Designation")
    @Expose
    private String designation;
    @SerializedName("ComapnyType")
    @Expose
    private Integer comapnyType;
    @SerializedName("UserType")
    @Expose
    private Integer userType;
    @SerializedName("DOB")
    @Expose
    private String dOB;
    @SerializedName("profileCompletePercent")
    @Expose
    private String profileCompletePercent;
    @SerializedName("profileCompletePercentmsg")
    @Expose
    private String profileCompletePercentmsg;
    @SerializedName("profileBar")
    @Expose
    private List<String> profileBar = null;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("IsSubscribe")
    @Expose
    private Boolean isSubscribe;
    @SerializedName("passwordForChangePasswordCheck")
    @Expose
    private String passwordForChangePasswordCheck;
    @SerializedName("InterestedIn")
    @Expose
    private String interestedIn;

    @SerializedName("CountryCode")
    @Expose
    private String CountryCode;

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getAlternateEmailID() {
        return alternateEmailID;
    }

    public void setAlternateEmailID(String alternateEmailID) {
        this.alternateEmailID = alternateEmailID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Integer getComapnyType() {
        return comapnyType;
    }

    public void setComapnyType(Integer comapnyType) {
        this.comapnyType = comapnyType;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getDOB() {
        return dOB;
    }

    public void setDOB(String dOB) {
        this.dOB = dOB;
    }

    public String getProfileCompletePercent() {
        return profileCompletePercent;
    }

    public void setProfileCompletePercent(String profileCompletePercent) {
        this.profileCompletePercent = profileCompletePercent;
    }

    public String getProfileCompletePercentmsg() {
        return profileCompletePercentmsg;
    }

    public void setProfileCompletePercentmsg(String profileCompletePercentmsg) {
        this.profileCompletePercentmsg = profileCompletePercentmsg;
    }

    public List<String> getProfileBar() {
        return profileBar;
    }

    public void setProfileBar(List<String> profileBar) {
        this.profileBar = profileBar;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Boolean getIsSubscribe() {
        return isSubscribe;
    }

    public void setIsSubscribe(Boolean isSubscribe) {
        this.isSubscribe = isSubscribe;
    }

    public String getPasswordForChangePasswordCheck() {
        return passwordForChangePasswordCheck;
    }

    public void setPasswordForChangePasswordCheck(String passwordForChangePasswordCheck) {
        this.passwordForChangePasswordCheck = passwordForChangePasswordCheck;
    }

    public String getInterestedIn() {
        return interestedIn;
    }

    public void setInterestedIn(String interestedIn) {
        this.interestedIn = interestedIn;
    }

}
