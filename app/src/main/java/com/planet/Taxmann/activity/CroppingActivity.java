package com.planet.Taxmann.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.planet.Taxmann.R;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yugasalabs-16 on 28/5/18.
 */

public class CroppingActivity extends AppCompatActivity {
    //     -----Creating all views used in class -----------
    @BindView(R.id.cropImageView)
    CropImageView cropImageView;
    Bitmap bitmap;
    @BindView(R.id.crop)
    ImageView crop;
    Intent intent;

    Uri uri;

    int rotation;

    //       -----Inflating layout used in class -----------
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_cropping);
        ButterKnife.bind(this);
        intent=getIntent();
        uri= Uri.parse(intent.getStringExtra("uri"));
        try {
            if(  uri!=null   ){
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver() , uri);
                try {
                    cropImageView.setImageBitmap(rotateImageIfRequired(CroppingActivity.this,bitmap,uri));
                    cropImageView.setFixedAspectRatio(true);
                    cropImageView.setAspectRatio(7,7);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            //handle exception
        }
        crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap=cropImageView.getCroppedImage();
                savebitmap(bitmap);
            }
        });


    }
    private void savebitmap(Bitmap bitmap) {
        File file = null;
        Random random=new Random();
        int rand_name=random.nextInt();
        try {
            String file_path = Environment.getExternalStorageDirectory().getAbsolutePath();
            File dir = new File(file_path);
            if (!dir.exists())
                dir.mkdirs();
            file = new File(dir, "profile_image_default.png");
            FileOutputStream fOut = new FileOutputStream(file);


            bitmap =bitmap.createScaledBitmap(bitmap,700,700,false);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            Intent intent =new Intent();
            intent.putExtra("image",file.getPath());
            setResult(Activity.RESULT_OK,intent);
            finish();


        }
        catch(Exception e){
            Log.d("checkException",""+e);
        }


    }
    private static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = context.getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            ei = new ExifInterface(selectedImage.getPath());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }
    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }
}
