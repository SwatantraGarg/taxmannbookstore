package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MySubscription {

    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("subscriberCode")
    @Expose
    private String subscriberCode;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("activationDate")
    @Expose
    private String activationDate;
    @SerializedName("expirationDate")
    @Expose
    private String expirationDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("productNo")
    @Expose
    private String productNo;
    @SerializedName("renewShow")
    @Expose
    private Integer renewShow;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSubscriberCode() {
        return subscriberCode;
    }

    public void setSubscriberCode(String subscriberCode) {
        this.subscriberCode = subscriberCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductNo() {
        return productNo;
    }

    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }

    public Integer getRenewShow() {
        return renewShow;
    }

    public void setRenewShow(Integer renewShow) {
        this.renewShow = renewShow;
    }

}
