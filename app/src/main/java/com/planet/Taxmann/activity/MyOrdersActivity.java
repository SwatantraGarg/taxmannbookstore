package com.planet.Taxmann.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.badge.BadgeDrawable;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.CartAdapter;
import com.planet.Taxmann.adapters.MyOrdersAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.CartData;
import com.planet.Taxmann.model.CartList;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.GetCartResponse;
import com.planet.Taxmann.model.MyOrder;
import com.planet.Taxmann.model.MyOrderResponse;
import com.planet.Taxmann.model.OrderData;
import com.planet.Taxmann.model.OrderSourceResponse;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.model.Years;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MyOrdersActivity extends AppCompatActivity {
    @BindView(R.id.layout_empty)
    RelativeLayout layoutEmpty;
    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.fill_layout)
    LinearLayout layoutFill;
    @BindView(R.id.order_recycler_view)
    RecyclerView orderRecyclerView;

    @BindView(R.id.total_order)
    TextView totalOrder;

    @BindView(R.id.year_spin)
    AppCompatSpinner spinner;

    @BindView(R.id.add_btn)
    TextView addBtn;

    MyOrdersAdapter ordersAdapter;
    UserData userData;
    List<MyOrder> myOrderList=new ArrayList<>();
    List<MyOrder> myOrderFilterList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(MyOrdersActivity.this,"user_data", UserData.class);

        getOrderSourceResponse();
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyOrdersActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getOrderList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyOrdersActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void getOrderSourceResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<MyOrderResponse> call= apiService.getMyOrderResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(MyOrdersActivity.this,"Please wait");
        call.enqueue(new Callback<MyOrderResponse>() {
            @Override
            public void onResponse(Call<MyOrderResponse> call, Response<MyOrderResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(MyOrdersActivity.this);
                int statusCode = response.code();
                MyOrderResponse myOrderResponse = response.body();
                if(myOrderResponse!=null) {
                    OrderData orderData = myOrderResponse.getData();
                    if(orderData!=null){
                        myOrderList = orderData.getMyOrder();
                        if(myOrderList!=null && myOrderList.size()>0){
                            for (int i = 0; i <myOrderList.size() ; i++) {
                                String date = myOrderList.get(i).getOrderDate();
                                String dateArr[] = date.split(", ");
                                String year = dateArr[1];
                                myOrderList.get(i).setYear(Integer.parseInt(year));
                            }
                        }
                        else {
                            layoutEmpty.setVisibility(View.VISIBLE);
                            layoutFill.setVisibility(View.GONE);
                        }

                        List<Years> yearlist = orderData.getYears();
                        if(yearlist!=null){
                            List<String> yearTxtlist = new ArrayList<>();
                            yearTxtlist.add("Past 6 months");
                            for (int i = 0; i <yearlist.size() ; i++) {
                                yearTxtlist.add(yearlist.get(i).getName());
                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(MyOrdersActivity.this, android.R.layout.simple_list_item_1,yearTxtlist);
                            spinner.setAdapter(adapter);
                        }
                    }
                    else {
                        Toast.makeText(MyOrdersActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                        layoutEmpty.setVisibility(View.VISIBLE);
                        layoutFill.setVisibility(View.GONE);
                    }
                }
                else {
                    Toast.makeText(MyOrdersActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }
            
            @Override
            public void onFailure(Call<MyOrderResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(MyOrdersActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    public void getCancelOrderResponse(View view, Dialog dialog, String order_no,String remark){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("orderNo",order_no);
            jsonObject.put("remarks",remark);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.getCancelOrderResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(MyOrdersActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(MyOrdersActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse!=null){
                    if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                        Toast.makeText(MyOrdersActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        view.setVisibility(View.GONE);

                    }
                    else {
                        Toast.makeText(MyOrdersActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                 Toast.makeText(MyOrdersActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void showDialogforAddReview(String order_id, View view){
        Dialog dialog= new Dialog(MyOrdersActivity.this);
        dialog.setContentView(R.layout.add_remark_layout);
        Window window= dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        EditText headEdit = (EditText)dialog.findViewById(R.id.remark_edit);
        TextView submitBtn = (TextView) dialog.findViewById(R.id.cancel_order_btn);
        TextView orderTxt = (TextView) dialog.findViewById(R.id.order_no);
        TextView cancelBtn = (TextView)dialog.findViewById(R.id.cancel);
        ImageView CrossBtn = (ImageView) dialog.findViewById(R.id.cross_btn);

        orderTxt.setText(Html.fromHtml("Order No : "+"<b>"+order_id+"</b>"));

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        CrossBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(headEdit==null || headEdit.getText().toString().isEmpty() ){
                    Toast.makeText(MyOrdersActivity.this, "Please fill the remarks", Toast.LENGTH_SHORT).show();
                }
                else {
                    String head= headEdit.getText().toString();
                    getCancelOrderResponse(view,dialog,order_id, head);
                }
            }
        });
    }
    private Date getPreviousSixMonthDate(){
        Date dat=null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, -6);
            String d = format.format(cal.getTime());
            dat = new SimpleDateFormat("dd/MM/yyyy").parse(d);
        }catch(Exception e){

        }
        return dat;
    }

    public  Date conversionStingToDate(String date1){
        String[] arr = date1.split(" ");
        String mm = arr[0];
        String dd = arr[1].substring(0,2);
        String yyyy = arr[2];
          date1=dd+"-"+mm+"-"+yyyy;
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

        Date date =null;
        try {
            date = formatter.parse(date1);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;

    }
    public List<MyOrder> getfilterData(List<MyOrder> myOrderList){
        myOrderFilterList.clear();
        if(spinner.getSelectedItem().toString().equalsIgnoreCase("Past 6 months")){
            for (int i = 0; i <myOrderList.size() ; i++) {
                if(conversionStingToDate(myOrderList.get(i).getOrderDate()).after(getPreviousSixMonthDate())) {
                    myOrderFilterList.add(myOrderList.get(i));
                }
            }
        }
        else{
            for (int i = 0; i <myOrderList.size() ; i++) {
                if(spinner.getSelectedItem().toString().equalsIgnoreCase(String.valueOf(myOrderList.get(i).getYear()))) {
                    myOrderFilterList.add(myOrderList.get(i));
                }
            }
        }
        return myOrderFilterList;
    }
    public  void getOrderList(){
        if(myOrderList!=null && myOrderList.size()>0){
            myOrderFilterList = getfilterData(myOrderList);
            String total = String.valueOf(myOrderFilterList.size());
            totalOrder.setText(total+" orders placed in");
            ordersAdapter = new MyOrdersAdapter(MyOrdersActivity.this, myOrderFilterList, new MyOrdersAdapter.ButtonClick() {
                @Override
                public void clicked(View view, int pos) {
                    if(view.getId()==R.id.cancel){
                        String order_id = myOrderList.get(pos).getOrderNo();
                        showDialogforAddReview(order_id,view);
                        //getCancelOrderResponse();
                    }
                }
            });
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MyOrdersActivity.this,LinearLayoutManager.VERTICAL,false);
            orderRecyclerView.setLayoutManager(linearLayoutManager);
            orderRecyclerView.setAdapter(ordersAdapter);
            ordersAdapter.notifyDataSetChanged();
        }
    }
}
