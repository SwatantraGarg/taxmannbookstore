package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductsRatingDetailsData {
    @SerializedName("bookId")
    @Expose
    private Integer bookId;
    @SerializedName("maxRating")
    @Expose
    private String maxRating;
    @SerializedName("foundRating")
    @Expose
    private String foundRating;
    @SerializedName("starRating")
    @Expose
    private List<String> starRating = null;
    @SerializedName("ratingCount")
    @Expose
    private String ratingCount;
    @SerializedName("reviewCount")
    @Expose
    private String reviewCount;
    @SerializedName("pointWiseStar")
    @Expose
    private PointWiseStar pointWiseStar;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getMaxRating() {
        return maxRating;
    }

    public void setMaxRating(String maxRating) {
        this.maxRating = maxRating;
    }

    public String getFoundRating() {
        return foundRating;
    }

    public void setFoundRating(String foundRating) {
        this.foundRating = foundRating;
    }

    public List<String> getStarRating() {
        return starRating;
    }

    public void setStarRating(List<String> starRating) {
        this.starRating = starRating;
    }

    public String getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(String ratingCount) {
        this.ratingCount = ratingCount;
    }

    public String getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(String reviewCount) {
        this.reviewCount = reviewCount;
    }

    public PointWiseStar getPointWiseStar() {
        return pointWiseStar;
    }

    public void setPointWiseStar(PointWiseStar pointWiseStar) {
        this.pointWiseStar = pointWiseStar;
    }
}
