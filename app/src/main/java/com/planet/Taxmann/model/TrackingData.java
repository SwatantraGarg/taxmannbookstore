package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TrackingData {
    @SerializedName("track_status")
    @Expose
    private Integer trackStatus;
    @SerializedName("shipment_status")
    @Expose
    private Integer shipmentStatus;
    @SerializedName("shipment_status_text")
    @Expose
    private String shipmentStatusText;
    @SerializedName("shipment_track")
    @Expose
    private List<ShipmentTrack> shipmentTrack = null;
    @SerializedName("shipment_track_activities")
    @Expose
    private List<ShipmentTrackActivity> shipmentTrackActivities = null;
    @SerializedName("track_url")
    @Expose
    private String trackUrl;

    public Integer getTrackStatus() {
        return trackStatus;
    }

    public void setTrackStatus(Integer trackStatus) {
        this.trackStatus = trackStatus;
    }

    public Integer getShipmentStatus() {
        return shipmentStatus;
    }

    public void setShipmentStatus(Integer shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    public String getShipmentStatusText() {
        return shipmentStatusText;
    }

    public void setShipmentStatusText(String shipmentStatusText) {
        this.shipmentStatusText = shipmentStatusText;
    }

    public List<ShipmentTrack> getShipmentTrack() {
        return shipmentTrack;
    }

    public void setShipmentTrack(List<ShipmentTrack> shipmentTrack) {
        this.shipmentTrack = shipmentTrack;
    }

    public List<ShipmentTrackActivity> getShipmentTrackActivities() {
        return shipmentTrackActivities;
    }

    public void setShipmentTrackActivities(List<ShipmentTrackActivity> shipmentTrackActivities) {
        this.shipmentTrackActivities = shipmentTrackActivities;
    }

    public String getTrackUrl() {
        return trackUrl;
    }

    public void setTrackUrl(String trackUrl) {
        this.trackUrl = trackUrl;
    }
}
