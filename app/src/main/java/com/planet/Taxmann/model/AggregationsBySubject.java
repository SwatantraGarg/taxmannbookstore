package com.planet.Taxmann.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class AggregationsBySubject implements Parcelable {
    @SerializedName("DocName")
    private String docName;

    @SerializedName("DocCount")
    private Integer docCount;

    public boolean isCHECKED() {
        return CHECKED;
    }

    public void setCHECKED(boolean CHECKED) {
        this.CHECKED = CHECKED;
    }

    private  boolean CHECKED = false;


    protected AggregationsBySubject(Parcel in) {
        docName = in.readString();
        if (in.readByte() == 0) {
            docCount = null;
        } else {
            docCount = in.readInt();
        }
    }

    public static final Creator<AggregationsBySubject> CREATOR = new Creator<AggregationsBySubject>() {
        @Override
        public AggregationsBySubject createFromParcel(Parcel in) {
            return new AggregationsBySubject(in);
        }

        @Override
        public AggregationsBySubject[] newArray(int size) {
            return new AggregationsBySubject[size];
        }
    };

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public Integer getDocCount() {
        return docCount;
    }

    public void setDocCount(Integer docCount) {
        this.docCount = docCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(docName);
        if (docCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(docCount);
        }
    }
}
