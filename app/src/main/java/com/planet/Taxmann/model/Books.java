package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Books {

    @SerializedName("bookId")
    @Expose
    private Integer bookId;
    @SerializedName("bookTitle")
    @Expose
    private String bookTitle;
    @SerializedName("productType")
    @Expose
    private Integer productType;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("bookImagesurl")
    @Expose
    private String bookImagesurl;
    @SerializedName("bookISBN")
    @Expose
    private String bookISBN;
    @SerializedName("frequency")
    @Expose
    private String frequency;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("volumns")
    @Expose
    private String volumns;
    @SerializedName("bookPriceINR")
    @Expose
    private Double bookPriceINR;
    @SerializedName("bookPriceUSD")
    @Expose
    private Integer bookPriceUSD;
    @SerializedName("bookRating")
    @Expose
    private List<String> bookRating = null;
    @SerializedName("bookRatingCount")
    @Expose
    private Integer bookRatingCount;
    @SerializedName("bookReviewCount")
    @Expose
    private Integer bookReviewCount;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("edition")
    @Expose
    private String edition;
    @SerializedName("noOfPages")
    @Expose
    private Integer noOfPages;
    @SerializedName("stock")
    @Expose
    private Boolean stock;
    @SerializedName("dateOfPublication")
    @Expose
    private String dateOfPublication;
    @SerializedName("binding")
    @Expose
    private String binding;
    @SerializedName("bookAuthor")
    @Expose
    private String bookAuthor;
    @SerializedName("aboutAuthor")
    @Expose
    private List<AboutAuthor> aboutAuthor = null;
    @SerializedName("aboutBook")
    @Expose
    private String aboutBook;
    @SerializedName("preOrder")
    @Expose
    private Boolean preOrder;

    public String getPreOrderReleaseDate() {
        return PreOrderReleaseDate;
    }

    public void setPreOrderReleaseDate(String preOrderReleaseDate) {
        PreOrderReleaseDate = preOrderReleaseDate;
    }

    @SerializedName("PreOrderReleaseDate")
    private String PreOrderReleaseDate;

    @SerializedName("bestSeller")
    @Expose
    private String bestSeller;
    @SerializedName("featuredProduct")
    @Expose
    private Boolean featuredProduct;
    @SerializedName("shortDesciption")
    @Expose
    private String shortDesciption;
    @SerializedName("eBookUrl")
    @Expose
    private String eBookUrl;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("BookOverview")
    @Expose
    private String bookOverview;
    @SerializedName("Preface")
    @Expose
    private String preface;
    @SerializedName("demosetupurl")
    @Expose
    private String demosetupurl;
    @SerializedName("SampleIssue")
    @Expose
    private String sampleIssue;
    @SerializedName("IsSample")
    @Expose
    private Boolean isSample;
    @SerializedName("catId")
    @Expose
    private String catId;
    @SerializedName("catName")
    @Expose
    private String catName;
    @SerializedName("catURL")
    @Expose
    private String catURL;
    @SerializedName("subId")
    @Expose
    private Object subId;
    @SerializedName("subName")
    @Expose
    private Object subName;
    @SerializedName("subURL")
    @Expose
    private Object subURL;
    @SerializedName("wishLishCount")
    @Expose
    private Integer wishLishCount;

    public Boolean isReaderavailable() {
        return readeravailable;
    }

    public void setReaderavailable(boolean readeravailable) {
        this.readeravailable = readeravailable;
    }

    @SerializedName("readeravailable")
    @Expose
    private boolean readeravailable;
    @SerializedName("productsBindingTypesDetails")
    @Expose
    private List<ProductsBindingTypesDetails> productsBindingTypesDetails = null;

    public List<VirtualBookDetail> getVirtualBookDetails() {
        return virtualBookDetails;
    }

    public void setVirtualBookDetails(List<VirtualBookDetail> virtualBookDetails) {
        this.virtualBookDetails = virtualBookDetails;
    }

    @SerializedName("virtualBookDetails")
    @Expose
    private List<VirtualBookDetail> virtualBookDetails = null;
    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBookImagesurl() {
        return bookImagesurl;
    }

    public void setBookImagesurl(String bookImagesurl) {
        this.bookImagesurl = bookImagesurl;
    }

    public String getBookISBN() {
        return bookISBN;
    }

    public void setBookISBN(String bookISBN) {
        this.bookISBN = bookISBN;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getVolumns() {
        return volumns;
    }

    public void setVolumns(String volumns) {
        this.volumns = volumns;
    }

    public Double getBookPriceINR() {
        return bookPriceINR;
    }

    public void setBookPriceINR(Double bookPriceINR) {
        this.bookPriceINR = bookPriceINR;
    }

    public Integer getBookPriceUSD() {
        return bookPriceUSD;
    }

    public void setBookPriceUSD(Integer bookPriceUSD) {
        this.bookPriceUSD = bookPriceUSD;
    }

    public List<String> getBookRating() {
        return bookRating;
    }

    public void setBookRating(List<String> bookRating) {
        this.bookRating = bookRating;
    }

    public Integer getBookRatingCount() {
        return bookRatingCount;
    }

    public void setBookRatingCount(Integer bookRatingCount) {
        this.bookRatingCount = bookRatingCount;
    }

    public Integer getBookReviewCount() {
        return bookReviewCount;
    }

    public void setBookReviewCount(Integer bookReviewCount) {
        this.bookReviewCount = bookReviewCount;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public Integer getNoOfPages() {
        return noOfPages;
    }

    public void setNoOfPages(Integer noOfPages) {
        this.noOfPages = noOfPages;
    }

    public Boolean getStock() {
        return stock;
    }

    public void setStock(Boolean stock) {
        this.stock = stock;
    }

    public String getDateOfPublication() {
        return dateOfPublication;
    }

    public void setDateOfPublication(String dateOfPublication) {
        this.dateOfPublication = dateOfPublication;
    }

    public String getBinding() {
        return binding;
    }

    public void setBinding(String binding) {
        this.binding = binding;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public List<AboutAuthor> getAboutAuthor() {
        return aboutAuthor;
    }

    public void setAboutAuthor(List<AboutAuthor> aboutAuthor) {
        this.aboutAuthor = aboutAuthor;
    }

    public String getAboutBook() {
        return aboutBook;
    }

    public void setAboutBook(String aboutBook) {
        this.aboutBook = aboutBook;
    }

    public Boolean getPreOrder() {
        return preOrder;
    }

    public void setPreOrder(Boolean preOrder) {
        this.preOrder = preOrder;
    }

    public String getBestSeller() {
        return bestSeller;
    }

    public void setBestSeller(String bestSeller) {
        this.bestSeller = bestSeller;
    }

    public Boolean getFeaturedProduct() {
        return featuredProduct;
    }

    public void setFeaturedProduct(Boolean featuredProduct) {
        this.featuredProduct = featuredProduct;
    }

    public String getShortDesciption() {
        return shortDesciption;
    }

    public void setShortDesciption(String shortDesciption) {
        this.shortDesciption = shortDesciption;
    }

    public String getEBookUrl() {
        return eBookUrl;
    }

    public void setEBookUrl(String eBookUrl) {
        this.eBookUrl = eBookUrl;
    }

    public String getBookOverview() {
        return bookOverview;
    }

    public void setBookOverview(String bookOverview) {
        this.bookOverview = bookOverview;
    }

    public String getPreface() {
        return preface;
    }

    public void setPreface(String preface) {
        this.preface = preface;
    }

    public String getDemosetupurl() {
        return demosetupurl;
    }

    public void setDemosetupurl(String demosetupurl) {
        this.demosetupurl = demosetupurl;
    }

    public String getSampleIssue() {
        return sampleIssue;
    }

    public void setSampleIssue(String sampleIssue) {
        this.sampleIssue = sampleIssue;
    }

    public Boolean getIsSample() {
        return isSample;
    }

    public void setIsSample(Boolean isSample) {
        this.isSample = isSample;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatURL() {
        return catURL;
    }

    public void setCatURL(String catURL) {
        this.catURL = catURL;
    }

    public Object getSubId() {
        return subId;
    }

    public void setSubId(Object subId) {
        this.subId = subId;
    }

    public Object getSubName() {
        return subName;
    }

    public void setSubName(Object subName) {
        this.subName = subName;
    }

    public Object getSubURL() {
        return subURL;
    }

    public void setSubURL(Object subURL) {
        this.subURL = subURL;
    }

    public Integer getWishLishCount() {
        return wishLishCount;
    }

    public void setWishLishCount(Integer wishLishCount) {
        this.wishLishCount = wishLishCount;
    }

    public List<ProductsBindingTypesDetails> getProductsBindingTypesDetails() {
        return productsBindingTypesDetails;
    }

    public void setProductsBindingTypesDetails(List<ProductsBindingTypesDetails> productsBindingTypesDetails) {
        this.productsBindingTypesDetails = productsBindingTypesDetails;
    }

}
