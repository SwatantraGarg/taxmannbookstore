package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.SubscriptionAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.BookDataResponse;
import com.planet.Taxmann.model.BookType;
import com.planet.Taxmann.model.MySubscription;
import com.planet.Taxmann.model.SubscriptionData;
import com.planet.Taxmann.model.SubscriptionResponse;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.model.Years;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import java.util.ArrayList;
import java.util.List;

public class MySubscriptionActivity extends AppCompatActivity {
    @BindView(R.id.my_subscription_recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.spin_layout)
    LinearLayout spinLayout;

    @BindView(R.id.no_data_txt)
    TextView noDataTxt;

    @BindView(R.id.view)
    View view;

    @BindView(R.id.sub_spin)
    AppCompatSpinner spinner;

    @BindView(R.id.year_txt)
    TextView yearTxt;

    UserData userData;
    List<MySubscription> mySubscriptionList = new ArrayList<>();
    List<Years> yearsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_subscription);
        ButterKnife.bind(this);
        userData = DroidPrefs.get(MySubscriptionActivity.this,"user_data", UserData.class);
        getMySubscriptionResponse();
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MySubscriptionActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });

    }
    public void getMySubscriptionResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<SubscriptionResponse> call= apiService.getMySubscriptionResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(MySubscriptionActivity.this,"Please wait");
        call.enqueue(new Callback<SubscriptionResponse>() {
            @Override
            public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(MySubscriptionActivity.this);
                int statusCode = response.code();
                SubscriptionResponse subscriptionResponse = response.body();
                if(subscriptionResponse!=null) {
                    SubscriptionData subscriptionData = subscriptionResponse.getData();

                    if (subscriptionData != null) {
                        mySubscriptionList = subscriptionData.getMySubscriptions();
                        yearsList = subscriptionData.getYear();
                    }
                    if (yearsList != null && yearsList.size() > 0) {
                        List<String> yearTxtlist = new ArrayList<>();
                        for (int i = 0; i < yearsList.size(); i++) {
                            yearTxtlist.add(yearsList.get(i).getName());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MySubscriptionActivity.this, android.R.layout.simple_list_item_1, yearTxtlist);
                        spinner.setAdapter(adapter);
                    }
                    else {
                        spinLayout.setVisibility(View.GONE);
                        yearTxt.setVisibility(View.GONE);
                        view.setVisibility(View.GONE);
                    }
                    if(mySubscriptionList!=null && mySubscriptionList.size()>0){
                        SubscriptionAdapter subscriptionAdapter = new SubscriptionAdapter(mySubscriptionList, MySubscriptionActivity.this);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MySubscriptionActivity.this, LinearLayoutManager.VERTICAL, false);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        recyclerView.setAdapter(subscriptionAdapter);
                        subscriptionAdapter.notifyDataSetChanged();
                    }
                    else {
                            noDataTxt.setVisibility(View.VISIBLE);
                            spinLayout.setVisibility(View.GONE);
                            yearTxt.setVisibility(View.GONE);
                            view.setVisibility(View.GONE);
                        }

                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            List<MySubscription> subscriptionList = new ArrayList<>();
                            if(mySubscriptionList!=null && mySubscriptionList.size()>0){
                            for (int i = 0; i < mySubscriptionList.size(); i++) {
                                if (spinner.getSelectedItem().toString().equalsIgnoreCase(mySubscriptionList.get(i).getYear())) {
                                    subscriptionList.add(mySubscriptionList.get(i));
                                }
                            }
                                if (subscriptionList != null && subscriptionList.size() > 0) {
                                    noDataTxt.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                    SubscriptionAdapter subscriptionAdapter = new SubscriptionAdapter(subscriptionList, MySubscriptionActivity.this);
                                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MySubscriptionActivity.this, LinearLayoutManager.VERTICAL, false);
                                    recyclerView.setLayoutManager(linearLayoutManager);
                                    recyclerView.setAdapter(subscriptionAdapter);
                                    subscriptionAdapter.notifyDataSetChanged();
                                } else {
                                    noDataTxt.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.GONE);
                                }
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                }
                }

            @Override
            public void onFailure(Call<SubscriptionResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(MySubscriptionActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

}
