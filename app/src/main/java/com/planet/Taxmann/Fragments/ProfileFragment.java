package com.planet.Taxmann.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.android.flexbox.FlexboxLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.UserInfo;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.activity.BookDetailsActivity;
import com.planet.Taxmann.activity.ChangePasswordActivity;
import com.planet.Taxmann.activity.CroppingActivity;
import com.planet.Taxmann.activity.EditAddressActivity;
import com.planet.Taxmann.activity.GstINActivity;
import com.planet.Taxmann.activity.LoginActivity;
import com.planet.Taxmann.activity.MainActivity;
import com.planet.Taxmann.activity.ManageAddressActivity;
import com.planet.Taxmann.activity.ManageInterestActivity;
import com.planet.Taxmann.activity.MyOrdersActivity;
import com.planet.Taxmann.activity.MySubscriptionActivity;
import com.planet.Taxmann.activity.MyWalletActivity;
import com.planet.Taxmann.activity.MyWishListActivity;
import com.planet.Taxmann.activity.PersonalInformationActivity;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.GstInfo;
import com.planet.Taxmann.model.PersonalInfo;
import com.planet.Taxmann.model.USerInfo;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.model.UserInfoResponse;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {
    @BindView(R.id.logout_btn)
    TextView logoutBtn;

    @BindView(R.id.user_name)
    EditText userName;

    @BindView(R.id.email_id)
    TextView emailId;

    @BindView(R.id.progress_layout)
    FlexboxLayout progressLayout;

    @BindView(R.id.gst_layout)
    RelativeLayout gstLayout;

    @BindView(R.id.address_layout)
    RelativeLayout addressLayout;

    @BindView(R.id.change_password_layout)
    RelativeLayout changePasswordLayout;

    @BindView(R.id.personal_layout)
    RelativeLayout personalLayout;

    @BindView(R.id.strength_txt)
    TextView strengthTxt;

    @BindView(R.id.suggest_txt)
    TextView suggestTxt;

    @BindView(R.id.edit_info)
    ImageView editInfo;

    @BindView(R.id.suugest_btn)
    TextView suggestBtn;

    @BindView(R.id.suggest_layout)
    LinearLayout suggestLayout;

    @BindView(R.id.my_orders_layout)
    RelativeLayout myOrdersLayout;


    @BindView(R.id.layout_change_pic)
    RelativeLayout changePicLayout;

    @BindView(R.id.manage_interest_layout)
    RelativeLayout manageInterestLayout;

    @BindView(R.id.my_wishlist_layout)
    RelativeLayout myWishlistLayout;

    @BindView(R.id.my_wallet_layout)
    RelativeLayout myWalletLayout;

    @BindView(R.id.user_img)
    ImageView userImg;

    @BindView(R.id.add_photo)
    ImageView addPhoto;

    @BindView(R.id.my_subscription_layout)
    RelativeLayout mySubscriptionLayout;

    Bitmap default_bitmap;
    UserData userData;
    String image_name;
    String[] PERMISSIONS;
    public boolean IS_EDIT_ENABLE=false;
    public boolean IS_CAM_ENABLE=false;
    private Uri picUri;
   public boolean camera_denied = false;
   public boolean gallery_denied = false;
    private static final int CAMERA_REQUEST = 1988;
    private static final int PIC_CROP = 3;
    MainActivity mainActivity;


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mainActivity= (MainActivity)getActivity();
        userData = DroidPrefs.get(getContext(),"user_data", UserData.class);
        getUserInfoResponse();
    }

    @Override
    public void onResume() {
        super.onResume();
      //  getUserInfoResponse();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.profile_layout, null);
        ButterKnife.bind(this,view);
        int PERMISSION_ALL = 1;
        PERMISSIONS = new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (!hasPermissions(getContext(), PERMISSIONS)) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
        }

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLogoutDialog();
            }
        });

        addressLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ManageAddressActivity.class);
                startActivity(intent);
            }
        });

        myOrdersLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MyOrdersActivity.class);
                startActivity(intent);
            }
        });

        myWishlistLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MyWishListActivity.class);
                startActivity(intent);
            }
        });

        gstLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), GstINActivity.class);
                startActivity(intent);
            }
        });

        mySubscriptionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MySubscriptionActivity.class);
                startActivity(intent);
            }
        });

        myWalletLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MyWalletActivity.class);
                startActivity(intent);
            }
        });

        personalLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), PersonalInformationActivity.class);
                startActivity(intent);
            }
        });

        changePasswordLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChangePasswordActivity.class);
                startActivity(intent);
            }
        });

        manageInterestLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ManageInterestActivity.class);
                startActivity(intent);
            }
        });

        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!IS_CAM_ENABLE){
                    addPhoto.setImageResource(R.drawable.ic_save);
                    showpictureUploadDialog();
                    IS_CAM_ENABLE=true;
                }
                else {
                    if(default_bitmap!=null){
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                        default_bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream .toByteArray();
                        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
                        getAddPhotoRespopnse(encoded);
                    }
                    addPhoto.setImageResource(R.drawable.ic_add_a_photo);
                    IS_CAM_ENABLE=false;
                }

            }
        });

        editInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!IS_EDIT_ENABLE){
                    userName.setEnabled(true);
                    userName.requestFocus();
                    Constants.ShowKeyboard(getContext(),userName);
                    userName.setSelection(userName.getText().length());
                    userName.setHint("Enter name");
                    editInfo.setImageResource(R.drawable.ic_save);
                    IS_EDIT_ENABLE=true;
                }

                else {
                    if(userName.getText().toString().isEmpty()){
                        Snackbar.make(userName,"Please enter name",Snackbar.LENGTH_SHORT).show();
                    }
                    else {
                        String name = userName.getText().toString();
                        addUserNameResponse(name);
                    }
                }
            }
        });

        changePicLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(IS_CAM_ENABLE){
                    showpictureUploadDialog();
                }
                else {
                    Snackbar.make(editInfo,"Please tap cam button to change profile pic",Snackbar.LENGTH_SHORT).show();
                }

            }
        });

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            for (String permission : permissions) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {
                    //denied
                    Log.e("denied", permission);
                    if (!permission.equalsIgnoreCase("android.permission.CAMERA")) {
                        camera_denied = true;
                        Toast.makeText(getContext(), "Please allow the permmision to access the camera", Toast.LENGTH_SHORT).show();
                    }
                    else if (permission.equalsIgnoreCase("android.permission.READ_EXTERNAL_STORAGE") || permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")) {
                        gallery_denied = true;
                        Toast.makeText(getContext(), "Please allow the permmision to access the gallery", Toast.LENGTH_SHORT).show();
                    }

                }
                else {
                    if (ActivityCompat.checkSelfPermission(getContext(), permission) == PackageManager.PERMISSION_GRANTED) {
                        //allowed
                        Log.e("allowed", permission);
                        if (permission.equalsIgnoreCase("android.permission.CAMERA")) {
                            camera_denied = false;
                        } else if (permission.equalsIgnoreCase("android.permission.READ_EXTERNAL_STORAGE") || permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")) {
                            gallery_denied = false;
                        }

                    } else {
                        //set to never ask again
                        Log.e("set to never ask again", permission);
                        if (permission.equalsIgnoreCase("android.permission.CAMERA")) {
                            camera_denied = true;
                        } else if (permission.equalsIgnoreCase("android.permission.READ_EXTERNAL_STORAGE") || permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")) {
                            gallery_denied = true;
                        }
                        //do something here.
                    }
                }
            }
        }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Intent intent = new Intent(getContext(), CroppingActivity.class);
            intent.putExtra("uri", picUri.toString());
            startActivityForResult(intent, PIC_CROP);
        }

        else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                picUri = data.getData();
                Intent intent = new Intent(getContext(), CroppingActivity.class);
                intent.putExtra("uri", picUri.toString());
                startActivityForResult(intent, PIC_CROP);

            }

        } else if (requestCode == PIC_CROP && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String path = data.getStringExtra("image");
                default_bitmap = BitmapFactory.decodeFile(path);
//                getimage(default_bitmap);
                userImg.setImageBitmap(default_bitmap);
            }
        }
    }

    public void logoutUser(String login_token){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.logutUser(login_token);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(getContext(),"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();

                GeneralResponse generalResponse=response.body();
                if(generalResponse!=null){
                    if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                        userData.setUser_token(null);
                        DroidPrefs.apply(getContext(),"user_data",userData);
                        Toast.makeText(getContext(), response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getContext(), LoginActivity.class));
                        getActivity().finish();
                    }

                    else
                    {
                        startActivity(new Intent(getContext(),LoginActivity.class));
                        getActivity().finish();
                        userData.setUser_token(null);
                        DroidPrefs.apply(getContext(),"user_data",userData);
                        Toast.makeText(getContext(), response.body().getStatusMsg(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void getUserInfoResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<UserInfoResponse> call= apiService.getUserInfoResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(getContext(),"Please wait");
        call.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                kProgressHUD.dismiss();
               // Constants.hideSoftKeyboard(getActivity());
                int statusCode = response.code();
                UserInfoResponse userInfoResponse=response.body();
                if(userInfoResponse!=null){
                    USerInfo userInfo = userInfoResponse.getData();
                    if(userInfo!=null){
                        PersonalInfo personalInfo =userInfo.getPersonalInfo();
                        GstInfo gstInfo = userInfo.getGtnInfo();
                        if(personalInfo!=null){

                        userName.setText(personalInfo.getFirstName()+personalInfo.getMiddleName()+personalInfo.getLastName());
                        emailId.setText(personalInfo.getEmailID());
                        if(!personalInfo.getProfilePic().equalsIgnoreCase("")){
                            final String encodedString = personalInfo.getProfilePic();
                            final String pureBase64Encoded = encodedString.substring(encodedString.indexOf(",")  + 1);
                            byte[] decodedString = Base64.decode(pureBase64Encoded, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            Glide.with(getActivity())
                                    .load(decodedByte)
                                    .placeholder(R.drawable.user_demo)
                                    .error(R.drawable.user_demo)
                                    .into(userImg);
                        }
                        else {
                            userImg.setImageResource(R.drawable.user_demo);
                        }

                        strengthTxt.setText("Profile Strength: "+personalInfo.getProfileCompletePercent()+" Profile Complete");
                        String res = personalInfo.getProfileCompletePercentmsg();
                        if(res!=null && !res.equalsIgnoreCase("") && !res.equalsIgnoreCase("0") && !res.equalsIgnoreCase("100") ){
                            suggestLayout.setVisibility(View.VISIBLE);
                            String [] separated = res.split("\\|");
                            String pre_text =  separated[0];
                            String after_text = separated[1];

                            if(pre_text.equalsIgnoreCase("COMP")){
                                suggestTxt.setText(after_text);
                                suggestBtn.setText("Add");
                                suggestBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(getContext(), PersonalInformationActivity.class);
                                        startActivity(intent);
                                    }
                                });

                            }
                            else if(pre_text.equalsIgnoreCase("BILL")){
                                suggestTxt.setText(after_text);
                                suggestBtn.setText("Add");
                                suggestBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(getContext(), EditAddressActivity.class);
                                        startActivity(intent);
                                    }
                                });
                            }
                            else if(pre_text.equalsIgnoreCase("GSTN")){
                                suggestTxt.setText(after_text);
                                suggestBtn.setText("Add");
                                suggestBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(getContext(), GstINActivity.class);
                                        intent.putExtra("gst_info", (Parcelable) gstInfo);
                                        startActivity(intent);
                                    }
                                });
                            }
                            else if(pre_text.equalsIgnoreCase("PHOT")){
                                suggestTxt.setText(after_text);
                                suggestBtn.setText("Add photo");
                                suggestBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Snackbar.make(editInfo,"Please tap cam button to change profile pic",Snackbar.LENGTH_SHORT).show();
                                    }
                                });
                            }

                            else {
                                suggestTxt.setText(after_text);
                                suggestBtn.setText("Add");
                            }


                        }
                        else {
                            suggestLayout.setVisibility(View.GONE);
                        }


                        List<String> progressbarList = personalInfo.getProfileBar();
                        int height= 120;
                        int width= 25;
                        FlexboxLayout.LayoutParams params = new FlexboxLayout.LayoutParams(height,width);
                        params.setMargins(12,10,12,10);

                        for (int i = 0; i < progressbarList.size(); i++) {
                            LinearLayout ll = new LinearLayout(getActivity());
                            ll.removeAllViewsInLayout();
                            ll.setLayoutParams(params);
                            if(progressbarList.get(i).equalsIgnoreCase("1")){
                                ll.setBackground(getContext().getResources().getDrawable(R.drawable.btn_bg_orange));
                            }
                            else {
                                ll.setBackground(getContext().getResources().getDrawable(R.drawable.rec_gray_btn));
                            }

                            progressLayout.addView(ll);

                        }
                            progressbarList.clear();

                    }
                    }


                }
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void showpictureUploadDialog(){

        final BottomSheetDialog bottomSheetDialog=new BottomSheetDialog(getContext());
        View v = getLayoutInflater().inflate(R.layout.dialog_photo_popup, null);
        ImageView img_cross=v.findViewById(R.id.img_cross);
        img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                addPhoto.setImageResource(R.drawable.ic_add_a_photo);
                IS_CAM_ENABLE=false;
            }
        });

        bottomSheetDialog.setContentView(v);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) v.getParent());
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        LinearLayout layout_gallery=v.findViewById(R.id.layout_gallery);
        LinearLayout  layout_camera=v.findViewById(R.id.layout_camera);
        layout_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!camera_denied) {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//folder stuff
                    File imagesFolder = new File(Environment.getExternalStorageDirectory(), "LocatemImages");
                    imagesFolder.mkdirs();

                    File image = new File(imagesFolder, "QR_" + timeStamp + ".png");
                    if (Build.VERSION.SDK_INT >= 24) {
                        picUri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".provider", image);
                    } else {
                        picUri = Uri.fromFile(image);
                    }

                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                } else {
                    showalert_camera();
                }
                bottomSheetDialog.dismiss();
            }
        });
        layout_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!gallery_denied) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(intent, 2);
                } else {

                    showalert_camera();

                }
                bottomSheetDialog.dismiss();

            }
        });

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    // bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // bottomSheetDialog.dismiss();
                //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

            }
        });

    }

    public void showalert_camera() {
        new AlertDialog.Builder(getContext()).setMessage("You have denied permission").setPositiveButton("Go to Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                dialog.dismiss();
            }
        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create().show();
    }
    public void getAddPhotoRespopnse(String image){
            //String img ="data:image/jpeg;base64,"+image;
            JSONObject jsonObject=new JSONObject();
            try {
                jsonObject.put("ImageString",image);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
            JsonParser jsonParser = new JsonParser();
            JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
            final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
            Call<GeneralResponse> call= apiService.getAddPhotoResponse(res,userData.getUser_token());
            final KProgressHUD kProgressHUD= Constants.ShowProgress(getContext(),"Please wait");
            call.enqueue(new Callback<GeneralResponse>() {
                @Override
                public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                    kProgressHUD.dismiss();
                    Constants.hideSoftKeyboard(getActivity());
                    int statusCode = response.code();
                    GeneralResponse generalResponse=response.body();
                    if(generalResponse!=null){
                        if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                            Toast.makeText(getContext(), generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(getContext(), generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                        }

                    }
                    else {
                        Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();

                    }

                }
                @Override
                public void onFailure(Call<GeneralResponse> call, Throwable t) {
                    kProgressHUD.dismiss();
                    Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();

                }
            });

        }


    public void addUserNameResponse(String name){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("FirstName",name);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.getAddUserNameResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(getContext(),"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(getActivity());
                userName.setEnabled(false);
                editInfo.setImageResource(R.drawable.ic_edit_info);
                IS_EDIT_ENABLE=false;
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse!=null){
                    if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                        Toast.makeText(getContext(), generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(getContext(), generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();

                }

            }
            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void showLogoutDialog() {
        new AlertDialog.Builder(getContext()).setMessage("Are you sure you want to logout?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(userData.getUser_token()!=null && !userData.getUser_token().equalsIgnoreCase("")){
                    logoutUser(userData.getUser_token());
                }
                else {

                }

            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create().show();
    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    }


