package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProffession {
    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("Value")
    @Expose
    private String value;
    @SerializedName("selected")
    @Expose
    private Integer selected;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getSelected() {
        return selected;
    }

    public void setSelected(Integer selected) {
        this.selected = selected;
    }
}
