package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SampleIssue {
    @SerializedName("Prodid")
    @Expose
    private String prodid;
    @SerializedName("prod_Name")
    @Expose
    private String prodName;
    @SerializedName("ISSN")
    @Expose
    private String iSSN;
    @SerializedName("bookImg")
    @Expose
    private String bookImg;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("edition")
    @Expose
    private String edition;

    public boolean isIS_CHECKED() {
        return IS_CHECKED;
    }

    public void setIS_CHECKED(boolean IS_CHECKED) {
        this.IS_CHECKED = IS_CHECKED;
    }

    public boolean IS_CHECKED = false;

    public String getProdid() {
        return prodid;
    }

    public void setProdid(String prodid) {
        this.prodid = prodid;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getISSN() {
        return iSSN;
    }

    public void setISSN(String iSSN) {
        this.iSSN = iSSN;
    }

    public String getBookImg() {
        return bookImg;
    }

    public void setBookImg(String bookImg) {
        this.bookImg = bookImg;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }
}
