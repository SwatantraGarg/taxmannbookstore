package com.planet.Taxmann.adapters;


import android.content.Context;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.planet.Taxmann.R;
import com.planet.Taxmann.model.BookData;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ViewHolder>{
    List<BookData> bookDataList;
    BookClicked bookClicked;
    Context context;


    public BookAdapter(Context context, List<BookData> bookDataList, BookClicked bookClicked  ) {
        this.context=context;
        this.bookClicked = bookClicked;
        this.bookDataList=bookDataList;

    }

    public interface BookClicked{
        void Clicked(View view, int position);
    }
    @NonNull
    @Override
    public BookAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BookAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.book_layout,null));
    }



    @Override
    public void onBindViewHolder(@NonNull final BookAdapter.ViewHolder holder, int position) {
        if(bookDataList!=null) {
            holder.bookTitle.setText(bookDataList.get(position).getProdName());
            Glide.with(context).load(bookDataList.get(position).getBookImg()).into(holder.bookImage);
            holder.authorName.setText(bookDataList.get(position).getAuthors());
            holder.bookPrice.setText("Rs "+bookDataList.get(position).getPriceInr().trim());

            int count=0;
            for (int i = 0; i < bookDataList.get(position).getRating().size(); i++) {
                if(bookDataList.get(position).getRating().get(i).equalsIgnoreCase("1")){
                    count++;
                }
            }
            if(count==0){
                holder.ratingBar.setRating(0);
            }
            else {
                holder.ratingBar.setRating(count);
            }
            Drawable progress1 = holder.ratingBar.getProgressDrawable();
            DrawableCompat.setTint(progress1,context.getResources().getColor(R.color.star_color));
        }
        if(bookDataList.get(position).getTag()!=null && !bookDataList.get(position).getTag().equalsIgnoreCase("")){
            holder.bestSellerImg.setVisibility(View.VISIBLE);
            holder.bestSellerImg.setText(bookDataList.get(position).getTag());
        }
        else {
            holder.bestSellerImg.setVisibility(View.GONE);
        }
        if(bookDataList.get(position).getProductTypeID()!=null){
            if(bookDataList.get(position).getProductTypeID().equalsIgnoreCase("4")){
                holder.bestSellerImg.setVisibility(View.VISIBLE);
                holder.bestSellerImg.setText("E-Book");
            }

        }


       holder.bookLayout.setTag(position);
        holder.bookLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos= (int) v.getTag();
                if(bookClicked!=null){
                    bookClicked.Clicked(v,pos);


                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return bookDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.book_layout)
        LinearLayout bookLayout;
        @BindView(R.id.book_image)
        ImageView bookImage;
        @BindView(R.id.book_title)
        TextView bookTitle;
        @BindView(R.id.myRatingBar)
        RatingBar ratingBar;
        @BindView(R.id.author_name)
        TextView authorName;
        @BindView(R.id.price)
        TextView bookPrice;
        @BindView(R.id.best_seller)
        TextView bestSellerImg;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}

