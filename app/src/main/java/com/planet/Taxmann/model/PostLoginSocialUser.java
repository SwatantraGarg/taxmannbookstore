package com.planet.Taxmann.model;

import com.google.gson.annotations.SerializedName;

public class PostLoginSocialUser {
    @SerializedName("email")
    private String email;

    @SerializedName("browser")
    private String browser;

    @SerializedName("browserVersion")
    private String browserVersion;

    @SerializedName("device")
    private String device;

    @SerializedName("deviceType")
    private String deviceType;

    @SerializedName("ipAddress")
    private String ipAddress;

    @SerializedName("name")
    private String name;

    @SerializedName("os")
    private String os;

    @SerializedName("osVersion")
    private String osVersion;

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public void setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    @SerializedName("userAgent")
    private String userAgent;

    @SerializedName("provider_name")
    private String provider_name;

    @SerializedName("provider_userId")
    private String provider_userId;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProvider_name() {
        return provider_name;
    }

    public void setProvider_name(String provider_name) {
        this.provider_name = provider_name;
    }

    public String getProvider_userId() {
        return provider_userId;
    }

    public void setProvider_userId(String provider_userId) {
        this.provider_userId = provider_userId;
    }



}
