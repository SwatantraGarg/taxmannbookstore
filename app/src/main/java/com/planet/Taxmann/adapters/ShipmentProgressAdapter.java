package com.planet.Taxmann.adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.vipulasri.timelineview.TimelineView;
import com.planet.Taxmann.R;
import com.planet.Taxmann.model.ShipmentTrackActivity;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.StringTokenizer;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ShipmentProgressAdapter extends RecyclerView.Adapter<ShipmentProgressAdapter.TimeLineViewHolder > {
    Context context;
    List<ShipmentTrackActivity> shipmentTrackActivityList;

    public ShipmentProgressAdapter(List<ShipmentTrackActivity> shipmentTrackActivityList, Context context) {
        this.shipmentTrackActivityList = shipmentTrackActivityList;
        this.context = context;
    }


    @NonNull
    @Override
    public TimeLineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.shipment_progress_layout, null);
        return new TimeLineViewHolder(view, viewType);
    }


    @Override
    public void onBindViewHolder(@NonNull TimeLineViewHolder holder, int position) {
        if(shipmentTrackActivityList!=null && shipmentTrackActivityList.size()>0){
            String date = shipmentTrackActivityList.get(position).getDate();
            String[] dateArr = date.split(" ");
            String time = dateArr[1];
           String[] timeArr = time.split(":");
           int  hr = Integer.parseInt(timeArr[0]);
           int min = Integer.parseInt(timeArr[1]);
          // String sec = timeArr[2];
            holder.date.setText(dateArr[0]);

            String Time = hr%12 + ":" + twoDigitString(min) + " " + ((hr>=12) ? "PM" : "AM");
            if(hr%12==0){
                Time =   "12"+":"+twoDigitString(min)+ " " + ((hr>=12) ? "PM" : "AM");
            }
            holder.time.setText(Time);

            holder.activityTxt.setText(shipmentTrackActivityList.get(position).getActivity());
            holder.locationTxt.setText(shipmentTrackActivityList.get(position).getLocation());
            if(shipmentTrackActivityList.get(position).getStatus()!=null && shipmentTrackActivityList.get(position).getStatus().equalsIgnoreCase("delivered")){
                holder.mTimelineView.setMarker(context.getResources().getDrawable(R.drawable.ic_check_box_circle));
            }
        }

    }

    @Override
    public int getItemCount() {
        return shipmentTrackActivityList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    public class TimeLineViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.time_marker)
        TimelineView mTimelineView;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.activity_txt)
        TextView activityTxt;
        @BindView(R.id.location_txt)
        TextView locationTxt;
        public TimeLineViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            mTimelineView.initLine(viewType);
        }
    }
    private String twoDigitString(long number)
    { if (number == 0)
    { return "00";
    } if (number / 10 == 0)

    { return "0" + number; }


        return String.valueOf(number);
    }

}
