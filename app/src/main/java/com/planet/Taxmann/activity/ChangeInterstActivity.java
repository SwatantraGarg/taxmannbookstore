package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.CheckboxAdapter;
import com.planet.Taxmann.adapters.IntrestCheckBoxAdapter;
import com.planet.Taxmann.adapters.SwitchInterestAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.PostLoginEmailUser;
import com.planet.Taxmann.model.PostLoginSocialUser;
import com.planet.Taxmann.model.PostStateProfessionInterst;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.model.UserInterest;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeInterstActivity extends AppCompatActivity {
    @BindView(R.id.other_layout)
    RelativeLayout otherLayout;
    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.head)
    TextView head;
    @BindView(R.id.cancel)
    TextView cancelBtn;
    @BindView(R.id.submit_btn)
    TextView submitBtn;

    @BindView(R.id.other_text_edit)
    EditText otherEditText;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    List<UserInterest> interestList = new ArrayList<>();
    List<String> valueList= new ArrayList<>();
    Intent intent;
    String type;
    UserData userData;
    String other;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_interst);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(ChangeInterstActivity.this,"user_data", UserData.class);
        intent = getIntent();
        if (intent != null) {

            interestList = intent.getParcelableArrayListExtra("intrest_list");
            valueList = intent.getStringArrayListExtra("value_list");
            type = intent.getStringExtra("type");
            other = intent.getStringExtra("other_text");

        }

        if (type != null && !type.equalsIgnoreCase("")) {
            otherLayout.setVisibility(View.VISIBLE);
            if(other!=null && !other.isEmpty()){
                otherEditText.setText(other);
            }

            head.setText("Select your Proffesion");

        } else {
            otherLayout.setVisibility(View.GONE);
        }
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChangeInterstActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SwitchInterestAdapter.count == null || SwitchInterestAdapter.count.size() == 0 && otherEditText.getText().toString().isEmpty()) {
                    Toast.makeText(ChangeInterstActivity.this, "Please select atleast one value", Toast.LENGTH_SHORT).show();

                }
                else {
                   /* HashSet<String> hashSet = new HashSet<String>();
                    hashSet.addAll(SwitchInterestAdapter.count);
                    SwitchInterestAdapter.count.clear();
                    SwitchInterestAdapter.count.addAll(hashSet);*/
                    PostStateProfessionInterst postStateProfessionInterst = new PostStateProfessionInterst();
                    postStateProfessionInterst.setEmail(userData.getEmail());
                    if(type != null && !type.equalsIgnoreCase("")){
                        postStateProfessionInterst.setProfession(SwitchInterestAdapter.count);
                       postStateProfessionInterst.setInterest(valueList);

                    }
                    else {
                        postStateProfessionInterst.setInterest(SwitchInterestAdapter.count);
                        postStateProfessionInterst.setProfession(valueList);
                    }

                    postStateProfessionInterst.setState("");
                    if (otherEditText.getText().toString().isEmpty()) {
                        postStateProfessionInterst.setProfessionOther("");
                    } else {
                        postStateProfessionInterst.setProfessionOther(otherEditText.getText().toString());
                    }

                    submitStateProfessionInterestResponse(postStateProfessionInterst);

                }

            }
        });


        SwitchInterestAdapter switchInterestAdapter = new SwitchInterestAdapter(interestList, ChangeInterstActivity.this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(switchInterestAdapter);

    }

    public void submitStateProfessionInterestResponse(PostStateProfessionInterst postStateProfessionInterst) {
        final RetrofitEasyApi apiService = ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call = apiService.postStateProfessionInterestInside(postStateProfessionInterst,userData.getUser_token());
        final KProgressHUD kProgressHUD = Constants.ShowProgress(ChangeInterstActivity.this, "Please wait...");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                int statusCode = response.code();
                kProgressHUD.dismiss();
                SwitchInterestAdapter.count.clear();
                valueList.clear();
                //interestList.clear();
                Constants.hideSoftKeyboard(ChangeInterstActivity.this);
                GeneralResponse generalResponse = response.body();
                if (generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")) {
                    Toast.makeText(ChangeInterstActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                    finish();


                } else {
                    Toast.makeText(ChangeInterstActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(ChangeInterstActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}