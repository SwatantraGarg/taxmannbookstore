package com.planet.Taxmann.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Filter;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.Fragments.ViewAllFragment;
import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.CheckboxAdapter;
import com.planet.Taxmann.adapters.FilterCategoryAdapter;
import com.planet.Taxmann.adapters.FilterSubCategoryAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.GetCount;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.AggregationsBySubject;
import com.planet.Taxmann.model.BookData;
import com.planet.Taxmann.model.CategorySubjectName;
import com.planet.Taxmann.model.CategorySubjectNameResponse;
import com.planet.Taxmann.model.FilterCategory;
import com.planet.Taxmann.model.PostProductIndex;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterActivity extends AppCompatActivity implements GetCount {
    @BindView(R.id.recycler_view_cat)
    RecyclerView recyclerViewCat;

    @BindView(R.id.recycler_view_sub_cat)
    RecyclerView recyclerViewSubCat;

    @BindView(R.id.reset)
    TextView resetBtn;

    @BindView(R.id.apply)
    TextView applyBtn;
    public  int count=0;
    FilterCategoryAdapter filterCategoryAdapter;
    List<String> filter_cat = new ArrayList<>();
    List<FilterCategory> filter_cataory = new ArrayList<>();
    List<String> filter_sub_cat = new ArrayList<>();
    List<AggregationsBySubject> aggregationsBySubjectList = new ArrayList<>();
    List<AggregationsBySubject> aggregationsBySubSubjectList = new ArrayList<>();
    List<AggregationsBySubject> aggregationsByFormatList = new ArrayList<>();
    List<AggregationsBySubject> aggregationsByAuthorList = new ArrayList<>();
    List<AggregationsBySubject> aggregationsByAvailablityList = new ArrayList<>();
    List<AggregationsBySubject> aggregationsByPublicationList = new ArrayList<>();
    List<AggregationsBySubject> aggregationByCategoryList = new ArrayList<>();

    List<CategorySubjectName> categorySubjectNameList = new ArrayList<>();
    Intent intent;
    String type,type1,type2,id;
    String cat_id;
    PostProductIndex postProductIndex=null;
    PostProductIndex postProductIndex1 = null;
    PostProductIndex postProductIndex2 = null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_layout);
        ButterKnife.bind(this);
        intent = getIntent();

        if(intent!=null){
            categorySubjectNameList = intent.getParcelableArrayListExtra("filter_list");
            type = intent.getStringExtra("type");
            type2 = intent.getStringExtra("type2");
            id = intent.getStringExtra("id");
        }

        postProductIndex = DroidPrefs.get(FilterActivity.this,"post_product",PostProductIndex.class);
        postProductIndex1 = DroidPrefs.get(FilterActivity.this,"cat_post_index",PostProductIndex.class);
        postProductIndex2 = DroidPrefs.get(FilterActivity.this,"post_filter",PostProductIndex.class);
        cat_id = postProductIndex1.getCatId();

if(type!=null && type.equalsIgnoreCase("category")){
    getProductIndexResponse(postProductIndex1);
}
else  if(type!=null && type.equalsIgnoreCase("filter")){
    getProductIndexResponse(postProductIndex2);
}
else {
    getProductIndexResponse(postProductIndex);
}

applyBtn.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
    applyFilter("");
    }

});

resetBtn.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        setResult(2,intent.putExtra("type1","reset"));
        finish();
    }
});
    }
    @Override
    public void onBackPressed() {
      applyFilter("");

    }
    public void getProductIndexResponse(PostProductIndex postProductIndex){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(FilterActivity.this,"Please wait");
        Call<CategorySubjectNameResponse> call;
        if(id!=null &&id.equalsIgnoreCase("action")){
           call = apiSerivce.getProductSearchFromIndex(postProductIndex);
        }else {
            call= apiSerivce.getProductIndexListResponse(postProductIndex);
        }
        call.enqueue(new Callback<CategorySubjectNameResponse>() {
            @Override
            public void onResponse(Call<CategorySubjectNameResponse> call, Response<CategorySubjectNameResponse> response) {
                kProgressHUD.dismiss();
                int status_code= response.code();
                CategorySubjectNameResponse categorySubjectNameResponse= response.body();
                if(categorySubjectNameResponse!=null){
                    List<CategorySubjectName> categorySubjectNameList=categorySubjectNameResponse.getData();
                    if( categorySubjectNameList!=null && categorySubjectNameList.size()>0) {
                        for (int i = 0; i < categorySubjectNameList.size(); i++) {

                            aggregationsBySubjectList = categorySubjectNameList.get(i).getAggregationsBySubject();
                            aggregationsBySubSubjectList = categorySubjectNameList.get(i).getAggregationsBySubSubject();
                            aggregationsByFormatList = categorySubjectNameList.get(i).getAggregationsByFormat();
                            aggregationsByAuthorList = categorySubjectNameList.get(i).getAggregationsByAuthor();
                            aggregationsByAvailablityList = categorySubjectNameList.get(i).getAggregationsByAvailability();
                            aggregationsByPublicationList = categorySubjectNameList.get(i).getAggregationsByYearOfPublication();
                            aggregationByCategoryList = categorySubjectNameList.get(i).getAggregationsByCategory();
                        }
                        if (aggregationsBySubjectList != null && aggregationsBySubjectList.size() > 0) {
                            if (postProductIndex.getCatId()!=null && postProductIndex.getCatId().equalsIgnoreCase("2") || postProductIndex.getCatId().equalsIgnoreCase("18")) {
                                filter_cataory.add(new FilterCategory("Course", 0));
                            } else if (postProductIndex.getCatId() != null && !postProductIndex.getCatId().equalsIgnoreCase("2") && !postProductIndex.getCatId().equalsIgnoreCase("6") && postProductIndex.getCatId().equalsIgnoreCase("18")) {
                                filter_cataory.add(new FilterCategory("Subject", 0));

                            } else if (postProductIndex.getCatId() != null && postProductIndex.getCatId().equalsIgnoreCase("6")) {
                                filter_cataory.add(new FilterCategory("Plan", 0));
                            } else {
                                filter_cataory.add(new FilterCategory("Subject", 0));
                            }
                            if (postProductIndex.getSubjectArray() != null && postProductIndex.getSubjectArray().size() > 0) {
                                for (int i = 0; i < aggregationsBySubjectList.size(); i++) {
                                    for (int j = 0; j < postProductIndex.getSubjectArray().size(); j++) {
                                        if (aggregationsBySubjectList.get(i).getDocName().equalsIgnoreCase(postProductIndex.getSubjectArray().get(j))) {
                                            aggregationsBySubjectList.get(i).setCHECKED(true);
                                        }
                                    }
                                }
                            }

                        }
                        if (aggregationsBySubSubjectList != null && aggregationsBySubSubjectList.size() > 0) {
                            if (postProductIndex.getCatId() != null && postProductIndex.getCatId().equalsIgnoreCase("2") || postProductIndex.getCatId().equalsIgnoreCase("18")) {

                                filter_cataory.add(new FilterCategory("Course Type", 0));
                            } else if (postProductIndex.getCatId() != null && !postProductIndex.getCatId().equalsIgnoreCase("2") && postProductIndex.getCatId().equalsIgnoreCase("18")) {
                                filter_cataory.add(new FilterCategory("Subject Type", 0));

                            } else {
                                filter_cataory.add(new FilterCategory("Subject Type", 0));
                            }

                            if (postProductIndex.getSubjectTypeArray() != null && postProductIndex.getSubjectTypeArray().size() > 0) {
                                for (int i = 0; i < aggregationsBySubSubjectList.size(); i++) {
                                    for (int j = 0; j < postProductIndex.getSubjectTypeArray().size(); j++) {
                                        if (aggregationsBySubSubjectList.get(i).getDocName().equalsIgnoreCase(postProductIndex.getSubjectTypeArray().get(j))) {
                                            aggregationsBySubSubjectList.get(i).setCHECKED(true);
                                        }
                                    }
                                }
                            }

                        }

                        if (aggregationByCategoryList != null && aggregationByCategoryList.size() > 0) {
                            filter_cataory.add(new FilterCategory("Category", 0));
                            if (postProductIndex.getCategoryArray() != null && postProductIndex.getCategoryArray().size() > 0) {
                                for (int i = 0; i < aggregationByCategoryList.size(); i++) {
                                    for (int j = 0; j < postProductIndex.getCategoryArray().size(); j++) {
                                        if (aggregationByCategoryList.get(i).getDocName().equalsIgnoreCase(postProductIndex.getCategoryArray().get(j))) {
                                            aggregationByCategoryList.get(i).setCHECKED(true);
                                        }
                                    }

                                }
                            }

                        }
                        if (aggregationsByFormatList != null && aggregationsByFormatList.size() > 0) {
                            filter_cataory.add(new FilterCategory("Format", 0));
                            if (postProductIndex.getFormatArray() != null && postProductIndex.getFormatArray().size() > 0) {
                                for (int i = 0; i < aggregationsByFormatList.size(); i++) {
                                    for (int j = 0; j < postProductIndex.getFormatArray().size(); j++) {
                                        if (aggregationsByFormatList.get(i).getDocName().equalsIgnoreCase(postProductIndex.getFormatArray().get(j))) {
                                            aggregationsByFormatList.get(i).setCHECKED(true);
                                        }
                                    }
                                }
                            }
                        }
                        if (aggregationsByAuthorList != null && aggregationsByAuthorList.size() > 0) {
                            filter_cataory.add(new FilterCategory("Authors", 0));
                            if (postProductIndex.getAuthorArray() != null && postProductIndex.getAuthorArray().size() > 0) {
                                for (int i = 0; i < aggregationsByAuthorList.size(); i++) {
                                    for (int j = 0; j < postProductIndex.getAuthorArray().size(); j++) {
                                        if (aggregationsByAuthorList.get(i).getDocName().equalsIgnoreCase(postProductIndex.getAuthorArray().get(j))) {
                                            aggregationsByAuthorList.get(i).setCHECKED(true);
                                        }
                                    }
                                }
                            }
                        }
                        if (aggregationsByAvailablityList != null && aggregationsByAvailablityList.size() > 0) {
                            filter_cataory.add(new FilterCategory("Availability", 0));
                            if (postProductIndex.getAvailabilityArray() != null && postProductIndex.getAvailabilityArray().size() > 0) {
                                for (int i = 0; i < aggregationsByAvailablityList.size(); i++) {
                                    for (int j = 0; j < postProductIndex.getAvailabilityArray().size(); j++) {
                                        if (aggregationsByAvailablityList.get(i).getDocName().equalsIgnoreCase(postProductIndex.getAvailabilityArray().get(j))) {
                                            aggregationsByAvailablityList.get(i).setCHECKED(true);
                                        }
                                    }
                                }
                            }
                        }
                        if (aggregationsByPublicationList != null && aggregationsByPublicationList.size() > 0) {
                            filter_cataory.add(new FilterCategory("Year of Publication", 0));
                            if (postProductIndex.getYearOfPublicationArray() != null && postProductIndex.getYearOfPublicationArray().size() > 0) {
                                for (int i = 0; i < aggregationsByPublicationList.size(); i++) {
                                    for (int j = 0; j < postProductIndex.getYearOfPublicationArray().size(); j++) {
                                        if (aggregationsByPublicationList.get(i).getDocName().equalsIgnoreCase(postProductIndex.getYearOfPublicationArray().get(j))) {
                                            aggregationsByPublicationList.get(i).setCHECKED(true);
                                        }
                                    }
                                }
                            }

                        }
                        filterCategoryAdapter = new FilterCategoryAdapter(filter_cataory, FilterActivity.this, new FilterCategoryAdapter.OnClicked() {
                            @Override
                            public void Clicked(View view, int position) {
                                if (view.getId() == R.id.rel && filter_cataory.get(position).getText().equalsIgnoreCase("Subject") || filter_cataory.get(position).getText().equalsIgnoreCase("Course") || filter_cataory.get(position).getText().equalsIgnoreCase("Plan")) {
                                    count = 0;
                                    for (int i = 0; i < aggregationsBySubjectList.size(); i++) {
                                        if (aggregationsBySubjectList.get(i).isCHECKED()) {
                                            count++;
                                        }
                                    }

                                    FilterSubCategoryAdapter filterSubCategoryAdapter = new FilterSubCategoryAdapter(position, filter_cataory, aggregationsBySubjectList, FilterActivity.this, new FilterSubCategoryAdapter.TextClick() {
                                        @Override
                                        public void clicked(View view, int pos) {
                                        }
                                    });
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                    recyclerViewSubCat.setLayoutManager(mLayoutManager);
                                    recyclerViewSubCat.setItemAnimator(new DefaultItemAnimator());
                                    recyclerViewSubCat.setAdapter(filterSubCategoryAdapter);
                                    filterSubCategoryAdapter.notifyDataSetChanged();
                                } else if (view.getId() == R.id.rel && filter_cataory.get(position).getText().equalsIgnoreCase("Subject Type") || filter_cataory.get(position).getText().equalsIgnoreCase("Course Type")) {
                                    count = 0;
                                    for (int i = 0; i < aggregationsBySubSubjectList.size(); i++) {
                                        if (aggregationsBySubSubjectList.get(i).isCHECKED()) {
                                            count++;

                                        }
                                    }
                                    FilterSubCategoryAdapter filterSubCategoryAdapter = new FilterSubCategoryAdapter(position, filter_cataory, aggregationsBySubSubjectList, FilterActivity.this, new FilterSubCategoryAdapter.TextClick() {
                                        @Override
                                        public void clicked(View view, int pos) {

                                        }
                                    });
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                    recyclerViewSubCat.setLayoutManager(mLayoutManager);
                                    recyclerViewSubCat.setItemAnimator(new DefaultItemAnimator());
                                    recyclerViewSubCat.setAdapter(filterSubCategoryAdapter);
                                    filterSubCategoryAdapter.notifyDataSetChanged();
                                }
                                else if (view.getId() == R.id.rel && filter_cataory.get(position).getText().equalsIgnoreCase("Category")) {
                                    FilterSubCategoryAdapter filterSubCategoryAdapter = new FilterSubCategoryAdapter(position, filter_cataory, aggregationByCategoryList, FilterActivity.this, new FilterSubCategoryAdapter.TextClick() {
                                        @Override
                                        public void clicked(View view, int pos) {
                                            if(aggregationByCategoryList.get(pos).getDocName().contains("|")){
                                                String s = aggregationByCategoryList.get(pos).getDocName().trim();
                                                List<String> word = new ArrayList<>();
                                                StringTokenizer st = new StringTokenizer(s,"\\|");
                                                while(st.hasMoreTokens())
                                                    word.add(st.nextToken());
                                                for (int i = 0; i <word.size() ; i++) {
                                                    applyFilter(word.get(0));
                                                }
                                            }
                                        }
                                    });
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                    recyclerViewSubCat.setLayoutManager(mLayoutManager);
                                    recyclerViewSubCat.setItemAnimator(new DefaultItemAnimator());
                                    recyclerViewSubCat.setAdapter(filterSubCategoryAdapter);
                                    filterSubCategoryAdapter.notifyDataSetChanged();
                                }
                                else if (view.getId() == R.id.rel && filter_cataory.get(position).getText().equalsIgnoreCase("Format")) {
                                    FilterSubCategoryAdapter filterSubCategoryAdapter = new FilterSubCategoryAdapter(position, filter_cataory, aggregationsByFormatList, FilterActivity.this, new FilterSubCategoryAdapter.TextClick() {
                                        @Override
                                        public void clicked(View view, int pos) {
                                        }
                                    });

                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                    recyclerViewSubCat.setLayoutManager(mLayoutManager);
                                    recyclerViewSubCat.setItemAnimator(new DefaultItemAnimator());
                                    recyclerViewSubCat.setAdapter(filterSubCategoryAdapter);
                                    filterSubCategoryAdapter.notifyDataSetChanged();

                                }
                                else if (view.getId() == R.id.rel && filter_cataory.get(position).getText().equalsIgnoreCase("Authors")) {
                                    FilterSubCategoryAdapter filterSubCategoryAdapter = new FilterSubCategoryAdapter(position, filter_cataory, aggregationsByAuthorList, FilterActivity.this, new FilterSubCategoryAdapter.TextClick() {
                                        @Override
                                        public void clicked(View view, int pos) {

                                        }
                                    });

                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                    recyclerViewSubCat.setLayoutManager(mLayoutManager);
                                    recyclerViewSubCat.setItemAnimator(new DefaultItemAnimator());
                                    recyclerViewSubCat.setAdapter(filterSubCategoryAdapter);
                                    filterSubCategoryAdapter.notifyDataSetChanged();

                                }
                                else if (view.getId() == R.id.rel && filter_cataory.get(position).getText().equalsIgnoreCase("Availability")) {
                                    FilterSubCategoryAdapter filterSubCategoryAdapter = new FilterSubCategoryAdapter(position, filter_cataory, aggregationsByAvailablityList, FilterActivity.this, new FilterSubCategoryAdapter.TextClick() {
                                        @Override
                                        public void clicked(View view, int pos) {

                                        }
                                    });
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                    recyclerViewSubCat.setLayoutManager(mLayoutManager);
                                    recyclerViewSubCat.setItemAnimator(new DefaultItemAnimator());
                                    recyclerViewSubCat.setAdapter(filterSubCategoryAdapter);
                                    filterSubCategoryAdapter.notifyDataSetChanged();


                                } else if (view.getId() == R.id.rel && filter_cataory.get(position).getText().equalsIgnoreCase("Year of Publication")) {
                                    FilterSubCategoryAdapter filterSubCategoryAdapter = new FilterSubCategoryAdapter(position, filter_cataory, aggregationsByPublicationList, FilterActivity.this, new FilterSubCategoryAdapter.TextClick() {
                                        @Override
                                        public void clicked(View view, int pos) {

                                        }
                                    });
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                    recyclerViewSubCat.setLayoutManager(mLayoutManager);
                                    recyclerViewSubCat.setItemAnimator(new DefaultItemAnimator());
                                    recyclerViewSubCat.setAdapter(filterSubCategoryAdapter);
                                    filterSubCategoryAdapter.notifyDataSetChanged();
                                }

                            }

                        });
                        for (int i = 0; i < filter_cataory.size(); i++) {
                            filter_cataory.get(i).setCount(count);
                        }
                        RecyclerView.LayoutManager LayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerViewCat.setLayoutManager(LayoutManager);
                        recyclerViewCat.setItemAnimator(new DefaultItemAnimator());
                        recyclerViewCat.setAdapter(filterCategoryAdapter);
                        filterCategoryAdapter.notifyDataSetChanged();

                        if (aggregationsBySubjectList != null && aggregationsBySubjectList.size() > 0) {
                            for (int i = 0; i < aggregationsBySubjectList.size(); i++) {
                                filter_sub_cat.add(aggregationsBySubjectList.get(i).getDocName());
                            }
                            FilterSubCategoryAdapter filterSubCategoryAdapter = new FilterSubCategoryAdapter(0, filter_cataory, aggregationsBySubjectList, FilterActivity.this, new FilterSubCategoryAdapter.TextClick() {
                                @Override
                                public void clicked(View view, int pos) {

                                }
                            }
                            );
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            recyclerViewSubCat.setLayoutManager(mLayoutManager);
                            recyclerViewSubCat.setItemAnimator(new DefaultItemAnimator());
                            recyclerViewSubCat.setAdapter(filterSubCategoryAdapter);
                            filterSubCategoryAdapter.notifyDataSetChanged();
                        } else {
                            for (int i = 0; i < aggregationByCategoryList.size(); i++) {
                                filter_sub_cat.add(aggregationByCategoryList.get(i).getDocName());
                            }

                            FilterSubCategoryAdapter filterSubCategoryAdapter = new FilterSubCategoryAdapter(0, filter_cataory, aggregationByCategoryList, FilterActivity.this, new FilterSubCategoryAdapter.TextClick() {
                                @Override
                                public void clicked(View view, int pos) {

                                }
                            });
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            recyclerViewSubCat.setLayoutManager(mLayoutManager);
                            recyclerViewSubCat.setItemAnimator(new DefaultItemAnimator());
                            recyclerViewSubCat.setAdapter(filterSubCategoryAdapter);
                            filterSubCategoryAdapter.notifyDataSetChanged();
                        }
                    }

                }
                else {

                    kProgressHUD.dismiss();
                    Toast.makeText(FilterActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CategorySubjectNameResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(FilterActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void Count(int count, int pos) {
        filter_cataory.get(pos).setCount(count);
        filterCategoryAdapter.notifyItemChanged(pos);
    }
    public void applyFilter(String cat){
        List<String> subject_array = new ArrayList<>();
        List<String> sub_subject_array = new ArrayList<>();
        List<String> format_array = new ArrayList<>();
        List<String> author_array = new ArrayList<>();
        List<String> avail_array = new ArrayList<>();
        List<String> pub_array = new ArrayList<>();
        List<String> cat_array = new ArrayList<>();
        if(aggregationsBySubjectList!=null && aggregationsBySubjectList.size()>0){
            for (int i = 0; i <aggregationsBySubjectList.size() ; i++) {
                if(aggregationsBySubjectList.get(i).isCHECKED()){
                    subject_array.add(aggregationsBySubjectList.get(i).getDocName());
                }
            }}
        if(aggregationsBySubSubjectList!=null && aggregationsBySubSubjectList.size()>0){
            for (int i = 0; i <aggregationsBySubSubjectList.size() ; i++) {
                if(aggregationsBySubSubjectList.get(i).isCHECKED()){
                    sub_subject_array.add(aggregationsBySubSubjectList.get(i).getDocName());
                }}}
        if(aggregationsByFormatList!=null && aggregationsByFormatList.size()>0){
            for (int i = 0; i <aggregationsByFormatList.size() ; i++) {
                if(aggregationsByFormatList.get(i).isCHECKED()){
                    format_array.add(aggregationsByFormatList.get(i).getDocName());
                }}}
        if(aggregationsByAuthorList!=null && aggregationsByAuthorList.size()>0){
            for (int i = 0; i <aggregationsByAuthorList.size() ; i++) {
                if(aggregationsByAuthorList.get(i).isCHECKED()){
                    author_array.add(aggregationsByAuthorList.get(i).getDocName());
                }
            }}
        if(aggregationsByAvailablityList!=null && aggregationsByAvailablityList.size()>0){
            for (int i = 0; i <aggregationsByAvailablityList.size() ; i++) {
                if(aggregationsByAvailablityList.get(i).isCHECKED()){
                    avail_array.add(aggregationsByAvailablityList.get(i).getDocName());
                }}
        }
        if(aggregationsByPublicationList!=null && aggregationsByPublicationList.size()>0){
            for (int i = 0; i <aggregationsByPublicationList.size() ; i++) {
                if(aggregationsByPublicationList.get(i).isCHECKED()){
                    pub_array.add(aggregationsByPublicationList.get(i).getDocName());
                }}
        }
        if(aggregationByCategoryList!=null && aggregationByCategoryList.size()>0){
            for (int i = 0; i <aggregationByCategoryList.size() ; i++) {
                if(aggregationByCategoryList.get(i).isCHECKED()){
                    cat_array.add(aggregationByCategoryList.get(i).getDocName());
                }}
        }
        if(type!=null && type.equalsIgnoreCase("category")){
            postProductIndex1.setSubjectArray(subject_array);
            postProductIndex1.setSubjectTypeArray(sub_subject_array);
            postProductIndex1.setFormatArray(format_array);
            postProductIndex1.setAuthorArray(author_array);
            postProductIndex1.setAvailabilityArray(avail_array);
            postProductIndex1.setYearOfPublicationArray(pub_array);
            postProductIndex1.setFilterFrom("subject");
            postProductIndex1.setSortBy("sortpublicationdate");
            postProductIndex1.setCatId(cat_id);
            postProductIndex.setPage(1);
            postProductIndex.setPagesize(200);
            postProductIndex1.setType("");
            DroidPrefs.apply(FilterActivity.this,"post_filter",postProductIndex1);
            setResult(2,intent.putExtra("type1","filter"));
            finish();
        }
        else {
            if(type2!=null && type2.equalsIgnoreCase("category")){
                postProductIndex.setSubjectArray(subject_array);
                postProductIndex.setSubjectTypeArray(sub_subject_array);
                postProductIndex.setFormatArray(format_array);
                postProductIndex.setCatId(cat_id);
                postProductIndex.setPage(1);
                postProductIndex.setPagesize(200);
                postProductIndex.setAuthorArray(author_array);
                postProductIndex.setAvailabilityArray(avail_array);
                postProductIndex.setYearOfPublicationArray(pub_array);
                postProductIndex.setCategoryArray(cat_array);
                postProductIndex.setFilterFrom("subject");
                postProductIndex.setType("");
                postProductIndex.setSearchString("");
                postProductIndex.setSearchId("1");
                postProductIndex.setSortBy("sortpublicationdate");
                DroidPrefs.apply(FilterActivity.this,"post_filter",postProductIndex);
                setResult(2,intent.putExtra("type1","filter"));
                finish();
            }
            else {
                if(cat!=null && !cat.equalsIgnoreCase("")){
                    postProductIndex.setSubjectArray(subject_array);
                    postProductIndex.setSubjectTypeArray(sub_subject_array);
                    postProductIndex.setFormatArray(format_array);
                    postProductIndex.setCatId(cat);
                    postProductIndex.setAuthorArray(author_array);
                    postProductIndex.setAvailabilityArray(avail_array);
                    postProductIndex.setYearOfPublicationArray(pub_array);
                    postProductIndex.setCategoryArray(cat_array);
                    postProductIndex.setFilterFrom("");
                    postProductIndex.setSearchId("0");
                    postProductIndex.setType("sub");
                    postProductIndex.setPage(1);
                    postProductIndex.setPagesize(200);
                    DroidPrefs.apply(FilterActivity.this,"post_filter",postProductIndex);
                    setResult(3,intent.putExtra("type1","filter"));
                    finish();
                }else {
                    postProductIndex.setSubjectArray(subject_array);
                    postProductIndex.setSubjectTypeArray(sub_subject_array);
                    postProductIndex.setFormatArray(format_array);
                    postProductIndex.setCatId(postProductIndex.getCatId());
                    postProductIndex.setAuthorArray(author_array);
                    postProductIndex.setAvailabilityArray(avail_array);
                    postProductIndex.setYearOfPublicationArray(pub_array);
                    postProductIndex.setCategoryArray(cat_array);
                    postProductIndex.setFilterFrom("subject");
                    postProductIndex.setType("");
                    postProductIndex.setPage(1);
                    postProductIndex.setPagesize(200);
                    DroidPrefs.apply(FilterActivity.this,"post_filter",postProductIndex);
                    setResult(2,intent.putExtra("type1","filter"));
                    finish();
                }

            }

        }
    }
}
