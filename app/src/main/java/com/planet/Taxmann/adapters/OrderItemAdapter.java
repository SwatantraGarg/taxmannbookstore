package com.planet.Taxmann.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.planet.Taxmann.R;
import com.planet.Taxmann.model.Item;
import com.planet.Taxmann.model.MyOrder;
import com.planet.Taxmann.model.Shippment;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemAdapter.ViewHolder> {
    List<Item> itemList;
    Context context;

    public OrderItemAdapter(List<Item> itemList, Context context) {
        this.itemList= itemList;
        this.context = context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OrderItemAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item,null));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(itemList!=null && itemList.size()>0){
                Glide.with(context).load(itemList.get(position).getImage()).into(holder.bookImg);
                holder.bookName.setText(itemList.get(position).getProdName());
                holder.authors.setText(itemList.get(position).getAuthors());
                if(itemList.get(position).getPrice()!=null && !itemList.get(position).getPrice().equalsIgnoreCase("")){
                holder.price.setText("₹ "+itemList.get(position).getPrice());
            }

        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.book_img)
        ImageView bookImg;
        @BindView(R.id.book_name)
        TextView bookName;
        @BindView(R.id.author)
        TextView authors;
        @BindView(R.id.price)
        TextView price;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
