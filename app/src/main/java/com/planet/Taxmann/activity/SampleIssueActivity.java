package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.SampleIssueAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.IndianStateResponse;
import com.planet.Taxmann.model.PostSampleIssue;
import com.planet.Taxmann.model.PostSampleJournalUserAddress;
import com.planet.Taxmann.model.SampleIssue;
import com.planet.Taxmann.model.SampleIssueResponse;
import com.planet.Taxmann.model.StatesList;
import com.planet.Taxmann.model.Sublist;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

public class SampleIssueActivity extends AppCompatActivity {
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.submit_btn)
    TextView submitBtn;
    UserData userData;
    Intent intent;
    String book_id;
    List<SampleIssue> sampleIssueList;
    List<StatesList> statedata;
    ArrayAdapter<String> stateAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_issue);
        ButterKnife.bind(this);
        userData = DroidPrefs.get(SampleIssueActivity.this,"user_data", UserData.class);
        getSampleIssueResponse();

        intent = getIntent();
        if(intent!=null){
         book_id = intent.getStringExtra("book_id");
        }

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sampleIssueList!=null && sampleIssueList.size()>0){
                    PostSampleIssue postSampleIssue = new PostSampleIssue();
                    List<Sublist> data = new ArrayList<>();
                    for (int i = 0; i <sampleIssueList.size() ; i++) {
                        if(sampleIssueList.get(i).isIS_CHECKED()){
                            Sublist sublist = new Sublist();
                            sublist.setSubjectId(sampleIssueList.get(i).getProdid());
                            sublist.setSubjectName(sampleIssueList.get(i).getProdName());
                            data.add(sublist);
                        }
                    }
                    postSampleIssue.setSublist(data);
                    showPopUp(postSampleIssue);
                }
            }
        });
    }
    public void getSampleIssueResponse(){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<SampleIssueResponse> call= apiSerivce.getSampleIssueResponse(userData.getUser_token());
        call.enqueue(new Callback<SampleIssueResponse>() {
            @Override
            public void onResponse(Call<SampleIssueResponse> call, Response<SampleIssueResponse> response) {
                int status_code= response.code();
                SampleIssueResponse sampleIssueResponse= response.body();
                if(sampleIssueResponse!=null){
                     sampleIssueList =sampleIssueResponse.getData();
                    if(sampleIssueList!=null && sampleIssueList.size()>0){
                        int pos = 0;
                        for (int i = 0; i <sampleIssueList.size() ; i++) {
                            if(sampleIssueList.get(i).getProdid().equalsIgnoreCase(book_id)){
                                pos = i;
                            }

                        }
                        SampleIssueAdapter sampleIssueAdapter = new SampleIssueAdapter(sampleIssueList, pos,SampleIssueActivity.this);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SampleIssueActivity.this, LinearLayoutManager.VERTICAL,false);
                        recycler_view.setLayoutManager(linearLayoutManager);
                        recycler_view.setAdapter(sampleIssueAdapter);
                    }
                }

            }

            @Override
            public void onFailure(Call<SampleIssueResponse> call, Throwable t) {
                Toast.makeText(SampleIssueActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void postSampleIssueResponse(Dialog dialog, PostSampleIssue postSampleIssue, PostSampleJournalUserAddress postSampleJournalUserAddress){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiSerivce.postSampleIssueResponse(postSampleIssue,userData.getUser_token());
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                int status_code= response.code();
                GeneralResponse generalResponse= response.body();
                if(generalResponse!=null){
                    SampleIssueAdapter.value.clear();
                    if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")) {
                        double rqst_id = (double) generalResponse.getData();
                        int id = (int)rqst_id;
                        postSampleJournalUserAddress.setRequestid(id);
                        postSampleJournalUserAddressResponse(dialog,postSampleJournalUserAddress);
                       //Toast.makeText(SampleIssueActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                Toast.makeText(SampleIssueActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void showPopUp(PostSampleIssue postSampleIssue){
        Dialog dialog= new Dialog(SampleIssueActivity.this);
        dialog.setContentView(R.layout.sample_issue_details);
        dialog.show();
        Window window= dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        ImageView cross = (ImageView)dialog.findViewById(R.id.cross);
        TextView submit = (TextView)dialog.findViewById(R.id.submit_btn);
        EditText fname = (EditText) dialog.findViewById(R.id.user_name);
        EditText company = (EditText) dialog.findViewById(R.id.company_name);
        EditText flatno = (EditText) dialog.findViewById(R.id.Flat_no);
        EditText streetName = (EditText) dialog.findViewById(R.id.street_name);
        EditText locality = (EditText) dialog.findViewById(R.id.locality_name);
        EditText pinCode = (EditText) dialog.findViewById(R.id.pincode_no);
        EditText city = (EditText) dialog.findViewById(R.id.city_name);
        EditText landmark = (EditText) dialog.findViewById(R.id.landmark);

        SearchableSpinner spinnerState = (SearchableSpinner)dialog.findViewById(R.id.spin_state);
        getIndianStateResponse(spinnerState);

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fname.getText().toString().isEmpty()){
                    Snackbar.make(fname,"Please enter your name",Snackbar.LENGTH_SHORT).show();
                }
                else if(company.getText().toString().isEmpty()){
                    Snackbar.make(company,"Please enter your company name",Snackbar.LENGTH_SHORT).show();
                }
                else if(flatno.getText().toString().isEmpty()){
                    Snackbar.make(flatno,"Please enter your flat no.",Snackbar.LENGTH_SHORT).show();
                }
                else  if(streetName.getText().toString().isEmpty()){
                    Snackbar.make(streetName,"Please enter your street name",Snackbar.LENGTH_SHORT).show();
                }
                else if(locality.getText().toString().isEmpty()){
                    Snackbar.make(locality,"Please enter your locality",Snackbar.LENGTH_SHORT).show();
                }
                else  if(pinCode.getText().toString().isEmpty()){
                    Snackbar.make(pinCode,"Please enter your pincode",Snackbar.LENGTH_SHORT).show();
                }
                else if(pinCode.getText().toString().length()<4){
                    Snackbar.make(pinCode,"Please enter valid pincode",Snackbar.LENGTH_SHORT).show();
                }
                else  if(city.getText().toString().isEmpty()){
                    Snackbar.make(city,"Please enter your city name",Snackbar.LENGTH_SHORT).show();
                }
                else if(spinnerState.getSelectedItem()==null || spinnerState.getSelectedItem().toString().isEmpty()){
                    Snackbar.make(spinnerState,"Please enter your state name",Snackbar.LENGTH_SHORT).show();
                }
                else  if(landmark.getText().toString().isEmpty()){
                    Snackbar.make(landmark,"Please enter your landmark",Snackbar.LENGTH_SHORT).show();
                }
                else {
                    String name = fname.getText().toString();
                    String companyName = company.getText().toString();
                    String flat = flatno.getText().toString();
                    String street = streetName.getText().toString();
                    String pincodeNo = pinCode.getText().toString();
                    String localityName = locality.getText().toString();
                    String cityName = city.getText().toString();
                    String landmarkName = landmark.getText().toString();
                    String state = spinnerState.getSelectedItem().toString();
                    PostSampleJournalUserAddress postSampleJournalUserAddress = new PostSampleJournalUserAddress();
                    postSampleJournalUserAddress.setFname(name);
                    postSampleJournalUserAddress.setCompanyname(companyName);
                    postSampleJournalUserAddress.setFlatdoorno(flat);
                    postSampleJournalUserAddress.setStreetroadno(street);
                    postSampleJournalUserAddress.setZipcode(pincodeNo);
                    postSampleJournalUserAddress.setArealocality(localityName);
                    postSampleJournalUserAddress.setCity(cityName);
                    postSampleJournalUserAddress.setLandmarks(landmarkName);
                    postSampleJournalUserAddress.setState(state);
                    postSampleIssueResponse(dialog,postSampleIssue,postSampleJournalUserAddress);
                }
            }
        });
    }
    public void getIndianStateResponse(SearchableSpinner spinner) {
        final RetrofitEasyApi apiService = ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<IndianStateResponse> call = apiService.getIndianStateResponse();
        final KProgressHUD kProgressHUD = Constants.ShowProgress(this, "Please wait");
        call.enqueue(new Callback<IndianStateResponse>() {
            @Override
            public void onResponse(Call<IndianStateResponse> call, Response<IndianStateResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(SampleIssueActivity.this);
                int statusCode = response.code();
                IndianStateResponse indianStateResponse = response.body();
                if (indianStateResponse != null) {
                    if(indianStateResponse.getData()!=null && indianStateResponse.getData().size()>0){
                        statedata = indianStateResponse.getData();
                        List<String> state = new ArrayList<>();
                        for (int i = 0; i <statedata.size() ; i++) {
                            String stat= statedata.get(i).getName();
                            state.add(stat);
                        }
                        // Creating adapter for spinner
                        stateAdapter = new ArrayAdapter<String>(SampleIssueActivity.this, android.R.layout.simple_spinner_item, state);
                        // Drop down layout style - list view with radio button
                        stateAdapter.setDropDownViewResource(R.layout.spinner_item);
                        // attaching data adapter to spinner
                        spinner.setAdapter(stateAdapter);
                    }

                }
            }
            @Override
            public void onFailure(Call<IndianStateResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(SampleIssueActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void postSampleJournalUserAddressResponse(Dialog dialog, PostSampleJournalUserAddress postSampleJournalUserAddress){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiSerivce.postSampleJournalUserAddressResponse(postSampleJournalUserAddress, userData.getUser_token());
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                int status_code= response.code();
                GeneralResponse generalResponse= response.body();
                if(generalResponse!=null){
                    if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")) {
                        Toast.makeText(SampleIssueActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                }
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                Toast.makeText(SampleIssueActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
