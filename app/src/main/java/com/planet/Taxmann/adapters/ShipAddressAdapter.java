package com.planet.Taxmann.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.planet.Taxmann.R;
import com.planet.Taxmann.activity.CheckOutActivity;
import com.planet.Taxmann.model.Addresses;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShipAddressAdapter extends RecyclerView.Adapter<ShipAddressAdapter.ViewHolder>  {

    List<Addresses>addressesList;
    Context context;
    ButtonClick buttonClick;
    String type;
    int lastCheckedPosition=0;


    public ShipAddressAdapter(Context context, List<Addresses> addressesList,String type, ButtonClick  buttonClick) {
        this.addressesList = addressesList;
        this.context = context;
        this.buttonClick = buttonClick;
        this.type=type;

    }
    public interface ButtonClick{
        void clicked(View view, int pos);
        }

    @NonNull
    @Override
    public ShipAddressAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ShipAddressAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.ship_add_item,null));
    }

    @Override
    public void onBindViewHolder(@NonNull ShipAddressAdapter.ViewHolder holder, int position) {
if(addressesList!=null && addressesList.size()>0){
    if(type.contains("checkout")){
        holder.tagAddType.setVisibility(View.GONE);
        holder.Radio.setVisibility(View.VISIBLE);
            holder.Radio.setChecked(position == lastCheckedPosition);
    }
    else {
        holder.tagAddType.setVisibility(View.VISIBLE);
        holder.Radio.setVisibility(View.GONE);
    }

        holder.userName.setText(addressesList.get(position).getName());
        holder.userNname.setText(addressesList.get(position).getName());
        holder.address.setText(addressesList.get(position).getFullAddress());
        holder.mobileNo.setText(addressesList.get(position).getCountrycode()+" "+addressesList.get(position).getUserMobile());
        if(addressesList.get(position).getAddType()==1){
            holder.tagAddType.setText("Home");
            holder.Radio.setText("Home Address");
        }
        else if(addressesList.get(position).getAddType()==2){
            holder.Radio.setText("Office Address");
            holder.tagAddType.setText("Office");
        }

    }
        holder.removeBtn.setTag(position);
        holder.editBtn.setTag(position);
        holder.removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos= (int) view.getTag();
                buttonClick.clicked(view,pos);

            }
        });

        holder.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int)v.getTag();
                buttonClick.clicked(v,pos);
            }
        });

        holder.shipLayout.setTag(position);
        holder.shipLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int)v.getTag();
                buttonClick.clicked(v,pos);
                lastCheckedPosition = position;
                //because of this blinking problem occurs so
                //i have a suggestion to add notifyDataSetChanged();
                //   notifyItemRangeChanged(0, list.length);//blink list problem
                notifyDataSetChanged();

            }
        });
        holder.Radio.setTag(position);
        holder.Radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int)v.getTag();
                buttonClick.clicked(v,pos);
                lastCheckedPosition = position;
                //because of this blinking problem occurs so
                //i have a suggestion to add notifyDataSetChanged();
                //   notifyItemRangeChanged(0, list.length);//blink list problem
                notifyDataSetChanged();

            }
        });

    }

    @Override
    public int getItemCount() {
        return addressesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.user_name)
        TextView userName;

        @BindView(R.id.ship_layout)
        RelativeLayout shipLayout;
        @BindView(R.id.tag_radio)
        RadioButton Radio;
        @BindView(R.id.user_nname)
        TextView userNname;

        @BindView(R.id.tag_add_type)
        TextView tagAddType;

        @BindView(R.id.address)
        TextView address;

        @BindView(R.id.mobile_no)
        TextView mobileNo;

        @BindView(R.id.edit_btn)
        TextView editBtn;

        @BindView(R.id.remove_btn)
        TextView removeBtn;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
    public  void removeAt(int position) {
        addressesList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, addressesList.size());
    }
}
