package com.planet.Taxmann.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInterest implements Parcelable {
    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("Value")
    @Expose
    private String value;
    @SerializedName("selected")
    @Expose
    private Integer selected;

    protected UserInterest(Parcel in) {
        iD = in.readString();
        value = in.readString();
        if (in.readByte() == 0) {
            selected = null;
        } else {
            selected = in.readInt();
        }
    }

    public static final Creator<UserInterest> CREATOR = new Creator<UserInterest>() {
        @Override
        public UserInterest createFromParcel(Parcel in) {
            return new UserInterest(in);
        }

        @Override
        public UserInterest[] newArray(int size) {
            return new UserInterest[size];
        }
    };

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getSelected() {
        return selected;
    }

    public void setSelected(Integer selected) {
        this.selected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(iD);
        dest.writeString(value);
        if (selected == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(selected);
        }
    }
}
