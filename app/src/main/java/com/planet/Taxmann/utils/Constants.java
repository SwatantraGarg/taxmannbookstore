package com.planet.Taxmann.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.core.app.ActivityCompat;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.Fragments.ProfileFragment;

public class Constants {
   // public static final String BASE_URL =  "https://43.249.54.99/taxmannapi/";  //Live web services
    public static final String BASE_URL =  "https://liveresearchapi.taxmann.com/";  //Live web services
    public static final String TEST_URL = "http://qc.taxmann.com/";
    public static final String WEB_URL = "https://www.taxmann.com/";


    public static boolean isInternetConnected(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public static KProgressHUD ShowProgress(Context context, String label){
        KProgressHUD kProgressHUD= KProgressHUD.create(context);
        kProgressHUD .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        kProgressHUD  .setCancellable(true);
        kProgressHUD.setLabel(label);
        kProgressHUD .setAnimationSpeed(2);
        kProgressHUD .setDimAmount(0.5f);
        kProgressHUD .show();
        return kProgressHUD;
    }

    public static void hideSoftKeyboard(Activity activity) {
        try{
            InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void ShowKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }

        else {

        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
