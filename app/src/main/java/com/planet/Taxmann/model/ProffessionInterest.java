package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class ProffessionInterest {

    @SerializedName("userInterest")
    @Expose
    private List<UserInterest> userInterest = null;

    @SerializedName("userProffession")
    @Expose
    private List<UserInterest> userProffession = null;

    public List<UserInterest> getUserInterest() {
        return userInterest;
    }

    public void setUserInterest(List<UserInterest> userInterest) {
        this.userInterest = userInterest;
    }

    public List<UserInterest> getUserProffession() {
        return userProffession;
    }

    public void setUserProffession(List<UserInterest> userProffession) {
        this.userProffession = userProffession;
    }

}
