package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.BookAdapter;
import com.planet.Taxmann.model.BookData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewAllBooksActivity extends AppCompatActivity {
    @BindView(R.id.view_all_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.records)
    TextView recordCount;
    List<BookData> bookDataList= new ArrayList<>();

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_books);
        ButterKnife.bind(this);

        intent=getIntent();
        if(intent!=null){
            bookDataList=intent.getParcelableArrayListExtra("book_list");
        }
        String count= "Records : "+"<b> "+bookDataList.size()+" </b>";
        recordCount.setText(Html.fromHtml(count));
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
        if(bookDataList!=null){
            BookAdapter bookAdapter = new BookAdapter(ViewAllBooksActivity.this,bookDataList, new BookAdapter.BookClicked() {
                @Override
                public void Clicked(View view, int position) {
                    Intent intent =new Intent(ViewAllBooksActivity.this,BookDetailsActivity.class);
                    intent.putExtra("book_id",bookDataList.get(position).getMainProdId());
                    startActivity(intent);
                }

            });
            bookAdapter.notifyDataSetChanged();

            GridLayoutManager gridLayoutManager=new GridLayoutManager(ViewAllBooksActivity.this,2);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(bookAdapter);


        }




    }
}
