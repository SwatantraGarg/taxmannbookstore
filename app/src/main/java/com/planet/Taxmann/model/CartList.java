package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartList {
    @SerializedName("ProdTypeID")
    @Expose
    private Integer prodTypeID;
    @SerializedName("prodID")
    @Expose
    private Integer prodID;
    @SerializedName("prodName")
    @Expose
    private String prodName;
    @SerializedName("qty")
    @Expose
    private Integer qty;
    @SerializedName("unitPrice")
    @Expose
    private Integer unitPrice;
    @SerializedName("totalPrice")
    @Expose
    private Integer totalPrice;

    @SerializedName("sMonth")
    @Expose
    private String sMonth;
    @SerializedName("sYear")
    @Expose
    private String sYear;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @SerializedName("currency")
    @Expose
    private String currency;

    public Integer getProdTypeID() {
        return prodTypeID;
    }

    public void setProdTypeID(Integer prodTypeID) {
        this.prodTypeID = prodTypeID;
    }

    public Integer getProdID() {
        return prodID;
    }

    public void setProdID(Integer prodID) {
        this.prodID = prodID;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Integer unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }


    public String getSMonth() {
        return sMonth;
    }

    public void setSMonth(String sMonth) {
        this.sMonth = sMonth;
    }

    public String getSYear() {
        return sYear;
    }

    public void setSYear(String sYear) {
        this.sYear = sYear;
    }
}
