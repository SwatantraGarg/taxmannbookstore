package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.time.Year;
import java.util.List;

public class OrderData {
    @SerializedName("myOrder")
    @Expose
    private List<MyOrder> myOrder = null;
    @SerializedName("years")
    @Expose
    private List<Years> years = null;
    @SerializedName("shippingAddress")
    @Expose
    private Object shippingAddress;

    public List<MyOrder> getMyOrder() {
        return myOrder;
    }

    public void setMyOrder(List<MyOrder> myOrder) {
        this.myOrder = myOrder;
    }

    public List<Years> getYears() {
        return years;
    }

    public void setYears(List<Years> years) {
        this.years = years;
    }

    public Object getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Object shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

}
