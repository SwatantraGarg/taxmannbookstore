package com.planet.Taxmann.model;

import com.google.gson.annotations.SerializedName;

public class VerifyOtp {
    @SerializedName("mobile")
    private String mobile;

    @SerializedName("OTP")
    private String OTP;

    @SerializedName("countrycode")
    private String countrycode;

    @SerializedName("emailID")
    private String email;

    @SerializedName("oldmobile")
    private String oldmobile;

    public String getOldmobile() {
        return oldmobile;
    }

    public void setOldmobile(String oldmobile) {
        this.oldmobile = oldmobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOTP() {
        return OTP;
    }

    public void setOTP(String OTP) {
        this.OTP = OTP;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }
}
