package com.planet.Taxmann.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.planet.Taxmann.R;
import com.planet.Taxmann.activity.BookDetailsActivity;
import com.planet.Taxmann.model.AboutAuthor;

import java.util.List;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.planet.Taxmann.activity.BookDetailsActivity.makeTextViewResizable;

public class AuthorAdapter extends RecyclerView.Adapter<AuthorAdapter.ViewHolder> {
    List<AboutAuthor> aboutAuthorList;
    Context context;

    public AuthorAdapter(List<AboutAuthor> aboutAuthorList, Context context) {
        this.aboutAuthorList = aboutAuthorList;
        this.context = context;
    }
    @NonNull
    @Override
    public AuthorAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AuthorAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.author_item,null));
    }

    @Override
    public void onBindViewHolder(@NonNull AuthorAdapter.ViewHolder holder, int position) {
        holder.authorName.setText(aboutAuthorList.get(position).getAuthorName());
        holder.aboutAuthorText.setText(Html.fromHtml(aboutAuthorList.get(position).getAboutAuthor()));
        if(holder.aboutAuthorText.getText().toString().isEmpty()){

        }
        else {
            makeTextViewResizable(holder.aboutAuthorText, 10, "\n"+"View More", true);
        }

        Glide.with(context).load(aboutAuthorList.get(position).getAuthorPic()).placeholder(R.drawable.noimage).into(holder.authorImg);

    }

    @Override
    public int getItemCount() {
        return aboutAuthorList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.about_author_text)
        TextView aboutAuthorText;

        @BindView(R.id.author_nname)
        TextView authorName;

        @BindView(R.id.author_img)
        ImageView authorImg;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
