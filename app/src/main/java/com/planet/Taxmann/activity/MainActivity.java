package com.planet.Taxmann.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.cursoradapter.widget.CursorAdapter;
import androidx.cursoradapter.widget.SimpleCursorAdapter;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.BaseColumns;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.Fragments.CategoryFragment;
import com.planet.Taxmann.Fragments.HomeFragment;
import com.planet.Taxmann.Fragments.NotificationFragment;
import com.planet.Taxmann.Fragments.ProfileFragment;
import com.planet.Taxmann.Fragments.ViewAllFragment;
import com.planet.Taxmann.Fragments.WishListFragment;
import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.AutoSearchCustomAdapter;
import com.planet.Taxmann.adapters.CartAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.AggregationsBySubject;
import com.planet.Taxmann.model.AggregationsBySubjectResponse;
import com.planet.Taxmann.model.BookData;
import com.planet.Taxmann.model.BookDataResponse;
import com.planet.Taxmann.model.BookType;
import com.planet.Taxmann.model.CartData;
import com.planet.Taxmann.model.CartList;
import com.planet.Taxmann.model.CategorySubjectName;
import com.planet.Taxmann.model.CategorySubjectNameResponse;
import com.planet.Taxmann.model.Datum;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.GetCartResponse;

import com.planet.Taxmann.model.PostProductIndex;
import com.planet.Taxmann.model.SuggestionResponse;
import com.planet.Taxmann.model.Suggetions;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements TextView.OnEditorActionListener {
    @BindView(R.id.menu_btn)
    ImageView menuBtn;

    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.cross_btn)
    ImageView crossBtn;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_search_id)
    AutoCompleteTextView editSearch;

    @BindView(R.id.left_drawer)
    ListView leftDrawer;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.container)
    FrameLayout container;

    UserData userData;
    MenuItem item;
    boolean itemClicked = false;
    List<Suggetions> data = new ArrayList<>();
    List<BookData> featureBookDataList= new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        isNetworkConnectionAvailable();

        userData = DroidPrefs.get(MainActivity.this, "user_data", UserData.class);

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomNavigationView.setSelectedItemId(R.id.home);
            }
        });

        if(!bottomNavigationView.isShown()){
         bottomNavigationView.setVisibility(View.VISIBLE);
        }

        String token = userData.getUser_token();
        if(token!=null &&!token.equalsIgnoreCase("")){
            getcartDetailsResponse(token);
            getWishListResponse(token);
        }

        menuBtn.setVisibility(View.VISIBLE);
        menuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // showHideDrawer();
            }
        });
       // BadgeDrawable badgeDrawable=bottomNavigationView.showBadge(R.id.notification);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        editSearch.setDropDownBackgroundResource(R.color.white_color);
        editSearch.setOnEditorActionListener(this);
        editSearch.setThreshold(1);

        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (itemClicked) {
                   itemClicked= false;

                } else {
                    if (s.length() > 2) {
                        getBookStoreSuggestionResponse(s.toString());
                    }
                }

            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });



        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem){
               bottomNavigationView.getMenu().findItem(R.id.categories).setIcon(R.drawable.ic_category_unselected);
                bottomNavigationView.getMenu().findItem(R.id.home).setIcon(R.drawable.ic_home_unselected);
               bottomNavigationView.getMenu().findItem(R.id.notification).setIcon(R.drawable.ic_wishlist_unselected);
                bottomNavigationView.getMenu().findItem(R.id.cart).setIcon(R.drawable.ic_cart_unselected);
                bottomNavigationView.getMenu().findItem(R.id.profile).setIcon(R.drawable.ic_profile_unselected);
                if (menuItem.getItemId() == R.id.categories) {
                    hideSearchBar();
                    menuItem.setIcon(R.drawable.ic_category_selected);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, new CategoryFragment()).commit();
                    return true;
                }
                else if (menuItem.getItemId() == R.id.home){
                    hideSearchBar();
                    menuItem.setIcon(R.drawable.ic_home_selected);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, new HomeFragment()).commit();
                    return true;
                }

                else if (menuItem.getItemId() == R.id.profile) {
                    hideSearchBar();

                    menuItem.setIcon(R.drawable.ic_profile_selected);
                    if(userData.getUser_token()!=null && !userData.getUser_token().equalsIgnoreCase("")){
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, new ProfileFragment()).commit();
                    }
                    else {
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    return true;
                }
                else if (menuItem.getItemId() == R.id.notification) {
                    hideSearchBar();
                    menuItem.setIcon(R.drawable.ic_wishlist_selected);
                    if(userData.getUser_token()!=null && !userData.getUser_token().equalsIgnoreCase("")){
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, new WishListFragment()).commit();
                    }
                    else {
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    return true;

                } else if (menuItem.getItemId() == R.id.cart) {
                    if(userData.getUser_token()!=null && !userData.getUser_token().equalsIgnoreCase("")){
                        Intent intent = new Intent(MainActivity.this, CartActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                    }
                    else {
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();

                    }
                    return true;

                }
                return false;
            }
        });



        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new HomeFragment()).commit();
        }
       /* Button crashButton = new Button(this);
        crashButton.setText("Crash!");
        crashButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                throw new RuntimeException("Test Crash"); // Force a crash
            }
        });

        addContentView(crashButton, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        */
        crossBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              hideSearchBar();
            }
        });
    }
    private int totalBackPressed = 0;
    private Long timeIntervalBackPressed = 0L;
    @Override
    public void onBackPressed() {
        bottomNavigationView.setVisibility(View.VISIBLE);
        FragmentManager fm = this.getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.container);
        int totalOpenedFragments = getSupportFragmentManager().getBackStackEntryCount();
        Log.v("Total  open fragments ", totalOpenedFragments + "");

    if(totalOpenedFragments<1){
        bottomNavigationView.setSelectedItemId(R.id.home);
        Long currentTime = System.currentTimeMillis()  ;
        Log.v("curnt", currentTime +"");
        Log.v("pre", timeIntervalBackPressed +"");
        if(totalBackPressed == 0 ){
            Toast.makeText(this, "Press Back Button Twice to Exit", Toast.LENGTH_SHORT).show();
            timeIntervalBackPressed = System.currentTimeMillis();
            totalBackPressed++;
        }else if(totalBackPressed == 1 && timeIntervalBackPressed + 2000 >= currentTime){
            finish();
            System.exit(0);
        }else {
            timeIntervalBackPressed = System.currentTimeMillis();
            totalBackPressed =0;
        }
}
    else {
        super.onBackPressed();
    }
        fm.popBackStack();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1){
            bottomNavigationView.getMenu().findItem(R.id.home).setIcon(R.drawable.ic_home_selected);
            bottomNavigationView.setSelectedItemId(R.id.home);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isNetworkConnectionAvailable();
        String token = userData.getUser_token();
        if(token!=null &&!token.equalsIgnoreCase("")){
            getcartDetailsResponse(token);
            getWishListResponse(token);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_item, menu);

         item = menu.findItem(R.id.action_search);

        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                item.setVisible(false);
                editSearch.setVisibility(View.VISIBLE);
                editSearch.setEnabled(true);
                editSearch.setCursorVisible(true);
                crossBtn.setVisibility(View.VISIBLE);
                logo.setVisibility(View.GONE);
                editSearch.requestFocus();
                Constants.ShowKeyboard(MainActivity.this,editSearch);

                return false;
            }
        });

        //searchView.setMenuItem(item);

        return true;
    }

    private void showHideDrawer() {
        Boolean mSlideState = false;//is Closed
        if (mSlideState) {
            drawerLayout.closeDrawer(Gravity.LEFT);
            mSlideState = false;

        } else {
            //leftDrawer.setVisibility(View.VISIBLE);
            drawerLayout.openDrawer(Gravity.LEFT);
//            getSupportActionBar().show();
            // Utils.hideSoftKeyboard(this);
            mSlideState = true;
        }
        // MySharedPrefs.INSTANCE.putPrevSearchString(Constants.PREV_SEARCH+"@");

    }

    public void getBookStoreSuggestionResponse( String search_string) {
        RetrofitEasyApi apiSerivce = ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<SuggestionResponse> call = apiSerivce.getBookStoreSuggestionResponse(search_string);
        call.enqueue(new Callback<SuggestionResponse>() {
            @Override
            public void onResponse(Call<SuggestionResponse> call, Response<SuggestionResponse> response) {
                int status_code = response.code();
               SuggestionResponse suggestionResponse = response.body();
                if (suggestionResponse != null) {
                    data = suggestionResponse.getData();
                    ArrayList<String> filterdata = new ArrayList<>();
                    if (data!=null && data.size()>0) {
                        String suggetion = "";
                        for (int i = 0; i < data.size(); i++) {
                            if(data.get(i).getId()>0){
                                if(data.get(i).getAuthorName()!=null && !data.get(i).getAuthorName().equalsIgnoreCase("")){

                                   suggetion = data.get(i).getTitle()+" by "+"<font color=\"red\">"+"("+data.get(i).getAuthorName()+")"+"</font>";
                                }
                                else {
                                    suggetion = data.get(i).getTitle();
                                }
                            }else {
                                    suggetion = data.get(i).getCategoryName()+data.get(i).getTitle();

                            }

                            filterdata.add(suggetion);
                        }
                        if(filterdata!=null) {
                           AutoSearchCustomAdapter adapter = new AutoSearchCustomAdapter(MainActivity.this,filterdata);
                           editSearch.setAdapter(adapter);
                           editSearch.showDropDown();
                           //Constants.hideSoftKeyboard(MainActivity.this);
                            editSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    editSearch.setText(search_string);
                                    editSearch.setSelection(editSearch.length());
                                    if(editSearch.isPerformingCompletion()){
                                        editSearch.dismissDropDown();
                                    }
                                    itemClicked=true;
if(data!=null){
    if(data.get(position).getId()==0){
        getSubjectIndexListResponse(data.get(position).getCategoryId(), data.get(position).getSubjectId(), data.get(position).getSubjectName());
    }
    else {
        if(data.get(position).getId()!=null){
            Intent intent =new Intent(MainActivity.this,BookDetailsActivity.class);
            intent.putExtra("book_id",String.valueOf(data.get(position).getId()));
            startActivity(intent);
        }

    }
}

                                }
                            });


                        }

                    }
                    else {
                        filterdata.add("No results found!!");
                        AutoSearchCustomAdapter adapter = new AutoSearchCustomAdapter(MainActivity.this,filterdata);
                        editSearch.setAdapter(adapter);
                        editSearch.showDropDown();

                    }

                }
            }

            @Override
            public void onFailure(Call<SuggestionResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            editSearch.setText(editSearch.getText().toString());
            editSearch.setSelection(editSearch.length());

                editSearch.dismissDropDown();

            itemClicked=true;
            if(data!=null){
                List<String> Availability_array= new ArrayList<>();
                Availability_array.add("");
                List<String> Format_array= new ArrayList<>();
                Format_array.add("");
                List<String> Category_array= new ArrayList<>();
                Category_array.add("");
                List<String> YearOfPublication_array= new ArrayList<>();
                YearOfPublication_array.add("");
                PostProductIndex postProductIndex = new PostProductIndex();
                postProductIndex.setAvailabilityArray(Availability_array);
                postProductIndex.setFormatArray(Format_array);
                postProductIndex.setYearOfPublicationArray(YearOfPublication_array);
                postProductIndex.setCategoryArray(Category_array);
                postProductIndex.setFilterFrom("");
                postProductIndex.setPage(1);
                postProductIndex.setPagesize(200);
                postProductIndex.setSearchString(editSearch.getText().toString());
                postProductIndex.setSortBy("sortrelavence");
                DroidPrefs.apply(MainActivity.this,"post_product",postProductIndex);
                getProductSearchIndexResponse(postProductIndex);

                }
            }


        return false;
    }

    public void getcartDetailsResponse(String token){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("cartSource","C");

        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GetCartResponse> call= apiService.getCartDetailsResponse(res,token);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(MainActivity.this,"Please wait");
        call.enqueue(new Callback<GetCartResponse>() {
            @Override
            public void onResponse(Call<GetCartResponse> call, Response<GetCartResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                GetCartResponse getCartResponse=response.body();
                if(getCartResponse!=null){
                    if(getCartResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                        if(getCartResponse.getData()!=null && !getCartResponse.getData().contains("")){

                            List<CartData> cartData = getCartResponse.getData();
                            List<CartList> cartList = new ArrayList<>();
                            for (int i = 0; i <cartData.size() ; i++) {
                                cartList=cartData.get(i).getCartList();
                            }
                            BadgeDrawable badgeDrawable=bottomNavigationView.showBadge(R.id.cart);

                            if(cartList!=null && cartList.size()>0){
                                badgeDrawable.setVisible(true,true);
                                badgeDrawable.setNumber(cartList.size());
                            }
                            else {
                                bottomNavigationView.removeBadge(R.id.cart);
                                badgeDrawable.setVisible(false,false);

                            }

                        }
                    }
                    else {
                        Toast.makeText(MainActivity.this, getCartResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                    }
                }


            }
            @Override
            public void onFailure(Call<GetCartResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                bottomNavigationView.removeBadge(R.id.cart);
               // badgeDrawable.setVisible(false,false);

                // Toast.makeText(CartActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void getSubjectIndexListResponse(String cat_id,String subject_id, String subject_name){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<AggregationsBySubjectResponse> call= apiSerivce.getSubjectIndexListResponse(cat_id);
        call.enqueue(new Callback<AggregationsBySubjectResponse>() {
            @Override
            public void onResponse(Call<AggregationsBySubjectResponse> call, Response<AggregationsBySubjectResponse> response) {
                int status_code= response.code();
                AggregationsBySubjectResponse aggregationsBySubjectResponse= response.body();
                if(aggregationsBySubjectResponse!=null){

                    List<Datum> datumList=aggregationsBySubjectResponse.getData();
                    List<AggregationsBySubject> aggregationsBySubjectList = new ArrayList<>();
                    for (int i = 0; i <datumList.size() ; i++) {
                        aggregationsBySubjectList=datumList.get(i).getAggregationsBySubject();
                        getCategorySubjectNameResponse(cat_id,subject_id,aggregationsBySubjectList.get(i).getDocName());

                    }

                }
            }

            @Override
            public void onFailure(Call<AggregationsBySubjectResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getCategorySubjectNameResponse(String cat_id, String subject_id, String subject_name){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("subjectId",subject_id);
            jsonObject.put("subjectName",subject_name);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        Call<CategorySubjectNameResponse> call= apiSerivce.getCategorySubjectNameResponse(res);
        call.enqueue(new Callback<CategorySubjectNameResponse>() {
            @Override
            public void onResponse(Call<CategorySubjectNameResponse> call, Response<CategorySubjectNameResponse> response) {
                int status_code= response.code();
                CategorySubjectNameResponse categorySubjectNameResponse= response.body();
                if(categorySubjectNameResponse!=null){
                    List<String> sub_array= new ArrayList<>();
                    sub_array.add("");
                    List<String> sub_type_array= new ArrayList<>();
                    sub_type_array.add("");
                    List<String> Author_array= new ArrayList<>();
                    Author_array.add("");
                    List<String> YearOfPublication_array= new ArrayList<>();
                    YearOfPublication_array.add("");
                    List<String> Availability_array= new ArrayList<>();
                    Availability_array.add("");
                    List<String> Format_array= new ArrayList<>();
                    Format_array.add("");


                    PostProductIndex postProductIndex = new PostProductIndex();
                    postProductIndex.setAuthorArray(Author_array);
                    postProductIndex.setSubjectTypeArray(sub_type_array);
                    postProductIndex.setFormatArray(Format_array);
                    postProductIndex.setYearOfPublicationArray(YearOfPublication_array);
                    postProductIndex.setAvailabilityArray(Availability_array);
                    postProductIndex.setSubjectArray(sub_array);
                    postProductIndex.setSearchId("0");
                    postProductIndex.setType("sub");
                    postProductIndex.setFilterFrom("");
                    postProductIndex.setPage(1);
                    postProductIndex.setPagesize(200);
                    postProductIndex.setCatId(cat_id);
                    postProductIndex.setSortBy("sortrelavence");
                    postProductIndex.setSearchString(editSearch.getText().toString());

                    DroidPrefs.apply(MainActivity.this,"post_product",postProductIndex);

                    getProductIndexResponse(postProductIndex);

                }
            }

            @Override
            public void onFailure(Call<CategorySubjectNameResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void getProductIndexResponse(PostProductIndex postProductIndex){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(MainActivity.this,"Please wait");
        Call<CategorySubjectNameResponse> call= apiSerivce.getProductIndexListResponse(postProductIndex);
        call.enqueue(new Callback<CategorySubjectNameResponse>() {
            @Override
            public void onResponse(Call<CategorySubjectNameResponse> call, Response<CategorySubjectNameResponse> response) {
                kProgressHUD.dismiss();
                int status_code= response.code();
                CategorySubjectNameResponse categorySubjectNameResponse= response.body();
                if(categorySubjectNameResponse!=null){
                    List<CategorySubjectName> categorySubjectNameList=categorySubjectNameResponse.getData();
                    List<BookData> featuredProductList= new ArrayList<>();
                    List<AggregationsBySubject> aggregationsBySubjectList= new ArrayList<>();
                    if( categorySubjectNameList!=null && categorySubjectNameList.size()>0){
                        for (int i = 0; i <categorySubjectNameList.size() ; i++) {
                            featuredProductList=categorySubjectNameList.get(i).getFeaturedProduct();
                            aggregationsBySubjectList=categorySubjectNameList.get(i).getAggregationsBySubject();

                        }
                    }

                    Bundle bundle = new Bundle();
                    bundle.putString("type","");
                    bundle.putParcelableArrayList("book_list", (ArrayList<? extends Parcelable>) categorySubjectNameList);
                    ViewAllFragment viewAllFragment = new ViewAllFragment();
                    viewAllFragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().addToBackStack(null)
                            .replace(R.id.container, viewAllFragment).commit();

                }
                else {

                    kProgressHUD.dismiss();
                    Toast.makeText(MainActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CategorySubjectNameResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(MainActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getProductSearchIndexResponse(PostProductIndex postProductIndex){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(MainActivity.this,"Please wait");
        Call<CategorySubjectNameResponse> call= apiSerivce.getProductSearchFromIndex(postProductIndex);
        call.enqueue(new Callback<CategorySubjectNameResponse>() {
            @Override
            public void onResponse(Call<CategorySubjectNameResponse> call, Response<CategorySubjectNameResponse> response) {
                kProgressHUD.dismiss();
                editSearch.dismissDropDown();
                int status_code= response.code();
                CategorySubjectNameResponse categorySubjectNameResponse= response.body();
                if(categorySubjectNameResponse!=null){
                    List<CategorySubjectName> categorySubjectNameList=categorySubjectNameResponse.getData();
                    List<BookData> featuredProductList= new ArrayList<>();
                    List<AggregationsBySubject> aggregationsBySubjectList= new ArrayList<>();
                    if( categorySubjectNameList!=null && categorySubjectNameList.size()>0){
                        for (int i = 0; i <categorySubjectNameList.size() ; i++) {
                            featuredProductList=categorySubjectNameList.get(i).getFeaturedProduct();
                            aggregationsBySubjectList=categorySubjectNameList.get(i).getAggregationsBySubject();

                        }
                    }

                    Bundle bundle = new Bundle();
                    bundle.putString("type","");
                    bundle.putString("id","action");
                    bundle.putParcelableArrayList("book_list", (ArrayList<? extends Parcelable>) categorySubjectNameList);
                    ViewAllFragment viewAllFragment = new ViewAllFragment();
                    viewAllFragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().addToBackStack(null)
                            .replace(R.id.container, viewAllFragment).commit();

                }
                else {

                    kProgressHUD.dismiss();
                    Toast.makeText(MainActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CategorySubjectNameResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(MainActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getWishListResponse(String token){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<BookDataResponse> call= apiService.getWishListResponse(token);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(MainActivity.this,"Please wait");
        call.enqueue(new Callback<BookDataResponse>() {
            @Override
            public void onResponse(Call<BookDataResponse> call, Response<BookDataResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(MainActivity.this);
                int statusCode = response.code();
                BookDataResponse bookDataResponse = response.body();
                if(bookDataResponse!=null) {
                    List<BookType> bookTypeList = bookDataResponse.getData();
                    if(bookTypeList!=null && bookTypeList.size()>0){
                        for (int i = 0; i <bookTypeList.size() ; i++) {
                            featureBookDataList =bookTypeList.get(i).getFeaturedProduct();
                        }


                    }

                }

            }


            @Override
            public void onFailure(Call<BookDataResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                bottomNavigationView.removeBadge(R.id.notification);
               // Toast.makeText(MainActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    public void checkNetworkConnection(){
        AlertDialog.Builder builder =new AlertDialog.Builder(this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public boolean isNetworkConnectionAvailable(){
        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if(isConnected) {
            Log.d("Network", "Connected");
            return true;
        }
        else{
            checkNetworkConnection();
            Log.d("Network","Not Connected");
            return false;
        }
}
public void hideSearchBar(){
    editSearch.setText("");
    item.setVisible(true);
    editSearch.setVisibility(View.GONE);

    if (editSearch != null) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editSearch.getWindowToken(), 0);
    }
    crossBtn.setVisibility(View.GONE);
    logo.setVisibility(View.VISIBLE);
}
}

