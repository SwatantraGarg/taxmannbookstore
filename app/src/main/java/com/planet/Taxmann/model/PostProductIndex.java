package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostProductIndex {
    @SerializedName("categoryArray")
    @Expose
    private List<String> categoryArray = null;

    @SerializedName("authorArray")
    @Expose
    private List<String> authorArray = null;
    @SerializedName("subjectArray")
    @Expose
    private List<String> subjectArray = null;
    @SerializedName("subjectTypeArray")
    @Expose
    private List<String> subjectTypeArray = null;
    @SerializedName("formatArray")
    @Expose
    private List<String> formatArray = null;
    @SerializedName("yearOfPublicationArray")
    @Expose
    private List<String> yearOfPublicationArray = null;
    @SerializedName("availabilityArray")
    @Expose
    private List<String> availabilityArray = null;
    @SerializedName("searchId")
    @Expose
    private String searchId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("filterFrom")
    @Expose
    private String filterFrom;
    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("pagesize")
    @Expose
    private Integer pagesize;
    @SerializedName("catId")
    @Expose
    private String catId;
    @SerializedName("sortBy")
    @Expose
    private String sortBy;
    @SerializedName("searchString")
    @Expose
    private String searchString;

    public List<String> getAuthorArray() {
        return authorArray;
    }

    public void setAuthorArray(List<String> authorArray) {
        this.authorArray = authorArray;
    }

    public List<String> getSubjectArray() {
        return subjectArray;
    }

    public void setSubjectArray(List<String> subjectArray) {
        this.subjectArray = subjectArray;
    }

    public List<String> getSubjectTypeArray() {
        return subjectTypeArray;
    }

    public void setSubjectTypeArray(List<String> subjectTypeArray) {
        this.subjectTypeArray = subjectTypeArray;
    }

    public List<String> getFormatArray() {
        return formatArray;
    }

    public void setFormatArray(List<String> formatArray) {
        this.formatArray = formatArray;
    }

    public List<String> getYearOfPublicationArray() {
        return yearOfPublicationArray;
    }

    public void setYearOfPublicationArray(List<String> yearOfPublicationArray) {
        this.yearOfPublicationArray = yearOfPublicationArray;
    }

    public List<String> getAvailabilityArray() {
        return availabilityArray;
    }

    public void setAvailabilityArray(List<String> availabilityArray) {
        this.availabilityArray = availabilityArray;
    }

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFilterFrom() {
        return filterFrom;
    }

    public void setFilterFrom(String filterFrom) {
        this.filterFrom = filterFrom;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPagesize() {
        return pagesize;
    }

    public void setPagesize(Integer pagesize) {
        this.pagesize = pagesize;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }
    public List<String> getCategoryArray() {
        return categoryArray;
    }

    public void setCategoryArray(List<String> categoryArray) {
        this.categoryArray = categoryArray;
    }
}
