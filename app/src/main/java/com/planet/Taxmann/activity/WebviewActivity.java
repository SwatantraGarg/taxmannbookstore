package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;

public class WebviewActivity extends AppCompatActivity {
    @BindView(R.id.web_view)
    WebView webView;
    String url;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        ButterKnife.bind(this);
        intent = getIntent();
        if(intent!=null){
            url = intent.getStringExtra("url");
        }
        startWebView(url);
    }
    // Open previous opened link from history on webview when back button pressed

    @Override
    // Detect when the back button is pressed
    public void onBackPressed() {
        if(webView.canGoBack()) {
            webView.goBack();
        } else {
            // Let the system handle the back button
            super.onBackPressed();
        }
    }
    private void startWebView(String url) {

        //Create new webview Client to show progress dialog
        //When opening a url or click on link

        webView.setWebViewClient(new WebViewClient() {
            KProgressHUD kProgressHUD;

            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            public void onLoadResource (WebView view, String url) {
                if (kProgressHUD == null) {
                    // in standard case YourActivity.this
                    kProgressHUD = new KProgressHUD(WebviewActivity.this);
                    kProgressHUD.setDetailsLabel("Please wait...");
                    kProgressHUD.show();
                }
            }
            public void onPageFinished(WebView view, String url) {
                try{
                    if (kProgressHUD.isShowing()) {
                        kProgressHUD.dismiss();
                       // kProgressHUD = null;
                    }
                }catch(Exception exception){
                    exception.printStackTrace();
                }
            }

        });

        // Javascript inabled on webview
        webView.getSettings().setJavaScriptEnabled(true);

        // Other webview options
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
       webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
    webView.getSettings().setDomStorageEnabled(true);
       // webView.getSettings().setBuiltInZoomControls(true);

        /*
         String summary = "<html><body>You scored <b>192</b> points.</body></html>";
         webview.loadData(summary, "text/html", null);
         */

        //Load url in webview
        webView.loadUrl(url);


    }
}
