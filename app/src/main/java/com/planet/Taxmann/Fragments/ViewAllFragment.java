package com.planet.Taxmann.Fragments;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.activity.BookDetailsActivity;
import com.planet.Taxmann.activity.FilterActivity;
import com.planet.Taxmann.activity.MainActivity;
import com.planet.Taxmann.activity.ViewAllBooksActivity;
import com.planet.Taxmann.adapters.AuthorAdapter;
import com.planet.Taxmann.adapters.BookAdapter;
import com.planet.Taxmann.adapters.FilterCategoryAdapter;
import com.planet.Taxmann.adapters.FilterSubCategoryAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.AggregationsBySubject;
import com.planet.Taxmann.model.AggregationsBySubjectResponse;
import com.planet.Taxmann.model.BookData;
import com.planet.Taxmann.model.BookDetails;
import com.planet.Taxmann.model.Books;
import com.planet.Taxmann.model.CategorySubjectName;
import com.planet.Taxmann.model.CategorySubjectNameResponse;
import com.planet.Taxmann.model.Datum;
import com.planet.Taxmann.model.FilterCategory;
import com.planet.Taxmann.model.GetBookDetailsResponse;
import com.planet.Taxmann.model.PostProductIndex;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAllFragment extends Fragment {
    @BindView(R.id.view_all_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.sort)
    TextView sortBtn;

    @BindView(R.id.filter_sort_layout)
    LinearLayout filterSortLayout;

    @BindView(R.id.filter)
    TextView filterBtn;
    @BindView(R.id.records)
    TextView recordCount;
    List<BookData> bookDataList= new ArrayList<>();
    BookAdapter bookAdapter;
    List<CategorySubjectName> categorySubjectNameList;
    String type,type1,id;
    PostProductIndex postProductIndex;
    MainActivity mainActivity;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mainActivity = (MainActivity)context;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categorySubjectNameList = getArguments().getParcelableArrayList("book_list");
            type = getArguments().getString("type");
            id = getArguments().getString("id");
            if(type!=null && type.equalsIgnoreCase("category")){
                PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putString("sort", "sortpublicationdate").apply();
            }
            else {
                PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putString("sort", "sortrelavence").apply();
            }

            if(categorySubjectNameList!=null && categorySubjectNameList.size()>0){
                for (int i = 0; i <categorySubjectNameList.size() ; i++) {
                    bookDataList = categorySubjectNameList.get(i).getFeaturedProduct();
                }
            }
        }
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_all_fragment, null);
        ButterKnife.bind(this, view);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) getActivity().findViewById(R.id.bottom_navigation);

        String count= "Records : "+"<b> "+bookDataList.size()+" </b>";
        recordCount.setText(Html.fromHtml(count));

        sortBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortDialog();
            }
        });

        filterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent intent = new Intent(getContext(), FilterActivity.class);
              intent.putParcelableArrayListExtra("filter_list", (ArrayList<? extends Parcelable>) categorySubjectNameList);
              intent.putExtra("id",id);
            if(type!=null && type.equalsIgnoreCase("category")){
                intent.putExtra("type","category");
            }
            else {
                intent.putExtra("type","");
            }
              if(type1!=null && type1.equalsIgnoreCase("filter")) {
                  intent.putExtra("type","filter");
                  if(type!=null && type.equalsIgnoreCase("category")){
                      intent.putExtra("type2","category");
                  }
                  else {
                      intent.putExtra("type2","");
                  }

                }
              else {
            /*      if(type!=null && type.equalsIgnoreCase("category")){
                      intent.putExtra("type","category");
                  }
                  else {
                      intent.putExtra("type","");
                  }*/
              }

              startActivityForResult(intent,2);
                getActivity().overridePendingTransition(0,0);
            }

        });

        if(bookDataList!=null){
            bookAdapter = new BookAdapter(getContext(),bookDataList, new BookAdapter.BookClicked() {
                @Override
                public void Clicked(View view, int position) {
                    Intent intent =new Intent(getContext(), BookDetailsActivity.class);
                    intent.putExtra("book_id",bookDataList.get(position).getMainProdId());
                    startActivity(intent);
                }

            });
            GridLayoutManager gridLayoutManager=new GridLayoutManager(getContext(),2);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(bookAdapter);
            bookAdapter.notifyDataSetChanged();
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 && bottomNavigationView.isShown()) {
                    bottomNavigationView.setVisibility(View.GONE);
                    ObjectAnimator animation = ObjectAnimator.ofFloat(filterSortLayout, "translationY", 100f);
                    animation.setDuration(100);
                    animation.start();
                } else if (dy < 0 ) {
                    bottomNavigationView.setVisibility(View.VISIBLE);
                    ObjectAnimator animation = ObjectAnimator.ofFloat(filterSortLayout, "translationY", 0f);
                    animation.setDuration(0);
                    animation.start();
                }
            }
        });
        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==2){
            type1 = data.getStringExtra("type1");
                if(type1!=null && type1.equalsIgnoreCase("reset")){
                    bookAdapter = new BookAdapter(getContext(),bookDataList, new BookAdapter.BookClicked() {
                        @Override
                        public void Clicked(View view, int position) {
                            Intent intent =new Intent(getContext(), BookDetailsActivity.class);
                            intent.putExtra("book_id",bookDataList.get(position).getMainProdId());
                            startActivity(intent);
                        }

                    });
                    GridLayoutManager gridLayoutManager=new GridLayoutManager(getContext(),2);
                    recyclerView.setLayoutManager(gridLayoutManager);
                    recyclerView.setAdapter(bookAdapter);
                    bookAdapter.notifyDataSetChanged();
                    String count= "Records : "+"<b> "+bookDataList.size()+" </b>";
                    recordCount.setText(Html.fromHtml(count));
                }
                else {
                    PostProductIndex postProductIndex = DroidPrefs.get(getContext(),"post_filter",PostProductIndex.class);
                    getProductIndexResponse(postProductIndex);
                }
        }
        else {
            PostProductIndex postProductIndex = DroidPrefs.get(getContext(),"post_filter",PostProductIndex.class);
            getCatProductIndexResponse(postProductIndex);
        }

    }


    public void sortDialog() {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getContext());
        View v = getLayoutInflater().inflate(R.layout.sort_layout, null);
        ImageView img_cross = v.findViewById(R.id.img_cross);
        RadioGroup radioGroup = v.findViewById(R.id.radio_group);
        RadioButton radioButton1 = v.findViewById(R.id.radio1);
        RadioButton radioButton2 = v.findViewById(R.id.radio2);
        RadioButton radioButton3 = v.findViewById(R.id.radio3);
        RadioButton radioButton4 = v.findViewById(R.id.radio4);


        if (type!= null && type.equalsIgnoreCase("category")) {

            postProductIndex = DroidPrefs.get(getContext(), "cat_post_index", PostProductIndex.class);
            postProductIndex.setFilterFrom("");
            postProductIndex.setType("");
            radioButton4.setVisibility(View.GONE);


        } else {
           // PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putString("sort", "sortrelavence").apply();
            postProductIndex = DroidPrefs.get(getContext(), "post_product", PostProductIndex.class);
            postProductIndex.setFilterFrom("subjecttype");
            postProductIndex.setType("");
            radioButton4.setVisibility(View.VISIBLE);
        }
        if (type1 != null && type1.equalsIgnoreCase("filter")) {
            postProductIndex = DroidPrefs.get(getContext(), "post_filter", PostProductIndex.class);
            postProductIndex.setFilterFrom("");
            postProductIndex.setType("");

        } else {
        /*    if (type != null && type.equalsIgnoreCase("category")) {
                postProductIndex = DroidPrefs.get(getContext(), "cat_post_index", PostProductIndex.class);
                postProductIndex.setFilterFrom("");
                postProductIndex.setType("");
                radioButton4.setVisibility(View.GONE);
            }
            else {
                postProductIndex = DroidPrefs.get(getContext(), "post_product", PostProductIndex.class);
                postProductIndex.setFilterFrom("subjecttype");
                postProductIndex.setType("");
                radioButton4.setVisibility(View.VISIBLE);
            }*/
        }

       String sort =PreferenceManager.getDefaultSharedPreferences(getContext()).getString("sort", "sortpublicationdate");
        if(sort!=null && !sort.equalsIgnoreCase("")){
            if(sort.equalsIgnoreCase("sortpublicationdate")){
                radioButton1.setChecked(true);
                postProductIndex.setSortBy("sortpublicationdate");
                getProductIndexResponse(postProductIndex);
            }else if(sort.equalsIgnoreCase("sortlowtohigh")){
                radioButton2.setChecked(true);
                postProductIndex.setSortBy("sortlowtohigh");
                getProductIndexResponse(postProductIndex);
            }else if(sort.equalsIgnoreCase("sorthightolow")){
                radioButton3.setChecked(true);
                postProductIndex.setSortBy("sorthightolow");
                getProductIndexResponse(postProductIndex);
            }else {
                radioButton4.setChecked(true);
                postProductIndex.setSortBy("sortrelavence");
                getProductIndexResponse(postProductIndex);

            }


        }
      radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                bottomSheetDialog.dismiss();
                switch(checkedId){
                    case R.id.radio1:
                        postProductIndex.setSortBy("sortpublicationdate");
                        getProductIndexResponse(postProductIndex);
                        break;

                    case R.id.radio2:
                        radioButton2.setChecked(true);
                        postProductIndex.setSortBy("sortlowtohigh");
                        getProductIndexResponse(postProductIndex);
                        break;

                    case R.id.radio3:
                        postProductIndex.setSortBy("sorthightolow");
                        getProductIndexResponse(postProductIndex);
                        break;

                    case R.id.radio4:
                        postProductIndex.setSortBy("sortrelavence");
                        getProductIndexResponse(postProductIndex);
                        break;
                }


            }
        });

        img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();

            }
        });
        bottomSheetDialog.setContentView(v);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) v.getParent());
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();
    }

    public void getProductIndexResponse(PostProductIndex postProductIndex){
       PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putString("sort", postProductIndex.getSortBy()).apply();
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);

        final KProgressHUD kProgressHUD= Constants.ShowProgress(getContext(),"Please wait");
        Call<CategorySubjectNameResponse> call;
        if(id!=null && id.equalsIgnoreCase("action")){
            call = apiSerivce.getProductSearchFromIndex(postProductIndex);
        }else {
            call= apiSerivce.getProductIndexListResponse(postProductIndex);
        }
        call.enqueue(new Callback<CategorySubjectNameResponse>() {
            @Override
            public void onResponse(Call<CategorySubjectNameResponse> call, Response<CategorySubjectNameResponse> response) {
                kProgressHUD.dismiss();
                int status_code= response.code();
                CategorySubjectNameResponse categorySubjectNameResponse= response.body();
                if(categorySubjectNameResponse!=null){
                    List<CategorySubjectName> categorySubjectNameList=categorySubjectNameResponse.getData();
                    if(categorySubjectNameList!=null){
                        for (int i = 0; i <categorySubjectNameList.size() ; i++) {
                           List<BookData> bookDataList = categorySubjectNameList.get(i).getFeaturedProduct();
                            if(bookDataList!=null){
                                String count= "Records : "+"<b> "+bookDataList.size()+" </b>";
                                recordCount.setText(Html.fromHtml(count));
                                bookAdapter = new BookAdapter(getContext(),bookDataList, new BookAdapter.BookClicked() {
                                    @Override
                                    public void Clicked(View view, int position) {
                                        Intent intent =new Intent(getContext(), BookDetailsActivity.class);
                                        intent.putExtra("book_id",bookDataList.get(position).getMainProdId());
                                        startActivity(intent);
                                    }

                                });
                                GridLayoutManager gridLayoutManager=new GridLayoutManager(getContext(),2);
                                recyclerView.setLayoutManager(gridLayoutManager);
                                recyclerView.setAdapter(bookAdapter);
                                bookAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(getContext(), response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CategorySubjectNameResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void getCatProductIndexResponse(PostProductIndex postProductIndex){
        PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putString("sort", postProductIndex.getSortBy()).apply();
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(getContext(),"Please wait");
        Call<CategorySubjectNameResponse> call;
            call= apiSerivce.getProductIndexListResponse(postProductIndex);
        call.enqueue(new Callback<CategorySubjectNameResponse>() {
            @Override
            public void onResponse(Call<CategorySubjectNameResponse> call, Response<CategorySubjectNameResponse> response) {
                kProgressHUD.dismiss();
                int status_code= response.code();
                CategorySubjectNameResponse categorySubjectNameResponse= response.body();
                if(categorySubjectNameResponse!=null){
                    List<CategorySubjectName> categorySubjectNameList=categorySubjectNameResponse.getData();
                    if(categorySubjectNameList!=null){
                        for (int i = 0; i <categorySubjectNameList.size() ; i++) {
                            List<BookData> bookDataList = categorySubjectNameList.get(i).getFeaturedProduct();
                            if(bookDataList!=null){
                                String count= "Records : "+"<b> "+bookDataList.size()+" </b>";
                                recordCount.setText(Html.fromHtml(count));
                                bookAdapter = new BookAdapter(getContext(),bookDataList, new BookAdapter.BookClicked() {
                                    @Override
                                    public void Clicked(View view, int position) {
                                        Intent intent =new Intent(getContext(), BookDetailsActivity.class);
                                        intent.putExtra("book_id",bookDataList.get(position).getMainProdId());
                                        startActivity(intent);
                                    }

                                });
                                GridLayoutManager gridLayoutManager=new GridLayoutManager(getContext(),2);
                                recyclerView.setLayoutManager(gridLayoutManager);
                                recyclerView.setAdapter(bookAdapter);
                                bookAdapter.notifyDataSetChanged();

                            }

                        }
                    }

                }
                else {

                    kProgressHUD.dismiss();
                    Toast.makeText(getContext(), response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CategorySubjectNameResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
