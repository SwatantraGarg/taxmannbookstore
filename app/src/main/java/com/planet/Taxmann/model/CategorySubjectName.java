package com.planet.Taxmann.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategorySubjectName  implements Parcelable {
    @SerializedName("featuredProduct")
    @Expose
    private List<BookData> featuredProduct = null;
    @SerializedName("newreleaseProduct")
    @Expose
    private Object newreleaseProduct;
    @SerializedName("AggregationsBySubject")
    @Expose
    private List<AggregationsBySubject> AggregationsBySubject = null;


    @SerializedName("AggregationsBySubSubject")
    @Expose
    private List<AggregationsBySubject> AggregationsBySubSubject = null;
    @SerializedName("AggregationsByAuthor")
    @Expose
    private List<AggregationsBySubject> AggregationsByAuthor = null;
    @SerializedName("AggregationsByFormat")
    @Expose
    private List<AggregationsBySubject> AggregationsByFormat = null;
    @SerializedName("AggregationsByAvailability")
    @Expose
    private List<AggregationsBySubject> AggregationsByAvailability = null;
    @SerializedName("AggregationsByYearOfPublication")
    @Expose
    private List<AggregationsBySubject> AggregationsByYearOfPublication = null;
    @SerializedName("AggregationsByCategory")
    @Expose
    private List<AggregationsBySubject> AggregationsByCategory = null;

    protected CategorySubjectName(Parcel in) {
        featuredProduct = in.createTypedArrayList(BookData.CREATOR);
    }

    public static final Creator<CategorySubjectName> CREATOR = new Creator<CategorySubjectName>() {
        @Override
        public CategorySubjectName createFromParcel(Parcel in) {
            return new CategorySubjectName(in);
        }

        @Override
        public CategorySubjectName[] newArray(int size) {
            return new CategorySubjectName[size];
        }
    };

    public List<BookData> getFeaturedProduct() {
        return featuredProduct;
    }

    public void setFeaturedProduct(List<BookData> featuredProduct) {
        this.featuredProduct = featuredProduct;
    }

    public Object getNewreleaseProduct() {
        return newreleaseProduct;
    }

    public void setNewreleaseProduct(Object newreleaseProduct) {
        this.newreleaseProduct = newreleaseProduct;
    }

    public List<AggregationsBySubject> getAggregationsBySubject() {
        return AggregationsBySubject;
    }

    public void setAggregationsBySubject(List<AggregationsBySubject> aggregationsBySubject) {
        this.AggregationsBySubject = aggregationsBySubject;
    }

    public List<AggregationsBySubject> getAggregationsBySubSubject() {
        return AggregationsBySubSubject;
    }

    public void setAggregationsBySubSubject(List<AggregationsBySubject> AggregationsBySubSubject) {
        this.AggregationsBySubSubject = AggregationsBySubSubject;
    }

    public List<AggregationsBySubject> getAggregationsByAuthor() {
        return AggregationsByAuthor;
    }

    public void setAggregationsByAuthor(List<AggregationsBySubject> AggregationsByAuthor) {
        this.AggregationsByAuthor = AggregationsByAuthor;
    }

    public List<AggregationsBySubject> getAggregationsByFormat() {
        return AggregationsByFormat;
    }

    public void setAggregationsByFormat(List<AggregationsBySubject> AggregationsByFormat) {
        this.AggregationsByFormat = AggregationsByFormat;
    }

    public List<AggregationsBySubject> getAggregationsByAvailability() {
        return AggregationsByAvailability;
    }

    public void setAggregationsByAvailability(List<AggregationsBySubject> AggregationsByAvailability) {
        this.AggregationsByAvailability = AggregationsByAvailability;
    }

    public List<AggregationsBySubject> getAggregationsByYearOfPublication() {
        return AggregationsByYearOfPublication;
    }

    public void setAggregationsByYearOfPublication(List<AggregationsBySubject> AggregationsByYearOfPublication) {
        this.AggregationsByYearOfPublication = AggregationsByYearOfPublication;
    }

    public List<AggregationsBySubject> getAggregationsByCategory() {
        return AggregationsByCategory;
    }

    public void setAggregationsByCategory(List<AggregationsBySubject> AggregationsByCategory) {
        this.AggregationsByCategory = AggregationsByCategory;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(featuredProduct);
    }
}
