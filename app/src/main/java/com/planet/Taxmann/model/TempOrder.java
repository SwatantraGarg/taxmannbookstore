package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TempOrder {

    @SerializedName("OrderNo")
    @Expose
    private String orderNo;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("RazorPayOrderID")
    @Expose
    private String razorPayOrderID;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRazorPayOrderID() {
        return razorPayOrderID;
    }

    public void setRazorPayOrderID(String razorPayOrderID) {
        this.razorPayOrderID = razorPayOrderID;
    }
}
