package com.planet.Taxmann.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.planet.Taxmann.R;
import com.planet.Taxmann.activity.FilterActivity;
import com.planet.Taxmann.interfaces.GetCount;
import com.planet.Taxmann.model.AggregationsBySubject;
import com.planet.Taxmann.model.FilterCategory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterSubCategoryAdapter extends RecyclerView.Adapter<FilterSubCategoryAdapter.ViewHolder>  {
    List<AggregationsBySubject> subCategoryList;
    List<FilterCategory> filterCategoryList;
    Context context;
    TextClick textClick;
    int pos;
     private RadioButton lastCheckedRB = null;
    GetCount getCount;
    public int mSelectedItem = -1;


    public FilterSubCategoryAdapter(int pos,List<FilterCategory> filterCategoryList ,List<AggregationsBySubject> subCategoryList, Context context,TextClick textClick) {
        this.subCategoryList = subCategoryList;
        this.context = context;
        this.pos = pos;
        this.getCount = (FilterActivity)context;
        this.filterCategoryList= filterCategoryList;
        this.textClick = textClick;
    }
    public interface TextClick{
        void clicked(View view, int pos);
    }


    @NonNull
    @Override
    public FilterSubCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FilterSubCategoryAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_sub_item,null));
}

    @Override
    public void onBindViewHolder(@NonNull FilterSubCategoryAdapter.ViewHolder holder, int position) {
        if(filterCategoryList.get(pos).getText().equalsIgnoreCase("Availability")){
            holder.radioButton.setVisibility(View.VISIBLE);
            holder.checkBox.setVisibility(View.GONE);
        }
        else if(filterCategoryList.get(pos).getText().equalsIgnoreCase("Category")){
            holder.radioButton.setVisibility(View.GONE);
            holder.checkBox.setVisibility(View.GONE);
            holder.text.setVisibility(View.VISIBLE);
        }
        else {
            holder.radioButton.setVisibility(View.GONE);
            holder.checkBox.setVisibility(View.VISIBLE);
        }
        holder.radioButton.setChecked(mSelectedItem==position);

    if(subCategoryList!=null && subCategoryList.size()>0){

    if(subCategoryList.get(position).getDocName().contains("|")){
        String s = subCategoryList.get(position).getDocName().trim();
        List<String> word = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(s,"\\|");
        while(st.hasMoreTokens())
            word.add(st.nextToken());
        for (int i = 0; i <word.size() ; i++) {
            holder.text.setText(word.get(1));
        }
    }
    holder.text.setTag(position);
    holder.text.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int pos= (int) view.getTag();
            textClick.clicked(view,pos);
        }
    });

        holder.checkBox.setText(subCategoryList.get(position).getDocName());
        holder.radioButton.setText(subCategoryList.get(position).getDocName());

    if(subCategoryList.get(position).isCHECKED()){
          //  holder.radioButton.setChecked(true);

        if(filterCategoryList.get(pos).getText().equalsIgnoreCase("Availability")){
            holder.radioButton.setChecked(true);
        }else {
            holder.checkBox.setChecked(true);
        }
       // holder.radioButton.setChecked(true);
    }else {
        if(filterCategoryList.get(pos).getText().equalsIgnoreCase("Availability")){
            holder.radioButton.setChecked(false);
        }else {
            holder.checkBox.setChecked(false);
        }
    }
holder.checkBox.setClickable(false);

  /*  holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked){
                subCategoryList.get(position).setCHECKED(true);
            }
            else {
                subCategoryList.get(position).setCHECKED(false);
            }
            int count = 0;
            for (int i = 0; i <subCategoryList.size() ; i++) {
                if(subCategoryList.get(i).isCHECKED()){
                    count++;
                }
            }
            getCount.Count(count,pos);
           // notifyDataSetChanged();
        }


    });
*/
    holder.checkBox.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(holder.checkBox.isChecked()){
                subCategoryList.get(position).setCHECKED(true);
            }
            else {
                subCategoryList.get(position).setCHECKED(false);
            }
            int count = 0;
            for (int i = 0; i <subCategoryList.size() ; i++) {
                if(subCategoryList.get(i).isCHECKED()){
                    count++;
                }
            }
            getCount.Count(count,pos);
        }
    });

//    holder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//        @Override
//        public void onCheckedChanged(RadioGroup group, int checkedId) {
//            RadioButton checked_rb = (RadioButton) group.findViewById(checkedId);
//            checked_rb.setChecked(true);
//            if(lastCheckedRB != null){
//                lastCheckedRB.setChecked(false);
//            }
//            lastCheckedRB = checked_rb;
//
//
//
//
//            }
//
//
//
//
//    });

}
    }

    @Override
    public int getItemCount() {
        return subCategoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cbox)
        CheckBox checkBox;
//        @BindView(R.id.rg)
//        RadioGroup radioGroup;
        @BindView(R.id.radio1)
        RadioButton radioButton;
        @BindView(R.id.text)
        TextView text;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    for (int i = 0; i <subCategoryList.size() ; i++) {
                      if(i==mSelectedItem){
                          subCategoryList.get(i).setCHECKED(true);
                      }else {
                          subCategoryList.get(i).setCHECKED(false);
                      }
                    }
                    notifyDataSetChanged();
                    //subCategoryList.get(mSelectedItem).setCHECKED(true);
                }
            };
            radioButton.setOnClickListener(clickListener);
        }
    }
}
