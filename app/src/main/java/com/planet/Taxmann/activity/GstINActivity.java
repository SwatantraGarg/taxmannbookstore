package com.planet.Taxmann.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.flexbox.FlexboxLayout;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.EntityResponse;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.GstInfo;
import com.planet.Taxmann.model.IndianStateResponse;
import com.planet.Taxmann.model.PersonalInfo;
import com.planet.Taxmann.model.StatesList;
import com.planet.Taxmann.model.USerInfo;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.model.UserInfoResponse;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GstINActivity extends AppCompatActivity {
    @BindView(R.id.entity_name)
    TextView entityName;

    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.entity_edit)
    EditText entityEdit;

    @BindView(R.id.business_name)
    TextView businessName;

    @BindView(R.id.spin_layout)
    RelativeLayout spinLayoutBusiness;

    @BindView(R.id.spin_state_layout)
    RelativeLayout spinStateLayout;

    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.spinner_state)
    Spinner spinnerState;

    @BindView(R.id.gst_in_no)
    TextView gstInNoTxt;

    @BindView(R.id.pan_business_name)
    TextView panBusinessName;

    @BindView(R.id.state_name)
    TextView stateName;

    @BindView(R.id.address)
    TextView address;

    @BindView(R.id.pan_edit)
    EditText panEdit;

    @BindView(R.id.address_edit)
    EditText addressEdit;

    @BindView(R.id.gst_in_edit)
    EditText gstInEdit;

    @BindView(R.id.submit_layout)
    LinearLayout submitLayout;

    @BindView(R.id.submit_btn)
    TextView submitBtn;

    @BindView(R.id.cancel)
    TextView cancelBtn;

    @BindView(R.id.edt_layout)
    LinearLayout editLayout;

    @BindView(R.id.edit_btn)
    TextView editBtn;

    @BindView(R.id.remove)
    TextView removeBtn;
    UserData userData;
    ArrayAdapter<String> dataAdapter;
    ArrayAdapter<String> stateAdapter;
    List<StatesList> statedata;
    Intent intent;
    GstInfo gstInfo;
    public static final String GSTINFORMAT_REGEX = "[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9A-Za-z]{1}[Z]{1}[0-9a-zA-Z]{1}";
    public static final String GSTN_CODEPOINT_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gst_in_layout);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(GstINActivity.this,"user_data",UserData.class);
        getIndianStateResponse();
        getGstInfoResponse();
        getEntityResponse();

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GstINActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entityName.setVisibility(View.GONE);
                businessName.setVisibility(View.GONE);
                gstInNoTxt.setVisibility(View.GONE);
                address.setVisibility(View.GONE);
                panBusinessName.setVisibility(View.GONE);
                stateName.setVisibility(View.GONE);

                entityEdit.setVisibility(View.VISIBLE);
                spinLayoutBusiness.setVisibility(View.VISIBLE);
                spinStateLayout.setVisibility(View.VISIBLE);
                gstInEdit.setVisibility(View.VISIBLE);
                addressEdit.setVisibility(View.VISIBLE);
                panEdit.setVisibility(View.VISIBLE);
                editLayout.setVisibility(View.GONE);
                submitLayout.setVisibility(View.VISIBLE);

                if(gstInfo!=null){
                    entityEdit.setText(gstInfo.getLegalName());
                    gstInEdit.setText(gstInfo.getGSTINofBusiness());
                    addressEdit.setText(gstInfo.getAddress());
                    panEdit.setText(gstInfo.getPANofBusiness());

                    if(dataAdapter!= null){
                        int pos = dataAdapter.getPosition(gstInfo.getTypeOfEntity());
                        spinner.setSelection(pos);
                    }

                    String state_id = gstInfo.getStateid();
                    if(statedata!=null && statedata.size()>0) {
                        int position = 0;
                        for (int i = 0; i < statedata.size(); i++) {
                            if (statedata.get(i).getId().equalsIgnoreCase(state_id)) {
                                position = i;
                            }

                        }
                        spinnerState.setSelection(position);
                    }}
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entityName.setVisibility(View.VISIBLE);
                businessName.setVisibility(View.VISIBLE);
                gstInNoTxt.setVisibility(View.VISIBLE);
                address.setVisibility(View.VISIBLE);
                panBusinessName.setVisibility(View.VISIBLE);
                stateName.setVisibility(View.VISIBLE);

                entityEdit.setVisibility(View.GONE);
                spinLayoutBusiness.setVisibility(View.GONE);
                spinStateLayout.setVisibility(View.GONE);
                gstInEdit.setVisibility(View.GONE);
                addressEdit.setVisibility(View.GONE);
                panEdit.setVisibility(View.GONE);
                editLayout.setVisibility(View.VISIBLE);
                submitLayout.setVisibility(View.GONE);
            }
        });

        removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(gstInfo!=null){
                    deleteGstnDetailsResponse(gstInfo.getRefId());
                }

            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(!validGSTIN(gstInEdit.getText().toString())){
                        Toast.makeText(GstINActivity.this, "Enter valid GSTIN number", Toast.LENGTH_SHORT).show();
                    }
                    else if(entityEdit.getText().toString().isEmpty()){
                    Toast.makeText(GstINActivity.this, "please enter entity name", Toast.LENGTH_SHORT).show();
                }
                    else if(gstInEdit.getText().toString().isEmpty()){
                    Toast.makeText(GstINActivity.this, "please enter GSTIN number", Toast.LENGTH_SHORT).show();
                }
                    else if(panEdit.getText().toString().isEmpty()){
                        Toast.makeText(GstINActivity.this, "please enter PAN of the business", Toast.LENGTH_SHORT).show();
                    }
                    else if(addressEdit.getText().toString().isEmpty()){
                        Toast.makeText(GstINActivity.this, "please enter your address", Toast.LENGTH_SHORT).show();
                    }
                else if(spinner.getSelectedItemPosition()==0){
                        Toast.makeText(GstINActivity.this, "please select type of business", Toast.LENGTH_SHORT).show();
                    }
                    else if(spinnerState.getSelectedItemPosition()==0){
                        Toast.makeText(GstINActivity.this, "please select your state", Toast.LENGTH_SHORT).show();
                    }

                else {
                    String legal_name = entityEdit.getText().toString();
                    String business = String.valueOf(spinner.getSelectedItem());
                    String gst_number = gstInEdit.getText().toString();
                    String name = userData.getName();
                    String pan_no = panEdit.getText().toString();
                    String address = addressEdit.getText().toString();
                    int pos = spinnerState.getSelectedItemPosition();
                        String state_id = null;
                    if(statedata!=null && statedata.size()>0){
                        state_id= statedata.get(pos).getId();

                    }

                   updateGstnDetailsResponse(legal_name,business,gst_number,name,pan_no,address,name,state_id);
                }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void getEntityResponse() {
        UserData userData = DroidPrefs.get(GstINActivity.this, "user_data", UserData.class);
        final RetrofitEasyApi apiService = ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<EntityResponse> call = apiService.getEntityResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD = Constants.ShowProgress(this, "Please wait");
        call.enqueue(new Callback<EntityResponse>() {
            @Override
            public void onResponse(Call<EntityResponse> call, Response<EntityResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(GstINActivity.this);
                int statusCode = response.code();
                EntityResponse entityResponse = response.body();
                if (entityResponse != null) {
                    if(entityResponse.getData()!=null && entityResponse.getData().size()>0){
                        List<String> entities= new ArrayList<>();
                        entities.add("select");
                        for (int i = 0; i <entityResponse.getData().size() ; i++) {
                            String entity= entityResponse.getData().get(i).getName();
                            entities.add(entity);
                        }
                        // Creating adapter for spinner
                         dataAdapter = new ArrayAdapter<String>(GstINActivity.this, android.R.layout.simple_spinner_item, entities);
                        // Drop down layout style - list view with radio button
                        dataAdapter.setDropDownViewResource(R.layout.spinner_item);
                        // attaching data adapter to spinner
                        spinner.setAdapter(dataAdapter);
                    }
                }
            }
            @Override
            public void onFailure(Call<EntityResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(GstINActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getIndianStateResponse() {
        final RetrofitEasyApi apiService = ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<IndianStateResponse> call = apiService.getIndianStateResponse();
        final KProgressHUD kProgressHUD = Constants.ShowProgress(this, "Please wait");
        call.enqueue(new Callback<IndianStateResponse>() {
            @Override
            public void onResponse(Call<IndianStateResponse> call, Response<IndianStateResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(GstINActivity.this);
                int statusCode = response.code();
                IndianStateResponse indianStateResponse = response.body();
                if (indianStateResponse != null) {
                    if(indianStateResponse.getData()!=null && indianStateResponse.getData().size()>0){
                      statedata = indianStateResponse.getData();
                        List<String> state = new ArrayList<>();
                        for (int i = 0; i <statedata.size() ; i++) {
                                String stat= statedata.get(i).getName();
                                state.add(stat);
                            }
                        // Creating adapter for spinner
                        stateAdapter = new ArrayAdapter<String>(GstINActivity.this, android.R.layout.simple_spinner_item, state);
                        // Drop down layout style - list view with radio button
                        stateAdapter.setDropDownViewResource(R.layout.spinner_item);
                        // attaching data adapter to spinner
                        spinnerState.setAdapter(stateAdapter);
                        }

                    }
                }
            @Override
            public void onFailure(Call<IndianStateResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(GstINActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void updateGstnDetailsResponse( String legal_name, String enitity, String gstno, String name,String pan, String add, String file_path, String state_id){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("GSTINofBusiness",gstno);
            jsonObject.put("LegalName",legal_name);
            jsonObject.put("Name",name);
            jsonObject.put("PANofBusiness",pan);
            jsonObject.put("TypeOfEntity",enitity);
            jsonObject.put("address",add);
            jsonObject.put("gstinFilePath",file_path);
            jsonObject.put("stateid",state_id);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.addGstnDetailsResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(GstINActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(GstINActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    Toast.makeText(GstINActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                    entityName.setVisibility(View.VISIBLE);
                    businessName.setVisibility(View.VISIBLE);
                    gstInNoTxt.setVisibility(View.VISIBLE);
                    panBusinessName.setVisibility(View.VISIBLE);
                    address.setVisibility(View.VISIBLE);
                    stateName.setVisibility(View.VISIBLE);

                    entityEdit.setVisibility(View.GONE);
                    panEdit.setVisibility(View.GONE);
                    addressEdit.setVisibility(View.GONE);
                    spinStateLayout.setVisibility(View.GONE);
                    spinLayoutBusiness.setVisibility(View.GONE);
                    gstInEdit.setVisibility(View.GONE);
                    editLayout.setVisibility(View.VISIBLE);
                    submitLayout.setVisibility(View.GONE);
                    getGstInfoResponse();
                }
                else {
                    Toast.makeText(GstINActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(GstINActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void deleteGstnDetailsResponse(String ref_id){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("refId",ref_id);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.deleteGstnDetailsResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(GstINActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(GstINActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    Toast.makeText(GstINActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                   getGstInfoResponse();
                }
                else {
                    Toast.makeText(GstINActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(GstINActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void getGstInfoResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<UserInfoResponse> call= apiService.getUserInfoResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(GstINActivity.this,"Please wait");
        call.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(GstINActivity.this);
                int statusCode = response.code();
                UserInfoResponse userInfoResponse=response.body();
                if(userInfoResponse!=null){
                    USerInfo userInfo = userInfoResponse.getData();
                    if(userInfo!=null){
                         gstInfo = userInfo.getGtnInfo();
                        if (gstInfo!= null) {
                            if (gstInfo.getLegalName() == null || gstInfo.getLegalName().equalsIgnoreCase("")) {
                                entityName.setText("N/A");
                            } else {
                                entityName.setText(gstInfo.getLegalName());
                            }

                            if (gstInfo.getTypeOfEntity() == null || gstInfo.getTypeOfEntity().equalsIgnoreCase("")) {
                                businessName.setText("N/A");
                            } else {
                                businessName.setText(gstInfo.getTypeOfEntity());
                            }

                            if (gstInfo.getGSTINofBusiness() == null || gstInfo.getGSTINofBusiness().equalsIgnoreCase("")) {
                                gstInNoTxt.setText("N/A");
                            }
                            else {
                                gstInNoTxt.setText(gstInfo.getGSTINofBusiness());
                            }
                            if (gstInfo.getPANofBusiness() == null || gstInfo.getPANofBusiness().equalsIgnoreCase("")) {
                                panBusinessName.setText("N/A");
                            }
                            else {
                                panBusinessName.setText(gstInfo.getPANofBusiness());
                            }
                            if (gstInfo.getAddress() == null || gstInfo.getAddress().equalsIgnoreCase("")) {
                                address.setText("N/A");
                            }
                            else {
                                address.setText(gstInfo.getAddress());
                            }
                            if (gstInfo.getStateid() == null || gstInfo.getStateid().equalsIgnoreCase("0")) {
                                stateName.setText("N/A");
                            }
                            else {
                              // String state = statedata.get(Integer.parseInt(gstInfo.getStateid())).getName();
                                String state_id = gstInfo.getStateid();
                                if(statedata!=null && statedata.size()>0){
                                    int position = 0;
                                    for (int i = 0; i <statedata.size() ; i++) {
                                        if(statedata.get(i).getId().equalsIgnoreCase(state_id)){
                                            position = i;
                                        }
                                    }
                                    stateName.setText(statedata.get(position).getName());

                                }
                            }
                        }
                        else {
                            entityName.setText("N/A");
                            businessName.setText("N/A");
                            gstInNoTxt.setText("N/A");
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(GstINActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private static boolean validGSTIN(String gstin) throws Exception {
        boolean isValidFormat = false;
        if (checkPattern(gstin, GSTINFORMAT_REGEX)) {
            isValidFormat = verifyCheckDigit(gstin);
        }
        return isValidFormat;

    }
    public static boolean checkPattern(String inputval, String regxpatrn) {
        boolean result = false;
        if ((inputval.trim()).matches(regxpatrn)) {
            result = true;
        }
        return result;
    }
    private static boolean verifyCheckDigit(String gstinWCheckDigit) throws Exception {
        Boolean isCDValid = false;
        String newGstninWCheckDigit = getGSTINWithCheckDigit(
                gstinWCheckDigit.substring(0, gstinWCheckDigit.length() - 1));

        if (gstinWCheckDigit.trim().equals(newGstninWCheckDigit)) {
            isCDValid = true;
        }
        return isCDValid;
    }
    public static String getGSTINWithCheckDigit(String gstinWOCheckDigit) throws Exception {
        int factor = 2;
        int sum = 0;
        int checkCodePoint = 0;
        char[] cpChars;
        char[] inputChars;

        try {
            if (gstinWOCheckDigit == null) {
                throw new Exception("GSTIN supplied for checkdigit calculation is null");
            }
            cpChars = GSTN_CODEPOINT_CHARS.toCharArray();
            inputChars = gstinWOCheckDigit.trim().toUpperCase().toCharArray();

            int mod = cpChars.length;
            for (int i = inputChars.length - 1; i >= 0; i--) {
                int codePoint = -1;
                for (int j = 0; j < cpChars.length; j++) {
                    if (cpChars[j] == inputChars[i]) {
                        codePoint = j;
                    }
                }
                int digit = factor * codePoint;
                factor = (factor == 2) ? 1 : 2;
                digit = (digit / mod) + (digit % mod);
                sum += digit;
            }
            checkCodePoint = (mod - (sum % mod)) % mod;
            return gstinWOCheckDigit + cpChars[checkCodePoint];
        } finally {
            inputChars = null;
            cpChars = null;
        }
    }

}