package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RelatedCombo {
    @SerializedName("mainProdId")
    @Expose
    private String mainProdId;
    @SerializedName("prod_Name")
    @Expose
    private String prodName;
    @SerializedName("priceInr")
    @Expose
    private String priceInr;
    @SerializedName("bookImg")
    @Expose
    private String bookImg;
    @SerializedName("authors")
    @Expose
    private String authors;
    @SerializedName("rating")
    @Expose
    private List<String> rating = null;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("DiscountPercent")
    @Expose
    private Object discountPercent;
    @SerializedName("url")
    @Expose
    private Object url;
    @SerializedName("EntryDate")
    @Expose
    private Object entryDate;
    @SerializedName("isbn")
    @Expose
    private Object isbn;
    @SerializedName("productType")
    @Expose
    private Integer productType;

    public String getMainProdId() {
        return mainProdId;
    }

    public void setMainProdId(String mainProdId) {
        this.mainProdId = mainProdId;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getPriceInr() {
        return priceInr;
    }

    public void setPriceInr(String priceInr) {
        this.priceInr = priceInr;
    }

    public String getBookImg() {
        return bookImg;
    }

    public void setBookImg(String bookImg) {
        this.bookImg = bookImg;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public List<String> getRating() {
        return rating;
    }

    public void setRating(List<String> rating) {
        this.rating = rating;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Object getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Object discountPercent) {
        this.discountPercent = discountPercent;
    }

    public Object getUrl() {
        return url;
    }

    public void setUrl(Object url) {
        this.url = url;
    }

    public Object getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Object entryDate) {
        this.entryDate = entryDate;
    }

    public Object getIsbn() {
        return isbn;
    }

    public void setIsbn(Object isbn) {
        this.isbn = isbn;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }
}
