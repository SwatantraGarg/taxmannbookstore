package com.planet.Taxmann.model;

import com.google.gson.annotations.SerializedName;

public class StateCity {
    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    @SerializedName("StateName")
    private String StateName;

    @SerializedName("CityName")
    private  String CityName;
}
