package com.planet.Taxmann.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.planet.Taxmann.R;
import com.planet.Taxmann.activity.WebviewActivity;
import com.planet.Taxmann.model.MySubscription;
import com.planet.Taxmann.utils.Constants;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SubscriptionAdapter extends RecyclerView.Adapter<SubscriptionAdapter.ViewHolder> {
    List<MySubscription> mySubscriptionList;
    Context context;

    public SubscriptionAdapter(List<MySubscription> mySubscriptions, Context context) {
        this.mySubscriptionList = mySubscriptions;
        this.context = context;
    }



    @NonNull
    @Override
    public SubscriptionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SubscriptionAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.subscription_item,null));
    }

    @Override
    public void onBindViewHolder(@NonNull SubscriptionAdapter.ViewHolder holder, int position) {
if(mySubscriptionList!=null && mySubscriptionList.size()>0){
    holder.productTxt.setText(mySubscriptionList.get(position).getProductName());
    holder.expireTxt.setText(mySubscriptionList.get(position).getExpirationDate());
    if(mySubscriptionList.get(position).getRenewShow()==1){
        holder.reNewBtn.setVisibility(View.VISIBLE);
        holder.statusTxt.setVisibility(View.GONE);
    }else {
        holder.reNewBtn.setVisibility(View.GONE);
        holder.statusTxt.setVisibility(View.VISIBLE);
    }
    holder.statusTxt.setText(mySubscriptionList.get(position).getStatus());
    if(mySubscriptionList.get(position).getStatus().equalsIgnoreCase("Expire")){
       holder.statusTxt.setTextColor(context.getResources().getColor(R.color.red_color));
    }
    else {
        holder.statusTxt.setTextColor(context.getResources().getColor(R.color.black_color));
    }
    holder.reNewBtn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent browserIntent = new Intent(context, WebviewActivity.class);
            browserIntent.putExtra("url", Constants.WEB_URL+"pricing-combo-plans");
            context.startActivity(browserIntent);
        }
    });

}
    }

    @Override
    public int getItemCount() {
        return mySubscriptionList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.product)
        TextView productTxt;

        @BindView(R.id.expire)
        TextView expireTxt;

        @BindView(R.id.re_new_btn)
        TextView reNewBtn;

        @BindView(R.id.status)
        TextView statusTxt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
