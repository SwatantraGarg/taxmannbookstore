package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostSampleJournalUserAddress {
    @SerializedName("fname")
    @Expose
    private String fname;
    @SerializedName("companyname")
    @Expose
    private String companyname;
    @SerializedName("flatdoorno")
    @Expose
    private String flatdoorno;
    @SerializedName("streetroadno")
    @Expose
    private String streetroadno;
    @SerializedName("arealocality")
    @Expose
    private String arealocality;
    @SerializedName("zipcode")
    @Expose
    private String zipcode;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("landmarks")
    @Expose
    private String landmarks;

    @SerializedName("requestid")
    @Expose
    private Integer requestid;

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getFlatdoorno() {
        return flatdoorno;
    }

    public void setFlatdoorno(String flatdoorno) {
        this.flatdoorno = flatdoorno;
    }

    public String getStreetroadno() {
        return streetroadno;
    }

    public void setStreetroadno(String streetroadno) {
        this.streetroadno = streetroadno;
    }

    public String getArealocality() {
        return arealocality;
    }

    public void setArealocality(String arealocality) {
        this.arealocality = arealocality;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLandmarks() {
        return landmarks;
    }

    public void setLandmarks(String landmarks) {
        this.landmarks = landmarks;
    }

    public Integer getRequestid() {
        return requestid;
    }

    public void setRequestid(Integer requestid) {
        this.requestid = requestid;
    }

}
