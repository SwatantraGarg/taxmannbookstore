package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.CartData;
import com.planet.Taxmann.model.CartList;
import com.planet.Taxmann.model.CartOrderDetails;
import com.planet.Taxmann.model.CouponResponse;
import com.planet.Taxmann.model.DeliveryCheckResponse;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.GetCartResponse;
import com.planet.Taxmann.model.PostTempOrder;
import com.planet.Taxmann.model.ProductOrder;
import com.planet.Taxmann.model.TempOrder;
import com.planet.Taxmann.model.TempOrderResponse;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PaymentActivity extends AppCompatActivity implements PaymentResultListener {
    @BindView(R.id.pay_opt_spin)
    Spinner spinner;

    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.gst)
    TextView gst;

    @BindView(R.id.gst_txt)
    TextView gstTxt;

    @BindView(R.id.subtotal_txt)
    TextView subtotalTxt;
    @BindView(R.id.price)
    TextView price;

    @BindView(R.id.price_txt)
    TextView priceTxt;
    @BindView(R.id.ship_charge)
    TextView shipCharge;

    @BindView(R.id.total_item)
    TextView totalItem;

    @BindView(R.id.order_total)
    TextView orderTotal;

    @BindView(R.id.sub_total_price)
    TextView subtotal;

    @BindView(R.id.online_pay_layout)
    LinearLayout onlinePayLayout;

    @BindView(R.id.neft_payment_layout)
    LinearLayout neftPayLayout;

    @BindView(R.id.pay_cod)
    TextView payBtnCod;

    @BindView(R.id.pay_check)
    TextView payBtnCheck;

    @BindView(R.id.pay_online)
    TextView payBtnOnline;

    @BindView(R.id.ship_india_txt)
    TextView shipIndiaTxt;

    @BindView(R.id.pay_neft)
    TextView payBtnNeft;

    @BindView(R.id.pay_check_layout)
    LinearLayout payCheckLayout;

    @BindView(R.id.cod_layout)
    LinearLayout codLayout;

    @BindView(R.id.promo_edit)
    EditText promoEdit;

    @BindView(R.id.apply_coupon)
    TextView applyCoupon;

    @BindView(R.id.radio_group)
    RadioGroup radioGroup;

    @BindView(R.id.radio_btn_1)
    RadioButton radioButton1;

    @BindView(R.id.radio_btn_2)
    RadioButton radioButton2;

    @BindView(R.id.remove_txt)
    TextView removeTxt;

    @BindView(R.id.journal_radio_layout)
    LinearLayout journalRadioLayout;

    @BindView(R.id.discount)
    TextView discount;

    @BindView(R.id.discount_txt)
    TextView discountTxt;

    Intent intent;
    String currency ="";
    String currency_value ="";
    UserData userData;
    String source,total_item, order_no, razor_pay_id, pinCode;
    int add_id,bill_add_id;
    List<CartList> cartList =new ArrayList<>();
    private static final String TAG = MainActivity.class.getSimpleName();
    long  amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(PaymentActivity.this,"user_data", UserData.class);
        intent = getIntent();

        if(intent!=null){
            source = intent.getStringExtra("source");
            total_item  = intent.getStringExtra("total_item");
              pinCode = intent.getStringExtra("pin_code");
            add_id  = intent.getIntExtra("add_id",0);
            bill_add_id  = intent.getIntExtra("bill_add_id",0);
            getcartDetailsResponse("",source,total_item);
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(radioButton1.isChecked()){
                    getcartDetailsResponseForJounals("ship","2",source,total_item);
                }
                else {
                    getcartDetailsResponseForJounals("","3",source,total_item);
                }
            }
        });

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        applyCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(promoEdit.getText().toString().isEmpty()){
                    Toast.makeText(PaymentActivity.this, "Please enter voucher code", Toast.LENGTH_SHORT).show();
                }
                else {
                    String promo  = promoEdit.getText().toString().trim();
                    getcartDetailsResponse(promo,source,total_item);
                }
            }
        });

        removeTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String promo  = "";
                getcartDetailsResponse(promo,source,total_item);
            }
        });

        payBtnOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostTempOrder postTempOrder = new PostTempOrder();
                postTempOrder.setCartSource(source);
                postTempOrder.setCurrency(currency_value);
                postTempOrder.setIPAddress("");
                if(radioButton1.isChecked()){
                    postTempOrder.setJournalPostageOption(2);
                }
                else {
                    postTempOrder.setJournalPostageOption(3);
                }
                postTempOrder.setPaymentFromDevice(0);
                postTempOrder.setPostageAmount(0);
                postTempOrder.setSessionID("");
                postTempOrder.setShippingAddress(add_id);
                postTempOrder.setDealerid(0);
                postTempOrder.setDealertypeid(0);
                if(cartList!=null && cartList.size()>0){
                    List<CartOrderDetails> cartOrderDetailsList = new ArrayList<>();
                    CartOrderDetails cartOrderDetails = new CartOrderDetails();
                    for (int i = 0; i <cartList.size() ; i++) {
                    cartOrderDetails.setProdID(cartList.get(i).getProdID());
                    cartOrderDetails.setOrderQty(cartList.get(i).getQty());
                    cartOrderDetails.setPrice(cartList.get(i).getUnitPrice());
                    cartOrderDetails.setAmount(cartList.get(i).getTotalPrice());
                    cartOrderDetails.setCurrency(cartList.get(i).getCurrency());
                    cartOrderDetails.setsMonth(cartList.get(i).getSMonth());
                    cartOrderDetails.setsYear(cartList.get(i).getSYear());
                    }
                    cartOrderDetailsList.add(cartOrderDetails);
                    postTempOrder.setProductorderdetails(cartOrderDetailsList);

                }


                getSaveTempOrderResponse(postTempOrder);
            }
        });
        neftPayLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostTempOrder postTempOrder = new PostTempOrder();
                postTempOrder.setCartSource(source);
                postTempOrder.setCurrency("INR");
                postTempOrder.setIPAddress("");
                if(radioButton1.isChecked()){
                    postTempOrder.setJournalPostageOption(2);
                }
                else {
                    postTempOrder.setJournalPostageOption(3);
                }
                postTempOrder.setPaymentFromDevice(0);
                postTempOrder.setPayment_Mode(3);
                postTempOrder.setPostageAmount(0);
                postTempOrder.setSessionID("");
                postTempOrder.setShippingAddress(add_id);
                postTempOrder.setDealerid(0);
                postTempOrder.setDealertypeid(0);
                if(cartList!=null && cartList.size()>0){
                    List<CartOrderDetails> cartOrderDetailsList = new ArrayList<>();
                    CartOrderDetails cartOrderDetails = new CartOrderDetails();
                    for (int i = 0; i <cartList.size() ; i++) {
                        cartOrderDetails.setProdID(cartList.get(i).getProdID());
                        cartOrderDetails.setOrderQty(cartList.get(i).getQty());
                        cartOrderDetails.setPrice(cartList.get(i).getUnitPrice());
                        cartOrderDetails.setAmount(cartList.get(i).getTotalPrice());
                        cartOrderDetails.setCurrency(cartList.get(i).getCurrency());
                        cartOrderDetails.setsMonth(cartList.get(i).getSMonth());
                        cartOrderDetails.setsYear(cartList.get(i).getSYear());
                    }
                    cartOrderDetailsList.add(cartOrderDetails);
                    postTempOrder.setProductorderdetails(cartOrderDetailsList);

                }
                getDDCheckOrderResponse(postTempOrder);
            }
        });

        payBtnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostTempOrder postTempOrder = new PostTempOrder();
                postTempOrder.setCartSource(source);
                postTempOrder.setCurrency("INR");
                postTempOrder.setIPAddress("");
                if(radioButton1.isChecked()){
                    postTempOrder.setJournalPostageOption(2);
                }
                else {
                    postTempOrder.setJournalPostageOption(3);
                }
                postTempOrder.setPaymentFromDevice(0);
                postTempOrder.setPayment_Mode(2);
                postTempOrder.setPostageAmount(0);
                postTempOrder.setSessionID("");
                postTempOrder.setShippingAddress(add_id);
                postTempOrder.setDealerid(0);
                postTempOrder.setDealertypeid(0);
                if(cartList!=null && cartList.size()>0){
                    List<CartOrderDetails> cartOrderDetailsList = new ArrayList<>();
                    CartOrderDetails cartOrderDetails = new CartOrderDetails();
                    for (int i = 0; i <cartList.size() ; i++) {
                        cartOrderDetails.setProdID(cartList.get(i).getProdID());
                        cartOrderDetails.setOrderQty(cartList.get(i).getQty());
                        cartOrderDetails.setPrice(cartList.get(i).getUnitPrice());
                        cartOrderDetails.setAmount(cartList.get(i).getTotalPrice());
                        cartOrderDetails.setCurrency(cartList.get(i).getCurrency());
                        cartOrderDetails.setsMonth(cartList.get(i).getSMonth());
                        cartOrderDetails.setsYear(cartList.get(i).getSYear());
                    }
                    cartOrderDetailsList.add(cartOrderDetails);
                    postTempOrder.setProductorderdetails(cartOrderDetailsList);

                }
                getDDCheckOrderResponse(postTempOrder);
            }
        });

        payBtnCod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostTempOrder postTempOrder = new PostTempOrder();
                postTempOrder.setCartSource(source);
                postTempOrder.setCurrency("INR");
                postTempOrder.setIPAddress("");
                if(radioButton1.isChecked()){
                    postTempOrder.setJournalPostageOption(2);
                }
                else {
                    postTempOrder.setJournalPostageOption(3);
                }
                postTempOrder.setPaymentFromDevice(0);
                postTempOrder.setPayment_Mode(4);
                postTempOrder.setPostageAmount(0);
                postTempOrder.setSessionID("");
                postTempOrder.setShippingAddress(add_id);
                postTempOrder.setDealerid(0);
                postTempOrder.setDealertypeid(0);
                if(cartList!=null && cartList.size()>0){
                    List<CartOrderDetails> cartOrderDetailsList = new ArrayList<>();
                    CartOrderDetails cartOrderDetails = new CartOrderDetails();
                    for (int i = 0; i <cartList.size() ; i++) {
                        cartOrderDetails.setProdID(cartList.get(i).getProdID());
                        cartOrderDetails.setOrderQty(cartList.get(i).getQty());
                        cartOrderDetails.setPrice(cartList.get(i).getUnitPrice());
                        cartOrderDetails.setAmount(cartList.get(i).getTotalPrice());
                        cartOrderDetails.setCurrency(cartList.get(i).getCurrency());
                        cartOrderDetails.setsMonth(cartList.get(i).getSMonth());
                        cartOrderDetails.setsYear(cartList.get(i).getSYear());
                    }
                    cartOrderDetailsList.add(cartOrderDetails);
                    postTempOrder.setProductorderdetails(cartOrderDetailsList);

                }
                getDDCheckOrderResponse(postTempOrder);
            }
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected_item = spinner.getSelectedItem().toString();
                if(selected_item.equalsIgnoreCase("Online Payment")){
                    onlinePayLayout.setVisibility(View.VISIBLE);

                }
                else {
                    onlinePayLayout.setVisibility(View.GONE);
                }
                if(selected_item.equalsIgnoreCase("NEFT")){
                    neftPayLayout.setVisibility(View.VISIBLE);

                }
                else {
                    neftPayLayout.setVisibility(View.GONE);
                }
                if(selected_item.equalsIgnoreCase("Cheque / Demand Draft")){
                    payCheckLayout.setVisibility(View.VISIBLE);

                }
                else {
                    payCheckLayout.setVisibility(View.GONE);
                }
                if(selected_item.equalsIgnoreCase("Cash On Delivery")){
                    codLayout.setVisibility(View.VISIBLE);

                }
                else {
                    codLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getcartDetailsResponse(String promo, String source,String total_item){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("cartSource",source);
            if(String.valueOf(add_id)!=null && !String.valueOf(add_id).equalsIgnoreCase("")){
                jsonObject.put("shippingid",add_id);
            }
            if(promo!=null && !promo.equalsIgnoreCase("")){
                jsonObject.put("couponCode",promo);
            }
            jsonObject.put("journalCheck",3);

        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GetCartResponse> call= apiService.getCartDetailsResponse(res,userData.getUser_token());
        //final KProgressHUD kProgressHUD= Constants.ShowProgress(CartActivity.this,"Please wait");
        call.enqueue(new Callback<GetCartResponse>() {
            @Override
            public void onResponse(Call<GetCartResponse> call, Response<GetCartResponse> response) {
                // kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(PaymentActivity.this);
                int statusCode = response.code();
                GetCartResponse getCartResponse=response.body();
                if(getCartResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    if(getCartResponse.getData()!=null && !getCartResponse.getData().contains("")){
                        List<CartData> cartData = getCartResponse.getData();
                        for (int i = 0; i <cartData.size() ; i++) {
                            cartList=cartData.get(i).getCartList();
                            currency_value = cartList.get(i).getCurrency();
                            if(currency_value.equalsIgnoreCase("USD")){
                                currency = "$";
                                shipIndiaTxt.setVisibility(View.GONE);
                            }
                            else {
                                shipIndiaTxt.setVisibility(View.VISIBLE);
                                currency = "₹";
                            }
                            if(cartData.get(i).getShippingCharge()>0){
                                shipCharge.setText(currency+Math.round(cartData.get(i).getShippingCharge()));
                            }

                            else {
                                shipCharge.setText("Free");
                            }

                            if(cartData.get(i).getCartAmt()!= null){
                                shipIndiaTxt.setText("Free shipping in India on order above ₹"+Math.round(cartData.get(i).getCartAmt()));
                            }
                            else {
                                shipIndiaTxt.setVisibility(View.GONE);
                            }
                            gst.setText(currency+Math.round(cartData.get(i).getGst()));
                            /*if(cartData.get(i).getJournalRadio()==0){
                              journalRadioLayout.setVisibility(View.GONE);
                            }
                            else {
                                journalRadioLayout.setVisibility(View.VISIBLE);
                            }*/
                            if(source.equalsIgnoreCase("B")){
                                totalItem.setText("1");
                            }
                            else {
                                totalItem.setText(total_item);
                            }
                            CouponResponse couponResponse = cartData.get(i).getCouponResponse();
                            if(couponResponse!=null){
                                discountTxt.setVisibility(View.VISIBLE);
                                discount.setVisibility(View.VISIBLE);
                                removeTxt.setVisibility(View.VISIBLE);
                                discountTxt.setText(Html.fromHtml("Discount ( "+"<b>"+couponResponse.getCouponCode()+"</b>"+" )"));
                                discount.setText("- "+currency+Math.round(couponResponse.getDiscAmount()));
                            }
                            else {
                                discountTxt.setVisibility(View.GONE);
                                discount.setVisibility(View.GONE);
                                removeTxt.setVisibility(View.GONE);
                            }
                           amount= Math.round(cartData.get(i).getTotalAmount()+Math.round(cartData.get(i).getGst()));

                             if(cartList.get(i).getCurrency().equalsIgnoreCase("USD") || cartList.get(i).getProdTypeID()==11 || cartList.get(i).getProdTypeID()==10){
                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(PaymentActivity.this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.pay_option_online));
                                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                                spinner.setAdapter(spinnerArrayAdapter);
                            }
                            else {
                                 if(amount<=5000){
                                     getCheckCodResponse(pinCode);
                                 }
                                 else {
                                     ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(PaymentActivity.this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.pay_option_without_cod));
                                     spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                                     spinner.setAdapter(spinnerArrayAdapter);
                                 }

                            }
                            orderTotal.setText(currency+amount);
                            payBtnCheck.setText("Pay "+currency+amount);
                            payBtnCod.setText("Place Order "+currency+Math.round(amount+60));
                            payBtnNeft.setText("Pay "+currency+amount);
                            payBtnOnline.setText("Pay "+currency+amount);
                        }
                        double total_gst= 0;
                        double total_subtotal = 0;
                        for (int i = 0; i <cartList.size() ; i++) {
                            if(cartList.get(i).getProdTypeID()==11){
                                gstTxt.setText("GST 5%");
                                subtotalTxt.setText("Amount");
                                priceTxt.setVisibility(View.VISIBLE);
                                price.setVisibility(View.VISIBLE);
                                subtotal.setText(currency+Math.round(cartList.get(i).getUnitPrice()));
                                orderTotal.setText(currency+Math.round(cartList.get(i).getUnitPrice()*cartList.get(i).getQty()));
                                price.setText(currency+Math.round(cartList.get(i).getTotalPrice()));
                            }else {
                                subtotalTxt.setText("Subtotal");
                                gstTxt.setText("GST 18%");
                                priceTxt.setVisibility(View.GONE);
                                price.setVisibility(View.GONE);
                                total_subtotal = total_subtotal+cartList.get(i).getTotalPrice();
                                subtotal.setText(currency+Math.round(total_subtotal));
                            }
                        }
                    }
                }
                else {
                    Toast.makeText(PaymentActivity.this, getCartResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetCartResponse> call, Throwable t) {
                //kProgressHUD.dismiss();
                Toast.makeText(PaymentActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    public void getcartDetailsResponseForJounals(String ship, String journal, String source,String total_item){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("cartSource",source);
            jsonObject.put("journalCheck",journal);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GetCartResponse> call= apiService.getCartDetailsResponse(res,userData.getUser_token());
        //final KProgressHUD kProgressHUD= Constants.ShowProgress(CartActivity.this,"Please wait");
        call.enqueue(new Callback<GetCartResponse>() {
            @Override
            public void onResponse(Call<GetCartResponse> call, Response<GetCartResponse> response) {
                // kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(PaymentActivity.this);
                int statusCode = response.code();
                GetCartResponse getCartResponse=response.body();
                if(getCartResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    if(getCartResponse.getData()!=null && !getCartResponse.getData().contains("")){
                        List<CartData> cartData = getCartResponse.getData();
                        for (int i = 0; i <cartData.size() ; i++) {
                            cartList=cartData.get(i).getCartList();
                            if(cartData.get(i).getShippingCharge()>0){
                                shipCharge.setText("₹"+Math.round(cartData.get(i).getShippingCharge()));
                            }
                            else {
                                shipCharge.setText("Free");
                            }
                            if(ship.equalsIgnoreCase("ship")){
                                shipCharge.setText("₹"+Math.round(cartData.get(i).getJournalShippingCharge()));
                            }
                            else {
                                shipCharge.setText("Free");
                            }
                            gst.setText("₹"+Math.round(cartData.get(i).getGst()));
                            if(source.equalsIgnoreCase("B")){
                                totalItem.setText("1");
                            }
                            else {
                                totalItem.setText(total_item);
                            }
                            amount= Math.round(cartData.get(i).getTotalAmount());
                            if(amount<=5000){
                                getCheckCodResponse(pinCode);
                            }
                            else {
                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(PaymentActivity.this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.pay_option_without_cod));
                                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                                spinner.setAdapter(spinnerArrayAdapter);
                            }
                            orderTotal.setText("₹"+Math.round(cartData.get(i).getTotalAmount()));
                            payBtnCheck.setText("Pay"+" ₹"+Math.round(cartData.get(i).getTotalAmount()));
                            payBtnCod.setText("Place Order"+" ₹"+Math.round(cartData.get(i).getTotalAmount()+60));
                            payBtnNeft.setText("Pay"+" ₹"+Math.round(cartData.get(i).getTotalAmount()));
                            payBtnOnline.setText("Pay"+" ₹"+Math.round(cartData.get(i).getTotalAmount()));
                        }

                        double total_gst= 0;
                        double total_subtotal = 0;
                        for (int i = 0; i <cartList.size() ; i++) {

                            total_subtotal = total_subtotal+cartList.get(i).getTotalPrice();

                        }
                        subtotal.setText("₹"+Math.round(total_subtotal));

                    }
                }
                else {
                    Toast.makeText(PaymentActivity.this, getCartResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GetCartResponse> call, Throwable t) {
                //kProgressHUD.dismiss();
                Toast.makeText(PaymentActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void getDDCheckOrderResponse(PostTempOrder postTempOrder){
        RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<TempOrderResponse> call= apiService.getDDCheckOrderResponse(postTempOrder,userData.getUser_token());
        final KProgressHUD kProgressHUD=Constants.ShowProgress(PaymentActivity.this,"Please wait");
        call.enqueue(new Callback<TempOrderResponse>() {
            @Override
            public void onResponse(Call<TempOrderResponse> call, Response<TempOrderResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                TempOrderResponse tempOrderResponse=response.body();
                if(tempOrderResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    TempOrder tempOrder = tempOrderResponse.getData();
                    if(tempOrder!=null){
                       String  order_no =  tempOrder.getOrderNo();
                        Intent intent = new Intent(PaymentActivity.this, OrderProcessedActivity.class);
                        intent.putExtra("order_id",order_no);
                        startActivity(intent);
                    }
                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(PaymentActivity.this, response.body().getStatusMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<TempOrderResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(PaymentActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void getSaveTempOrderResponse(PostTempOrder postTempOrder){
        RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<TempOrderResponse> call= apiService.getTempOrderResponse(postTempOrder,userData.getUser_token());
        final KProgressHUD kProgressHUD=Constants.ShowProgress(PaymentActivity.this,"Please wait");
        call.enqueue(new Callback<TempOrderResponse>() {
            @Override
            public void onResponse(Call<TempOrderResponse> call, Response<TempOrderResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                TempOrderResponse tempOrderResponse=response.body();
                if(tempOrderResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    TempOrder tempOrder = tempOrderResponse.getData();
                    if(tempOrder!=null){
                       order_no =  tempOrder.getOrderNo();
                      // String date =  tempOrder.getDate();
                        razor_pay_id =  tempOrder.getRazorPayOrderID();
                        startPayment();
                    }
                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(PaymentActivity.this, response.body().getStatusMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<TempOrderResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(PaymentActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    public void getPostProductResponse(ProductOrder productOrder){
        RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<TempOrderResponse> call= apiService.getPostProductResponse(productOrder, userData.getUser_token());
        final KProgressHUD kProgressHUD=Constants.ShowProgress(PaymentActivity.this,"Please wait");
        call.enqueue(new Callback<TempOrderResponse>() {
            @Override
            public void onResponse(Call<TempOrderResponse> call, Response<TempOrderResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                TempOrderResponse tempOrderResponse=response.body();
                if(tempOrderResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    TempOrder tempOrder = tempOrderResponse.getData();
                    if(tempOrder!=null){
                        String order_no =  tempOrder.getOrderNo();
                        Intent intent = new Intent(PaymentActivity.this, OrderProcessedActivity.class);
                        intent.putExtra("order_id",order_no);
                        startActivity(intent);
                        finish();
                    }
                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(PaymentActivity.this, response.body().getStatusMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<TempOrderResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(PaymentActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    public void startPayment() {

        /**
         * Instantiate Checkout
         */
        Checkout checkout = new Checkout();
        checkout.setKeyID("rzp_live_o2Fh5USXaEgp8b");
        /**
         * Set your logo here
         */
        checkout.setImage(R.drawable.top_final_logo);

        /**
         * Reference to current activity
         */
        final Activity activity = this;

        /**
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */
        try {
            JSONObject options = new JSONObject();

            /**
             * Merchant Name
             * eg: ACME Corp || HasGeek etc.
             */
            options.put("name", "Taxmann.com");
            JSONObject preFill = new JSONObject();
            preFill.put("email", userData.getEmail());
            preFill.put("contact", userData.getMobile());
            options.put("prefill", preFill);

            /**
             * Description can be anything
             * eg: Order #123123
             *     Invoice Payment
             *     etc.
             */
            options.put("description", "Purchase");

            options.put("currency", currency_value);

            /**
             * Amount is always passed in PAISE
             * Eg: "500" = Rs 5.00
             */
            options.put("amount", Math.round(amount*100));

            checkout.open(activity, options);
        }
        catch(Exception e) {
            Log.e("", "Error in starting Razorpay Checkout", e);
        }

    }

    @Override
    public void onPaymentSuccess(String s) {
        if(s!=null && !s.equalsIgnoreCase("")){
            ProductOrder productOrder = new ProductOrder();
            productOrder.setOrderNo(order_no);
            productOrder.setTransactionID(razor_pay_id);
            getPostProductResponse(productOrder);
        }



    }

    @Override
    public void onPaymentError(int i, String s) {

    }

    public void getCheckCodResponse(String pin_code){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiSerivce.getCheckCodResponse(pin_code);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(PaymentActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                GeneralResponse generalResponse = response.body();
                if(generalResponse!=null){
                    if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                        if(generalResponse.getData()==1){
                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(PaymentActivity.this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.pay_option));
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                            spinner.setAdapter(spinnerArrayAdapter);
                        }else {
                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(PaymentActivity.this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.pay_option_without_cod));
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                            spinner.setAdapter(spinnerArrayAdapter);
                        }
                    }
                    else {
                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(PaymentActivity.this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.pay_option_without_cod));
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                        spinner.setAdapter(spinnerArrayAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(PaymentActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}







