package com.planet.Taxmann.interfaces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.planet.Taxmann.utils.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static  String BASE_URL = Constants.BASE_URL;    //"http://qc.taxmann.com/";
    private static  String TEST_URL = Constants.TEST_URL;    //"http://qc.taxmann.com/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

}
