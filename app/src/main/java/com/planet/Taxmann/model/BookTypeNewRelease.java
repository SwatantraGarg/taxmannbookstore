package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BookTypeNewRelease {
    @SerializedName("featuredProduct")
    @Expose
    private Object featuredProduct;
    @SerializedName("newreleaseProduct")
    @Expose
    private List<BookData> newreleaseProduct = null;

    public Object getFeaturedProduct() {
        return featuredProduct;
    }

    public void setFeaturedProduct(Object featuredProduct) {
        this.featuredProduct = featuredProduct;
    }

    public List<BookData> getNewreleaseProduct() {
        return newreleaseProduct;
    }

    public void setNewreleaseProduct(List<BookData> newreleaseProduct) {
        this.newreleaseProduct = newreleaseProduct;
    }
}
