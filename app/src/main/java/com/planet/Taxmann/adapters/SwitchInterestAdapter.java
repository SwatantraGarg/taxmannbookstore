package com.planet.Taxmann.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.planet.Taxmann.R;
import com.planet.Taxmann.model.UserInterest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SwitchInterestAdapter extends RecyclerView.Adapter<SwitchInterestAdapter.ViewHolder> {
    List<UserInterest> userInterestList;
    Context context;
    public  static List<String> count = new ArrayList<>();

    public SwitchInterestAdapter(List<UserInterest> userInterestList, Context context) {
        this.userInterestList = userInterestList;
        this.context = context;
    }


    @NonNull
    @Override
    public SwitchInterestAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SwitchInterestAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.interest_switch_item,null));
    }

    @Override
    public void onBindViewHolder(@NonNull SwitchInterestAdapter.ViewHolder holder, int position) {
        if(userInterestList!= null){

            holder.interstTxt.setText(userInterestList.get(position).getValue().trim());
            holder.toggle.setOnCheckedChangeListener (null);
            if(userInterestList.get(position).getSelected()==1){
                count.add(userInterestList.get(position).getID());
                holder.toggle.setChecked(true);
            }
            else {
                holder.toggle.setChecked(false);
                count.remove(userInterestList.get(position).getID());
            }
        holder.toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                 if(isChecked){
                count.add(userInterestList.get(position).getID());
            }

        else {
            count.remove(userInterestList.get(position).getID());

        }
    }
});



        }

    }

    @Override
    public int getItemCount() {
        return userInterestList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.intrest_txt)
        TextView interstTxt;

        @BindView(R.id.toggle)
        Switch toggle;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
