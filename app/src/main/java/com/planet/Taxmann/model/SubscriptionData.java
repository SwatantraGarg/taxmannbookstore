package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.time.Year;
import java.util.List;

public class SubscriptionData {
    @SerializedName("mySubscriptions")
    @Expose
    private List<MySubscription> mySubscriptions = null;
    @SerializedName("year")
    @Expose
    private List<Years> year = null;

    public List<MySubscription> getMySubscriptions() {
        return mySubscriptions;
    }

    public void setMySubscriptions(List<MySubscription> mySubscriptions) {
        this.mySubscriptions = mySubscriptions;
    }

    public List<Years> getYear() {
        return year;
    }

    public void setYear(List<Years> year) {
        this.year = year;
    }
}
