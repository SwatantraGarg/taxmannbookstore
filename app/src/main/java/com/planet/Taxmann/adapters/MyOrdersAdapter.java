package com.planet.Taxmann.adapters;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.planet.Taxmann.R;
import com.planet.Taxmann.activity.BookDetailsActivity;
import com.planet.Taxmann.activity.MyOrdersActivity;
import com.planet.Taxmann.activity.WebviewActivity;
import com.planet.Taxmann.model.Item;
import com.planet.Taxmann.model.MyOrder;
import com.planet.Taxmann.model.Shippment;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.ViewHolder> {
    Context context;
    List<MyOrder> myOrderList;
    List<Item> itemList;
    List<Shippment> shippmentList;
    ButtonClick buttonClick;

    public MyOrdersAdapter(Context context, List<MyOrder> myOrderList,ButtonClick buttonClick) {
        this.context = context;
        this.myOrderList = myOrderList;
        this.buttonClick = buttonClick;
    }

    public interface ButtonClick{
        void clicked(View view, int pos);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyOrdersAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item_layout,null));
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(myOrderList!=null && myOrderList.size()>0){
            String status = myOrderList.get(position).getStatus();
            if(status.equalsIgnoreCase("In Process")){
                holder.cancel.setVisibility(View.VISIBLE);
            }
            else {
                holder.cancel.setVisibility(View.GONE);
            }
            holder.date.setText(myOrderList.get(position).getOrderDate());
            holder.totalAmount.setText("₹"+myOrderList.get(position).getOrderAmount());
            holder.orderStatus.setText(myOrderList.get(position).getStatus());
            holder.orderId.setText("Order Id : "+myOrderList.get(position).getOrderNo());
           // holder.curierTxt.setText(myOrderList.get(position).getCuriurName()+"#"+myOrderList.get(position).getCuriurNo());
            shippmentList = myOrderList.get(position).getShippment();
            if(shippmentList!=null && shippmentList.size()>0 && shippmentList.size()==1){
                List<Item> itemList = new ArrayList<>();
                for (int i = 0; i < shippmentList.size(); i++) {
                    itemList = shippmentList.get(i).getItems();
                }
                OrderItemAdapter orderItemAdapter = new OrderItemAdapter(itemList, context);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
                holder.recyclerView.setLayoutManager(linearLayoutManager);
                holder.recyclerView.setAdapter(orderItemAdapter);
            }
            else if(shippmentList!=null && shippmentList.size()>0 && shippmentList.size()>1){
                holder.delTxt.setText("You'ill receive your order in "+shippmentList.size()+" deliveries");
                List<Item> itemList = new ArrayList<>();
                for (int i = 0; i < shippmentList.size(); i++) {
                    itemList = shippmentList.get(i).getItems();
                }
                OrderShippmentAdapter orderShippmentAdapter = new OrderShippmentAdapter(shippmentList,itemList,context);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
                holder.recyclerView.setLayoutManager(linearLayoutManager);
                holder.recyclerView.setAdapter(orderShippmentAdapter);
            }

            holder.cancel.setTag(position);
            holder.cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos= (int) view.getTag();
                    buttonClick.clicked(view,pos);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return myOrderList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.date)
        TextView date;

        @BindView(R.id.cancel)
        TextView cancel;

        @BindView(R.id.del_txt)
        TextView delTxt;

        @BindView(R.id.item_recyclerView)
        RecyclerView recyclerView;

        @BindView(R.id.total_amount)
        TextView totalAmount;

        @BindView(R.id.order_id)
        TextView orderId;

        @BindView(R.id.order_status)
        TextView orderStatus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
