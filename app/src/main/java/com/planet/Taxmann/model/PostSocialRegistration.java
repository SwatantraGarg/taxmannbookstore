package com.planet.Taxmann.model;

import com.google.gson.annotations.SerializedName;

public class PostSocialRegistration {
    @SerializedName("providerUsername")
    private String providerUsername;

    @SerializedName("name")
    private String name;

    @SerializedName("providerName")
    private String providerName;

    @SerializedName("providerUserId")
    private String providerUserId;

    public String getProviderUsername() {
        return providerUsername;
    }

    public void setProviderUsername(String providerUsername) {
        this.providerUsername = providerUsername;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getProviderUserId() {
        return providerUserId;
    }

    public void setProviderUserId(String providerUserId) {
        this.providerUserId = providerUserId;
    }
}
