package com.planet.Taxmann.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Addresses implements Parcelable {

    @SerializedName("AddressID")
    @Expose
    private Integer addressID;
    @SerializedName("AddressTitle")
    @Expose
    private String addressTitle;
    @SerializedName("Salutation")
    @Expose
    private String salutation;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("AddressType")
    @Expose
    private Integer addressType;
    @SerializedName("AddType")
    @Expose
    private Integer addType;
    @SerializedName("IsDefault")
    @Expose
    private Integer isDefault;
    @SerializedName("NickName")
    @Expose
    private String nickName;
    @SerializedName("Address1")
    @Expose
    private String address1;
    @SerializedName("Address2")
    @Expose
    private String address2;
    @SerializedName("AreaLocality")
    @Expose
    private String areaLocality;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("PinCode")
    @Expose
    private String pinCode;
    @SerializedName("Landmark")
    @Expose
    private String landmark;
    @SerializedName("CurrentUser")
    @Expose
    private Integer currentUser;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("FullAddress")
    @Expose
    private String fullAddress;
    @SerializedName("UserMobile")
    @Expose
    private String userMobile;
    @SerializedName("Designation")
    @Expose
    private String designation;
    @SerializedName("CompanyName")
    @Expose
    private String companyName;

    @SerializedName("countrycode")
    @Expose
    private String countrycode;

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    protected Addresses(Parcel in) {
        if (in.readByte() == 0) {
            addressID = null;
        } else {
            addressID = in.readInt();
        }
        addressTitle = in.readString();
        salutation = in.readString();
        name = in.readString();
        if (in.readByte() == 0) {
            addressType = null;
        } else {
            addressType = in.readInt();
        }
        if (in.readByte() == 0) {
            addType = null;
        } else {
            addType = in.readInt();
        }
        if (in.readByte() == 0) {
            isDefault = null;
        } else {
            isDefault = in.readInt();
        }
        nickName = in.readString();
        address1 = in.readString();
        address2 = in.readString();
        areaLocality = in.readString();
        city = in.readString();
        state = in.readString();
        country = in.readString();
        countrycode = in.readString();
        pinCode = in.readString();
        landmark = in.readString();
        if (in.readByte() == 0) {
            currentUser = null;
        } else {
            currentUser = in.readInt();
        }
        fullName = in.readString();
        fullAddress = in.readString();
        userMobile = in.readString();
        designation = in.readString();
        companyName = in.readString();
    }

    public static final Creator<Addresses> CREATOR = new Creator<Addresses>() {
        @Override
        public Addresses createFromParcel(Parcel in) {
            return new Addresses(in);
        }

        @Override
        public Addresses[] newArray(int size) {
            return new Addresses[size];
        }
    };

    public Integer getAddressID() {
        return addressID;
    }

    public void setAddressID(Integer addressID) {
        this.addressID = addressID;
    }

    public String getAddressTitle() {
        return addressTitle;
    }

    public void setAddressTitle(String addressTitle) {
        this.addressTitle = addressTitle;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAddressType() {
        return addressType;
    }

    public void setAddressType(Integer addressType) {
        this.addressType = addressType;
    }

    public Integer getAddType() {
        return addType;
    }

    public void setAddType(Integer addType) {
        this.addType = addType;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAreaLocality() {
        return areaLocality;
    }

    public void setAreaLocality(String areaLocality) {
        this.areaLocality = areaLocality;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public Integer getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(Integer currentUser) {
        this.currentUser = currentUser;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (addressID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(addressID);
        }
        dest.writeString(addressTitle);
        dest.writeString(salutation);
        dest.writeString(name);
        if (addressType == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(addressType);
        }
        if (addType == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(addType);
        }
        if (isDefault == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(isDefault);
        }
        dest.writeString(nickName);
        dest.writeString(address1);
        dest.writeString(address2);
        dest.writeString(areaLocality);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(country);
        dest.writeString(countrycode);
        dest.writeString(pinCode);
        dest.writeString(landmark);
        if (currentUser == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(currentUser);
        }
        dest.writeString(fullName);
        dest.writeString(fullAddress);
        dest.writeString(userMobile);
        dest.writeString(designation);
        dest.writeString(companyName);
    }
}
