package com.planet.Taxmann.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.planet.Taxmann.R;
import com.planet.Taxmann.activity.FinalRegistrationStepActivity;
import com.planet.Taxmann.model.AoiList;
import com.planet.Taxmann.model.ProfessionList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CheckboxAdapter extends RecyclerView.Adapter<CheckboxAdapter.ViewHolder> {
    List<ProfessionList> professionsList;
    public static List<String> professionCount= new ArrayList<>();
    Context context;


    public CheckboxAdapter(List<ProfessionList> professionsList, Context context) {
        this.professionsList = professionsList;
        this.context = context;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkbox_text_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.checkBoxTxt.setText(professionsList.get(position).getId().trim());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked){
                professionCount.add(professionsList.get(position).getName());

                }
            else {
                professionCount.remove(professionsList.get(position).getName());
            }
            }
        });

    }

    @Override
    public int getItemCount() {
        return professionsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.checkbox_text)
        TextView checkBoxTxt;
        @BindView(R.id.checkbox)
        CheckBox checkBox;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
