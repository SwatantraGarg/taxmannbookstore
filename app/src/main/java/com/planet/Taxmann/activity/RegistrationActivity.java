package com.planet.Taxmann.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.PostEmailMobile;
import com.planet.Taxmann.model.PostSocialRegistration;
import com.planet.Taxmann.model.RegistrationRequest;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.pass_hint)
    TextView passHint;

    @BindView(R.id.email_hint)
    TextView emailHint;

    @BindView(R.id.name_hint)
    TextView nameHint;

    @BindView(R.id.user_name)
    EditText userName;

    @BindView(R.id.email_address)
    EditText emailId;

    @BindView(R.id.password)
    EditText password;

    @BindView(R.id.next)
    Button nextBtn;

    @BindView(R.id.sign_in_taxmann)
    TextView signInTaxmann;

    @BindView(R.id.fb)
    LinearLayout fbBtn;

    @BindView(R.id.google)
    LinearLayout google_btn;

    CallbackManager callbackManager;
    GoogleSignInClient mGoogleSignInClient;
    GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;
    private static final String TAG = "SignInActivity";
    GeneralResponse generalResponse;
    List<String> errorList = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //FacebookSdk.sdkInitialize(getApplicationContext());
        //AppEventsLogger.activateApp(this);

        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        fbBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        signInTaxmann.setOnClickListener(this);
        google_btn.setOnClickListener(this);

        userName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                nameHint.setVisibility(View.GONE);
                if(s.length()<1){
                    nameHint.setVisibility(View.VISIBLE);
                    nameHint.setText("Please enter name");
                }
                else {
                    nameHint.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        emailId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                emailHint.setVisibility(View.GONE);
                if(s.length()<1){
                    emailHint.setVisibility(View.VISIBLE);
                    emailHint.setText("Please enter email id");
                }
                else {
                    emailHint.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                passHint.setVisibility(View.GONE);
                if(s.length()<1){
                    passHint.setVisibility(View.VISIBLE);
                    passHint.setText("Please enter password");
                }
                else {
                    passHint.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
       // printKeyHash();
        // If you are using in a fragment, call loginButton.setFragment(this);
        // Callback registration through fb
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.v("facebook login", "facebook login sucess" + loginResult.getAccessToken().getToken());

                GraphRequest graphRequest= GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.i("LoginActivity", response.toString());
                                // Get facebook data from login
                                try {
                                    String  id = object.getString("id");
                                    String  email = object.optString("email");
                                    String  name = object.optString("name");

                                    if(email==null || email.equalsIgnoreCase("")){
                                        PostSocialRegistration postSocialRegistration= new PostSocialRegistration();
                                        postSocialRegistration.setName(name);
                                        postSocialRegistration.setProviderName("facebook");
                                        postSocialRegistration.setProviderUserId(id);
                                        showDialogforNoEmail(postSocialRegistration);
                                    }
                                    else {
                                        PostSocialRegistration postSocialRegistration= new PostSocialRegistration();
                                        postSocialRegistration.setName(name);
                                        postSocialRegistration.setProviderName("facebook");
                                        postSocialRegistration.setProviderUsername(email);
                                        postSocialRegistration.setProviderUserId(id);
                                        registerUserBySocial(postSocialRegistration);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                //Bundle bFacebookData = getFacebookData(object);
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name, email, gender, birthday, location"); // Parámetros que pedimos a facebook
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(RegistrationActivity.this, exception.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        /*AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email"));
*/

// Configure sign-in to request the user's ID, email address, and basic
// profile. ID and basic profile are included in DEFAULT_SIGN_IN.


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.default_web_client_id))
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(RegistrationActivity.this /* FragmentActivity */, null/* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
       // updateUI(account);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
        else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }
    private void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.planet.Taxmann", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("KeyHash:", e.toString());
        }
    }
    private void signIn() {
        if (!mGoogleApiClient.isConnecting()) {

            mGoogleApiClient.connect();
        }
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if(account!=null){
                String name= account.getDisplayName();
                String email= account.getEmail();
                String id= account.getId();

                PostSocialRegistration postSocialRegistration= new PostSocialRegistration();
                postSocialRegistration.setName(name);
                postSocialRegistration.setProviderName("google");
                postSocialRegistration.setProviderUsername(email);
                postSocialRegistration.setProviderUserId(id);
                registerUserBySocial(postSocialRegistration);

            }

            // Signed in successfully, show authenticated UI.
            //updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            //updateUI(null);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.google:
                signIn();
                break;
               //google login

            case R.id.sign_in_taxmann:
                finish();
                break;

            case R.id.fb:
                fblogin();
                break;

            case R.id.next:
                if (userName.getText().toString().trim().isEmpty()) {
                    nameHint.setVisibility(View.VISIBLE);
                    nameHint.setText("Enter your name");
                    //Snackbar.make(userName, "Enter your name", Snackbar.LENGTH_SHORT).show();
                }

                else if(emailId.getText().toString().isEmpty()){
                    emailHint.setVisibility(View.VISIBLE);
                    emailHint.setText("Please enter email id");
                    //Snackbar.make(emailId, "Please enter email id", Snackbar.LENGTH_SHORT).show();
                }
                else if(!Patterns.EMAIL_ADDRESS.matcher(emailId.getText().toString().trim()).matches()) {
                    emailHint.setVisibility(View.VISIBLE);
                    emailHint.setText("Please enter valid email address");
                   // Snackbar.make(emailId, "Please enter valid email address", Snackbar.LENGTH_SHORT).show();
                }
                else if (!isValid(password.getText().toString(),errorList)) {
                    for (String error :errorList) {
                        //password.setError(error);
                        passHint.setVisibility(View.VISIBLE);
                        passHint.setText(error);
                        //Snackbar.make(password, error, Snackbar.LENGTH_SHORT).show();
                    }
                }
                else if (password.getText().toString().trim().isEmpty()) {
                    passHint.setVisibility(View.VISIBLE);
                    passHint.setText("Please enter password");
                   // Snackbar.make(password, " Please enter password ", Snackbar.LENGTH_SHORT).show();
                }
                else {

                    String email= emailId.getText().toString();
                    String name= userName.getText().toString();
                    String pasword= password.getText().toString();

                    RegistrationRequest registrationRequest=new RegistrationRequest();
                    registrationRequest.setEmail(email);
                    registrationRequest.setName(name);
                    registrationRequest.setPassword(pasword);
                    //service call
                    registerUserByEmail(registrationRequest);
                }
                break;

        }
    }
    public void fblogin() {
        if (Constants.isInternetConnected(this)) {
            LoginManager.getInstance().logOut();
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        }
        else {
            Toast.makeText(RegistrationActivity.this, "This feature requires internet connection please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }
    public void registerUserByEmail(RegistrationRequest registrationRequest){
        RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.getRegistrationByEmailResponse(registrationRequest);
        final KProgressHUD kProgressHUD=Constants.ShowProgress(RegistrationActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {

            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    if(generalResponse.getStatus().equalsIgnoreCase("USER_ALREADY_REGISTERED")){
                        Toast.makeText(RegistrationActivity.this,response.body().getStatusMsg(),Toast.LENGTH_LONG);
                    }

                    else {
                    UserData userData = new UserData();
                    userData.setEmail(registrationRequest.getEmail());
                    userData.setPassword(registrationRequest.getPassword());
                    userData.setName(registrationRequest.getName());
                    DroidPrefs.apply(RegistrationActivity.this,"user_data",userData);
                    Toast.makeText(RegistrationActivity.this,response.body().getStatusMsg(),Toast.LENGTH_LONG);
                    Constants.hideSoftKeyboard(RegistrationActivity.this);
                    startActivity(new Intent(RegistrationActivity.this, MobileRegistrationActivity.class));
                        overridePendingTransition(0, 0);

                }

                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(RegistrationActivity.this, response.body().getStatusMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(RegistrationActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public static boolean isValid(String passwordhere, List<String> errorList) {

        Pattern specailCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
        Pattern lowerCasePatten = Pattern.compile("[a-z ]");
        Pattern digitCasePatten = Pattern.compile("[0-9 ]");
        errorList.clear();
        boolean flag=true;


        if (passwordhere.length() < 8) {
            errorList.add("Password length must have atleast 8 characters !!");
            flag=false;
        }
       /* if (!specailCharPatten.matcher(passwordhere).find()) {
            errorList.add("Password must have atleast one specail character !!");
            flag=false;
        }*/
        else if (!UpperCasePatten.matcher(passwordhere).find()) {
            errorList.add("Password must have atleast one uppercase character !!");
            flag=false;
        }
       else if (!lowerCasePatten.matcher(passwordhere).find()) {
            errorList.add("Password must have atleast one lowercase character !!");
            flag=false;
        }
        else if (!digitCasePatten.matcher(passwordhere).find()) {
            errorList.add("Password must have atleast one digit character !!");
            flag=false;
        }
        return flag;

    }

    public void registerUserBySocial(PostSocialRegistration postSocialRegistration){
        RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.getRegistrationBySocialResponse(postSocialRegistration);
        final KProgressHUD kProgressHUD=Constants.ShowProgress(RegistrationActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                generalResponse=response.body();
                if(generalResponse!=null && generalResponse.getResponseType()!=null){
                    if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                        if(generalResponse.getStatus().equalsIgnoreCase("USER_ALREADY_REGISTERED")){
                            Toast.makeText(RegistrationActivity.this,response.body().getStatusMsg(),Toast.LENGTH_LONG).show();
                        }

                        else {
                            UserData userData= new UserData();
                            userData.setName(postSocialRegistration.getName());
                            userData.setEmail(postSocialRegistration.getProviderUsername());
                            userData.setProviderName(postSocialRegistration.getProviderName());
                            userData.setProviderUserId(postSocialRegistration.getProviderUserId());
                            DroidPrefs.apply(RegistrationActivity.this,"user_data",userData);
                            Toast.makeText(RegistrationActivity.this,response.body().getStatusMsg(),Toast.LENGTH_LONG);
                            startActivity(new Intent(RegistrationActivity.this, MobileRegistrationActivity.class));
                            overridePendingTransition(0, 0);
                            finish();
                        }
                    }
                    else {
                        kProgressHUD.dismiss();
                        Toast.makeText(RegistrationActivity.this, response.body().getStatusMsg(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(RegistrationActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void showDialogforNoEmail(PostSocialRegistration postSocialRegistration){
        Dialog dialog= new Dialog(RegistrationActivity.this);
        dialog.setContentView(R.layout.no_email_popup_layout);
        dialog.show();
        Window window= dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        EditText email_id=(EditText)dialog.findViewById(R.id.email_address);
        TextView submit_btn=(TextView)dialog.findViewById(R.id.submit);

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(email_id.getText().toString().isEmpty()){
                    Toast.makeText(RegistrationActivity.this, "Please fill the email id", Toast.LENGTH_SHORT).show();
                }

                else if(!Patterns.EMAIL_ADDRESS.matcher(email_id.getText().toString().trim()).matches()) {
                    Toast.makeText(RegistrationActivity.this, "Please enter valid email address", Toast.LENGTH_SHORT).show();
                }

                else {
                    String email= email_id.getText().toString();
                    postSocialRegistration.setProviderUsername(email);
                    registerUserBySocial(postSocialRegistration);
                    dialog.dismiss();
                }
            }
        });


    }

}
