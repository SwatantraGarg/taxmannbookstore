package com.planet.Taxmann.model;

public class FilterCategory {
    private String text;

    public FilterCategory(String text, int count) {

        this.text = text;
        this.count = count;
    }
    public  FilterCategory(){}

    private int count;


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
