package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.PasswordResponse;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {
    @BindView(R.id.old_password)
    EditText oldPassword;

    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.new_password)
    EditText newPassword;

    @BindView(R.id.confirm_password)
    EditText confirmPassword;

    @BindView(R.id.submit_btn)
    TextView submitBtn;

    UserData userData;
    List<String> errorList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(ChangePasswordActivity.this,"user_data", UserData.class);

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(oldPassword.getText().toString().trim().isEmpty()){
                    Snackbar.make(oldPassword, " Please enter your old password ", Snackbar.LENGTH_SHORT).show();
                }
             /*   else if (!isValid(oldPassword.getText().toString(),errorList)) {
                    for (String error :errorList) {
                        Snackbar.make(oldPassword, error, Snackbar.LENGTH_SHORT).show();
                    }
                }*/
                else if(newPassword.getText().toString().trim().isEmpty()){
                    Snackbar.make(newPassword, " Please enter your new password ", Snackbar.LENGTH_SHORT).show();
                }
                else if (!isValid(newPassword.getText().toString(),errorList)) {
                    for (String error :errorList) {
                        //password.setError(error);
                        Snackbar.make(newPassword, error, Snackbar.LENGTH_SHORT).show();
                    }
                }
                else if(confirmPassword.getText().toString().trim().isEmpty()){
                    Snackbar.make(confirmPassword, " Please confirm your password ", Snackbar.LENGTH_SHORT).show();
                }
                else if(oldPassword.getText().toString().trim().equalsIgnoreCase(newPassword.getText().toString())){
                    Snackbar.make(confirmPassword, "You have entered same current and new password", Snackbar.LENGTH_SHORT).show();
                }
               else if(!confirmPassword.getText().toString().trim().equalsIgnoreCase(newPassword.getText().toString().trim())){
                    Snackbar.make(confirmPassword, " new password does not match ", Snackbar.LENGTH_SHORT).show();
                }
               else {
                   String old_pass = oldPassword.getText().toString().trim();
                   String new_pass = newPassword.getText().toString().trim();
                   getChangePasswordResponse(old_pass, new_pass);
                }
            }
        });
    }

    public void getChangePasswordResponse(String old_pass, String new_pass){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("oldPassword",old_pass);
            jsonObject.put("newPassword",new_pass);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<PasswordResponse> call= apiService.getChangePasswordResponse(res, userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(ChangePasswordActivity.this,"Please wait");
        call.enqueue(new Callback<PasswordResponse>() {
            @Override
            public void onResponse(Call<PasswordResponse> call, Response<PasswordResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(ChangePasswordActivity.this);
                int statusCode = response.code();
                PasswordResponse passwordResponse=response.body();

                if(passwordResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    Toast.makeText(ChangePasswordActivity.this, passwordResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                        finish();
                }
                else {
                    Toast.makeText(ChangePasswordActivity.this, passwordResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<PasswordResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(ChangePasswordActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });



    }
    public static boolean isValid(String passwordhere, List<String> errorList) {

        Pattern specailCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
        Pattern lowerCasePatten = Pattern.compile("[a-z ]");
        Pattern digitCasePatten = Pattern.compile("[0-9 ]");
        errorList.clear();

        boolean flag=true;

        if (passwordhere.length() < 8) {
            errorList.add("Password length must have atleast 8 characters !!");
            flag=false;
        }
       /* if (!specailCharPatten.matcher(passwordhere).find()) {
            errorList.add("Password must have atleast one specail character !!");
            flag=false;
        }*/
        else if (!UpperCasePatten.matcher(passwordhere).find()) {
            errorList.add("Password must have atleast one uppercase character !!");
            flag=false;
        }
        else if (!lowerCasePatten.matcher(passwordhere).find()) {
            errorList.add("Password must have atleast one lowercase character !!");
            flag=false;
        }
        else if (!digitCasePatten.matcher(passwordhere).find()) {
            errorList.add("Password must have atleast one digit character !!");
            flag=false;
        }


        return flag;

    }
}
