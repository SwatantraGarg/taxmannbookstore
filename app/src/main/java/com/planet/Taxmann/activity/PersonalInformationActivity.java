package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.CountryCodeData;
import com.planet.Taxmann.model.CountryCodeResponse;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.GstInfo;
import com.planet.Taxmann.model.PersonalInfo;
import com.planet.Taxmann.model.PostEmailMobile;
import com.planet.Taxmann.model.USerInfo;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.model.UserInfoResponse;
import com.planet.Taxmann.model.VerifyOtp;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalInformationActivity extends AppCompatActivity {
    @BindView(R.id.email_address)
    TextView emailAddress;

    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.spinner_layout)
    RelativeLayout spinnerLayout;

    @BindView(R.id.spinner)
    SearchableSpinner spinner;

    @BindView(R.id.mobile_no)
    TextView mobileNo;

    @BindView(R.id.comp_name)
    TextView compName;

    @BindView(R.id.dob)
    TextView dobTxt;

    @BindView(R.id.designation)
    TextView designation;

    @BindView(R.id.type)
    TextView type;

    @BindView(R.id.edit_btn)
    TextView editBtn;

    @BindView(R.id.email_edit)
    EditText emailEdit;

    @BindView(R.id.mobile_edit)
    EditText mobileEdit;

    @BindView(R.id.company_edit)
    EditText companyEdit;

    @BindView(R.id.calander)
    EditText calendarView;

    @BindView(R.id.spinner_type)
    Spinner spinnerType;

    @BindView(R.id.spinner_desig)
    SearchableSpinner spinnerDesignation;

    @BindView(R.id.design_layout)
    RelativeLayout designLayout;

    @BindView(R.id.type_layout)
    RelativeLayout typeLayout;

    @BindView(R.id.submit_layout)
    LinearLayout submitLayout;

    @BindView(R.id.submit_btn)
    TextView submitBtn;

    @BindView(R.id.cancel)
    TextView cancelBtn;

    @BindView(R.id.otp_edit)
    EditText otpEdit;

    @BindView(R.id.send_otp)
    TextView sendOtp;

    @BindView(R.id.verify_otp)
    TextView verifyOtp;

    @BindView(R.id.otp_title)
    TextView otpTitle;

    UserData userData;
    String mob_no;
    Intent intent;
    ArrayAdapter<String> designAdapter;
    ArrayAdapter<String> typeAdapter;
    List<CountryCodeData> countryCodeData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_information);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(PersonalInformationActivity.this,"user_data", UserData.class);
        getCountryCodeResponse();
        getPersonalInfoResponse();

        spinnerDesignation.setTitle("Select Designation");



        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PersonalInformationActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        calendarView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // date picker dialog
                DatePickerDialog datePickerDialog = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    datePickerDialog = new DatePickerDialog(PersonalInformationActivity.this);
                    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis()- (1000 * 60 * 60 * 24 * 1));
                    datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            String date = twoDigitString(dayOfMonth) + "/" + twoDigitString(month+1) + "/"+ year ;
                            calendarView.setText(date);
                        }
                    });
                    datePickerDialog.show();
                }

            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailAddress.setVisibility(View.VISIBLE);
                emailEdit.setVisibility(View.GONE);
                sendOtp.setVisibility(View.GONE);
                otpEdit.setVisibility(View.GONE);
                otpTitle.setVisibility(View.GONE);
                verifyOtp.setVisibility(View.GONE);
                mobileNo.setVisibility(View.VISIBLE);
                mobileEdit.setVisibility(View.GONE);
                spinnerLayout.setVisibility(View.GONE);
                compName.setVisibility(View.VISIBLE);
                companyEdit.setVisibility(View.GONE);
                designation.setVisibility(View.VISIBLE);
                designLayout.setVisibility(View.GONE);
                type.setVisibility(View.VISIBLE);
                typeLayout.setVisibility(View.GONE);
                editBtn.setVisibility(View.VISIBLE);
                submitLayout.setVisibility(View.GONE);
                dobTxt.setVisibility(View.VISIBLE);
                calendarView.setVisibility(View.GONE);
            }
        });

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailAddress.setVisibility(View.GONE);
                emailEdit.setVisibility(View.VISIBLE);
                mobileNo.setVisibility(View.GONE);
                mobileEdit.setVisibility(View.VISIBLE);
                spinnerLayout.setVisibility(View.VISIBLE);
                compName.setVisibility(View.GONE);
                companyEdit.setVisibility(View.VISIBLE);
                designation.setVisibility(View.GONE);
                designLayout.setVisibility(View.VISIBLE);
                type.setVisibility(View.GONE);
                typeLayout.setVisibility(View.VISIBLE);
                editBtn.setVisibility(View.GONE);
                submitLayout.setVisibility(View.VISIBLE);
                dobTxt.setVisibility(View.GONE);
                calendarView.setVisibility(View.VISIBLE);
            }
        });

        mobileEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(s.length()==10){
             if(!s.toString().equalsIgnoreCase(mob_no)){
                sendOtp.setVisibility(View.VISIBLE);
             }
            else {
                sendOtp.setVisibility(View.GONE);
             }
            }
            else {
                sendOtp.setVisibility(View.GONE);
                verifyOtp.setVisibility(View.GONE);
                otpEdit.setVisibility(View.GONE);
                otpTitle.setVisibility(View.GONE);
            }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        sendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.hideSoftKeyboard(PersonalInformationActivity.this);
                PostEmailMobile postEmailMobile=new PostEmailMobile();
                postEmailMobile.setEmail(userData.getEmail());
                postEmailMobile.setMobile(mobileEdit.getText().toString());
                postEmailMobile.setType("edit");
                generateOTP(postEmailMobile);
            }
        });

        verifyOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(otpEdit==null || otpEdit.getText().toString().isEmpty()){
                    Toast.makeText(PersonalInformationActivity.this, "Please enter recieved OTP", Toast.LENGTH_SHORT).show();
                }

               else if(otpEdit.getText().toString().length()<4){
                    Toast.makeText(PersonalInformationActivity.this, "Please enter valid OTP", Toast.LENGTH_SHORT).show();
                }
                else {
                    VerifyOtp verifyOtp= new VerifyOtp();
                    verifyOtp.setEmail(userData.getEmail());
                    verifyOtp.setOTP(otpEdit.getText().toString());
                    verifyOtp.setMobile(mobileEdit.getText().toString());
                    int pos = spinner.getSelectedItemPosition();
                    if(countryCodeData!=null && countryCodeData.size()>0){
                        verifyOtp.setCountrycode(countryCodeData.get(pos).getDialCode());
                    }
                    verifyOtp.setOldmobile(mob_no);
                    verifyOTP(verifyOtp);
                }
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(companyEdit.getText().toString().isEmpty()){
                    Toast.makeText(PersonalInformationActivity.this, "Please enter company name", Toast.LENGTH_SHORT).show();
                }
                else if(calendarView.getText().toString().isEmpty()){
                    Toast.makeText(PersonalInformationActivity.this, "Please enter your date of birth", Toast.LENGTH_SHORT).show();
                }
                else if(spinnerDesignation.getSelectedItem().toString().isEmpty() || spinnerDesignation.getSelectedItem()==null){
                    Toast.makeText(PersonalInformationActivity.this, "Please select a designation", Toast.LENGTH_SHORT).show();
                }
                else if(mobileEdit.getText().toString().isEmpty()){
                    Toast.makeText(PersonalInformationActivity.this, "Please enter mobile no.", Toast.LENGTH_SHORT).show();

                }
                else if(mobileEdit.getText().toString().length()<10){
                    Toast.makeText(PersonalInformationActivity.this, "Please enter valid mobile no.", Toast.LENGTH_SHORT).show();
                }
                else if(otpEdit.getText().toString().isEmpty() && otpEdit.getVisibility()==View.VISIBLE){
                    Toast.makeText(PersonalInformationActivity.this, "Please verify mobile no.", Toast.LENGTH_SHORT).show();
                }
                else if(otpEdit.getText().toString().length()<4 && otpEdit.getVisibility()==View.VISIBLE){
                    Toast.makeText(PersonalInformationActivity.this, "Verify field must be atleast 4 characters long.", Toast.LENGTH_SHORT).show();
                }
                else if(spinnerDesignation.getSelectedItem().toString().equalsIgnoreCase("Select")){
                    Toast.makeText(PersonalInformationActivity.this, "Please select a designation", Toast.LENGTH_SHORT).show();
                }
                else if(spinnerType.getSelectedItemPosition()==0){
                    Toast.makeText(PersonalInformationActivity.this, "Please select a type", Toast.LENGTH_SHORT).show();
                }
                else {
                    String comp = companyEdit.getText().toString();
                    String dob = calendarView.getText().toString();
                    String desig = String.valueOf(spinnerDesignation.getSelectedItem());
                    String type = String.valueOf(spinnerType.getSelectedItemPosition());
                    updatePersonalInfoResponse(type,comp,desig,dob);
                }
            }
        });

    }
    public void getPersonalInfoResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<UserInfoResponse> call= apiService.getUserInfoResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(PersonalInformationActivity.this,"Please wait");
        call.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(PersonalInformationActivity.this);
                int statusCode = response.code();
                UserInfoResponse userInfoResponse=response.body();
                if(userInfoResponse!=null){
                    USerInfo userInfo = userInfoResponse.getData();
                    if(userInfo!=null){
                        if(userInfo.getDesignations()!=null && userInfo.getDesignations().size()>0) {
                            List<String> designations = new ArrayList<>();
                            designations.add("Select");
                            for (int i = 0; i < userInfo.getDesignations().size(); i++) {
                                String entity = userInfo.getDesignations().get(i).getName();
                                designations.add(entity);
                            }
                            // Creating adapter for spinner
                             designAdapter = new ArrayAdapter<String>(PersonalInformationActivity.this, android.R.layout.simple_list_item_1, designations);

                            // Drop down layout style - list view with radio button
                            designAdapter.setDropDownViewResource(R.layout.spinner_item);


                            // attaching data adapter to spinner
                            spinnerDesignation.setAdapter(designAdapter);
                        }
                        HashMap<String, String> typeshash = new HashMap<>();
                        if(userInfo.getUserType()!=null && userInfo.getUserType().size()>0) {

                            List<String> user_types = new ArrayList<>();
                            user_types.add("Select");
                            for (int i = 0; i < userInfo.getUserType().size(); i++) {
                                String type = userInfo.getUserType().get(i).getName();
                                String id = userInfo.getUserType().get(i).getId();
                                user_types.add(type);
                                typeshash.put(id,type);
                            }
                            // Creating adapter for spinner
                            typeAdapter = new ArrayAdapter<String>(PersonalInformationActivity.this, android.R.layout.simple_spinner_item, user_types);

                            // Drop down layout style - list view with radio button
                            typeAdapter.setDropDownViewResource(R.layout.spinner_item);

                            // attaching data adapter to spinner
                            spinnerType.setAdapter(typeAdapter);

                        }
                         PersonalInfo personalInfo = userInfo.getPersonalInfo();
                        if (personalInfo != null) {

                            if (personalInfo.getEmailID() == null || personalInfo.getEmailID().equalsIgnoreCase("")) {
                                emailAddress.setText("N/A");
                            } else {
                                emailAddress.setText(personalInfo.getEmailID());
                                emailEdit.setText(personalInfo.getEmailID());
                            }

                            if (personalInfo.getMobile() == null || personalInfo.getMobile().equalsIgnoreCase("")) {
                                mobileNo.setText("N/A");
                            } else {
                                mob_no = personalInfo.getMobile();
                                mobileEdit.setText(mob_no);
                                if(personalInfo.getCountryCode()!=null && !personalInfo.getCountryCode().equalsIgnoreCase("")){
                                 String  mob_no = personalInfo.getCountryCode()+" "+personalInfo.getMobile();
                                    mobileNo.setText(mob_no);
                                }



                            }
                            if(personalInfo.getCountryCode()!=null && !personalInfo.getCountryCode().equalsIgnoreCase("")){
                                int pos = 0;
                                if(countryCodeData!=null && countryCodeData.size()>0){
                                    for (int i = 0; i <countryCodeData.size() ; i++) {
                                        if(personalInfo.getCountryCode().equalsIgnoreCase(countryCodeData.get(i).getDialCode())){
                                            pos = i;
                                        }
                                        spinner.setSelection(pos);
                                    }

                                }
                            }else {
                                mob_no = personalInfo.getMobile();
                                mobileNo.setText(mob_no);
                            }

                            if (personalInfo.getCompanyName() == null || personalInfo.getCompanyName().equalsIgnoreCase("")) {
                                compName.setText("N/A");
                            }
                            else {
                                compName.setText(personalInfo.getCompanyName());
                                companyEdit.setText(personalInfo.getCompanyName());
                            }
                            if (personalInfo.getDOB() == null || personalInfo.getDOB().equalsIgnoreCase("")) {
                                dobTxt.setText("N/A");
                            }
                            else {
                                dobTxt.setText(personalInfo.getDOB());
                                calendarView.setText(personalInfo.getDOB());
                            }
                            if (personalInfo.getDesignation() == null || personalInfo.getDesignation().equalsIgnoreCase("")) {
                                designation.setText("N/A");
                            }
                            else {
                                designation.setText(personalInfo.getDesignation());
                                int spinnerPosition = designAdapter.getPosition(personalInfo.getDesignation());
                                spinnerDesignation.setSelection(spinnerPosition);
                            }
                            if (personalInfo.getUserType() == null || personalInfo.getDesignation().equalsIgnoreCase("")) {
                                type.setText("N/A");
                            }
                            else {
                                String type_txt= typeshash.get(personalInfo.getUserType().toString());
                                type.setText(type_txt);
                                 spinnerType.setSelection(personalInfo.getUserType());
                            }
                        }
                        else {
                            emailAddress.setText("N/A");
                            mobileNo.setText("N/A");
                            compName.setText("N/A");
                            dobTxt.setText("N/A");
                        }

                    }

                }
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(PersonalInformationActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void updatePersonalInfoResponse( String user_type, String comp_name, String design, String dob){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("UserType",user_type);
            jsonObject.put("CompanyName",comp_name);
            jsonObject.put("Designation",design);
            jsonObject.put("DOB",dob);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.editPersonalInfoResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(PersonalInformationActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(PersonalInformationActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    Toast.makeText(PersonalInformationActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                    emailAddress.setVisibility(View.VISIBLE);
                    emailEdit.setVisibility(View.GONE);
                    mobileNo.setVisibility(View.VISIBLE);
                    mobileEdit.setVisibility(View.GONE);
                    spinnerLayout.setVisibility(View.GONE);
                    otpEdit.setVisibility(View.GONE);
                    sendOtp.setVisibility(View.GONE);
                    verifyOtp.setVisibility(View.GONE);
                    compName.setVisibility(View.VISIBLE);
                    companyEdit.setVisibility(View.GONE);
                    designation.setVisibility(View.VISIBLE);
                    designLayout.setVisibility(View.GONE);
                    type.setVisibility(View.VISIBLE);
                    typeLayout.setVisibility(View.GONE);
                    editBtn.setVisibility(View.VISIBLE);
                    submitLayout.setVisibility(View.GONE);
                    dobTxt.setVisibility(View.VISIBLE);
                    calendarView.setVisibility(View.GONE);
                    otpTitle.setVisibility(View.GONE);
                    getPersonalInfoResponse();
                }
                else {
                    Toast.makeText(PersonalInformationActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(PersonalInformationActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void generateOTP(PostEmailMobile postEmailMobile){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.generateOTP(postEmailMobile);
        final KProgressHUD kProgressHUD=Constants.ShowProgress(PersonalInformationActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(PersonalInformationActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    otpEdit.setVisibility(View.VISIBLE);
                    otpTitle.setVisibility(View.VISIBLE);
                    verifyOtp.setVisibility(View.VISIBLE);
                    otpEdit.setEnabled(true);
                    otpEdit.setFocusable(true);
                    otpEdit.setText("");
                    Snackbar.make(otpEdit,"A code send to your mobile number, please enter code here",Snackbar.LENGTH_LONG).show();

                }
                else {
                    kProgressHUD.dismiss();
                    Constants.hideSoftKeyboard(PersonalInformationActivity.this);
                    Toast.makeText(PersonalInformationActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(PersonalInformationActivity.this);
                Toast.makeText(PersonalInformationActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    public void verifyOTP(VerifyOtp verify_Otp){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.verifyOTP(verify_Otp);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(PersonalInformationActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(PersonalInformationActivity.this);
                int statusCode = response.code();
               GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    UserData userData=DroidPrefs.get(PersonalInformationActivity.this,"user_data",UserData.class);
                    userData.setMobile(verify_Otp.getMobile());
                    DroidPrefs.apply(PersonalInformationActivity.this,"user_data",userData);
                    Snackbar.make(otpEdit,response.body().getStatusMsg(),Snackbar.LENGTH_LONG).show();
                    otpEdit.setVisibility(View.GONE);
                    otpTitle.setVisibility(View.GONE);
                    verifyOtp.setVisibility(View.GONE);
                    sendOtp.setVisibility(View.GONE);
                    getPersonalInfoResponse();
                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(PersonalInformationActivity.this,response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(PersonalInformationActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    private String twoDigitString(long number) {
        if (number == 0) {
        return "00";
    }
        if (number / 10 == 0) {
        return "0" + number;
    }
        return String.valueOf(number);
    }

    public void getCountryCodeResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<CountryCodeResponse> call= apiService.getCountryCodeResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(PersonalInformationActivity.this,"Please wait");
        call.enqueue(new Callback<CountryCodeResponse>() {
            @Override
            public void onResponse(Call<CountryCodeResponse> call, Response<CountryCodeResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(PersonalInformationActivity.this);
                int statusCode = response.code();
                CountryCodeResponse countryCodeResponse = response.body();
                if(countryCodeResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    countryCodeData = countryCodeResponse.getData();
                    List<String> country_list = new ArrayList<>();
                    if(countryCodeData!=null && countryCodeData.size()>0){
                        for (int i = 0; i < countryCodeData.size(); i++) {
                            country_list.add(countryCodeData.get(i).getName());
                        }
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(PersonalInformationActivity.this, android.R.layout.simple_list_item_1,country_list);
                    spinner.setAdapter(adapter);

                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(PersonalInformationActivity.this,response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CountryCodeResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(PersonalInformationActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

}
