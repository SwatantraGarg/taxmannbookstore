package com.planet.Taxmann.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.PostEmailMobile;
import com.planet.Taxmann.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {
@BindView(R.id.link_text)
TextView linkText;
@BindView(R.id.email_address)
    EditText emailAddress;
@BindView(R.id.submit)
TextView submitBtn;
GeneralResponse generalResponse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(emailAddress.getText().toString().isEmpty()){
                    Toast.makeText(ForgotPasswordActivity.this, "Please enter email id", Toast.LENGTH_SHORT).show();
                }
                else {
                    String email= emailAddress.getText().toString();

                    resetPassword(email);
                }
            }
        });


        String text="If you still need help, contact Taxmann support.";
        SpannableString ss= new SpannableString(text);

        ClickableSpan clickableSpan= new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Uri uri = Uri.parse("https://www.taxmann.com/contactus"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        };

        ss.setSpan(clickableSpan,32,48, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        linkText.setText(ss);
        linkText.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void resetPassword(String email){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("email",email);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
            final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
            Call<GeneralResponse> call= apiService.getResetPasswordResponse(res);
            final KProgressHUD kProgressHUD= Constants.ShowProgress(ForgotPasswordActivity.this,"Please wait");
            call.enqueue(new Callback<GeneralResponse>() {

                @Override
                public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                    kProgressHUD.dismiss();
                    Constants.hideSoftKeyboard(ForgotPasswordActivity.this);
                    int statusCode = response.code();
                    generalResponse=response.body();
                    if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                        Snackbar.make(emailAddress,R.string.forget_password_success_text, Snackbar.LENGTH_LONG).show();
                    }
                    else {
                        kProgressHUD.dismiss();
                        Toast.makeText(ForgotPasswordActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GeneralResponse> call, Throwable t) {
                    kProgressHUD.dismiss();
                    Toast.makeText(ForgotPasswordActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

                }
            });

    }
}
