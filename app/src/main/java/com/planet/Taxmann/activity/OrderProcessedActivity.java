package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.planet.Taxmann.R;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.utils.DroidPrefs;

public class OrderProcessedActivity extends AppCompatActivity {
    @BindView(R.id.order_no_txt)
    TextView orderTxt;

    @BindView(R.id.order_no_txt_2)
    TextView orderNoText2;

    @BindView(R.id.print)
    TextView printBtn;

    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.continue_btn)
    TextView continueBtn;
    Intent intent ;
    String order_id;
    UserData userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_processed);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(OrderProcessedActivity.this,"user_data", UserData.class);

        intent =getIntent();
        if(intent!=null){
            order_id = intent.getStringExtra("order_id");
        }

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderProcessedActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });

        orderTxt.setText("Thank you! \n" +
                "Your order has been processed and \n" +
                "the reference no. is \n"+order_id);

        orderNoText2.setText("You will receive an e-mail shortly on "+userData.getEmail());


        printBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("https://www.taxmann.com/bookstore/invoice/"+order_id));
                startActivity(viewIntent);
            }
        });

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderProcessedActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
