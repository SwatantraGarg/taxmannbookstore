package com.planet.Taxmann.utils;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

public class TaxmannApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }
}
