package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostShippingAddress {

    @SerializedName("AddType")
    @Expose
    private Integer addType;
    @SerializedName("Address1")
    @Expose
    private String address1;
    @SerializedName("Address2")
    @Expose
    private String address2;
    @SerializedName("AddressID")
    @Expose
    private Integer addressID;
    @SerializedName("AddressTitle")
    @Expose
    private String addressTitle;
    @SerializedName("AddressType")
    @Expose
    private Integer addressType;
    @SerializedName("AreaLocality")
    @Expose
    private String areaLocality;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("CompanyName")
    @Expose
    private String companyName;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("CurrentUser")
    @Expose
    private Integer currentUser;
    @SerializedName("Designation")
    @Expose
    private String designation;
    @SerializedName("FullAddress")
    @Expose
    private String fullAddress;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("IsDefault")
    @Expose
    private Integer isDefault;
    @SerializedName("Landmark")
    @Expose
    private String landmark;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("NickName")
    @Expose
    private String nickName;
    @SerializedName("PinCode")
    @Expose
    private String pinCode;
    @SerializedName("Salutation")
    @Expose
    private String salutation;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("UserMobile")
    @Expose
    private String userMobile;

    @SerializedName("countrycode")
    @Expose
    private String countrycode;

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public Integer getAddType() {
        return addType;
    }

    public void setAddType(Integer addType) {
        this.addType = addType;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public Integer getAddressID() {
        return addressID;
    }

    public void setAddressID(Integer addressID) {
        this.addressID = addressID;
    }

    public String getAddressTitle() {
        return addressTitle;
    }

    public void setAddressTitle(String addressTitle) {
        this.addressTitle = addressTitle;
    }

    public Integer getAddressType() {
        return addressType;
    }

    public void setAddressType(Integer addressType) {
        this.addressType = addressType;
    }

    public String getAreaLocality() {
        return areaLocality;
    }

    public void setAreaLocality(String areaLocality) {
        this.areaLocality = areaLocality;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(Integer currentUser) {
        this.currentUser = currentUser;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

}
