package com.planet.Taxmann.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.LoginResponse;
import com.planet.Taxmann.model.LoginUserData;
import com.planet.Taxmann.model.PostLoginEmailUser;
import com.planet.Taxmann.model.PostLoginSocialUser;
import com.planet.Taxmann.model.PostSocialRegistration;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import androidx.appcompat.widget.AppCompatCheckBox;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.google)
    LinearLayout google_btn;
    @BindView(R.id.forgot_pass)
    TextView forgotPassBtn;
    @BindView(R.id.email_hint)
    TextView emailHint;
    @BindView(R.id.pass_hint)
    TextView passHint;
    @BindView(R.id.submitBtn)
    Button signInBtn;
    @BindView(R.id.email_address)
    EditText emailId;
    @BindView(R.id.password)
    TextInputEditText password;
    @BindView(R.id.checkbox)
    AppCompatCheckBox checkBox;

    @BindView(R.id.signup_taxmann)
    TextView signUpTaxmann;
    @BindView(R.id.fb)
    LinearLayout fbBtn;

    CallbackManager callbackManager;
    GoogleSignInClient mGoogleSignInClient;
    GoogleApiClient mGoogleApiClient;
    LoginResponse generalResponse;
    private static final int RC_SIGN_IN = 9001;
    private static final String TAG = "SignInActivity";
    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    private Boolean saveLogin;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //FacebookSdk.sdkInitialize(getApplicationContext());
        //AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        signUpTaxmann.setOnClickListener(this);

        signInBtn.setOnClickListener(this);
        fbBtn.setOnClickListener(this);
        google_btn.setOnClickListener(this);
        forgotPassBtn.setOnClickListener(this);
       // printKeyHash();

        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();

        saveLogin = loginPreferences.getBoolean("saveLogin", false);

        if (saveLogin == true) {
            emailId.setText(loginPreferences.getString("username", ""));
            password.setText(loginPreferences.getString("password", ""));
            checkBox.setChecked(true);
        }

        emailId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            emailHint.setVisibility(View.GONE);
                if(s.length()<1){
                    emailHint.setVisibility(View.VISIBLE);
                    emailHint.setText("Please enter email id");
                }
                else {
                    emailHint.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
          passHint.setVisibility(View.GONE);
          if(s.length()<1){
              passHint.setVisibility(View.VISIBLE);
              passHint.setText("Please enter password");
          }
          else {
              passHint.setVisibility(View.GONE);
          }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        // If you are using in a fragment, call loginButton.setFragment(this);

        // Callback registration through fb

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.v("facebook login", "facebook login sucess" + loginResult.getAccessToken().getToken());

                GraphRequest graphRequest= GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.i("LoginActivity", response.toString());
                                // Get facebook data from login
                                try {
                                    if(object!=null){
                                        String  id = object.getString("id");
                                        String  email = object.optString("email");
                                        String  name = object.optString("name");


                                        if(email==null || email.equalsIgnoreCase("")){
                                            PostLoginSocialUser postSocialRegistration= new PostLoginSocialUser();
                                            postSocialRegistration.setProvider_name("facebook");
                                            postSocialRegistration.setProvider_userId(id);
                                            postSocialRegistration.setName(name);
                                            showDialogforNoEmail(postSocialRegistration);
                                        }

                                        else {
                                            PostLoginSocialUser postLoginSocialUser= new PostLoginSocialUser();
                                            postLoginSocialUser.setEmail(email);
                                            postLoginSocialUser.setProvider_name("facebook");
                                            postLoginSocialUser.setProvider_userId(id);
                                            postLoginSocialUser.setName(name);
                                            WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
                                            String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                                            String os1 = Build.BRAND;
                                            String os2 = Build.DEVICE;
                                            PackageManager manager = getApplicationContext().getPackageManager();
                                            PackageInfo info = null;
                                            try {
                                                info = manager.getPackageInfo(
                                                        getApplicationContext().getPackageName(), 0);
                                            } catch (PackageManager.NameNotFoundException e) {
                                                e.printStackTrace();
                                            }
                                            int sdkVersion = android.os.Build.VERSION.SDK_INT;
                                            String version = info.versionName;
                                            postLoginSocialUser.setOs("Android");
                                            postLoginSocialUser.setOsVersion(String.valueOf(sdkVersion));
                                            postLoginSocialUser.setDevice(os1+" "+os2);
                                            postLoginSocialUser.setDeviceType("MOBILE");
                                            postLoginSocialUser.setBrowser("Taxmann BookStore");
                                            postLoginSocialUser.setBrowserVersion(version);
                                            postLoginSocialUser.setUserAgent(os1+os2+"/"+version+"/"+sdkVersion);
                                            postLoginSocialUser.setIpAddress(ip);
                                            loginUserBySocial(postLoginSocialUser);

                                        }
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }







                                //Bundle bFacebookData = getFacebookData(object);
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name, email, gender, birthday, location"); // Parámetros que pedimos a facebook
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(LoginActivity.this, exception.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        /*AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email"));
*/

// Configure sign-in to request the user's ID, email address, and basic
// profile. ID and basic profile are included in DEFAULT_SIGN_IN.


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.default_web_client_id))
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(LoginActivity.this /* FragmentActivity */, null/* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(0,0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        // updateUI(account);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
        else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }
    private void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.planet.Taxmann", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {

                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("KeyHash:", e.toString());
        }
    }
    private void signIn() {
        if (!mGoogleApiClient.isConnecting()) {

            mGoogleApiClient.connect();
        }
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if(account!=null){
                String id= account.getId();
                String email= account.getEmail();
                String name= account.getDisplayName();
                PostLoginSocialUser postLoginSocialUser= new PostLoginSocialUser();
                postLoginSocialUser.setProvider_userId(id);
                postLoginSocialUser.setProvider_name("google");
                postLoginSocialUser.setEmail(email);
                postLoginSocialUser.setName(name);
                WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
                String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                String os1 = Build.BRAND;
                String os2 = Build.DEVICE;
                PackageManager manager = getApplicationContext().getPackageManager();
                PackageInfo info = null;
                try {
                    info = manager.getPackageInfo(
                            getApplicationContext().getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                int sdkVersion = android.os.Build.VERSION.SDK_INT;
                String appversion = info.versionName;
                postLoginSocialUser.setOs("Android");
                postLoginSocialUser.setOsVersion(String.valueOf(sdkVersion));
                postLoginSocialUser.setDevice(os1+" "+os2);
                postLoginSocialUser.setDeviceType("MOBILE");
                postLoginSocialUser.setBrowser("Taxmann BookStore");
                postLoginSocialUser.setBrowserVersion(appversion);
                postLoginSocialUser.setUserAgent(os1+os2+"/"+appversion+"/"+sdkVersion);
                postLoginSocialUser.setIpAddress(ip);
                loginUserBySocial(postLoginSocialUser);

            }
            // Signed in successfully, show authenticated UI.
            //updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            //updateUI(null);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.google:    //google login
                signIn();
                break;

            case R.id.fb:
                fblogin();
                break;
            case R.id.forgot_pass:
               startActivity(new Intent(LoginActivity.this,ForgotPasswordActivity.class));
                break;

            case R.id.signup_taxmann:
               startActivity(new Intent(LoginActivity.this,RegistrationActivity.class));
                break;

            case R.id.submitBtn:
                  if(emailId.getText().toString().isEmpty()){
                      emailHint.setVisibility(View.VISIBLE);
                      emailHint.setText("Please enter email id");
               // Snackbar.make(emailId, "Please enter email id", Snackbar.LENGTH_SHORT).show();
            }

            else if(!Patterns.EMAIL_ADDRESS.matcher(emailId.getText().toString().trim()).matches()) {
                      emailHint.setVisibility(View.VISIBLE);
                      emailHint.setText("Please enter valid email address");
               // Snackbar.make(emailId, "Please enter valid email address", Snackbar.LENGTH_SHORT).show();
            }

                  else if (password.getText().toString().trim().isEmpty()) {
                      passHint.setVisibility(View.VISIBLE);
                      passHint.setText("Please enter password");
                      //Snackbar.make(password, " Please enter password ", Snackbar.LENGTH_SHORT).show();
                  }
                  else {

                      String email= emailId.getText().toString();
                      String password_txt=password.getText().toString();

                      if (checkBox.isChecked()) {
                          loginPrefsEditor.putBoolean("saveLogin", true);
                          loginPrefsEditor.putString("username", email);
                          loginPrefsEditor.putString("password", password_txt);
                          loginPrefsEditor.commit();
                      } else {
                          loginPrefsEditor.clear();
                          loginPrefsEditor.commit();
                      }
                      WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
                      String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                      String os1 = Build.BRAND;
                      String os2 = Build.DEVICE;
                      PackageManager manager = getApplicationContext().getPackageManager();
                      PackageInfo info = null;
                      try {
                          info = manager.getPackageInfo(
                                  getApplicationContext().getPackageName(), 0);
                      } catch (PackageManager.NameNotFoundException e) {
                          e.printStackTrace();
                      }
                      String version = info.versionName;
                      int sdkVersion = android.os.Build.VERSION.SDK_INT;
                      PostLoginEmailUser postLoginEmailUser= new PostLoginEmailUser();
                      postLoginEmailUser.setEmail(email);
                      postLoginEmailUser.setOs("Android");
                      postLoginEmailUser.setOsVersion(String.valueOf(sdkVersion));
                      postLoginEmailUser.setDevice(os1+" "+os2);
                      postLoginEmailUser.setDeviceType("MOBILE");
                      postLoginEmailUser.setBrowser("Taxmann BookStore");
                      postLoginEmailUser.setBrowserVersion(version);
                      postLoginEmailUser.setUserAgent(os1+os2+"/"+version+"/"+sdkVersion);
                      postLoginEmailUser.setPassword(password_txt);
                      postLoginEmailUser.setIpAddress(ip);
                      loginUserByEmail(postLoginEmailUser);
                  }
        }
    }
    public void fblogin() {
        if (Constants.isInternetConnected(this)) {
            LoginManager.getInstance().logOut();
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        }

        else {
            Toast.makeText(LoginActivity.this, "This feature requires internet connection please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }
    public void loginUserBySocial(PostLoginSocialUser postLoginSocialUser){
       final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<LoginResponse> call= apiService.getLoginResponseOfSocialUser(postLoginSocialUser);
        final KProgressHUD kProgressHUD=Constants.ShowProgress(LoginActivity.this,"Please wait");
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                generalResponse=response.body();
                if(generalResponse!=null){
                    if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                        if(generalResponse.getData()!=null){
                            LoginUserData loginUserData= generalResponse.getData();
                            String user_token=loginUserData.getLoginToken();
                            String user_name=loginUserData.getUserName();
                            String user_email=loginUserData.getUserEmail();

                            UserData userData= new UserData();
                            userData.setUser_token(user_token);
                            userData.setName(user_name);
                            userData.setEmail(user_email);
                            DroidPrefs.apply(LoginActivity.this,"user_data", userData);
                            startActivity(new Intent(LoginActivity.this,MainActivity.class));
                            finish();
                        }
                    }

                else if(generalResponse.getResponseType().equalsIgnoreCase("OTP_NOT_VERIFIED")){
                    UserData userData= new UserData();
                    userData.setEmail(postLoginSocialUser.getEmail());
                    userData.setProviderUserId(postLoginSocialUser.getProvider_userId());
                    userData.setProviderName(postLoginSocialUser.getProvider_name());
                    DroidPrefs.apply(LoginActivity.this,"user_data",userData);
                    startActivity(new Intent(LoginActivity.this,MobileRegistrationActivity.class));
                }

                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(LoginActivity.this, response.body().getStatusMsg(), Toast.LENGTH_LONG).show();
                }

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(LoginActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
}
    public void loginUserByEmail(PostLoginEmailUser postLoginEmailUser){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<LoginResponse> call= apiService.getLoginResponseOfEmailUser(postLoginEmailUser);
        final KProgressHUD kProgressHUD=Constants.ShowProgress(LoginActivity.this,"Please wait");
        call.enqueue(new Callback<LoginResponse>() {

            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                  if(generalResponse.getData()!=null){
                      LoginUserData loginUserData= generalResponse.getData();
                      String user_token=loginUserData.getLoginToken();
                      String user_name=loginUserData.getUserName();
                      String user_email=loginUserData.getUserEmail();

                      UserData userData= new UserData();
                      userData.setUser_token(user_token);
                      userData.setName(user_name);
                      userData.setEmail(user_email);

                      DroidPrefs.apply(LoginActivity.this,"user_data", userData);
                      startActivity(new Intent(LoginActivity.this,MainActivity.class));
                      if(generalResponse.getStatusMsg()!=null && !generalResponse.getStatusMsg().equalsIgnoreCase("")){
                          Toast.makeText(LoginActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                      }
                      finish();
                  }

                }
                else if(generalResponse.getResponseType().equalsIgnoreCase("OTP_NOT_VERIFIED")){
                    UserData userData= new UserData();
                    userData.setEmail(postLoginEmailUser.getEmail());
                    DroidPrefs.apply(LoginActivity.this,"user_data",userData);
                    startActivity(new Intent(LoginActivity.this,MobileRegistrationActivity.class));
                }

                else {
                    Toast.makeText(LoginActivity.this, response.body().getStatusMsg(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(LoginActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void showDialogforNoEmail(PostLoginSocialUser postLoginSocialUser){
        Dialog dialog= new Dialog(LoginActivity.this);
        dialog.setContentView(R.layout.no_email_popup_layout);
        dialog.show();
        Window window= dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        EditText email_id=(EditText)dialog.findViewById(R.id.email_address);
        TextView submit_btn=(TextView)dialog.findViewById(R.id.submit);

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(email_id.getText().toString().isEmpty()){
                    Toast.makeText(LoginActivity.this, "Please fill the email id", Toast.LENGTH_SHORT).show();
                }

                else if(!Patterns.EMAIL_ADDRESS.matcher(email_id.getText().toString().trim()).matches()) {
                    Toast.makeText(LoginActivity.this, "Please enter valid email address", Toast.LENGTH_SHORT).show();
                }

                else {
                    String email= email_id.getText().toString();
                    postLoginSocialUser.setEmail(email);
                    loginUserBySocial(postLoginSocialUser);
                    dialog.dismiss();

                }
            }
        });


    }
}
