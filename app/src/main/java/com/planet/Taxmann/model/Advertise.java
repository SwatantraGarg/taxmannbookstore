package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Advertise {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("imgurl")
    @Expose
    private String imgurl;
    @SerializedName("siteurl")
    @Expose
    private String siteurl;
    @SerializedName("heading1")
    @Expose
    private String heading1;
    @SerializedName("heading2")
    @Expose
    private String heading2;
    @SerializedName("heading3")
    @Expose
    private String heading3;
    @SerializedName("heading4")
    @Expose
    private String heading4;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getSiteurl() {
        return siteurl;
    }

    public void setSiteurl(String siteurl) {
        this.siteurl = siteurl;
    }

    public String getHeading1() {
        return heading1;
    }

    public void setHeading1(String heading1) {
        this.heading1 = heading1;
    }

    public String getHeading2() {
        return heading2;
    }

    public void setHeading2(String heading2) {
        this.heading2 = heading2;
    }

    public String getHeading3() {
        return heading3;
    }

    public void setHeading3(String heading3) {
        this.heading3 = heading3;
    }

    public String getHeading4() {
        return heading4;
    }

    public void setHeading4(String heading4) {
        this.heading4 = heading4;
    }
}
