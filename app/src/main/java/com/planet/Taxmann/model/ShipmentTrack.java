package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShipmentTrack {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("awb_code")
    @Expose
    private String awbCode;
    @SerializedName("courier_company_id")
    @Expose
    private Integer courierCompanyId;
    @SerializedName("courier_company")
    @Expose
    private String courierCompany;
    @SerializedName("shipment_id")
    @Expose
    private String shipmentId;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("pickup_date")
    @Expose
    private String pickupDate;
    @SerializedName("delivered_date")
    @Expose
    private String deliveredDate;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("packages")
    @Expose
    private Integer packages;
    @SerializedName("current_status")
    @Expose
    private String currentStatus;
    @SerializedName("delivered_to")
    @Expose
    private String deliveredTo;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("consignee_name")
    @Expose
    private String consigneeName;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("courier_agent_details")
    @Expose
    private Object courierAgentDetails;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAwbCode() {
        return awbCode;
    }

    public void setAwbCode(String awbCode) {
        this.awbCode = awbCode;
    }

    public Integer getCourierCompanyId() {
        return courierCompanyId;
    }

    public void setCourierCompanyId(Integer courierCompanyId) {
        this.courierCompanyId = courierCompanyId;
    }

    public String getCourierCompany() {
        return courierCompany;
    }

    public void setCourierCompany(String courierCompany) {
        this.courierCompany = courierCompany;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getDeliveredDate() {
        return deliveredDate;
    }

    public void setDeliveredDate(String deliveredDate) {
        this.deliveredDate = deliveredDate;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public Integer getPackages() {
        return packages;
    }

    public void setPackages(Integer packages) {
        this.packages = packages;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getDeliveredTo() {
        return deliveredTo;
    }

    public void setDeliveredTo(String deliveredTo) {
        this.deliveredTo = deliveredTo;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Object getCourierAgentDetails() {
        return courierAgentDetails;
    }

    public void setCourierAgentDetails(Object courierAgentDetails) {
        this.courierAgentDetails = courierAgentDetails;
    }
}
