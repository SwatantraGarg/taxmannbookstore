package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.CountryCodeData;
import com.planet.Taxmann.model.CountryCodeResponse;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.PostEmailMobile;
import com.planet.Taxmann.model.RegistrationRequest;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.model.VerifyOtp;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MobileRegistrationActivity extends AppCompatActivity implements View.OnClickListener {
@BindView(R.id.next)
    Button nextBtn;
@BindView(R.id.resend_code)
TextView resendCodeText;

@BindView(R.id.spinner)
SearchableSpinner spinner;

@BindView(R.id.send_otp)
Button getOtp;

@BindView(R.id.mobileEditText)
TextInputEditText mobileEditText;
@BindView(R.id.otp_edit_text)
TextInputEditText otpEditText;
RetrofitEasyApi apiService;
GeneralResponse generalResponse;
UserData userData;
    List<CountryCodeData> countryCodeData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_register);
        ButterKnife.bind(this);


            userData= DroidPrefs.get(MobileRegistrationActivity.this,"user_data", UserData.class);

        nextBtn.setOnClickListener(this);
        resendCodeText.setOnClickListener(this);
        getOtp.setOnClickListener(this);
        apiService= ApiClient.getClient().create(RetrofitEasyApi.class);

        getCountryCodeResponse();
        mobileEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()>=6){
                    getOtp.setVisibility(View.VISIBLE);
                    /*Constants.hideSoftKeyboard(MobileRegistrationActivity.this);
                    PostEmailMobile postEmailMobile=new PostEmailMobile();
                    postEmailMobile.setEmail(userData.getEmail());
                    postEmailMobile.setMobile(s.toString());
                    generateOTP(postEmailMobile);*/
                }
                else {
                    getOtp.setVisibility(View.GONE);
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.next:
                if(mobileEditText==null || mobileEditText.getText().toString().isEmpty()|| mobileEditText.getText().toString().length()<6){

                    Toast.makeText(this, "Please enter mobile no.", Toast.LENGTH_SHORT).show();
                }

                else if(otpEditText==null || otpEditText.getText().toString().isEmpty()){
                    Toast.makeText(this, "Please enter recieved OTP", Toast.LENGTH_SHORT).show();
                }

                else {
                    VerifyOtp verifyOtp= new VerifyOtp();
                    verifyOtp.setEmail(userData.getEmail());
                    verifyOtp.setOTP(otpEditText.getText().toString());
                    verifyOtp.setMobile(mobileEditText.getText().toString());
                    int pos = spinner.getSelectedItemPosition();
                    if(countryCodeData!=null && countryCodeData.size()>0){
                        verifyOtp.setCountrycode(countryCodeData.get(pos).getDialCode());
                    }

                    verifyOTP(verifyOtp);
                }
                break;
            case R.id.resend_code:
            case R.id.send_otp:
                if(mobileEditText==null|| mobileEditText.getText().toString().equalsIgnoreCase("")|| mobileEditText.getText().length()<6){
                    Snackbar.make(mobileEditText, "Please enter correct mobile number", Snackbar.LENGTH_LONG).show();
                }else {
                    PostEmailMobile postEmailMobile=new PostEmailMobile();
                    postEmailMobile.setEmail(userData.getEmail());
                    postEmailMobile.setMobile(mobileEditText.getText().toString());
                    int pos = spinner.getSelectedItemPosition();
                    if(countryCodeData!=null && countryCodeData.size()>0){
                        postEmailMobile.setCountrycode(countryCodeData.get(pos).getDialCode());
                    }
                    generateOTP(postEmailMobile);
                }
                break;
        }
    }
    public void generateOTP(PostEmailMobile postEmailMobile){

        Call<GeneralResponse> call= apiService.generateOTP(postEmailMobile);
        final KProgressHUD kProgressHUD=Constants.ShowProgress(MobileRegistrationActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(MobileRegistrationActivity.this);
                int statusCode = response.code();
                generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    otpEditText.setEnabled(true);
                    otpEditText.setFocusable(true);
                Snackbar.make(mobileEditText,"A code send to your mobile number, please enter code here",Snackbar.LENGTH_LONG).show();

                }
                else {
                    kProgressHUD.dismiss();
                    Constants.hideSoftKeyboard(MobileRegistrationActivity.this);
                 Toast.makeText(MobileRegistrationActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(MobileRegistrationActivity.this);
                Toast.makeText(MobileRegistrationActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    public void verifyOTP(VerifyOtp verifyOtp){
        Call<GeneralResponse> call= apiService.verifyOTP(verifyOtp);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(MobileRegistrationActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(MobileRegistrationActivity.this);
                int statusCode = response.code();
                generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    UserData userData=DroidPrefs.get(MobileRegistrationActivity.this,"user_data",UserData.class);
                    userData.setMobile(verifyOtp.getMobile());
                    DroidPrefs.apply(MobileRegistrationActivity.this,"user_data",userData);
                    Snackbar.make(mobileEditText,response.body().getStatusMsg(),Snackbar.LENGTH_LONG).show();

                    startActivity(new Intent(MobileRegistrationActivity.this,FinalRegistrationStepActivity.class));
                    overridePendingTransition(0, 0);
                    finish();
                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(MobileRegistrationActivity.this,response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(MobileRegistrationActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void getCountryCodeResponse(){
        Call<CountryCodeResponse> call= apiService.getCountryCodeResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(MobileRegistrationActivity.this,"Please wait");
        call.enqueue(new Callback<CountryCodeResponse>() {
            @Override
            public void onResponse(Call<CountryCodeResponse> call, Response<CountryCodeResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(MobileRegistrationActivity.this);
                int statusCode = response.code();
                CountryCodeResponse countryCodeResponse = response.body();
                if(countryCodeResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                   countryCodeData = countryCodeResponse.getData();
                    List<String> country_list = new ArrayList<>();
                    if(countryCodeData!=null && countryCodeData.size()>0){
                        for (int i = 0; i < countryCodeData.size(); i++) {
                            country_list.add(countryCodeData.get(i).getName());

                        }
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(MobileRegistrationActivity.this, android.R.layout.simple_list_item_1,country_list);
                    spinner.setAdapter(adapter);

                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(MobileRegistrationActivity.this,response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CountryCodeResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(MobileRegistrationActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
