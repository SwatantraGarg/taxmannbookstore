package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Datum {
    @SerializedName("featuredProduct")
    @Expose
    private List<BookData> featuredProduct = null;
    @SerializedName("newreleaseProduct")
    @Expose
    private Object newreleaseProduct;
    @SerializedName("AggregationsBySubject")
    @Expose
    private List<AggregationsBySubject> aggregationsBySubject = null;
    @SerializedName("AggregationsBySubSubject")
    @Expose
    private Object aggregationsBySubSubject;
    @SerializedName("AggregationsByAuthor")
    @Expose
    private Object aggregationsByAuthor;
    @SerializedName("AggregationsByFormat")
    @Expose
    private Object aggregationsByFormat;
    @SerializedName("AggregationsByAvailability")
    @Expose
    private Object aggregationsByAvailability;
    @SerializedName("AggregationsByYearOfPublication")
    @Expose
    private Object aggregationsByYearOfPublication;
    @SerializedName("AggregationsByCategory")
    @Expose
    private Object aggregationsByCategory;

    public List<BookData> getFeaturedProduct() {
        return featuredProduct;
    }

    public void setFeaturedProduct(List<BookData> featuredProduct) {
        this.featuredProduct = featuredProduct;
    }

    public Object getNewreleaseProduct() {
        return newreleaseProduct;
    }

    public void setNewreleaseProduct(Object newreleaseProduct) {
        this.newreleaseProduct = newreleaseProduct;
    }

    public List<AggregationsBySubject> getAggregationsBySubject() {
        return aggregationsBySubject;
    }

    public void setAggregationsBySubject(List<AggregationsBySubject> aggregationsBySubject) {
        this.aggregationsBySubject = aggregationsBySubject;
    }

    public Object getAggregationsBySubSubject() {
        return aggregationsBySubSubject;
    }

    public void setAggregationsBySubSubject(Object aggregationsBySubSubject) {
        this.aggregationsBySubSubject = aggregationsBySubSubject;
    }

    public Object getAggregationsByAuthor() {
        return aggregationsByAuthor;
    }

    public void setAggregationsByAuthor(Object aggregationsByAuthor) {
        this.aggregationsByAuthor = aggregationsByAuthor;
    }

    public Object getAggregationsByFormat() {
        return aggregationsByFormat;
    }

    public void setAggregationsByFormat(Object aggregationsByFormat) {
        this.aggregationsByFormat = aggregationsByFormat;
    }

    public Object getAggregationsByAvailability() {
        return aggregationsByAvailability;
    }

    public void setAggregationsByAvailability(Object aggregationsByAvailability) {
        this.aggregationsByAvailability = aggregationsByAvailability;
    }

    public Object getAggregationsByYearOfPublication() {
        return aggregationsByYearOfPublication;
    }

    public void setAggregationsByYearOfPublication(Object aggregationsByYearOfPublication) {
        this.aggregationsByYearOfPublication = aggregationsByYearOfPublication;
    }

    public Object getAggregationsByCategory() {
        return aggregationsByCategory;
    }

    public void setAggregationsByCategory(Object aggregationsByCategory) {
        this.aggregationsByCategory = aggregationsByCategory;
    }
}
