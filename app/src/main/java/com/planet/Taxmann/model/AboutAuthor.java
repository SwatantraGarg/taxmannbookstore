package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AboutAuthor {
    @SerializedName("AuthorID")
    @Expose
    private Integer authorID;
    @SerializedName("AuthorName")
    @Expose
    private String authorName;
    @SerializedName("aboutAuthor")
    @Expose
    private String aboutAuthor;
    @SerializedName("authorPic")
    @Expose
    private String authorPic;

    public Integer getAuthorID() {
        return authorID;
    }

    public void setAuthorID(Integer authorID) {
        this.authorID = authorID;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAboutAuthor() {
        return aboutAuthor;
    }

    public void setAboutAuthor(String aboutAuthor) {
        this.aboutAuthor = aboutAuthor;
    }

    public String getAuthorPic() {
        return authorPic;
    }

    public void setAuthorPic(String authorPic) {
        this.authorPic = authorPic;
    }
}
