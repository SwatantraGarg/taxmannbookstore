package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartData {
    @SerializedName("cartList")
    @Expose
    private List<CartList> cartList = null;
    @SerializedName("Cart_Amt")
    @Expose
    private Double cartAmt;

    public Double getGst() {
        return gst;
    }

    public void setGst(Double gst) {
        this.gst = gst;
    }

    @SerializedName("gst")
    @Expose
    private Double gst;
    @SerializedName("ShippingCharge")
    @Expose
    private Double shippingCharge;
    @SerializedName("alertMessage")
    @Expose
    private String alertMessage;
    @SerializedName("journalRadio")
    @Expose
    private Integer journalRadio;
    @SerializedName("journalShippingCharge")
    @Expose
    private Integer journalShippingCharge;
    @SerializedName("cartSource")
    @Expose
    private String cartSource;
    @SerializedName("totalAmount")
    @Expose
    private Double totalAmount;
    @SerializedName("loyaltyAmount")
    @Expose
    private Double loyaltyAmount;
    @SerializedName("AppiledloyaltyAmount")
    @Expose
    private Double appiledloyaltyAmount;
    @SerializedName("couponResponse")
    @Expose
    private CouponResponse couponResponse;

    public List<CartList> getCartList() {
        return cartList;
    }

    public void setCartList(List<CartList> cartList) {
        this.cartList = cartList;
    }

    public Double getCartAmt() {
        return cartAmt;
    }

    public void setCartAmt(Double cartAmt) {
        this.cartAmt = cartAmt;
    }

    public Double getShippingCharge() {
        return shippingCharge;
    }

    public void setShippingCharge(Double shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public String getAlertMessage() {
        return alertMessage;
    }

    public void setAlertMessage(String alertMessage) {
        this.alertMessage = alertMessage;
    }

    public Integer getJournalRadio() {
        return journalRadio;
    }

    public void setJournalRadio(Integer journalRadio) {
        this.journalRadio = journalRadio;
    }

    public Integer getJournalShippingCharge() {
        return journalShippingCharge;
    }

    public void setJournalShippingCharge(Integer journalShippingCharge) {
        this.journalShippingCharge = journalShippingCharge;
    }

    public String getCartSource() {
        return cartSource;
    }

    public void setCartSource(String cartSource) {
        this.cartSource = cartSource;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getLoyaltyAmount() {
        return loyaltyAmount;
    }

    public void setLoyaltyAmount(Double loyaltyAmount) {
        this.loyaltyAmount = loyaltyAmount;
    }

    public Double getAppiledloyaltyAmount() {
        return appiledloyaltyAmount;
    }

    public void setAppiledloyaltyAmount(Double appiledloyaltyAmount) {
        this.appiledloyaltyAmount = appiledloyaltyAmount;
    }

    public CouponResponse getCouponResponse() {
        return couponResponse;
    }

    public void setCouponResponse(CouponResponse couponResponse) {
        this.couponResponse = couponResponse;
    }

}
