package com.planet.Taxmann.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.planet.Taxmann.R;

import java.util.ArrayList;

import androidx.annotation.Nullable;

public class AutoSearchCustomAdapter extends ArrayAdapter<String> implements Filterable {
    private ArrayList<String> items;
    TextView customerNameLabel;

    public AutoSearchCustomAdapter(Context context, ArrayList<String> items) {
        super(context, R.layout.suggetion_item, items);
        this.items = items;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate( R.layout.suggetion_item, null);
             customerNameLabel= (TextView) v.findViewById(R.id.suggestion_text);
        }
        if(items.size()>0){

            String customer = getItem(position);
            if (customer != null) {

                    customerNameLabel.setText(Html.fromHtml(customer));






            }
        }



        return v;
    }

    @Override
    public int getCount() {
        return items.size();


    }

    @Nullable
    @Override
    public String getItem(int position) {
        return items.get(position);
    }
}
