package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.CartAdapter;
import com.planet.Taxmann.adapters.ReviewAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.CartData;
import com.planet.Taxmann.model.CartList;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.GetCartResponse;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CartActivity extends AppCompatActivity {
    @BindView(R.id.logo)
    ImageView logo;
    UserData userData;
   @BindView(R.id.recycler_view_cart)
    RecyclerView recyclerView;

   @BindView(R.id.total_item)
   TextView totalItem;

   @BindView(R.id.gst)
   TextView gst;

   @BindView(R.id.sub_total_price)
   TextView subTotalPrice;

   @BindView(R.id.proceed_btn)
   TextView proceedBtn;

   @BindView(R.id.ship_charge)
   TextView shipCharge;

   @BindView(R.id.order_total)
   TextView orderTotal;

   @BindView(R.id.shopping_btn)
   TextView shoppingBtn;

   @BindView(R.id.subtotal)
   TextView subtotal;

   @BindView(R.id.layout_empty)
    RelativeLayout layoutEmpty;

   @BindView(R.id.remove)
   ImageView crossBtn;

   @BindView(R.id.layout_fill)
    LinearLayout layoutFill;
   @BindView(R.id.continue_btn)
   TextView continueBtn;

   @BindView(R.id.price)
           TextView price;

   @BindView(R.id.price_txt)
           TextView priceTxt;

   @BindView(R.id.subtotal_txt)
   TextView subtotalTxt;
   @BindView(R.id.gst_txt)
   TextView gstTxt;
   @BindView(R.id.ship_india_txt)
    TextView shipIndiaTxt;

    CartAdapter cartAdapter;
    String totalitem,type,alert;
    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(CartActivity.this,"user_data",UserData.class);
        getcartDetailsResponse();

        intent = getIntent();
        if(intent!= null){
            type = intent.getStringExtra("type");
        }
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CartActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        shoppingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CartActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        crossBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent(CartActivity.this,MainActivity.class);
               startActivity(intent);
               finish();
            }
        });

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CartActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(alert!=null && !alert.equalsIgnoreCase("")){
                    showAlert(alert);
                }
                else {
                    Intent intent = new Intent(CartActivity.this,CheckOutActivity.class);
                    intent.putExtra("source","C");
                    if(totalitem!=null &&!totalitem.equalsIgnoreCase("")){
                        intent.putExtra("total_item",totalitem);
                    }
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(type!=null && type.equalsIgnoreCase("detail")){

        }else {
            Intent intent = new Intent(CartActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void getcartDetailsResponse(){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("cartSource","C");
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GetCartResponse> call= apiService.getCartDetailsResponse(res,userData.getUser_token());
        //final KProgressHUD kProgressHUD= Constants.ShowProgress(CartActivity.this,"Please wait");
        call.enqueue(new Callback<GetCartResponse>() {
            @Override
            public void onResponse(Call<GetCartResponse> call, Response<GetCartResponse> response) {
               // kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(CartActivity.this);
                int statusCode = response.code();
                GetCartResponse getCartResponse=response.body();
                if(getCartResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    if(getCartResponse.getData()!=null && !getCartResponse.getData().contains("")){
                        List<CartData> cartData = getCartResponse.getData();
                        List<CartList> cartList = new ArrayList<>();
                        for (int i = 0; i <cartData.size() ; i++) {
                            cartList=cartData.get(i).getCartList();
                            if(cartData.get(i).getAlertMessage()!=null && !cartData.get(i).getAlertMessage().equals("")){
                             alert = cartData.get(i).getAlertMessage();
                            }
                            if(cartData.get(i).getShippingCharge()>0){
                                shipCharge.setText("₹"+Math.round(cartData.get(i).getShippingCharge()));
                            }
                            else {
                                shipCharge.setText("Free");
                            }
                            if(cartData.get(i).getCartAmt()!=null){
                                shipIndiaTxt.setText("Free shipping in India on order above ₹"+Math.round(cartData.get(i).getCartAmt()));
                            }
                            else {
                                shipIndiaTxt.setVisibility(View.GONE);
                            }
                            gst.setText("₹"+Math.round(cartData.get(i).getGst()));
                            subTotalPrice.setText("₹"+Math.round(cartData.get(i).getTotalAmount()));
                            subtotal.setText("₹"+Math.round(cartData.get(i).getTotalAmount()));
                            orderTotal.setText("₹"+Math.round(cartData.get(i).getTotalAmount()));
                                                   }
                        if(cartList!=null && cartList.size()>0){
                            layoutFill.setVisibility(View.VISIBLE);
                            layoutEmpty.setVisibility(View.GONE);
                            totalitem = String.valueOf(cartList.size());
                            totalItem.setText(String.valueOf(cartList.size()));


                            double total_gst= 0;
                            double total_subtotal = 0;
                            for (int i = 0; i <cartList.size() ; i++) {
                                if(cartList.get(i).getProdTypeID()==11){
                                    gstTxt.setText("GST 5%");
                                    subtotalTxt.setText("Amount");
                                    priceTxt.setVisibility(View.VISIBLE);
                                    price.setVisibility(View.VISIBLE);
                                    subTotalPrice.setText("₹"+Math.round(cartList.get(i).getUnitPrice()));
                                    orderTotal.setText("₹"+Math.round(cartList.get(i).getUnitPrice()*cartList.get(i).getQty()));
                                    price.setText("₹"+Math.round(cartList.get(i).getTotalPrice()));
                                }else {
                                    subtotalTxt.setText("Subtotal");
                                    gstTxt.setText("GST 18%");
                                    priceTxt.setVisibility(View.GONE);
                                    price.setVisibility(View.GONE);
                                    total_subtotal = total_subtotal+cartList.get(i).getTotalPrice();
                                    subTotalPrice.setText("₹"+Math.round(total_subtotal));
                                    subtotal.setText("₹"+Math.round(total_subtotal));
                                }


                            }



                            List<CartList> finalCartList = cartList;
                            cartAdapter =new CartAdapter(cartList, CartActivity.this, new CartAdapter.OnClicked() {
                                @Override
                                public void Clicked(View view, int position,int no) {
                                    if(view.getId()==R.id.remove){
                                        deleteProductFromcartResponse(String.valueOf(finalCartList.get(position).getProdID()));
                                        cartAdapter.notifyDataSetChanged();
                                    }
                                    else if(view.getId()==R.id.increment){

                                           addProductTocartResponse(finalCartList.get(position).getProdID().toString(),String.valueOf(no));
                                    }
                                    else if(view.getId()==R.id.decrement){
                                        if(no>=1){
                                            addProductTocartResponse(finalCartList.get(position).getProdID().toString(),String.valueOf(no));
                                        }




                                    }
                                    else if(view.getId()==R.id.no_of_books){
                                        addProductTocartResponse(finalCartList.get(position).getProdID().toString(),String.valueOf(no));
                                    }
                                }
                            });
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CartActivity.this,LinearLayoutManager.VERTICAL,false);
                            recyclerView.setLayoutManager(linearLayoutManager);
                            recyclerView.setNestedScrollingEnabled(false);
                            recyclerView.setAdapter(cartAdapter);
                            cartAdapter.notifyDataSetChanged();
                        }
                        else {
                            layoutFill.setVisibility(View.GONE);
                            layoutEmpty.setVisibility(View.VISIBLE);
                        }



                    }
                }
                else {
                    Toast.makeText(CartActivity.this, getCartResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<GetCartResponse> call, Throwable t) {
                //kProgressHUD.dismiss();
                layoutFill.setVisibility(View.GONE);
                layoutEmpty.setVisibility(View.VISIBLE);
               // Toast.makeText(CartActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void deleteProductFromcartResponse( String prod_id){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("prodID",prod_id);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.deleteProductFromCartResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(CartActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(CartActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    alert="";
                    getcartDetailsResponse();
                    Toast.makeText(CartActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(CartActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(CartActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void addProductTocartResponse( String prod_id, String qty){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("prodID",prod_id);
            jsonObject.put("qTy",qty);
            jsonObject.put("cartSource","c");

        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.addProductTocartResponse(res,userData.getUser_token());
       // final KProgressHUD kProgressHUD= Constants.ShowProgress(CartActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
               // kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(CartActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    getcartDetailsResponse();

                }
                else {
                    Toast.makeText(CartActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
               // kProgressHUD.dismiss();
                Toast.makeText(CartActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void showAlert(String alert_msg){
        Dialog dialog= new Dialog(CartActivity.this);
        dialog.setContentView(R.layout.journal_alert_layout);
        dialog.show();
        TextView heading = (TextView)dialog.findViewById(R.id.alert_txt);
        TextView review_btn = (TextView)dialog.findViewById(R.id.review_cart_btn);
        TextView continue_btn = (TextView)dialog.findViewById(R.id.continue_btn);
        ImageView cross_btn = (ImageView)dialog.findViewById(R.id.cross_btn);
        dialog.setCanceledOnTouchOutside(true);
        heading.setText(alert_msg);
        review_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        cross_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CartActivity.this,CheckOutActivity.class);
                intent.putExtra("source","C");
                if(totalitem!=null &&!totalitem.equalsIgnoreCase("")){
                    intent.putExtra("total_item",totalitem);
                }
                startActivity(intent);
            }
        });




    }
}
