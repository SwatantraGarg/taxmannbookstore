package com.planet.Taxmann.model;

import com.google.gson.annotations.SerializedName;

public class ProductOrder {
    @SerializedName("OrderNo")
    private String OrderNo;

    @SerializedName("TransactionID")
    private String TransactionID;

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }

    public String getTransactionID() {
        return TransactionID;
    }

    public void setTransactionID(String transactionID) {
        TransactionID = transactionID;
    }


}
