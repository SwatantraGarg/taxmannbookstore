package com.planet.Taxmann.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BookData implements Parcelable {
    @SerializedName("mainProdId")
    @Expose
    private String mainProdId;
    @SerializedName("prod_Name")
    @Expose
    private String prodName;
    @SerializedName("priceInr")
    @Expose
    private String priceInr;
    @SerializedName("bookImg")
    @Expose
    private String bookImg;
    @SerializedName("authors")
    @Expose
    private String authors;
    @SerializedName("rating")
    @Expose
    private List<String> rating = null;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("DiscountPercent")
    @Expose
    private Object discountPercent;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("EntryDate")
    @Expose
    private Object entryDate;

    public Object getIsbn() {
        return isbn;
    }

    public void setIsbn(Object isbn) {
        this.isbn = isbn;
    }

    @SerializedName("isbn")
    @Expose
    private Object isbn;
    @SerializedName("productType")
    @Expose
    private String productType;

    @SerializedName("productTypeid")
    @Expose
    private String productTypeID;

    public String getProductTypeID() {
        return productTypeID;
    }

    public void setProductTypeID(String productTypeID) {
        this.productTypeID = productTypeID;
    }

    public static Creator<BookData> getCREATOR() {
        return CREATOR;
    }



    protected BookData(Parcel in) {
        mainProdId = in.readString();
        prodName = in.readString();
        priceInr = in.readString();
        bookImg = in.readString();
        authors = in.readString();
        rating = in.createStringArrayList();
        url = in.readString();

    }

    public static final Creator<BookData> CREATOR = new Creator<BookData>() {
        @Override
        public BookData createFromParcel(Parcel in) {
            return new BookData(in);
        }

        @Override
        public BookData[] newArray(int size) {
            return new BookData[size];
        }
    };

    public String getMainProdId() {
        return mainProdId;
    }

    public void setMainProdId(String mainProdId) {
        this.mainProdId = mainProdId;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getPriceInr() {
        return priceInr;
    }

    public void setPriceInr(String priceInr) {
        this.priceInr = priceInr;
    }

    public String getBookImg() {
        return bookImg;
    }

    public void setBookImg(String bookImg) {
        this.bookImg = bookImg;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public List<String> getRating() {
        return rating;
    }

    public void setRating(List<String> rating) {
        this.rating = rating;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Object getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Object discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Object getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Object entryDate) {
        this.entryDate = entryDate;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mainProdId);
        dest.writeString(prodName);
        dest.writeString(priceInr);
        dest.writeString(bookImg);
        dest.writeString(authors);
        dest.writeStringList(rating);
        dest.writeString(url);

    }
}
