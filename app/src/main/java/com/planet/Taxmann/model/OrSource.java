package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrSource {
    @SerializedName("refId")
    @Expose
    private String refId;
    @SerializedName("refName")
    @Expose
    private String refName;
    @SerializedName("orSource")
    @Expose
    private Object orSource;

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getRefName() {
        return refName;
    }

    public void setRefName(String refName) {
        this.refName = refName;
    }

    public Object getOrSource() {
        return orSource;
    }

    public void setOrSource(Object orSource) {
        this.orSource = orSource;
    }
}
