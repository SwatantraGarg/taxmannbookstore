package com.planet.Taxmann.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.planet.Taxmann.R;
import com.planet.Taxmann.activity.BookDetailsActivity;
import com.planet.Taxmann.activity.MainActivity;
import com.planet.Taxmann.activity.MyOrdersActivity;
import com.planet.Taxmann.activity.ViewAllBooksActivity;
import com.planet.Taxmann.adapters.BannerAdapter;
import com.planet.Taxmann.adapters.BookAdapter;
import com.planet.Taxmann.adapters.OffersAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.Advertise;
import com.planet.Taxmann.model.AdvertiseResponse;
import com.planet.Taxmann.model.BookData;
import com.planet.Taxmann.model.BookDataResponse;
import com.planet.Taxmann.model.BookType;
import com.planet.Taxmann.model.GetRecommendedResponse;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.utils.DroidPrefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends  Fragment {
    @BindView(R.id.recycler_view_2)
    RecyclerView newReleseBooksList;

    @BindView(R.id.offer_rv)
    RecyclerView offerRecyclerView;

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.view_all_featured_books)
    TextView viewAllFeaturedBooks;

    @BindView(R.id.view_all_new_release)
    TextView viewAllNewRelesedBooks;

    @BindView(R.id.view_all_recommended_books)
    TextView viewAllRecommendedBooks;

    @BindView(R.id.all_txt)
    TextView allTxt;

    @BindView(R.id.offer_txt)
    TextView offerTxt;

    @BindView(R.id.recycler_view_4)
    RecyclerView featuredBooksList;

    @BindView(R.id.recycler_view_3)
    RecyclerView recommendedBooksList;

    @BindView(R.id.rel3)
    RelativeLayout allRecommendedLayout;
    @BindView(R.id.view_4)
    View view4;
    UserData userData;
    MainActivity mainActivity;
    BookAdapter bookAdapter;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    List<BookData> releaseBookDataList= new ArrayList<>();
    List<BookData> featureBookDataList= new ArrayList<>();
    List<BookData> recommendedBookDataList= new ArrayList<>();

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.home_layout, null);
        ButterKnife.bind(this,view);
        userData = DroidPrefs.get(getContext(),"user_data", UserData.class);
        getTopBannerResponse();
        getOffersResponse();
        getNewReleaseBooksResponse();
        getFeaturedBooksResponse();
        getHomePageTopLatestProductsResponse();
        String token = userData.getUser_token();

        if(token!=null &&!token.equalsIgnoreCase("")){
            getHomePageRecommendedProductsResponse(token);
            getHomePageRecommendedAllProductsResponse(token);
        }


viewAllFeaturedBooks.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if(featureBookDataList!=null && featureBookDataList.size()>0){
            Intent intent= new Intent(getContext(), ViewAllBooksActivity.class);
            intent.putParcelableArrayListExtra("book_list", (ArrayList<? extends Parcelable>) featureBookDataList);
            startActivity(intent);
        }
    }
});

    viewAllNewRelesedBooks.setOnClickListener(new View.OnClickListener() {
        @Override
         public void onClick(View v) {
        if(releaseBookDataList!=null && releaseBookDataList.size()>0){
            Intent intent= new Intent(getContext(), ViewAllBooksActivity.class);
            intent.putParcelableArrayListExtra("book_list", (ArrayList<? extends Parcelable>) releaseBookDataList);
            startActivity(intent);
        }
    }
});
        viewAllRecommendedBooks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(recommendedBookDataList!=null && recommendedBookDataList.size()>0){
                    Intent intent= new Intent(getContext(), ViewAllBooksActivity.class);
                    intent.putParcelableArrayListExtra("book_list", (ArrayList<? extends Parcelable>) recommendedBookDataList);
                    startActivity(intent);
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mainActivity= (MainActivity)getActivity();
    }

    public  void getNewReleaseBooksResponse(){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<BookDataResponse> call= apiSerivce.getnewReleasedBookResponse();
        call.enqueue(new Callback<BookDataResponse>() {
            @Override

            public void onResponse(Call<BookDataResponse> call, Response<BookDataResponse> response) {
                int status_code= response.code();
                BookDataResponse newReleaseBookResponse= response.body();
                if(newReleaseBookResponse!=null){
                    List<BookType> bookTypeNewReleaseList= newReleaseBookResponse.getData();
                        for (int i = 0; i <bookTypeNewReleaseList.size() ; i++) {
                            releaseBookDataList=bookTypeNewReleaseList.get(i).getNewreleaseProduct();
                        }
                }
            }

            @Override
            public void onFailure(Call<BookDataResponse> call, Throwable t) {
                Toast.makeText(mainActivity, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public  void getFeaturedBooksResponse(){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<BookDataResponse> call= apiSerivce.getnewFeaturedBookResponse();
        call.enqueue(new Callback<BookDataResponse>() {
            @Override
            public void onResponse(Call<BookDataResponse> call, Response<BookDataResponse> response) {
                int status_code= response.code();
                BookDataResponse bookDataResponse= response.body();
                if(bookDataResponse!=null){
                    List<BookType> bookTypeFeaturedList=bookDataResponse.getData();
                    if(bookTypeFeaturedList!=null){
                        for (int i = 0; i <bookTypeFeaturedList.size() ; i++) {
                            featureBookDataList=bookTypeFeaturedList.get(i).getFeaturedProduct();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BookDataResponse> call, Throwable t) {
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void init(List<Advertise> advertiseList) {
       BannerAdapter bannerAdapter= new BannerAdapter(getContext(),advertiseList);
       viewPager.setAdapter( bannerAdapter);


      /*  CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);
*/
        NUM_PAGES =advertiseList.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
     /*   indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });*/

    }

    public  void getHomePageTopLatestProductsResponse(){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<BookDataResponse> call= apiSerivce.getHomePageTopLatestProductsResponse();
        call.enqueue(new Callback<BookDataResponse>() {
            @Override
            public void onResponse(Call<BookDataResponse> call, Response<BookDataResponse> response) {
                int status_code= response.code();
                BookDataResponse featuredBookResponse= response.body();
                if(featuredBookResponse!=null){
                    List<BookType> bookTypeFeaturedList=featuredBookResponse.getData();
                    List<BookData> releaseBookList= new ArrayList<>();
                    List<BookData> featureBookList= new ArrayList<>();
                    for (int i = 0; i <bookTypeFeaturedList.size() ; i++) {
                        releaseBookList=bookTypeFeaturedList.get(i).getNewreleaseProduct();
                        featureBookList=bookTypeFeaturedList.get(i).getFeaturedProduct();
                    }

                    List<BookData> finalFeatureBookList = featureBookList;
                    if(finalFeatureBookList!=null && finalFeatureBookList.size()>0){
                        allTxt.setVisibility(View.VISIBLE);
                        viewAllFeaturedBooks.setVisibility(View.VISIBLE);
                        bookAdapter = new BookAdapter(getActivity(),finalFeatureBookList, new BookAdapter.BookClicked() {
                            @Override
                            public void Clicked(View view, int position) {
                                Intent intent =new Intent(getContext(),BookDetailsActivity.class);
                                intent.putExtra("book_id", finalFeatureBookList.get(position).getMainProdId());
                                startActivity(intent);

                            }
                        });

                        GridLayoutManager gridLayoutManager=new GridLayoutManager(getContext(),2);
                        // LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false);
                        featuredBooksList.setLayoutManager(gridLayoutManager);
                        featuredBooksList.setAdapter(bookAdapter);
                    }

                    List<BookData> finalReleaseBookList = releaseBookList;
                    if(finalReleaseBookList!= null && finalReleaseBookList.size()>0){
                        bookAdapter = new BookAdapter(getActivity(),finalReleaseBookList, new BookAdapter.BookClicked() {
                            @Override
                            public void Clicked(View view, int position) {
                                Intent intent =new Intent(getContext(), BookDetailsActivity.class);
                                intent.putExtra("book_id", finalReleaseBookList.get(position).getMainProdId());
                                startActivity(intent);
                            }
                        });

                        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false);
                        newReleseBooksList.setLayoutManager(linearLayoutManager);
                        newReleseBooksList.setNestedScrollingEnabled(false);
                        newReleseBooksList.setAdapter(bookAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<BookDataResponse> call, Throwable t) {
                Toast.makeText(mainActivity, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public  void getHomePageRecommendedProductsResponse(String token){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GetRecommendedResponse> call= apiSerivce.getHomePageRecommendedProductsResponse(token);
        call.enqueue(new Callback<GetRecommendedResponse>() {
            @Override
            public void onResponse(Call<GetRecommendedResponse> call, Response<GetRecommendedResponse> response) {
                int status_code= response.code();
                GetRecommendedResponse getRecommendedResponse= response.body();
                if(getRecommendedResponse!=null){
                    List<BookData> recommendedBookList =getRecommendedResponse.getData();
                    if(recommendedBookList!=null && recommendedBookList.size()>0){
                        allRecommendedLayout.setVisibility(View.VISIBLE);
                        view4.setVisibility(View.VISIBLE);
                        bookAdapter = new BookAdapter(getActivity(),recommendedBookList, new BookAdapter.BookClicked() {
                            @Override
                            public void Clicked(View view, int position) {
                                Intent intent =new Intent(getContext(),BookDetailsActivity.class);
                                intent.putExtra("book_id", recommendedBookList.get(position).getMainProdId());
                                startActivity(intent);

                            }
                        });

                       // GridLayoutManager gridLayoutManager=new GridLayoutManager(getContext(),2);
                        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false);
                        recommendedBooksList.setLayoutManager(linearLayoutManager);
                        recommendedBooksList.setAdapter(bookAdapter);
                    }

                }
            }

            @Override
            public void onFailure(Call<GetRecommendedResponse> call, Throwable t) {
              //  Toast.makeText(mainActivity, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    public  void getHomePageRecommendedAllProductsResponse(String token){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GetRecommendedResponse> call= apiSerivce.getHomePageRecommendedAllProductsResponse(token);
        call.enqueue(new Callback<GetRecommendedResponse>() {
            @Override
            public void onResponse(Call<GetRecommendedResponse> call, Response<GetRecommendedResponse> response) {
                int status_code= response.code();
                GetRecommendedResponse getRecommendedResponse= response.body();
                if(getRecommendedResponse!=null){
                    recommendedBookDataList =getRecommendedResponse.getData();
                }
            }

            @Override
            public void onFailure(Call<GetRecommendedResponse> call, Throwable t) {
             //   Toast.makeText(mainActivity, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    
    public  void getTopBannerResponse(){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<AdvertiseResponse> call= apiSerivce.getTopBannerResponse();
        call.enqueue(new Callback<AdvertiseResponse>() {
            @Override
            public void onResponse(Call<AdvertiseResponse> call, Response<AdvertiseResponse> response) {
                int status_code= response.code();
                AdvertiseResponse advertiseResponse= response.body();
                if(advertiseResponse!=null){
                    List<Advertise> advertiseList =advertiseResponse.getData();
                    if(advertiseList!= null && advertiseList.size()>0){
                        init(advertiseList);
                    }

                }

                }

            @Override
            public void onFailure(Call<AdvertiseResponse> call, Throwable t) {
                Toast.makeText(mainActivity, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
}
    public  void getOffersResponse(){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<AdvertiseResponse> call= apiSerivce.getThirdPanelAdvertiseResponse();
        call.enqueue(new Callback<AdvertiseResponse>() {
            @Override
            public void onResponse(Call<AdvertiseResponse> call, Response<AdvertiseResponse> response) {
                int status_code= response.code();
                AdvertiseResponse advertiseResponse= response.body();
                if(advertiseResponse!=null){
                    List<Advertise> advertiseList =advertiseResponse.getData();
                    if(advertiseList!= null && advertiseList.size()>0){
                        OffersAdapter offersAdapter = new OffersAdapter(getContext(),advertiseList);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
                        offerRecyclerView.setLayoutManager(linearLayoutManager);
                        offerRecyclerView.setAdapter(offersAdapter);
                        offersAdapter.notifyDataSetChanged();
                    }
                    else {
                        offerTxt.setVisibility(View.GONE);
                    }

                }

            }

            @Override
            public void onFailure(Call<AdvertiseResponse> call, Throwable t) {
                Toast.makeText(mainActivity, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
