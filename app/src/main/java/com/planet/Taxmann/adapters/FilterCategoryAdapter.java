package com.planet.Taxmann.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.planet.Taxmann.R;
import com.planet.Taxmann.activity.FilterActivity;
import com.planet.Taxmann.model.FilterCategory;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterCategoryAdapter extends RecyclerView.Adapter<FilterCategoryAdapter.ViewHolder> {
    List<FilterCategory> categorylist = new ArrayList<>();
    Context context;
    OnClicked onclicked;
    int row_index =0;

    FilterActivity filterActivity;

    public FilterCategoryAdapter(List<FilterCategory> categorylist, Context context, OnClicked onclicked) {
        this.categorylist = categorylist;
        this.context = context;
        this.onclicked = onclicked;

    }
    public interface OnClicked{
        void Clicked(View view, int position);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FilterCategoryAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_category_layout,null));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(categorylist!=null && categorylist.size()>0){
            holder.filterCat.setText(categorylist.get(position).getText());
        }
        holder.layout.setTag(position);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int pos =(int)view.getTag();
                if(onclicked!=null){
                    onclicked.Clicked(view,pos);
                }

                row_index=position;
                notifyDataSetChanged();
            }
        });
if(categorylist.get(position).getCount()>0){
    holder.count.setVisibility(View.VISIBLE);
}
else {
    holder.count.setVisibility(View.GONE);
}
        holder.count.setText(String.valueOf(categorylist.get(position).getCount()));


        if(row_index==position){

            holder.layout.setBackgroundColor(Color.parseColor("#FEE6D6"));
            holder.filterCat.setTextColor(Color.parseColor("#000000"));
        }
        else
        {

            holder.layout.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.filterCat.setTextColor(Color.parseColor("#000000"));
        }

    }

    @Override
    public int getItemCount() {
        return categorylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.filter_cat)
        TextView filterCat;
        @BindView(R.id.rel)
        RelativeLayout layout;
        @BindView(R.id.f_count)
        TextView count;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
