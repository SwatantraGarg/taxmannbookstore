package com.planet.Taxmann.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.ReviewAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.PointWiseStar;
import com.planet.Taxmann.model.ProductRatingDetailsResponse;
import com.planet.Taxmann.model.RatingData;
import com.planet.Taxmann.model.ReviewList;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewsActivity extends AppCompatActivity {
    @BindView(R.id.rate_and_review_text)
    TextView rateNReview;
    @BindView(R.id.found_rating)
    TextView foundRating;
    @BindView(R.id.RatingBar)
    RatingBar ratingBarLarge;
    @BindView(R.id.review_layout)
    LinearLayout reviewLayout;
    @BindView(R.id.progress_bar5)
    ProgressBar progressBar5;

    @BindView(R.id.progress_bar4)
    ProgressBar progressBar4;

    @BindView(R.id.progress_bar3)
    ProgressBar progressBar3;

    @BindView(R.id.progress_bar2)
    ProgressBar progressBar2;

    @BindView(R.id.progress_bar1)
    ProgressBar progressBar1;

    @BindView(R.id.rate_5)
    TextView rate5;

    @BindView(R.id.rate_4)
    TextView rate4;

    @BindView(R.id.rate_3)
    TextView rate3;

    @BindView(R.id.rate_2)
    TextView rate2;

    @BindView(R.id.rate_1)
    TextView rate1;
    @BindView(R.id.review_count_text)
    TextView reviewCountTxt;
    @BindView(R.id.recyclerview_review_list)
    RecyclerView recyclerViewReview;
    @BindView(R.id.add_review_btn)
    Button addreviewBtn;
    Intent intent;
    String book_id,book_img_url,book_name;
    UserData userData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.review_layout);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(ReviewsActivity.this,"user_data",UserData.class);
        intent = getIntent();
        if(intent!=null){
            book_id = intent.getStringExtra("book_id");
            book_name = intent.getStringExtra("book_name");
            book_img_url = intent.getStringExtra("book_image_url");
        }
        getRatingResponse(book_id);
    }
    public void getRatingResponse(String book_id){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<ProductRatingDetailsResponse> call= apiSerivce.getProductRatingDetailsResponse(book_id);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(ReviewsActivity.this,"Please wait");
        call.enqueue(new Callback<ProductRatingDetailsResponse>() {

            @Override
            public void onResponse(Call<ProductRatingDetailsResponse> call, Response<ProductRatingDetailsResponse> response) {
                kProgressHUD.dismiss();
                // Constants.hideSoftKeyboard(BookDetailsActivity.this);
                int statusCode = response.code();
                ProductRatingDetailsResponse productRatingDetailsResponse=response.body();

                if(productRatingDetailsResponse.getData()!=null){
                    RatingData ratingData= productRatingDetailsResponse.getData();

                    rateNReview.setText(ratingData.getProductsRatingDetailsData().getRatingCount()+" Ratings and "+ratingData.getProductsRatingDetailsData().getReviewCount()+" Reviews");
                    foundRating.setText(ratingData.getProductsRatingDetailsData().getFoundRating());
                    int count=0;
                    for (int i = 0; i < ratingData.getProductsRatingDetailsData().getStarRating().size(); i++) {
                        if(ratingData.getProductsRatingDetailsData().getStarRating().get(i).equalsIgnoreCase("1")){
                            count++;
                        }
                    }
                    if(count==0){
                        ratingBarLarge.setRating(0);
                    }
                    else {
                        ratingBarLarge.setRating(count);
                    }
                    Drawable progress = ratingBarLarge.getProgressDrawable();
                    DrawableCompat.setTint(progress,getResources().getColor(R.color.star_color));
                    PointWiseStar pointWiseStar = ratingData.getProductsRatingDetailsData().getPointWiseStar();
                    if(pointWiseStar!=null){
                        rate1.setText(pointWiseStar.getStarOne());
                        rate2.setText(pointWiseStar.getStarTwo());
                        rate3.setText(pointWiseStar.getStarThree());
                        rate4.setText(pointWiseStar.getStarFour());
                        rate5.setText(pointWiseStar.getStarFive());
                    }
                    int rating_count= Integer.parseInt(ratingData.getProductsRatingDetailsData().getRatingCount());
                    int rate5= Integer.parseInt(pointWiseStar.getStarFive());
                    Double res5 = ((double)rate5/rating_count) * 100;
                    int progress5= (int)res5.doubleValue();
                    progressBar5.setProgress(progress5);

                    int rate4= Integer.parseInt(pointWiseStar.getStarFour());
                    Double res4 = ((double)rate4/rating_count) * 100;
                    int progress4= (int)res4.doubleValue();
                    progressBar4.setProgress(progress4);

                    int rate3= Integer.parseInt(pointWiseStar.getStarThree());
                    Double res3 = ((double)rate3/rating_count) * 100;
                    int progress3= (int)res3.doubleValue();
                    progressBar3.setProgress(progress3);

                    int rate2= Integer.parseInt(pointWiseStar.getStarTwo());
                    Double res2 = ((double)rate2/rating_count) * 100;
                    int progress2= (int)res2.doubleValue();
                    if(progress2<=0){
                        progressBar2.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar));
                    }
                    else {
                        progressBar2.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar_yellow));
                    }
                    progressBar2.setProgress(progress2);

                    int rate1= Integer.parseInt(pointWiseStar.getStarOne());
                    Double res1 = ((double)rate1/rating_count) * 100;
                    int progress1= (int)res1.doubleValue();
                    if(progress1<=0){
                        progressBar1.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar));
                    }
                    else {
                        progressBar1.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar_red));
                    }
                    progressBar1.setProgress(progress1);

                    List<ReviewList> reviewList =ratingData.getReviewList();
                    if(reviewList!=null && reviewList.size()>0){
                        reviewLayout.setVisibility(View.VISIBLE);
                        reviewCountTxt.setText("Showing "+ratingData.getProductsRatingDetailsData().getReviewCount()+" reviews");
                        ReviewAdapter reviewAdapter =new ReviewAdapter(ReviewsActivity.this,reviewList);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ReviewsActivity.this,LinearLayoutManager.VERTICAL,false);
                        recyclerViewReview.setLayoutManager(linearLayoutManager);
                        recyclerViewReview.setNestedScrollingEnabled(false);
                        recyclerViewReview.setAdapter(reviewAdapter);
                    }



                    addreviewBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(userData.getUser_token()!=null && !userData.getUser_token().equalsIgnoreCase("")) {
                                showDialogforAddReview();
                            } else{
                                Intent intent = new Intent(ReviewsActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }
                        }
                    });

                }

            }

            @Override
            public void onFailure(Call<ProductRatingDetailsResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                //Constants.hideSoftKeyboard(BookDetailsActivity.this);
                Toast.makeText(ReviewsActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    public void showDialogforAddReview(){
        Dialog dialog= new Dialog(ReviewsActivity.this);
        dialog.setContentView(R.layout.add_review_layout);
        dialog.show();
        Window window= dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        EditText headEdit=(EditText)dialog.findViewById(R.id.head_edit);
        EditText reviewEdit=(EditText) dialog.findViewById(R.id.review_edit);
        ImageView bookImg=(ImageView) dialog.findViewById(R.id.book_img);
        TextView bookName=(TextView)dialog.findViewById(R.id.book_name);
        RatingBar ratingBar=(RatingBar) dialog.findViewById(R.id.myRatingBar);
        TextView submitBtn = (TextView) dialog.findViewById(R.id.review_submit_btn);
        TextView cancelBtn = (TextView)dialog.findViewById(R.id.cancel);

        Drawable progress1 = ratingBar.getProgressDrawable();
        DrawableCompat.setTint(progress1, getResources().getColor(R.color.star_color));

        if(book_name!=null){
            bookName.setText(book_name);
        }
        if(book_img_url!=null){
            Glide.with(ReviewsActivity.this).load(book_img_url).placeholder(R.drawable.demo_book).into(bookImg);
        }

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    String head= headEdit.getText().toString();
                    String body= reviewEdit.getText().toString();
                    int rating_count= (int) ratingBar.getRating();
                if(rating_count==0){
                    Toast.makeText(ReviewsActivity.this, "Rating is required", Toast.LENGTH_SHORT).show();
                }else {
                    submitReview(dialog,head,body,rating_count, Integer.parseInt(book_id));
                }
                }

        });
    }
    public void submitReview(Dialog dialog,String head, String body, int rating, int book_id){
        userData = DroidPrefs.get(ReviewsActivity.this,"user_data", UserData.class);

        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("rating",rating);
            jsonObject.put("heading",head);
            jsonObject.put("comments",body);
            jsonObject.put("prodID",book_id);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.submitReviewResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(ReviewsActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(ReviewsActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    getRatingResponse(String.valueOf(book_id));
                    dialog.dismiss();
                    Toast.makeText(ReviewsActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
                else {
                    dialog.dismiss();
                    Toast.makeText(ReviewsActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(ReviewsActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
}
