package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class PointWiseStar {
    @SerializedName("starOne")
    @Expose
    private String starOne;
    @SerializedName("starTwo")
    @Expose
    private String starTwo;
    @SerializedName("starThree")
    @Expose
    private String starThree;
    @SerializedName("starFour")
    @Expose
    private String starFour;
    @SerializedName("starFive")
    @Expose
    private String starFive;

    public String getStarOne() {
        return starOne;
    }

    public void setStarOne(String starOne) {
        this.starOne = starOne;
    }

    public String getStarTwo() {
        return starTwo;
    }

    public void setStarTwo(String starTwo) {
        this.starTwo = starTwo;
    }

    public String getStarThree() {
        return starThree;
    }

    public void setStarThree(String starThree) {
        this.starThree = starThree;
    }

    public String getStarFour() {
        return starFour;
    }

    public void setStarFour(String starFour) {
        this.starFour = starFour;
    }

    public String getStarFive() {
        return starFive;
    }

    public void setStarFive(String starFive) {
        this.starFive = starFive;
    }
}
