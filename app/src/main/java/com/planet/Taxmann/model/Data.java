package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
    @SerializedName("statesList")
    @Expose
    private List<StatesList> statesList = null;
    @SerializedName("professionList")
    @Expose
    private List<ProfessionList> professionList = null;
    @SerializedName("aoiList")
    @Expose
    private List<AoiList> aoiList = null;
    @SerializedName("professionOther")
    @Expose
    private String professionOther;

    public List<StatesList> getStatesList() {
        return statesList;
    }

    public void setStatesList(List<StatesList> statesList) {
        this.statesList = statesList;
    }

    public List<ProfessionList> getProfessionList() {
        return professionList;
    }

    public void setProfessionList(List<ProfessionList> professionList) {
        this.professionList = professionList;
    }

    public List<AoiList> getAoiList() {
        return aoiList;
    }

    public void setAoiList(List<AoiList> aoiList) {
        this.aoiList = aoiList;
    }

    public String getProfessionOther() {
        return professionOther;
    }

    public void setProfessionOther(String professionOther) {
        this.professionOther = professionOther;
    }
}
