package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.AuthorAdapter;
import com.planet.Taxmann.adapters.BookAdapter;
import com.planet.Taxmann.adapters.ReviewAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.BookDetails;
import com.planet.Taxmann.model.Books;
import com.planet.Taxmann.model.CartData;
import com.planet.Taxmann.model.CartList;
import com.planet.Taxmann.model.DeliveryCheckResponse;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.GetBookDetailsResponse;
import com.planet.Taxmann.model.GetCartResponse;
import com.planet.Taxmann.model.Journals;
import com.planet.Taxmann.model.MySpannable;
import com.planet.Taxmann.model.PointWiseStar;
import com.planet.Taxmann.model.PostLoginSocialUser;
import com.planet.Taxmann.model.ProductRatingDetailsResponse;
import com.planet.Taxmann.model.ProductsBindingTypesDetails;
import com.planet.Taxmann.model.RatingData;
import com.planet.Taxmann.model.ReviewList;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;
import com.planet.Taxmann.utils.ShareUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Action;
import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nikartm.support.ImageBadgeView;
import ru.nikartm.support.model.Badge;
import uk.co.senab.photoview.PhotoViewAttacher;

public class BookDetailsActivity extends AppCompatActivity {
    @BindView(R.id.book_image)
    ImageView bookImage;

    @BindView(R.id.book_title)
    TextView bookTitle;

    @BindView(R.id.author_name)
    TextView authorName;

    @BindView(R.id.myRatingBar)
    RatingBar ratingBar;

    @BindView(R.id.rate_n_review_text)
    TextView rateNReviewText;

    @BindView(R.id.sample_chapter)
    TextView sampleChapterBtn;

    @BindView(R.id.view_content)
    TextView viewContentBtn;

    @BindView(R.id.radio_group)
    RadioGroup radioGroup;

    @BindView(R.id.stock_txt)
    TextView stockTxt;

    @BindView(R.id.indian_price)
    TextView indPrice;

    @BindView(R.id.us_price)
    TextView usPrice;

    @BindView(R.id.book_detail_text)
    TextView bookDetailTxt;

    @BindView(R.id.view_more_btn)
    TextView viewMoreBtn;

    @BindView(R.id.pin_code)
    EditText pinCodeEdit;

    @BindView(R.id.pin_code_check)
    TextView pinCodeCheck;

    @BindView(R.id.scrollView)
    ScrollView sv;

    @BindView(R.id.publication)
    TextView publicationTxt;

    @BindView(R.id.isbn)
    TextView isbnTxt;

    @BindView(R.id.edition)
    TextView editionTxt;

    @BindView(R.id.no_page)
    TextView noPageTxt;

    @BindView(R.id.author)
    TextView authorTxt;

    @BindView(R.id.weight)
    TextView weightTxt;

    @BindView(R.id.about_book_txt)
    WebView aboutBookTxt;

    @BindView(R.id.recycler_view_author)
    RecyclerView recyclerViewAuthors;

    @BindView(R.id.recycler_view_other_books)
    RecyclerView recyclerViewOtherBooks;

    @BindView(R.id.del_info_layout)
    LinearLayout delInfoLayout;

    @BindView(R.id.del_info)
    TextView delInfo;

    @BindView(R.id.found_rating)
    TextView foundRating;

    @BindView(R.id.RatingBar)
    RatingBar ratingBarLarge;

    @BindView(R.id.rate_and_review_text)
    TextView rateNReview;

    @BindView(R.id.view_reviews)
    TextView viewReviews;

    @BindView(R.id.progress_bar5)
    ProgressBar progressBar5;

    @BindView(R.id.progress_bar4)
    ProgressBar progressBar4;

    @BindView(R.id.progress_bar3)
    ProgressBar progressBar3;

    @BindView(R.id.progress_bar2)
    ProgressBar progressBar2;

    @BindView(R.id.progress_bar1)
    ProgressBar progressBar1;

    @BindView(R.id.rate_5)
    TextView rate5;

    @BindView(R.id.rate_4)
    TextView rate4;

    @BindView(R.id.rate_3)
    TextView rate3;

    @BindView(R.id.rate_2)
    TextView rate2;

    @BindView(R.id.rate_1)
    TextView rate1;

    @BindView(R.id.review_count_text)
    TextView reviewCountTxt;

    @BindView(R.id.recyclerview_review_list)
    RecyclerView recyclerViewReview;

    @BindView(R.id.review_layout)
    LinearLayout reviewLayout;

    @BindView(R.id.del_layout)
    LinearLayout delLayout;

    @BindView(R.id.free_ship_txt)
    TextView freeShipTxt;

    @BindView(R.id.price_layout)
    LinearLayout priceLayout;

    @BindView(R.id.add_review_btn)
    Button addreviewBtn;

    @BindView(R.id.buy_now)
    TextView buyNowBtn;

    @BindView(R.id.wish_img)
    ImageView wishImg;

    @BindView(R.id.fb_layout)
    LinearLayout fbLayout;

    @BindView(R.id.twitter_layout)
    LinearLayout twitterLayout;

    @BindView(R.id.email_layout)
    LinearLayout emailLayout;

    @BindView(R.id.what_layout)
    LinearLayout whatLayout;

    @BindView(R.id.sample_issue)
    TextView sampleIssueTxt;

    @BindView(R.id.pre_face)
    TextView preFaceTxt;

    @BindView(R.id.return_txt_layout)
    LinearLayout returnTxtLayout;

    @BindView(R.id.shipping_txt_layout)
    LinearLayout shipTxtLayout;

    @BindView(R.id.details)
    TextView detailsTxt;

    @BindView(R.id.details_layout)
    RelativeLayout detailsLayout;

    @BindView(R.id.duration_layout)
    LinearLayout durationLayout;

    @BindView(R.id.add_to_cart)
    TextView addToCartBtn;

    @BindView(R.id.also_bought)
            TextView alsoBought;

    @BindView(R.id.about_author)
            TextView aboutAuthor;

    @BindView(R.id.description)
    TextView description;

    @BindView(R.id.del_txt)
    TextView delTxt;

    @BindView(R.id.detail_link)
    TextView detailLink;

    @BindView(R.id.print_book_image)
    ImageView printBookImg;

    @BindView(R.id.short_des)
    TextView shortDes;

    @BindView(R.id.avail_txt)
    TextView availTxt;

    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.price_view)
    View priceView;

    @BindView(R.id.cart)
    ImageBadgeView cartImg;

    @BindView(R.id.preorder_date)
            TextView preorderDate;

    @BindView(R.id.rel_date_txt)
    TextView relDateTxt;

    @BindView(R.id.preorder_layout)
    LinearLayout preorderLayout;

    @BindView(R.id.preorder_head)
    TextView preorderHead;

    @BindView(R.id.ship_text_2)
    TextView shipTxt2;

    @BindView(R.id.ads_layout)
    LinearLayout adsLayout;

    @BindView(R.id.covid_img)
    ImageView covidImg;
    Books books;
    Journals journals;
    RadioButton rbn;
    Intent intent;
    String book_id,book_name,book_img_url, main_prod_id;
    UserData userData;
    List<ProductsBindingTypesDetails> bindingTypesDetailsList = new ArrayList<>();
    boolean ISEBOOK_CHECKED = false;
    boolean ISEJOURNAL_CHECKED = false;
    boolean IS_WISH_TAP = false;
    public  String ebook_id;
    GetBookDetailsResponse getBookDetailsResponse;
    PhotoViewAttacher pAttacher;;
    public String SHARE_URL = "https://www.taxmann.com/bookstore/product/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(BookDetailsActivity.this,"user_data",UserData.class);
        intent=getIntent();

        if(intent!=null){
            book_id=intent.getStringExtra("book_id");
        }
        getBookDetailsResponse(book_id,"");
        getRatingResponse(book_id);
        String token = userData.getUser_token();
        if(token!=null &&!token.equalsIgnoreCase("")){
            getcartDetailsResponse(token);
        }
        pAttacher = new PhotoViewAttacher(bookImage);
        pAttacher.update();
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookDetailsActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        sampleIssueTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userData.getUser_token()!=null && !userData.getUser_token().equalsIgnoreCase("")){
                Intent intent = new Intent(BookDetailsActivity.this, SampleIssueActivity.class);
                intent.putExtra("book_id",book_id);
                startActivity(intent);
            }else {
                    Intent intent = new Intent(BookDetailsActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        cartImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userData.getUser_token()!=null && !userData.getUser_token().equalsIgnoreCase("")){
                Intent intent = new Intent(BookDetailsActivity.this,CartActivity.class);
                intent.putExtra("type","detail");
                startActivity(intent);
            }
                else {
                    Intent intent = new Intent(BookDetailsActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        addToCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ISEBOOK_CHECKED){

                }else {
                    if(userData.getUser_token()!=null && !userData.getUser_token().equalsIgnoreCase("")){
                        if(main_prod_id!=null &&!main_prod_id.equalsIgnoreCase("")){
                            addProductTocartResponse(main_prod_id,"0","C");
                        }
                    }
                    else {
                        Intent intent = new Intent(BookDetailsActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });

        printBookImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(books!=null && books.getVirtualBookDetails().size()>0){
                    for (int i = 0; i <books.getVirtualBookDetails().size() ; i++) {
                        String id = String.valueOf(books.getVirtualBookDetails().get(i).getBookId());
                        getBookDetailsResponse(id,"");
                        getRatingResponse(id);
                    }
                }
            }
        });

        detailLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(BookDetailsActivity.this, WebviewActivity.class);
                browserIntent.putExtra("url","https://www.taxmann.com/legal/return-policy");
                startActivity(browserIntent);
            }
        });

        whatLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getBookDetailsResponse.getData().getItemType()==3){
                    ShareUtils.shareWhatsapp(BookDetailsActivity.this,journals.getBookTitle(),SHARE_URL+journals.getUrl());

                }else {
                    ShareUtils.shareWhatsapp(BookDetailsActivity.this,books.getBookTitle(),SHARE_URL+books.getUrl());

                }
                           }
        });

        fbLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getBookDetailsResponse.getData().getItemType()==3){
                    ShareUtils.shareFacebook(BookDetailsActivity.this,journals.getBookTitle(),SHARE_URL+journals.getUrl());
                }else {
                    ShareUtils.shareFacebook(BookDetailsActivity.this,books.getBookTitle(),SHARE_URL+books.getUrl());

                }
            }
        });

        twitterLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getBookDetailsResponse.getData().getItemType()==3){
                    ShareUtils.shareTwitter(BookDetailsActivity.this,journals.getBookTitle(),SHARE_URL+journals.getUrl()+" #taxmannindia","","");

                }else {
                    ShareUtils.shareTwitter(BookDetailsActivity.this,books.getBookTitle(),SHARE_URL+books.getUrl()+" #taxmannindia","","");

                }
                           }
        });

        emailLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getBookDetailsResponse.getData().getItemType()==3){
                    Intent share = new Intent(android.content.Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    share.putExtra(Intent.EXTRA_SUBJECT, journals.getBookTitle());
                    share.putExtra(Intent.EXTRA_TEXT, SHARE_URL+journals.getUrl());
                    startActivity(Intent.createChooser(share, "Share link!"));
                }else {
                    Intent share = new Intent(android.content.Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    share.putExtra(Intent.EXTRA_SUBJECT, books.getBookTitle());
                    share.putExtra(Intent.EXTRA_TEXT, SHARE_URL+books.getUrl());
                    startActivity(Intent.createChooser(share, "Share link!"));
                }

            }
        });

        wishImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userData.getUser_token()!=null && !userData.getUser_token().equalsIgnoreCase("")){
                    if(IS_WISH_TAP){
                        getRemoveWishListResponse(book_id);
                        wishImg.setImageResource(R.drawable.ic_non_wishlist);
                        // wishImg.setBackgroundResource(R.drawable.wish_bg_btn);
                        IS_WISH_TAP = false;
                    }
                    else {
                        getPostWishListResponse(book_id);
                        wishImg.setImageResource(R.drawable.ic_wishlist);
                        // wishImg.setBackgroundResource(R.drawable.circle_fill_orange);
                        IS_WISH_TAP = true;
                    }
                }
                else {
                    Intent intent = new Intent(BookDetailsActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });

        buyNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userData.getUser_token()!=null && !userData.getUser_token().equalsIgnoreCase("")){
                    if(main_prod_id!=null &&!main_prod_id.equalsIgnoreCase("")){
                        if(ISEBOOK_CHECKED){
                            if(books!=null && !books.getEBookUrl().equalsIgnoreCase("")){
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(books.getEBookUrl()));
                                startActivity(browserIntent);
                            }
                        }
                        else if(ISEJOURNAL_CHECKED){
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(journals.getEBookUrl()));
                            startActivity(browserIntent);
                        }
                        else {
                            if(getBookDetailsResponse.getData().getItemType()==3){
                                if(main_prod_id!=null &&!main_prod_id.equalsIgnoreCase("")){
                                    addProductTocartResponse(main_prod_id,"0","C");
                                }
                            }
                            else {
                                if(books.getStock()){
                                    addProductTocartResponse(main_prod_id,"0","B");
                                    Intent intent = new Intent(BookDetailsActivity.this,CheckOutActivity.class);
                                    intent.putExtra("source","B");
                                    startActivity(intent);
                                }
                                else {
                                    showDialogNotifyme();
                                }
                            }
                        }
                    }
                }
                else {
                    Intent intent = new Intent(BookDetailsActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override public void onCheckedChanged(RadioGroup group, int checkedId)
            {
               RadioButton rbn = (RadioButton) findViewById(checkedId);
                int pos = -1;
                for (int i = 0; i <bindingTypesDetailsList.size() ; i++) {
                    if(bindingTypesDetailsList.get(i).getBindingTypes().equalsIgnoreCase(rbn.getText().toString())){
                        pos = i;
                    }
                }
                    book_id = String.valueOf(bindingTypesDetailsList.get(pos).getBookId());
                    getBookDetailsResponse(book_id, "radio");
                    getRatingResponse(book_id);

            }
        });

        covidImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://www.linkedin.com/posts/taxmann_taxmanncares-taxmannbooks-activity-6670931227412983809-iRgW/"));
                startActivity(intent);
            }
        });

        rateNReviewText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sv.fullScroll(View.FOCUS_DOWN);
            }
        });

        pinCodeCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pinCodeEdit.getText().toString().isEmpty()){
                    Toast.makeText(BookDetailsActivity.this, "Please enter pin code", Toast.LENGTH_SHORT).show();
                }
                else if(pinCodeEdit.getText().toString().length()<4){
                    Toast.makeText(BookDetailsActivity.this, "pin code digits should be atleast 4.", Toast.LENGTH_SHORT).show();
                }
                else {
                    String pinCode = pinCodeEdit.getText().toString();
                    getDeliveryCheckResponse(pinCode);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getRatingResponse(book_id);
    }

    public void getBookDetailsResponse(String book_ID, String type){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(BookDetailsActivity.this,"Please wait");
        Call<GetBookDetailsResponse> call= apiSerivce.getProductDetailsResponse(book_ID,userData.getUser_token());

        call.enqueue(new Callback<GetBookDetailsResponse>() {
            @Override
            public void onResponse(Call<GetBookDetailsResponse> call, Response<GetBookDetailsResponse> response) {
                kProgressHUD.dismiss();
                int status_code= response.code();
                getBookDetailsResponse = response.body();
                if(getBookDetailsResponse.getData()!=null){
                    if(getBookDetailsResponse.getData().getItemType()==4){
                        stockTxt.setVisibility(View.GONE);
                        freeShipTxt.setVisibility(View.GONE);
                        shipTxt2.setVisibility(View.GONE);
                        priceLayout.setVisibility(View.GONE);
                        delLayout.setVisibility(View.GONE);
                        returnTxtLayout.setVisibility(View.GONE);
                        shipTxtLayout.setVisibility(View.GONE);
                        detailsLayout.setVisibility(View.GONE);
                        detailsTxt.setVisibility(View.GONE);
                        delInfoLayout.setVisibility(View.GONE);
                        addToCartBtn.setVisibility(View.GONE);
                        ISEBOOK_CHECKED = true;
                    }
                    else if(getBookDetailsResponse.getData().getItemType()==11 || getBookDetailsResponse.getData().getItemType()==10){
                        stockTxt.setVisibility(View.GONE);
                        addToCartBtn.setVisibility(View.GONE);
                        availTxt.setVisibility(View.GONE);
                        freeShipTxt.setVisibility(View.GONE);
                        shipTxt2.setVisibility(View.GONE);
                        delLayout.setVisibility(View.GONE);
                        returnTxtLayout.setVisibility(View.GONE);
                        shipTxtLayout.setVisibility(View.GONE);
                        detailsLayout.setVisibility(View.GONE);
                        detailsTxt.setVisibility(View.GONE);
                        delInfoLayout.setVisibility(View.GONE);
                        priceView.setVisibility(View.GONE);
                        usPrice.setVisibility(View.GONE);
                    }
                    else if(getBookDetailsResponse.getData().getItemType()==2){
                        freeShipTxt.setVisibility(View.GONE);
                        shipTxt2.setVisibility(View.GONE);
                        delLayout.setVisibility(View.GONE);
                        returnTxtLayout.setVisibility(View.GONE);
                        shipTxtLayout.setVisibility(View.GONE);
                        detailsLayout.setVisibility(View.GONE);
                        detailsTxt.setVisibility(View.GONE);
                        delInfoLayout.setVisibility(View.GONE);
                        priceView.setVisibility(View.GONE);
                        indPrice.setText("Rs. "+Math.round(getBookDetailsResponse.getData().getBooks().getBookPriceINR()));
                        usPrice.setVisibility(View.GONE);
                    }
                    else if(getBookDetailsResponse.getData().getItemType()==3){
                        if(getBookDetailsResponse.getData().getJournals()!=null){
                            stockTxt.setVisibility(View.GONE);
                            freeShipTxt.setVisibility(View.VISIBLE);
                            shipTxt2.setVisibility(View.GONE);
                            delTxt.setVisibility(View.VISIBLE);
                            freeShipTxt.setText(
                                    "♦ All In-Print Journals shall be sent through Registered Post, at no additional cost.\n\n"+
                                    "♦ In case of journal issues are missing, we will send the replacement part free of cost.");
                            delInfoLayout.setVisibility(View.GONE);
                            returnTxtLayout.setVisibility(View.GONE);
                            shipTxtLayout.setVisibility(View.GONE);
                            detailsLayout.setVisibility(View.GONE);
                            detailsTxt.setVisibility(View.GONE);
                            pinCodeCheck.setVisibility(View.GONE);
                            pinCodeEdit.setVisibility(View.GONE);
                            priceView.setVisibility(View.GONE);
                            shortDes.setVisibility(View.GONE);
                            aboutAuthor.setVisibility(View.GONE);
                             journals = getBookDetailsResponse.getData().getJournals();
                            book_name= journals.getBookTitle();
                            book_img_url= journals.getBookImagesurl();
                            if(book_img_url!=null){
                                Glide.with(BookDetailsActivity.this).load(book_img_url).placeholder(R.drawable.demo_book).into(bookImage);
                            }
                            if(journals.getEBookUrl()!=null && !journals.getEBookUrl().equalsIgnoreCase("")){
                                ISEJOURNAL_CHECKED = true;
                                durationLayout.setVisibility(View.GONE);
                            }
                            else {
                               ISEJOURNAL_CHECKED= false;
                               durationLayout.setVisibility(View.VISIBLE);
                            }
                            if(journals.getIsSample()){
                                sampleIssueTxt.setVisibility(View.VISIBLE);
                            }else {
                                sampleIssueTxt.setVisibility(View.GONE);
                            }
                            main_prod_id= String.valueOf(journals.getBookId());
                            bookTitle.setText(journals.getBookTitle());
                            indPrice.setText("Rs. "+Math.round(journals.getBookPriceINR()));
                            usPrice.setVisibility(View.GONE);
                            if(journals.getProductsBindingTypesDetails()!=null){
                                if(journals.getProductsBindingTypesDetails().size()>=1){
                                    radioGroup.setVisibility(View.VISIBLE);
                                    radioGroup.setOrientation(LinearLayout.HORIZONTAL);

                                    bindingTypesDetailsList  =journals.getProductsBindingTypesDetails();
                                    if(bindingTypesDetailsList!=null && bindingTypesDetailsList.size()>0 && type.equalsIgnoreCase("")){
                                        for (int i = 0; i < bindingTypesDetailsList.size(); i++) {
                                            rbn = new RadioButton(BookDetailsActivity.this);
                                            rbn.setId(bindingTypesDetailsList.get(i).getBookId());
                                            rbn.setText(bindingTypesDetailsList.get(i).getBindingTypes());
                                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
                                            radioGroup.addView(rbn,params);
                                            if(journals.getBinding().equalsIgnoreCase(bindingTypesDetailsList.get(i).getBindingTypes()) ){
                                                rbn.setChecked(true);
                                            }
                                        }
                                    }
                                }
                            }

                            if(journals.getWishLishCount().toString().equalsIgnoreCase("1")){
                                wishImg.setImageResource(R.drawable.ic_wishlist);
                                // wishImg.setBackgroundResource(R.drawable.circle_fill_orange);
                                IS_WISH_TAP=true;
                            }
                            else {
                                wishImg.setImageResource(R.drawable.ic_non_wishlist);
                                // wishImg.setBackgroundResource(R.drawable.wish_bg_btn);
                                IS_WISH_TAP = false;
                            }
                            if(journals.getDescription()!=null && !journals.getDescription().equalsIgnoreCase("")){
                                description.setVisibility(View.VISIBLE);
                                aboutBookTxt.setVisibility(View.VISIBLE);
                                aboutBookTxt.loadDataWithBaseURL(null, journals.getDescription(), "text/html", "utf-8", null);
                            }
                            else {
                                description.setVisibility(View.GONE);
                                aboutBookTxt.setVisibility(View.GONE);
                            }
                            int count=0;
                            for (int i = 0; i < journals.getBookRating().size(); i++) {
                                if(journals.getBookRating().get(i).equalsIgnoreCase("1")){
                                    count++;
                                }
                            }
                            if(count==0){
                                ratingBar.setRating(0);
                            }
                            else {
                                ratingBar.setRating(count);
                            }
                            Drawable progress = ratingBar.getProgressDrawable();
                            DrawableCompat.setTint(progress,getResources().getColor(R.color.star_color));

                            if(journals.getAboutBook()!=null && !journals.getAboutBook().equalsIgnoreCase(""))
                            {
                                viewContentBtn.setVisibility(View.VISIBLE);
                            }
                            else {
                                viewContentBtn.setVisibility(View.INVISIBLE);
                            }
                            if(journals.getReaderavailable()!=null && journals.getReaderavailable()==true){
                                availTxt.setVisibility(View.VISIBLE);
                            }
                            else {
                                availTxt.setVisibility(View.GONE);
                            }

                            viewContentBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent= new Intent(Intent.ACTION_VIEW);
                                    intent.setData(Uri.parse(journals.getAboutBook()));
                                    startActivity(intent);
                                }
                            });
                            if(journals.getSampleIssue()!=null && !journals.getSampleIssue().equalsIgnoreCase(""))
                            {
                                sampleChapterBtn.setVisibility(View.VISIBLE);
                            }
                            else {
                                sampleChapterBtn.setVisibility(View.INVISIBLE);
                            }
                            sampleChapterBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent= new Intent(Intent.ACTION_VIEW);
                                    intent.setData(Uri.parse(journals.getSampleIssue()));
                                    startActivity(intent);
                                }
                            });
                            if(journals.getPreface()!=null && !journals.getPreface().equalsIgnoreCase(""))
                            {
                                preFaceTxt.setVisibility(View.VISIBLE);
                            }
                            else {
                                preFaceTxt.setVisibility(View.GONE);
                            }
                            preFaceTxt.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                        Intent intent= new Intent(Intent.ACTION_VIEW);
                                        intent.setData(Uri.parse(journals.getPreface()));
                                        startActivity(intent);
                                }
                            });
                            if(getBookDetailsResponse.getData().getItemType()==3){

                            }else {
                                if(getBookDetailsResponse.getData().getRelatedCombos()!=null && getBookDetailsResponse.getData().getRelatedCombos().size()>0){
                                    alsoBought.setVisibility(View.VISIBLE);
                                    BookAdapter bookAdapter =new BookAdapter(BookDetailsActivity.this,getBookDetailsResponse.getData().getRelatedCombos(), new BookAdapter.BookClicked() {
                                        @Override
                                        public void Clicked(View view, int position) {
                                            Intent intent =new Intent(BookDetailsActivity.this,BookDetailsActivity.class);
                                            intent.putExtra("book_id",getBookDetailsResponse.getData().getRelatedCombos().get(position).getMainProdId());
                                            startActivity(intent);
                                        }
                                    });
                                    LinearLayoutManager linearLayoutManager=new LinearLayoutManager(BookDetailsActivity.this,LinearLayoutManager.HORIZONTAL,false);
                                    recyclerViewOtherBooks.setLayoutManager(linearLayoutManager);
                                    recyclerViewOtherBooks.setAdapter(bookAdapter);
                                }
                                else {
                                    alsoBought.setVisibility(View.GONE);
                                }
                            }

                        }
                    }
                    else {
                        adsLayout.setVisibility(View.VISIBLE);
                        stockTxt.setVisibility(View.VISIBLE);
                        freeShipTxt.setVisibility(View.VISIBLE);
                        shipTxt2.setVisibility(View.VISIBLE);
                        priceLayout.setVisibility(View.VISIBLE);
                        priceView.setVisibility(View.VISIBLE);
                        usPrice.setVisibility(View.VISIBLE);
                        delLayout.setVisibility(View.VISIBLE);
                        returnTxtLayout.setVisibility(View.VISIBLE);
                        shipTxtLayout.setVisibility(View.VISIBLE);
                        detailsLayout.setVisibility(View.VISIBLE);
                        detailsTxt.setVisibility(View.VISIBLE);
                        addToCartBtn.setVisibility(View.VISIBLE);
                        ISEBOOK_CHECKED = false;
                    }

                    //set book details data
                    if(getBookDetailsResponse.getData().getBooks()!=null){
                      books = getBookDetailsResponse.getData().getBooks();
                      if(books.getVirtualBookDetails()!=null && books.getVirtualBookDetails().size()>0){
                          for (int i = 0; i <books.getVirtualBookDetails().size(); i++) {
                              if(books.getVirtualBookDetails().get(i).getBindingTypes().equalsIgnoreCase("Paperback")){
                                  printBookImg.setImageResource(R.drawable.avail_printed);
                              }
                              else {
                                  printBookImg.setImageResource(R.drawable.avail);
                              }
                          }
                          printBookImg.setVisibility(View.VISIBLE);
                      }
                      else {
                          printBookImg.setVisibility(View.GONE);
                      }
                        if(books.isReaderavailable()!=null && books.isReaderavailable()==true){
                            if(books.getProductType()==11){
                                availTxt.setVisibility(View.GONE);
                            }else {
                                availTxt.setVisibility(View.VISIBLE);
                            }
                        }
                        else {
                            availTxt.setVisibility(View.GONE);
                        }
                        book_name= books.getBookTitle();
                        book_img_url= books.getBookImagesurl();
                        main_prod_id= String.valueOf(books.getBookId());
                        bookTitle.setText(books.getBookTitle());
                        authorName.setText(books.getBookAuthor());
                        authorTxt.setText(books.getBookAuthor());
                        shortDes.setText(Html.fromHtml(books.getShortDesciption()));

                        if(books.getStock()){
                            stockTxt.setText("In Stock");
                            buyNowBtn.setText("Buy Now");
                            stockTxt.setTextColor(getResources().getColor(R.color.green_color));
                        }
                        else {
                            stockTxt.setText("Out of stock");
                            addToCartBtn.setVisibility(View.GONE);
                            buyNowBtn.setText("Notify me");
                            stockTxt.setTextColor(getResources().getColor(R.color.red_color));
                        }
                        if(books.getPreOrder()){
                            preorderLayout.setVisibility(View.VISIBLE);
                            if(books.getPreOrderReleaseDate()!=null && !books.getPreOrderReleaseDate().equalsIgnoreCase("")){
                                preorderDate.setText(books.getPreOrderReleaseDate());
                            }
                            else {
                                preorderDate.setVisibility(View.GONE);
                                relDateTxt.setVisibility(View.GONE);
                            }

                            preorderHead.setVisibility(View.VISIBLE);
                            stockTxt.setText("Pre Order");
                            stockTxt.setTextColor(getResources().getColor(R.color.green_color));
                        }
                        else {
                            preorderLayout.setVisibility(View.GONE);
                            preorderHead.setVisibility(View.GONE);
                        }
                        if(books.getWishLishCount().toString().equalsIgnoreCase("1")){
                            wishImg.setImageResource(R.drawable.ic_wishlist);
                           // wishImg.setBackgroundResource(R.drawable.circle_fill_orange);
                            IS_WISH_TAP=true;
                        }
                        else {
                            wishImg.setImageResource(R.drawable.ic_non_wishlist);
                           // wishImg.setBackgroundResource(R.drawable.wish_bg_btn);
                            IS_WISH_TAP = false;
                        }
                        publicationTxt.setText(books.getDateOfPublication());
                        isbnTxt.setText(books.getBookISBN());
                        editionTxt.setText(books.getEdition());
                        weightTxt.setText(books.getWeight()+" kg");

                        noPageTxt.setText(String.valueOf(books.getNoOfPages()+" pages"));
                        if(books.getDescription()!=null && !books.getDescription().equalsIgnoreCase("")){
                            description.setVisibility(View.VISIBLE);
                            aboutBookTxt.setVisibility(View.VISIBLE);
                            aboutBookTxt.loadDataWithBaseURL(null, books.getDescription(), "text/html", "utf-8", null);
                           /* if(aboutBookTxt.getText().toString().isEmpty()){

                            }
                            else {
                                makeTextViewResizable(aboutBookTxt, 10, "\n"+"View More", true);
                            }*/
                        }
                        else {
                            description.setVisibility(View.GONE);
                            aboutBookTxt.setVisibility(View.GONE);
                        }
if(books.getProductsBindingTypesDetails()!=null){
    if(books.getProductsBindingTypesDetails().size()>=1){
        radioGroup.setVisibility(View.VISIBLE);
        radioGroup.setOrientation(LinearLayout.HORIZONTAL);

        bindingTypesDetailsList  =books.getProductsBindingTypesDetails();
        if(bindingTypesDetailsList!=null && bindingTypesDetailsList.size()>0 && type.equalsIgnoreCase("")){
            for (int i = 0; i < bindingTypesDetailsList.size(); i++) {
                rbn = new RadioButton(BookDetailsActivity.this);
                rbn.setId(bindingTypesDetailsList.get(i).getBookId());
                rbn.setText(bindingTypesDetailsList.get(i).getBindingTypes());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
                radioGroup.addView(rbn,params);

                if(books.getBinding().equalsIgnoreCase(bindingTypesDetailsList.get(i).getBindingTypes()) ){
                    rbn.setChecked(true);
                }
            }
        }
    }
}
if(books.getBookPriceINR()==0){
    indPrice.setVisibility(View.GONE);
}
else {
    indPrice.setText("Rs. "+Math.round(books.getBookPriceINR()));
}

                        if(books.getBookPriceUSD()==0){
                         usPrice.setVisibility(View.GONE);
                        priceView.setVisibility(View.GONE);
                        }
                        else {
                            usPrice.setText("USD "+Math.round(books.getBookPriceUSD()));
                        }

                        if(books.getAboutBook()!=null && !books.getAboutBook().equalsIgnoreCase(""))
                        {
                            viewContentBtn.setVisibility(View.VISIBLE);
                        }else {
                            viewContentBtn.setVisibility(View.INVISIBLE);
                        }

                        viewContentBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent= new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(books.getAboutBook()));
                                startActivity(intent);
                            }
                        });
                        if(books.getSampleIssue()!=null && !books.getSampleIssue().equalsIgnoreCase(""))
                        {
                            sampleChapterBtn.setVisibility(View.VISIBLE);
                        }
                        else {
                            sampleChapterBtn.setVisibility(View.INVISIBLE);
                        }
                        sampleChapterBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                    Intent intent= new Intent(Intent.ACTION_VIEW);
                                    intent.setData(Uri.parse(books.getSampleIssue()));
                                    startActivity(intent);
                            }
                        });

                        if(books.getPreface()!=null && !books.getPreface().equalsIgnoreCase(""))
                        {
                            preFaceTxt.setVisibility(View.VISIBLE);
                        }
                        else {
                            preFaceTxt.setVisibility(View.GONE);
                        }
                        preFaceTxt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                    Intent intent= new Intent(Intent.ACTION_VIEW);
                                    intent.setData(Uri.parse(books.getPreface()));
                                    startActivity(intent);
                            }
                        });

                        if(books.getAboutAuthor()!=null && books.getAboutAuthor().size()>0){
                            aboutAuthor.setVisibility(View.VISIBLE);
                            AuthorAdapter authorAdapter = new AuthorAdapter(books.getAboutAuthor(),BookDetailsActivity.this);
                            LinearLayoutManager linearLayoutManager =new LinearLayoutManager(BookDetailsActivity.this,LinearLayoutManager.VERTICAL,false);
                            recyclerViewAuthors.setLayoutManager(linearLayoutManager);
                            recyclerViewAuthors.setNestedScrollingEnabled(false);
                            recyclerViewAuthors.setAdapter(authorAdapter);
                        }
                        else {
                            aboutAuthor.setVisibility(View.GONE);
                        }
                        Glide.with(BookDetailsActivity.this).load(books.getBookImagesurl()).placeholder(R.drawable.demo_book).into(bookImage);
                        int count=0;
                        for (int i = 0; i < books.getBookRating().size(); i++) {
                            if(books.getBookRating().get(i).equalsIgnoreCase("1")){
                                count++;
                            }
                        }
                        if(count==0){
                            ratingBar.setRating(0);
                        }
                        else {
                            ratingBar.setRating(count);
                        }
                        Drawable progress = ratingBar.getProgressDrawable();
                        DrawableCompat.setTint(progress,getResources().getColor(R.color.star_color));
                    }

                   if(getBookDetailsResponse.getData().getBookData()!=null && getBookDetailsResponse.getData().getBookData().size()>0){
                       alsoBought.setVisibility(View.VISIBLE);
                       BookAdapter bookAdapter =new BookAdapter(BookDetailsActivity.this,getBookDetailsResponse.getData().getBookData(), new BookAdapter.BookClicked() {
                           @Override
                           public void Clicked(View view, int position) {
                               Intent intent =new Intent(BookDetailsActivity.this,BookDetailsActivity.class);
                               intent.putExtra("book_id",getBookDetailsResponse.getData().getBookData().get(position).getMainProdId());
                               startActivity(intent);
                           }
                       });
                       LinearLayoutManager linearLayoutManager=new LinearLayoutManager(BookDetailsActivity.this,LinearLayoutManager.HORIZONTAL,false);
                       recyclerViewOtherBooks.setLayoutManager(linearLayoutManager);
                       recyclerViewOtherBooks.setAdapter(bookAdapter);
                   }
                   else {
                       alsoBought.setVisibility(View.GONE);
                   }
                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(BookDetailsActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<GetBookDetailsResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(BookDetailsActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void getDeliveryCheckResponse(String pin_code){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<DeliveryCheckResponse> call= apiSerivce.getDeliveryCheckResponse(pin_code);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(BookDetailsActivity.this,"Please wait");
        call.enqueue(new Callback<DeliveryCheckResponse>() {
            @Override
            public void onResponse(Call<DeliveryCheckResponse> call, Response<DeliveryCheckResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(BookDetailsActivity.this);
                int statusCode = response.code();
                DeliveryCheckResponse deliveryCheckResponse=response.body();
                if(deliveryCheckResponse!=null){
                    delInfoLayout.setVisibility(View.VISIBLE);
                    delInfo.setText(Html.fromHtml(deliveryCheckResponse.getData()));
                }

            }

            @Override
            public void onFailure(Call<DeliveryCheckResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(BookDetailsActivity.this);
                Toast.makeText(BookDetailsActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void getRatingResponse(String book_id){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<ProductRatingDetailsResponse> call= apiSerivce.getProductRatingDetailsResponse(book_id);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(BookDetailsActivity.this,"Please wait");
        call.enqueue(new Callback<ProductRatingDetailsResponse>() {

            @Override
            public void onResponse(Call<ProductRatingDetailsResponse> call, Response<ProductRatingDetailsResponse> response) {
                kProgressHUD.dismiss();
               //Constants.hideSoftKeyboard(BookDetailsActivity.this);
                int statusCode = response.code();
                ProductRatingDetailsResponse productRatingDetailsResponse=response.body();

                if(productRatingDetailsResponse.getData()!=null){
                    RatingData ratingData= productRatingDetailsResponse.getData();
                        rateNReviewText.setPaintFlags(rateNReviewText.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
                        rateNReviewText.setText(ratingData.getProductsRatingDetailsData().getRatingCount()+" Ratings and "+ratingData.getProductsRatingDetailsData().getReviewCount()+" Reviews");
                        rateNReview.setText(ratingData.getProductsRatingDetailsData().getRatingCount()+" Ratings and "+ratingData.getProductsRatingDetailsData().getReviewCount()+" Reviews");
                        foundRating.setText(ratingData.getProductsRatingDetailsData().getFoundRating());

                    int count=0;
                    for (int i = 0; i < ratingData.getProductsRatingDetailsData().getStarRating().size(); i++) {

                        if(ratingData.getProductsRatingDetailsData().getStarRating().get(i).equalsIgnoreCase("1")){
                            count++;
                        }
                    }
                    if(count==0){
                        ratingBarLarge.setRating(0);
                    }
                    else {
                        ratingBarLarge.setRating(count);
                    }

                    Drawable progress = ratingBarLarge.getProgressDrawable();
                    DrawableCompat.setTint(progress,getResources().getColor(R.color.star_color));
                    PointWiseStar pointWiseStar = ratingData.getProductsRatingDetailsData().getPointWiseStar();
                    if(pointWiseStar!=null){
                        rate1.setText(pointWiseStar.getStarOne());
                        rate2.setText(pointWiseStar.getStarTwo());
                        rate3.setText(pointWiseStar.getStarThree());
                        rate4.setText(pointWiseStar.getStarFour());
                        rate5.setText(pointWiseStar.getStarFive());
                    }
                    int rating_count= Integer.parseInt(ratingData.getProductsRatingDetailsData().getRatingCount());
                    int rate5= Integer.parseInt(pointWiseStar.getStarFive());
                    Double res5 = ((double)rate5/rating_count) * 100;
                    int progress5= (int)res5.doubleValue();
                     progressBar5.setProgress(progress5);

                    int rate4= Integer.parseInt(pointWiseStar.getStarFour());
                    Double res4 = ((double)rate4/rating_count) * 100;
                    int progress4= (int)res4.doubleValue();
                    progressBar4.setProgress(progress4);

                    int rate3= Integer.parseInt(pointWiseStar.getStarThree());
                    Double res3 = ((double)rate3/rating_count) * 100;
                    int progress3= (int)res3.doubleValue();
                    progressBar3.setProgress(progress3);

                    int rate2= Integer.parseInt(pointWiseStar.getStarTwo());
                    Double res2 = ((double)rate2/rating_count) * 100;
                    int progress2= (int)res2.doubleValue();
                   if(progress2<=0){
                        progressBar2.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar));
                    }
                    else {
                        progressBar2.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar_yellow));
                    }
                    progressBar2.setProgress(progress2);

                    int rate1= Integer.parseInt(pointWiseStar.getStarOne());
                    Double res1 = ((double)rate1/rating_count) * 100;
                    int progress1= (int)res1.doubleValue();
                   if(progress1<=0){
                        progressBar1.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar));
                    }
                    else {
                        progressBar1.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar_red));
                    }
                    progressBar1.setProgress(progress1);

                    List<ReviewList> reviewList =ratingData.getReviewList();
                    if(reviewList!=null && reviewList.size()>0){
                        viewReviews.setVisibility(View.VISIBLE);
                        reviewCountTxt.setText("Showing "+ratingData.getProductsRatingDetailsData().getReviewCount()+" reviews");
                        ReviewAdapter reviewAdapter =new ReviewAdapter(BookDetailsActivity.this,reviewList);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(BookDetailsActivity.this,LinearLayoutManager.VERTICAL,false);
                        recyclerViewReview.setLayoutManager(linearLayoutManager);
                        recyclerViewReview.setNestedScrollingEnabled(false);
                        recyclerViewReview.setAdapter(reviewAdapter);
                    }

                    viewReviews.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                           Intent intent = new Intent(BookDetailsActivity.this,ReviewsActivity.class);
                           intent.putExtra("book_id",book_id);
                           intent.putExtra("book_name",book_name);
                           intent.putExtra("book_image_url",book_img_url);
                           startActivity(intent);
                        }
                    });

                    addreviewBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(userData.getUser_token()!=null && !userData.getUser_token().equalsIgnoreCase("")) {
                                showDialogforAddReview();
                            } else{
                                Intent intent = new Intent(BookDetailsActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }
                        }
                    });

                }

            }

            @Override
            public void onFailure(Call<ProductRatingDetailsResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                //Constants.hideSoftKeyboard(BookDetailsActivity.this);
                Toast.makeText(BookDetailsActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                String text;
                int lineEndIndex;
                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    lineEndIndex = tv.getLayout().getLineEnd(0);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else {
                    lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                }
                tv.setText(text);
                tv.setMovementMethod(LinkMovementMethod.getInstance());
                tv.setText(
                        addClickablePartTextViewResizable(tv.getText().toString(), tv, lineEndIndex, expandText,
                                viewMore), TextView.BufferType.SPANNABLE);
            }
        });
    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final String strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new MySpannable(true){
                @Override
                public void onClick(View widget) {
                    tv.setLayoutParams(tv.getLayoutParams());
                    tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                    tv.invalidate();
                    if (viewMore) {
                        makeTextViewResizable(tv, -1, "\n"+"View Less", false);
                    } else {
                        makeTextViewResizable(tv, 10, "\n"+"View More", true);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;
    }
    public void showDialogforAddReview(){
        Dialog dialog= new Dialog(BookDetailsActivity.this);
        dialog.setContentView(R.layout.add_review_layout);
        dialog.show();
        Window window= dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        EditText headEdit=(EditText)dialog.findViewById(R.id.head_edit);
        EditText reviewEdit=(EditText) dialog.findViewById(R.id.review_edit);
        ImageView bookImg=(ImageView) dialog.findViewById(R.id.book_img);
        TextView bookName=(TextView)dialog.findViewById(R.id.book_name);
        RatingBar ratingBar=(RatingBar) dialog.findViewById(R.id.myRatingBar);
        TextView submitBtn = (TextView) dialog.findViewById(R.id.review_submit_btn);
        TextView cancelBtn = (TextView)dialog.findViewById(R.id.cancel);

        Drawable progress1 = ratingBar.getProgressDrawable();
        DrawableCompat.setTint(progress1, getResources().getColor(R.color.star_color));

        if(book_name!=null){
            bookName.setText(book_name);
        }
        if(book_img_url!=null){
            Glide.with(BookDetailsActivity.this).load(book_img_url).placeholder(R.drawable.demo_book).into(bookImg);
        }

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    String head= headEdit.getText().toString();
                    String body= reviewEdit.getText().toString();
                    int rating_count= (int) ratingBar.getRating();
                    if(rating_count==0){
                        Toast.makeText(BookDetailsActivity.this, "Rating is required", Toast.LENGTH_SHORT).show();
                    }else {
                        submitReview(dialog,head,body,rating_count, Integer.parseInt(book_id));    
                    }
                  
                }
        });
    }

    public void submitReview(Dialog dialog,String head, String body, int rating, int book_id){
        userData = DroidPrefs.get(BookDetailsActivity.this,"user_data",UserData.class);

        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("rating",rating);
            jsonObject.put("heading",head);
            jsonObject.put("comments",body);
            jsonObject.put("prodID",book_id);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.submitReviewResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(BookDetailsActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(BookDetailsActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    getRatingResponse(String.valueOf(book_id));
                    dialog.dismiss();
                    Toast.makeText(BookDetailsActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
                else {
                        dialog.dismiss();
                    Toast.makeText(BookDetailsActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(BookDetailsActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void addProductTocartResponse( String prod_id, String qty,String source){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("prodID",prod_id);
            jsonObject.put("qTy",qty);
            jsonObject.put("cartSource",source);

        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.addProductTocartResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(BookDetailsActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(BookDetailsActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    getcartDetailsResponse(userData.getUser_token());
                    Snackbar.make(addToCartBtn,generalResponse.getStatusMsg(),Snackbar.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(BookDetailsActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(BookDetailsActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void getPostWishListResponse(String book_id){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("booksId",book_id);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.getPostWishListResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(BookDetailsActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(BookDetailsActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse!=null){
                    if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                        Toast.makeText(BookDetailsActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();

                    }
                    else {
                        Toast.makeText(BookDetailsActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(BookDetailsActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void getRemoveWishListResponse(String book_id){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("booksId",book_id);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.getRemoveWishListResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(BookDetailsActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(BookDetailsActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse!=null){
                    if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                        Toast.makeText(BookDetailsActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();

                    }
                    else {
                        Toast.makeText(BookDetailsActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                    }
                }


            }
            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(BookDetailsActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void getcartDetailsResponse(String token){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("cartSource","C");

        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GetCartResponse> call= apiService.getCartDetailsResponse(res,token);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(BookDetailsActivity.this,"Please wait");
        call.enqueue(new Callback<GetCartResponse>() {
            @Override
            public void onResponse(Call<GetCartResponse> call, Response<GetCartResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                GetCartResponse getCartResponse=response.body();
                if(getCartResponse!=null){
                    if(getCartResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                        if(getCartResponse.getData()!=null && !getCartResponse.getData().contains("")){
                            List<CartData> cartData = getCartResponse.getData();
                            List<CartList> cartList = new ArrayList<>();
                            for (int i = 0; i <cartData.size() ; i++) {
                                cartList=cartData.get(i).getCartList();
                            }

                            if(cartList!=null && cartList.size()>0){
                                cartImg.visibleBadge(true);
                                cartImg.setBadgeValue(cartList.size());
                            }
                            else {
                                cartImg.visibleBadge(false);
                            }

                        }
                    }
                    else {
                        Toast.makeText(BookDetailsActivity.this, getCartResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

            }
            @Override
            public void onFailure(Call<GetCartResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                 cartImg.visibleBadge(false);

                // Toast.makeText(CartActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void showDialogNotifyme(){
        Dialog dialog= new Dialog(BookDetailsActivity.this);
        dialog.setContentView(R.layout.notify_me_pop_up_layout);
        dialog.show();
        Window window= dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        EditText email_id=(EditText)dialog.findViewById(R.id.email_address);
        TextView title=(TextView) dialog.findViewById(R.id.title);
        TextView sub_title=(TextView)dialog.findViewById(R.id.sub_title);
        TextView submit_btn=(TextView)dialog.findViewById(R.id.submit);
        ImageView cross_btn=(ImageView) dialog.findViewById(R.id.cross);

        cross_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(email_id.getText().toString().isEmpty()){
                    Toast.makeText(BookDetailsActivity.this, "Please fill the email id", Toast.LENGTH_SHORT).show();
                }

                else if(!Patterns.EMAIL_ADDRESS.matcher(email_id.getText().toString().trim()).matches()) {
                    Toast.makeText(BookDetailsActivity.this, "Please enter valid email address", Toast.LENGTH_SHORT).show();
                }

                else {
                    String email= email_id.getText().toString();
                    getnotifyResponse(email, Integer.parseInt(book_id));
                    dialog.dismiss();

                }
            }
        });
    }

    public void getnotifyResponse(String email, int prod_id){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("emailid",email);
            jsonObject.put("prodid",prod_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.getnotifyMeResponse(res);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(BookDetailsActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(BookDetailsActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    showDialogForThanksPopUp();
                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(BookDetailsActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(BookDetailsActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void showDialogForThanksPopUp(){
        Dialog dialog= new Dialog(BookDetailsActivity.this);
        dialog.setContentView(R.layout.thanks_popup_layout);
        dialog.show();
        Window window= dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        ImageView cross_btn =(ImageView) dialog.findViewById(R.id.cross);

        cross_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}
