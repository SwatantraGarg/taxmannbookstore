package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyOrder {
    @SerializedName("OrderNo")
    @Expose
    private String orderNo;
    @SerializedName("Order_Date")
    @Expose
    private String orderDate;
    @SerializedName("OrderAmount")
    @Expose
    private String orderAmount;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Shippment")
    @Expose
    private List<Shippment> shippment = null;


    public List<Shippment> getShippment() {
        return shippment;
    }

    public void setShippment(List<Shippment> shippment) {
        this.shippment = shippment;
    }
    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    private int year;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



}
