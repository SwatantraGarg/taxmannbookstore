package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VirtualBookDetail {
    @SerializedName("bookId")
    @Expose
    private Integer bookId;
    @SerializedName("bindingTypes")
    @Expose
    private String bindingTypes;
    @SerializedName("url")
    @Expose
    private String url;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getBindingTypes() {
        return bindingTypes;
    }

    public void setBindingTypes(String bindingTypes) {
        this.bindingTypes = bindingTypes;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
