package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryItemList {

    @SerializedName("camenuID")
    @Expose
    private String camenuID;
    @SerializedName("itemId")
    @Expose
    private Integer itemId;
    @SerializedName("categoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("itemName")
    @Expose
    private String itemName;
    @SerializedName("viewType")
    @Expose
    private Integer viewType;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("subCategoryItemList")
    @Expose
    private Object subCategoryItemList;

    public String getCamenuID() {
        return camenuID;
    }

    public void setCamenuID(String camenuID) {
        this.camenuID = camenuID;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getViewType() {
        return viewType;
    }

    public void setViewType(Integer viewType) {
        this.viewType = viewType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Object getSubCategoryItemList() {
        return subCategoryItemList;
    }

    public void setSubCategoryItemList(Object subCategoryItemList) {
        this.subCategoryItemList = subCategoryItemList;
    }

}
