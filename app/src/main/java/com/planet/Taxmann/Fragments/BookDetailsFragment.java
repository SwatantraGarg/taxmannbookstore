package com.planet.Taxmann.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.activity.BookDetailsActivity;
import com.planet.Taxmann.activity.CheckOutActivity;
import com.planet.Taxmann.activity.MainActivity;
import com.planet.Taxmann.adapters.AuthorAdapter;
import com.planet.Taxmann.adapters.BookAdapter;
import com.planet.Taxmann.adapters.ReviewAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.Books;
import com.planet.Taxmann.model.DeliveryCheckResponse;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.GetBookDetailsResponse;
import com.planet.Taxmann.model.MySpannable;
import com.planet.Taxmann.model.PointWiseStar;
import com.planet.Taxmann.model.ProductRatingDetailsResponse;
import com.planet.Taxmann.model.RatingData;
import com.planet.Taxmann.model.ReviewList;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookDetailsFragment extends Fragment {
    @BindView(R.id.scrollView)
    ScrollView scrollview;

    @BindView(R.id.book_image)
    ImageView bookImage;

    @BindView(R.id.book_title)
    TextView bookTitle;

    @BindView(R.id.author_name)
    TextView authorName;


    @BindView(R.id.myRatingBar)
    RatingBar ratingBar;

    @BindView(R.id.rate_n_review_text)
    TextView rateNReviewText;

    @BindView(R.id.sample_chapter)
    TextView sampleChapterBtn;

    @BindView(R.id.view_content)
    TextView viewContentBtn;

    @BindView(R.id.radio_group)
    RadioGroup radioGroup;

    @BindView(R.id.stock_txt)
    TextView stockTxt;

    @BindView(R.id.indian_price)
    TextView indPrice;

    @BindView(R.id.us_price)
    TextView usPrice;

    @BindView(R.id.book_detail_text)
    TextView bookDetailTxt;

    @BindView(R.id.view_more_btn)
    TextView viewMoreBtn;

    @BindView(R.id.pin_code)
    EditText pinCodeEdit;

    @BindView(R.id.pin_code_check)
    TextView pinCodeCheck;

    @BindView(R.id.publication)
    TextView publicationTxt;

    @BindView(R.id.isbn)
    TextView isbnTxt;

    @BindView(R.id.edition)
    TextView editionTxt;

    @BindView(R.id.no_page)
    TextView noPageTxt;

    @BindView(R.id.author)
    TextView authorTxt;

    @BindView(R.id.weight)
    TextView weightTxt;

    @BindView(R.id.about_book_txt)
    TextView aboutBookTxt;

    @BindView(R.id.recycler_view_author)
    RecyclerView recyclerViewAuthors;

    @BindView(R.id.recycler_view_other_books)
    RecyclerView recyclerViewOtherBooks;

    @BindView(R.id.del_info_layout)
    LinearLayout delInfoLayout;

    @BindView(R.id.del_info)
    TextView delInfo;

    @BindView(R.id.found_rating)
    TextView foundRating;

    @BindView(R.id.RatingBar)
    RatingBar ratingBarLarge;

    @BindView(R.id.rate_and_review_text)
    TextView rateNReview;

    @BindView(R.id.view_reviews)
    TextView viewReviews;

    @BindView(R.id.progress_bar5)
    ProgressBar progressBar5;
    @BindView(R.id.progress_bar4)
    ProgressBar progressBar4;

    @BindView(R.id.progress_bar3)
    ProgressBar progressBar3;

    @BindView(R.id.progress_bar2)
    ProgressBar progressBar2;

    @BindView(R.id.progress_bar1)
    ProgressBar progressBar1;

    @BindView(R.id.rate_5)
    TextView rate5;

    @BindView(R.id.rate_4)
    TextView rate4;

    @BindView(R.id.rate_3)
    TextView rate3;

    @BindView(R.id.rate_2)
    TextView rate2;

    @BindView(R.id.rate_1)
    TextView rate1;

    @BindView(R.id.review_count_text)
    TextView reviewCountTxt;

    @BindView(R.id.recyclerview_review_list)
    RecyclerView recyclerViewReview;

    @BindView(R.id.review_layout)
    LinearLayout reviewLayout;


    @BindView(R.id.add_review_btn)
    Button addreviewBtn;

    @BindView(R.id.buy_now)
    TextView buyNowBtn;

    @BindView(R.id.add_to_cart)
    TextView addToCartBtn;

    Intent intent;
    String book_id,book_name,book_img_url, main_prod_id;
    UserData userData;
MainActivity mainActivity;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            book_id= getArguments().getString("book_id");

        }







    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mainActivity= (MainActivity)getActivity();
       // mainActivity.editSearch.dismissDropDown();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.book_detail_fragment, container, false);
        ButterKnife.bind(this,v);
        userData = DroidPrefs.get(getContext(),"user_data",UserData.class);



        getBookDetailsResponse(book_id);
        getRatingResponse(book_id);

        addToCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(main_prod_id!=null &&!main_prod_id.equalsIgnoreCase("")){
                    addProductTocartResponse(main_prod_id,"0","C");
                }


            }
        });

        buyNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(main_prod_id!=null &&!main_prod_id.equalsIgnoreCase("")){
                    addProductTocartResponse(main_prod_id,"0","B");
                    Intent intent = new Intent(getContext(), CheckOutActivity.class);
                    intent.putExtra("source","B");
                    startActivity(intent);
                }


            }
        });
        pinCodeCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(pinCodeEdit.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "Please enter pin code", Toast.LENGTH_SHORT).show();
                }
                else if(pinCodeEdit.getText().toString().length()<6){
                    Toast.makeText(getContext(), "pin code digits should be 6.", Toast.LENGTH_SHORT).show();
                }

                else {
                    String pinCode = pinCodeEdit.getText().toString();
                    getDeliveryCheckResponse(pinCode);
                }
            }
        });

        return v;
    }

    public void getBookDetailsResponse(String book_ID){

        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(getContext(),"Please wait");
        Call<GetBookDetailsResponse> call= apiSerivce.getProductDetailsResponse(book_ID,userData.getUser_token());

        call.enqueue(new Callback<GetBookDetailsResponse>() {
            @Override
            public void onResponse(Call<GetBookDetailsResponse> call, Response<GetBookDetailsResponse> response) {
                kProgressHUD.dismiss();
                int status_code= response.code();
                GetBookDetailsResponse getBookDetailsResponse= response.body();
                if(getBookDetailsResponse.getData()!=null){
                    //set book details data
                    if(getBookDetailsResponse.getData().getBooks()!=null){
                        Books books=getBookDetailsResponse.getData().getBooks();
                        book_name= books.getBookTitle();
                        book_img_url= books.getBookImagesurl();
                        main_prod_id= String.valueOf(books.getBookId());
                        bookTitle.setText(books.getBookTitle());
                        authorName.setText(books.getBookAuthor());
                        authorTxt.setText(books.getBookAuthor());
                        if(books.getStock()){
                            stockTxt.setText("In Stock");
                        }
                        else {
                            stockTxt.setText("Out of stock");
                        }

                        publicationTxt.setText(books.getDateOfPublication());
                        isbnTxt.setText(books.getBookISBN());
                        editionTxt.setText(books.getEdition());
                        weightTxt.setText(books.getWeight()+" kg");

                        noPageTxt.setText(String.valueOf(books.getNoOfPages()+" pages"));
                        aboutBookTxt.setText(Html.fromHtml(books.getDescription()));
                        if(aboutBookTxt.getText().toString().isEmpty()){

                        }
                        else {
                            makeTextViewResizable(aboutBookTxt, 10, "\n"+"View More", true);
                        }
                        if(books.getProductsBindingTypesDetails().size()>=1){
                            radioGroup.setVisibility(View.VISIBLE);
                            radioGroup.setOrientation(LinearLayout.HORIZONTAL);

                            List<String> binding=new ArrayList<>();
                            for (int i = 0; i < books.getProductsBindingTypesDetails().size(); i++) {
                                binding.add(books.getProductsBindingTypesDetails().get(i).getBindingTypes());
                                RadioButton rbn = new RadioButton(radioGroup.getContext());
                                rbn.setId(books.getProductsBindingTypesDetails().get(i).getBookId());
                                rbn.setText(binding.get(i));
                                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
                                radioGroup.addView(rbn,params);
                                if(books.getBinding().contains(binding.get(i)) ){
                                    rbn.setChecked(true);
                                }


                            }
                        }
                        else {

                        }
                        indPrice.setText("Rs. "+String.valueOf(books.getBookPriceINR()));
                        usPrice.setText("USD "+String.valueOf(books.getBookPriceUSD()));
                        viewContentBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent= new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(books.getAboutBook()));
                                startActivity(intent);
                            }
                        });
                        sampleChapterBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent= new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(books.getSampleIssue()));
                                startActivity(intent);
                            }
                        });

                        if(books.getAboutAuthor()!=null && books.getAboutAuthor().size()>0){
                            AuthorAdapter authorAdapter = new AuthorAdapter(books.getAboutAuthor(),getContext());
                            LinearLayoutManager linearLayoutManager =new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
                            recyclerViewAuthors.setLayoutManager(linearLayoutManager);
                            recyclerViewAuthors.setNestedScrollingEnabled(false);
                            recyclerViewAuthors.setAdapter(authorAdapter);
                        }

                        Glide.with(getActivity()).load(books.getBookImagesurl()).placeholder(R.drawable.demo_book).into(bookImage);

                        int count=0;
                        for (int i = 0; i < books.getBookRating().size(); i++) {

                            if(books.getBookRating().get(i).equalsIgnoreCase("1")){
                                count++;
                            }
                        }
                        if(count==0){
                            ratingBar.setRating(0);
                        }
                        else {
                            ratingBar.setRating(count);
                        }
                        Drawable progress = ratingBar.getProgressDrawable();
                        DrawableCompat.setTint(progress,getResources().getColor(R.color.star_color));

                    }
                    else {
                        Toast.makeText(getContext(), "Book details are not available", Toast.LENGTH_SHORT).show();
                    }

                    if(getBookDetailsResponse.getData().getBookData()!=null && getBookDetailsResponse.getData().getBookData().size()>0){
                        BookAdapter bookAdapter =new BookAdapter(getContext(),getBookDetailsResponse.getData().getBookData(), new BookAdapter.BookClicked() {
                            @Override
                            public void Clicked(View view, int position) {
                                Intent intent =new Intent(getContext(),BookDetailsActivity.class);
                                intent.putExtra("book_id",getBookDetailsResponse.getData().getBookData().get(position).getMainProdId());
                                startActivity(intent);
                            }

                        });

                        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
                        recyclerViewOtherBooks.setLayoutManager(linearLayoutManager);
                        recyclerViewOtherBooks.setAdapter(bookAdapter);
                    }



                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(getContext(), response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetBookDetailsResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void getDeliveryCheckResponse(String pin_code){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<DeliveryCheckResponse> call= apiSerivce.getDeliveryCheckResponse(pin_code);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(getContext(),"Please wait");
        call.enqueue(new Callback<DeliveryCheckResponse>() {
            @Override
            public void onResponse(Call<DeliveryCheckResponse> call, Response<DeliveryCheckResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(getActivity());
                int statusCode = response.code();
                DeliveryCheckResponse deliveryCheckResponse=response.body();

                if(deliveryCheckResponse.getData()!=null){
                    delInfoLayout.setVisibility(View.VISIBLE);
                    delInfo.setText(Html.fromHtml(deliveryCheckResponse.getData()));
                }

            }

            @Override
            public void onFailure(Call<DeliveryCheckResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(getActivity());
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    public void getRatingResponse(String book_id){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<ProductRatingDetailsResponse> call= apiSerivce.getProductRatingDetailsResponse(book_id);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(getContext(),"Please wait");
        call.enqueue(new Callback<ProductRatingDetailsResponse>() {

            @Override
            public void onResponse(Call<ProductRatingDetailsResponse> call, Response<ProductRatingDetailsResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(getActivity());
                int statusCode = response.code();
                ProductRatingDetailsResponse productRatingDetailsResponse=response.body();

                if(productRatingDetailsResponse.getData()!=null){
                    RatingData ratingData= productRatingDetailsResponse.getData();
                    rateNReviewText.setText(ratingData.getProductsRatingDetailsData().getRatingCount()+" ratings and "+ratingData.getProductsRatingDetailsData().getReviewCount()+" reviews");
                    rateNReview.setText(ratingData.getProductsRatingDetailsData().getRatingCount()+" ratings and "+ratingData.getProductsRatingDetailsData().getReviewCount()+" reviews");
                    foundRating.setText(ratingData.getProductsRatingDetailsData().getFoundRating());
                    int count=0;
                    for (int i = 0; i < ratingData.getProductsRatingDetailsData().getStarRating().size(); i++) {

                        if(ratingData.getProductsRatingDetailsData().getStarRating().get(i).equalsIgnoreCase("1")){
                            count++;
                        }
                    }
                    if(count==0){
                        ratingBarLarge.setRating(0);
                    }
                    else {
                        ratingBarLarge.setRating(count);
                    }
                    Drawable progress = ratingBarLarge.getProgressDrawable();
                    DrawableCompat.setTint(progress,getActivity().getResources().getColor(R.color.star_color));
                    PointWiseStar pointWiseStar = ratingData.getProductsRatingDetailsData().getPointWiseStar();
                    if(pointWiseStar!=null){
                        rate1.setText(pointWiseStar.getStarOne());
                        rate2.setText(pointWiseStar.getStarTwo());
                        rate3.setText(pointWiseStar.getStarThree());
                        rate4.setText(pointWiseStar.getStarFour());
                        rate5.setText(pointWiseStar.getStarFive());
                    }
                    int rating_count= Integer.parseInt(ratingData.getProductsRatingDetailsData().getRatingCount());
                    int rate5= Integer.parseInt(pointWiseStar.getStarFive());
                    Double res5 = ((double)rate5/rating_count) * 100;
                    int progress5= (int)res5.doubleValue();
                    if(progress5<50 && progress5>25){
                        progressBar5.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar_yellow));
                    }
                    else  if(progress5<=25){
                        progressBar5.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar_red));
                    }
                    else {
                        progressBar5.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar));
                    }
                    progressBar5.setProgress(progress5);

                    int rate4= Integer.parseInt(pointWiseStar.getStarFour());
                    Double res4 = ((double)rate4/rating_count) * 100;
                    int progress4= (int)res4.doubleValue();
                    if(progress4<50 && progress4>25){
                        progressBar4.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar_yellow));
                    }
                    else  if(progress4<=25){
                        progressBar4.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar_red));
                    }
                    else {
                        progressBar4.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar));
                    }
                    progressBar4.setProgress(progress4);

                    int rate3= Integer.parseInt(pointWiseStar.getStarThree());
                    Double res3 = ((double)rate3/rating_count) * 100;
                    int progress3= (int)res3.doubleValue();
                    if(progress3<50 && progress3>25){
                        progressBar3.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar_yellow));
                    }
                    else  if(progress3<=25){
                        progressBar3.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar_red));
                    }
                    else {
                        progressBar3.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar));
                    }
                    progressBar3.setProgress(progress3);

                    int rate2= Integer.parseInt(pointWiseStar.getStarTwo());
                    Double res2 = ((double)rate2/rating_count) * 100;
                    int progress2= (int)res2.doubleValue();
                    if(progress2<50 && progress2>25){
                        progressBar2.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar_yellow));
                    }
                    else  if(progress2<=25){
                        progressBar2.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar_red));
                    }
                    else {
                        progressBar2.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar));
                    }
                    progressBar2.setProgress(progress2);

                    int rate1= Integer.parseInt(pointWiseStar.getStarOne());
                    Double res1 = ((double)rate1/rating_count) * 100;
                    int progress1= (int)res1.doubleValue();
                    if(progress1<50 && progress1>25){
                        progressBar1.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar_yellow));
                    }
                    else  if(progress1<=25){
                        progressBar1.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar_red));
                    }
                    else {
                        progressBar1.setProgressDrawable(getResources().getDrawable(R.drawable.curved_progress_bar));
                    }
                    progressBar1.setProgress(progress1);

                    reviewCountTxt.setText("Showing "+ratingData.getProductsRatingDetailsData().getReviewCount()+" reviews");
                    List<ReviewList> reviewList =ratingData.getReviewList();
                    if(reviewList!=null){
                        ReviewAdapter reviewAdapter =new ReviewAdapter(getContext(),reviewList);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
                        recyclerViewReview.setLayoutManager(linearLayoutManager);
                        recyclerViewReview.setNestedScrollingEnabled(false);
                        recyclerViewReview.setAdapter(reviewAdapter);
                    }

                    viewReviews.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            reviewLayout.setVisibility(View.VISIBLE);
                        }
                    });

                    addreviewBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showDialogforAddReview();
                        }
                    });

                }

            }

            @Override
            public void onFailure(Call<ProductRatingDetailsResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(getActivity());
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                String text;
                int lineEndIndex;
                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0 && tv!=null) {
                    lineEndIndex = tv.getLayout().getLineEnd(0);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else {
                    lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                }
                tv.setText(text);
                tv.setMovementMethod(LinkMovementMethod.getInstance());
                tv.setText(
                        addClickablePartTextViewResizable(tv.getText().toString(), tv, lineEndIndex, expandText,
                                viewMore), TextView.BufferType.SPANNABLE);
            }
        });
    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final String strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new MySpannable(true){
                @Override
                public void onClick(View widget) {
                    tv.setLayoutParams(tv.getLayoutParams());
                    tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                    tv.invalidate();
                    if (viewMore) {
                        makeTextViewResizable(tv, -1, "\n"+"View Less", false);
                    } else {
                        makeTextViewResizable(tv, 10, "\n"+"View More", true);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;
    }
    public void showDialogforAddReview(){
        Dialog dialog= new Dialog(getContext());
        dialog.setContentView(R.layout.add_review_layout);
        dialog.show();
        Window window= dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        EditText headEdit=(EditText)dialog.findViewById(R.id.head_edit);
        EditText reviewEdit=(EditText) dialog.findViewById(R.id.review_edit);
        ImageView bookImg=(ImageView) dialog.findViewById(R.id.book_img);
        TextView bookName=(TextView)dialog.findViewById(R.id.book_name);
        RatingBar ratingBar=(RatingBar) dialog.findViewById(R.id.myRatingBar);
        TextView submitBtn = (TextView) dialog.findViewById(R.id.review_submit_btn);
        TextView cancelBtn = (TextView)dialog.findViewById(R.id.cancel);

        Drawable progress1 = ratingBar.getProgressDrawable();
        DrawableCompat.setTint(progress1, getResources().getColor(R.color.star_color));

        if(book_name!=null){
            bookName.setText(book_name);
        }
        if(book_img_url!=null){
            Glide.with(getContext()).load(book_img_url).placeholder(R.drawable.demo_book).into(bookImg);
        }

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(headEdit.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "Please fill the headline", Toast.LENGTH_SHORT).show();
                }

                else if(reviewEdit.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), "Please enter review text", Toast.LENGTH_SHORT).show();
                }
                else  if(ratingBar.getRating()==0){
                    Toast.makeText(getContext(), "Please select rating", Toast.LENGTH_SHORT).show();
                }
                else {
                    String head= headEdit.getText().toString();
                    String body= reviewEdit.getText().toString();
                    int rating_count= (int) ratingBar.getRating();
                    submitReview(dialog,head,body,rating_count, Integer.parseInt(book_id));
                }
            }
        });

    }

    public void submitReview(Dialog dialog,String head, String body, int rating, int book_id){
        userData = DroidPrefs.get(getContext(),"user_data",UserData.class);

        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("rating",rating);
            jsonObject.put("heading",head);
            jsonObject.put("comments",body);
            jsonObject.put("prodID",book_id);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.submitReviewResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(getContext(),"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(getActivity());
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    dialog.dismiss();
                    Toast.makeText(getContext(), generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
                else {
                    dialog.dismiss();
                    Toast.makeText(getContext(), generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void addProductTocartResponse( String prod_id, String qty,String source){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("prodID",prod_id);
            jsonObject.put("qTy",qty);
            jsonObject.put("cartSource",source);

        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.addProductTocartResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(getContext(),"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(getActivity());
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    Snackbar.make(addToCartBtn,generalResponse.getStatusMsg(),Snackbar.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(getContext(), generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
}
