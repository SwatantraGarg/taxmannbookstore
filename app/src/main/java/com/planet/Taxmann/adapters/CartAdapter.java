package com.planet.Taxmann.adapters;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.planet.Taxmann.R;
import com.planet.Taxmann.model.CartList;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
List<CartList> cartList;
OnClicked onclicked;
    Context context;


    public CartAdapter(List<CartList> cartList, Context context, OnClicked onclicked) {
        this.cartList = cartList;
        this.context = context;
        this.onclicked = onclicked;
    }
    public interface OnClicked{
        void Clicked(View view, int position, int qty);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CartAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_add_item,null));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(cartList.get(position).getQty()==1){
            holder.decrement.setBackgroundResource(R.drawable.gray_fill_circle);
        }else {
            holder.decrement.setBackgroundResource(R.drawable.circle_fill_orange);
        }


        holder.decrement.setTag(position);
        holder.decrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               int no = Integer.parseInt(holder.noOfBooks.getText().toString());
                int pos =(int)v.getTag();
                if(onclicked!=null){
                    no--;
                    onclicked.Clicked(v,pos,no);
                }

                if(no>=1){
                    holder.noOfBooks.setText(String.valueOf(no));

                }
                else {

                }
                if(no>1){
                    holder.decrement.setBackgroundResource(R.drawable.circle_fill_orange);
                }else {
                    holder.decrement.setBackgroundResource(R.drawable.gray_fill_circle);
                }
            }
        });

             holder.increment.setTag(position);
        holder.increment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               int no = Integer.parseInt(holder.noOfBooks.getText().toString());
                int pos =(int)v.getTag();
                if(onclicked!=null){
                    no++;
                    onclicked.Clicked(v,pos,no);
                }

                if(no>=1){
                    holder.noOfBooks.setText(String.valueOf(no));
                }
                if(no>1){
                    holder.decrement.setBackgroundResource(R.drawable.circle_fill_orange);
                }else {
                    holder.decrement.setBackgroundResource(R.drawable.gray_fill_circle);
                }


            }
        });
        holder.noOfBooks.setTag(position);
        holder.noOfBooks.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE)  {
                    v.clearFocus();
                    int pos =(int)v.getTag();
                    if(onclicked!=null){
                        onclicked.Clicked(v,pos, Integer.parseInt(v.getText().toString()));
                    }

                    InputMethodManager imm = (InputMethodManager) v.getContext()
                            .getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    if(Integer.parseInt(holder.noOfBooks.getText().toString())==1){
                        holder.decrement.setBackgroundResource(R.drawable.gray_fill_circle);
                    }else {
                        holder.decrement.setBackgroundResource(R.drawable.circle_fill_orange);
                    }
                }
                return true;
            }
        });


        holder.removeBtn.setTag(position);
        holder.removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos =(int)v.getTag();
                if(onclicked!=null){
                    onclicked.Clicked(v,pos,0);
                }

            }
        });
if(cartList!=null &&cartList.size()>0){
    holder.totalPrice.setText("₹"+Math.round(cartList.get(position).getTotalPrice()));
    holder.bookName.setText(cartList.get(position).getProdName());
    holder.bookPrice.setText("₹"+Math.round(cartList.get(position).getUnitPrice()));
    holder.noOfBooks.setText(String.valueOf(cartList.get(position).getQty()));
    if(cartList.get(position).getSMonth()!=null && !cartList.get(position).getSMonth().equalsIgnoreCase("") && cartList.get(position).getSYear()!=null && !cartList.get(position).getSYear().equalsIgnoreCase("")){
        holder.subscripLayout.setVisibility(View.VISIBLE);
        holder.monthSpin.setSelection(Integer.parseInt(cartList.get(position).getSMonth())-1);
        holder.yearSpin.setSelection(getIndex(holder.yearSpin,cartList.get(position).getSYear()));
    }
    else {
        holder.subscripLayout.setVisibility(View.GONE);
    }


}
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.decrement)
        ImageView decrement;

        @BindView(R.id.increment)
        ImageView increment;

        @BindView(R.id.no_of_books)
        EditText noOfBooks;

        @BindView(R.id.total_price)
        TextView totalPrice;

        @BindView(R.id.subcrip_layout)
        LinearLayout subscripLayout;

        @BindView(R.id.month_spin)
        Spinner monthSpin;

        @BindView(R.id.year_spin)
        Spinner yearSpin;


        @BindView(R.id.remove)
        ImageView removeBtn;

        @BindView(R.id.book_name)
        TextView bookName;

        @BindView(R.id.book_price)
        TextView bookPrice;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
    private int getIndex(Spinner spinner, String myString){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                return i;
            }
        }

        return 0;
    }
}
