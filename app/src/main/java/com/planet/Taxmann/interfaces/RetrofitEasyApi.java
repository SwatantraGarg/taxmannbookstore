package com.planet.Taxmann.interfaces;

import com.google.gson.JsonObject;
import com.planet.Taxmann.model.AdvertiseResponse;
import com.planet.Taxmann.model.AggregationsBySubjectResponse;
import com.planet.Taxmann.model.BillAddressResponse;
import com.planet.Taxmann.model.BookDataResponse;
import com.planet.Taxmann.model.CategoryResponse;
import com.planet.Taxmann.model.CategorySubjectNameResponse;
import com.planet.Taxmann.model.CountryCodeResponse;
import com.planet.Taxmann.model.DeliveryCheckResponse;
import com.planet.Taxmann.model.EntityResponse;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.GetBookDetailsResponse;
import com.planet.Taxmann.model.GetCartResponse;
import com.planet.Taxmann.model.GetCountryStateResponse;
import com.planet.Taxmann.model.GetDesignationListResponse;
import com.planet.Taxmann.model.GetRecommendedResponse;
import com.planet.Taxmann.model.GetStateCityResponse;
import com.planet.Taxmann.model.IndianStateResponse;
import com.planet.Taxmann.model.LoginResponse;
import com.planet.Taxmann.model.MyOrderResponse;
import com.planet.Taxmann.model.OrderSourceResponse;
import com.planet.Taxmann.model.PasswordResponse;
import com.planet.Taxmann.model.PostEmailMobile;
import com.planet.Taxmann.model.PostLoginEmailUser;
import com.planet.Taxmann.model.PostLoginSocialUser;
import com.planet.Taxmann.model.PostProductIndex;
import com.planet.Taxmann.model.PostSampleIssue;
import com.planet.Taxmann.model.PostSampleJournalUserAddress;
import com.planet.Taxmann.model.PostShippingAddress;
import com.planet.Taxmann.model.PostSocialRegistration;
import com.planet.Taxmann.model.PostStateProfessionInterst;
import com.planet.Taxmann.model.PostTempOrder;
import com.planet.Taxmann.model.ProductOrder;
import com.planet.Taxmann.model.ProductRatingDetailsResponse;
import com.planet.Taxmann.model.RegistrationRequest;
import com.planet.Taxmann.model.SampleIssueResponse;
import com.planet.Taxmann.model.StateProfessionInterestResponse;
import com.planet.Taxmann.model.SubscriptionResponse;
import com.planet.Taxmann.model.SuggestionResponse;
import com.planet.Taxmann.model.TempOrderResponse;
import com.planet.Taxmann.model.TrackShipmentResponse;
import com.planet.Taxmann.model.UserInfoResponse;
import com.planet.Taxmann.model.VerifyOtp;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

//*** created by swatantra garg ***//

public interface RetrofitEasyApi {
    // user register with social
    @POST("registration/registerUserBySocial")
    Call<GeneralResponse> getRegistrationBySocialResponse(@Body PostSocialRegistration postSocialRegistration);

    //user login with social
    @POST("https://api.taxmann.com/research_module/login")
    Call<LoginResponse> getLoginResponseOfSocialUser(@Body PostLoginSocialUser postLoginSocialUser);

    //user login with email
    @POST("https://api.taxmann.com/research_module/login")
    Call<LoginResponse> getLoginResponseOfEmailUser(@Body PostLoginEmailUser postLoginEmailUser);

    //logout user
    @POST("https://api.taxmann.com/research_module/logout")
    Call<GeneralResponse> logutUser(@Header("TaxmannAuthorization") String token);

    //forgot password
    @POST("login/resetPasswordSendRequest")
    Call<GeneralResponse> getResetPasswordResponse(@Body JsonObject body);

    // user registration with email
    @POST("registration/registerUserByEmail")
    Call<GeneralResponse> getRegistrationByEmailResponse(@Body RegistrationRequest registrationRequest);

    //get state, profession and area of intrest
    @GET("registration/getStateProfessionInterest")
    Call<StateProfessionInterestResponse> getStateProfessionInterestResponse();

    //put state, profession and area of intrest
    @POST("registration/putStateProfessionInterest")
    Call<GeneralResponse> postStateProfessionInterest(@Body PostStateProfessionInterst postStateProfessionInterst);

    @POST("registration/putStateProfessionInterest")
    Call<GeneralResponse> postStateProfessionInterestInside(@Body PostStateProfessionInterst postStateProfessionInterst,  @Header("TaxmannAuthorization") String user_token);
    //generate otp

        @POST("registration/generateOTP")
        Call<GeneralResponse> generateOTP(@Body PostEmailMobile postEmailMobile);

        //verify otp
    @POST("registration/verifyOTP")
    Call<GeneralResponse> verifyOTP(@Body VerifyOtp verifyOtp);

    //get getnewReleasedBookResponse
    @GET("BookStoreCustomProducts/getHomePageAllProducts/1/200")
    Call<BookDataResponse> getnewReleasedBookResponse();

    //get Featured book response
    @GET("BookStoreCustomProducts/getHomePageAllProducts/2/200")
    Call<BookDataResponse> getnewFeaturedBookResponse();

    // get Category response
    @GET("bookStoreLeftMenu/allCategories")
    Call<CategoryResponse> getCategoryResponse();

    //get recommended products
    @GET("BookStoreCustomProducts/getHomePageRecommendedProducts/6")
    Call<GetRecommendedResponse> getHomePageRecommendedProductsResponse(@Header("TaxmannAuthorization") String user_token);

    //getHomePage Recommended Products
    @GET("BookStoreCustomProducts/getHomePageRecommendedProducts/30")
    Call<GetRecommendedResponse> getHomePageRecommendedAllProductsResponse(@Header("TaxmannAuthorization") String user_token);

    //getHomePage Latest Products
    @GET("BookStoreCustomProducts/getHomePageTopLatestProducts/6")
    Call<BookDataResponse> getHomePageTopLatestProductsResponse();

    @GET("BookStoreProductDetails/GetSubjectIndexList/{id}")
    Call<AggregationsBySubjectResponse>  getSubjectIndexListResponse(@Path("id") String id);

    @POST("BookStoreProductDetails/GetCategorySubjectName/")
    Call<CategorySubjectNameResponse> getCategorySubjectNameResponse(@Body JsonObject body);

    @POST("BookStoreProductDetails/GetProductsIndexList/")
    Call<CategorySubjectNameResponse> getProductIndexListResponse(@Body PostProductIndex postProductIndex);

    @POST("BookStoreProductDetails/GetProductsSearchFromIndex/")
    Call<CategorySubjectNameResponse> getProductSearchFromIndex(@Body PostProductIndex postProductIndex);
        // get book details
    @GET("BookStoreProductDetails/getProductsDetails/{book_id}")
    Call<GetBookDetailsResponse> getProductDetailsResponse(@Path("book_id") String book_id, @Header("TaxmannAuthorization") String user_token);

    @POST("BookStoreProductDetails/notifyme")
    Call<GeneralResponse> getnotifyMeResponse(@Body JsonObject body);

    @GET("BookStoreProductDetails/getDeliveryCheck/{pin_code}")
    Call<DeliveryCheckResponse> getDeliveryCheckResponse(@Path("pin_code") String pin_code);

    @GET("BookStoreProductDetails/checkcod/{pin_code}")
    Call<GeneralResponse> getCheckCodResponse(@Path("pin_code") String pin_code);

    @GET("BookStoreProductDetails/getProductsRatingDetails/{book_id}")
    Call<ProductRatingDetailsResponse> getProductRatingDetailsResponse(@Path("book_id") String book_id);

    @POST("BookStoreProductDetails/updateReview")
    Call<GeneralResponse> submitReviewResponse(@Body JsonObject body, @Header("TaxmannAuthorization") String user_token);

    @GET("myAccount/getuserInfo")
    Call<UserInfoResponse> getUserInfoResponse(@Header("TaxmannAuthorization") String user_token);

    @GET("BookStoreCustomerData/getEntityTypes/")
    Call<EntityResponse> getEntityResponse(@Header("TaxmannAuthorization") String user_token);

    @GET("research/countrycodes")
    Call<CountryCodeResponse> getCountryCodeResponse(@Header("TaxmannAuthorization") String user_token);

    @POST("BookStoreCustomerData/updateGstnDetails")
    Call<GeneralResponse> updateGstnDetailsResponse(@Body JsonObject body, @Header("TaxmannAuthorization") String user_token);

    @POST("myAccount/addGstinInfo")
    Call<GeneralResponse> addGstnDetailsResponse(@Body JsonObject body, @Header("TaxmannAuthorization") String user_token);

    @POST("BookStoreCustomerData/deleteGstnDetails/")
    Call<GeneralResponse> deleteGstnDetailsResponse(@Body JsonObject body, @Header("TaxmannAuthorization") String user_token);

    @GET("iipmember/getIndiansate")
    Call<IndianStateResponse> getIndianStateResponse();

    @POST("myAccount/editPersonalInfo")
    Call<GeneralResponse> editPersonalInfoResponse(@Body JsonObject body, @Header("TaxmannAuthorization") String user_token);

    @PUT("https://api.taxmann.com/research_module/password")
    Call<PasswordResponse> getChangePasswordResponse(@Body JsonObject body, @Header("TaxmannAuthorization") String user_token);

    @GET("https://api.taxmann.com/research_module/activeLoyaltyPoint")
    Call<GeneralResponse> getLoyalityPointResponse(@Header("TaxmannAuthorization") String user_token);

    @GET("myAccount/getDesignationList")
    Call<GetDesignationListResponse> getDesignationListResponse(@Header("TaxmannAuthorization") String user_token);

    @GET ("BookStoreCustomerData/getCountryStates/")
    Call<GetCountryStateResponse>  getCountryStateListresponse(@Header("TaxmannAuthorization") String user_token);

    @POST("myAccount/addPhoto")
    Call<GeneralResponse> getAddPhotoResponse(@Body JsonObject body, @Header("TaxmannAuthorization") String user_token);

    @POST("myAccount/editUserName")
    Call<GeneralResponse> getAddUserNameResponse(@Body JsonObject body, @Header("TaxmannAuthorization") String user_token);

    @GET("myAccount/getStateCitybyPIN/{pin_code}")
    Call<GetStateCityResponse> getStateCityResponse(@Path("pin_code") String pin_code, @Header("TaxmannAuthorization") String user_token);

    @GET("BookStoreCustomerData/GetBillingAddress/")
    Call<BillAddressResponse> getBillingAddressesResponse(@Header("TaxmannAuthorization") String user_token);

    @GET("BookStoreCustomerData/getOrderSource/")
    Call<OrderSourceResponse> getOrderSourceResponse(@Header("TaxmannAuthorization") String user_token);

    @POST("BookStoreCustomerData/PostShippingAddress/")
    Call<GeneralResponse> postShippingAddressResponse(@Body PostShippingAddress postShippingAddress, @Header("TaxmannAuthorization") String user_token);

    @POST("BookStoreCustomerData/PostBillingAddress/")
    Call<GeneralResponse> postBillingAddressResponse(@Body PostShippingAddress postShippingAddress, @Header("TaxmannAuthorization") String user_token);

    @POST("BookStoreCustomerData/DeleteUserAddress/")
    Call<GeneralResponse> deleteUserAddressResponse(@Body JsonObject body, @Header("TaxmannAuthorization") String user_token);

    @GET("BookStoreProductDetails/GetBookstoreSuggestion/{search_string}")
    Call<SuggestionResponse> getBookStoreSuggestionResponse(@Path("search_string") String searchString);

    @POST("BookStoreProductDetails/AddProductToCart/")
    Call<GeneralResponse> addProductTocartResponse(@Body JsonObject body, @Header("TaxmannAuthorization") String user_token);

    @POST("BookStoreProductDetails/DeleteProductFromCart/")
    Call<GeneralResponse> deleteProductFromCartResponse(@Body JsonObject body, @Header("TaxmannAuthorization") String user_token);

    @POST("BookStoreProductDetails/GetCartDetails/")
    Call<GetCartResponse> getCartDetailsResponse(@Body JsonObject body, @Header("TaxmannAuthorization") String user_token);

    @POST("order/savetemporder/")
    Call<TempOrderResponse> getTempOrderResponse(@Body PostTempOrder postTempOrder, @Header("TaxmannAuthorization") String user_token);

    @POST("order/postProductOrder/")
    Call<TempOrderResponse> getPostProductResponse(@Body ProductOrder productOrderResponse, @Header("TaxmannAuthorization") String user_token);

    @POST("order/saveDDCheckOrder/")
    Call<TempOrderResponse> getDDCheckOrderResponse(@Body PostTempOrder postTempOrder, @Header("TaxmannAuthorization") String user_token);

    @POST("myAccount/cancelOrder")
    Call<GeneralResponse> getCancelOrderResponse(@Body JsonObject body, @Header("TaxmannAuthorization") String user_token);

    @GET("myAccount/myOrder")
    Call<MyOrderResponse> getMyOrderResponse(@Header("TaxmannAuthorization") String user_token);

    @GET("myAccount/trackMyOrder/{curiur_no}")
    Call<MyOrderResponse> getTrackMyOrderResponse(@Path("curiur_no") String curiur_no, @Header("TaxmannAuthorization") String user_token);

    @GET("shiprocket/trackShipmentByCourierNo")
    Call<TrackShipmentResponse> getTrackShipmentResponse(@Query("awbcode") String awbcode, @Header("TaxmannAuthorization") String user_token);

    @GET("myAccount/getMySubscription")
    Call<SubscriptionResponse> getMySubscriptionResponse(@Header("TaxmannAuthorization") String user_token);
    
    @GET("myAccount/getUserWishList")
    Call<BookDataResponse> getWishListResponse(@Header("TaxmannAuthorization") String user_token);

    @POST("myAccount/postWishList")
    Call<GeneralResponse> getPostWishListResponse(@Body JsonObject body, @Header("TaxmannAuthorization") String user_token);

    @GET("banners/getAdvertisements/bookstore_home/topbanner")
    Call<AdvertiseResponse> getTopBannerResponse();

    @GET("banners/getAdvertisements/bookstore_home/thirdpanel")
    Call<AdvertiseResponse> getThirdPanelAdvertiseResponse();

    @GET("BookStoreProductDetails/getSampleIssueList/6")
    Call<SampleIssueResponse> getSampleIssueResponse(@Header("TaxmannAuthorization") String user_token);

    @POST("BookStoreProductDetails/postSampleIssueList")
    Call<GeneralResponse> postSampleIssueResponse(@Body PostSampleIssue postSampleIssue, @Header("TaxmannAuthorization") String user_token);

    @POST("BookStoreProductDetails/postSampleJournalUserAddress")
    Call<GeneralResponse> postSampleJournalUserAddressResponse(@Body PostSampleJournalUserAddress postSampleJournalUserAddress, @Header("TaxmannAuthorization") String user_token);

    @POST("myAccount/removeFromWishList")
    Call<GeneralResponse> getRemoveWishListResponse(@Body JsonObject body, @Header("TaxmannAuthorization") String user_token);
}
