package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Productorderdetail {
    @SerializedName("ProdID")
    @Expose
    private Integer prodID;
    @SerializedName("OrderQty")
    @Expose
    private Integer orderQty;
    @SerializedName("Price")
    @Expose
    private Integer price;
    @SerializedName("Amount")
    @Expose
    private Integer amount;
    @SerializedName("Currency")
    @Expose
    private String currency;
    @SerializedName("GST")
    @Expose
    private Integer gST;
    @SerializedName("sMonth")
    @Expose
    private String sMonth;
    @SerializedName("sYear")
    @Expose
    private String sYear;

    public Integer getProdID() {
        return prodID;
    }

    public void setProdID(Integer prodID) {
        this.prodID = prodID;
    }

    public Integer getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(Integer orderQty) {
        this.orderQty = orderQty;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getGST() {
        return gST;
    }

    public void setGST(Integer gST) {
        this.gST = gST;
    }

    public String getSMonth() {
        return sMonth;
    }

    public void setSMonth(String sMonth) {
        this.sMonth = sMonth;
    }

    public String getSYear() {
        return sYear;
    }

    public void setSYear(String sYear) {
        this.sYear = sYear;
    }
}
