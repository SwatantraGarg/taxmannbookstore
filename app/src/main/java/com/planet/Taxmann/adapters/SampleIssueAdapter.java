package com.planet.Taxmann.adapters;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.planet.Taxmann.R;
import com.planet.Taxmann.model.SampleIssue;
import com.planet.Taxmann.model.Sublist;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SampleIssueAdapter extends RecyclerView.Adapter<SampleIssueAdapter.ViewHolder> {
    List<SampleIssue> sampleIssueList;
    int pos = 0;
    Context context;
    public static List<Sublist> value= new ArrayList<>();

    public SampleIssueAdapter(List<SampleIssue> sampleIssueList, int pos, Context context) {
        this.sampleIssueList = sampleIssueList;
        this.pos = pos;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SampleIssueAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.sample_issue_layout,null));
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(sampleIssueList!=null && sampleIssueList.size()>0){
            if(pos==position){
                holder.checkbox.setChecked(true);
                sampleIssueList.get(position).setIS_CHECKED(true);
            }
            Glide.with(context).load(sampleIssueList.get(position).getBookImg()).placeholder(R.drawable.demo_book).into(holder.bookImg);
            holder.bookName.setText(sampleIssueList.get(position).getProdName());
            holder.edition.setText(Html.fromHtml("<b>"+"Edition : " +"</b>"+sampleIssueList.get(position).getEdition()));
            holder.isbn.setText(Html.fromHtml("<b>"+"ISBN No : "+"</b>"+sampleIssueList.get(position).getISSN()));
            holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked){
                sampleIssueList.get(position).setIS_CHECKED(true);
                }
            else {
                sampleIssueList.get(position).setIS_CHECKED(false);
            }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return sampleIssueList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.checkbox)
        CheckBox checkbox;
        @BindView(R.id.book_img)
        ImageView bookImg;
        @BindView(R.id.book_name)
        TextView bookName;
        @BindView(R.id.edition)
        TextView edition;
        @BindView(R.id.isbn)
        TextView isbn;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
