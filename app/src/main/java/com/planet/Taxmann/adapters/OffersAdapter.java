package com.planet.Taxmann.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.planet.Taxmann.R;
import com.planet.Taxmann.model.Advertise;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.ViewHolder> {
    Context context;
    List<Advertise> advertiseList;

    public OffersAdapter(Context context, List<Advertise> advertiseList) {
        this.context = context;
        this.advertiseList = advertiseList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OffersAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.offers_layout,null));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.heading1.setText(advertiseList.get(position).getHeading1());
        holder.heading2.setText(advertiseList.get(position).getHeading2());

        holder.knowMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(advertiseList.get(position).getSiteurl());
                Intent intent= new Intent(Intent.ACTION_VIEW,uri);
                context.startActivity(intent);
            }
        });

        Glide.with(context).load(advertiseList.get(position).getImgurl()).into(holder.image);

    }

    @Override
    public int getItemCount() {
        return advertiseList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;

        @BindView(R.id.heading_1)
        TextView heading1;

        @BindView(R.id.heading_2)
        TextView heading2;

        @BindView(R.id.know_more)
        TextView knowMoreBtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
