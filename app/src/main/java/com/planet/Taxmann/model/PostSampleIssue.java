package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostSampleIssue {
    @SerializedName("Sublist")
    @Expose
    private List<Sublist> sublist = null;

    public List<Sublist> getSublist() {
        return sublist;
    }

    public void setSublist(List<Sublist> sublist) {
        this.sublist = sublist;
    }
}
