package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.CartAdapter;
import com.planet.Taxmann.adapters.ShipAddressAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.Addresses;
import com.planet.Taxmann.model.BillAddressResponse;
import com.planet.Taxmann.model.CartData;
import com.planet.Taxmann.model.CartList;
import com.planet.Taxmann.model.Country;
import com.planet.Taxmann.model.EntityResponse;
import com.planet.Taxmann.model.GeneralResponse;
import com.planet.Taxmann.model.GetCartResponse;
import com.planet.Taxmann.model.GstInfo;
import com.planet.Taxmann.model.OrSource;
import com.planet.Taxmann.model.OrderSource;
import com.planet.Taxmann.model.OrderSourceResponse;
import com.planet.Taxmann.model.States;
import com.planet.Taxmann.model.USerInfo;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.model.UserInfoResponse;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CheckOutActivity extends AppCompatActivity {
    @BindView(R.id.gst)
    TextView gst;

    @BindView(R.id.ship_india_txt)
    TextView shipIndiaTxt;

    @BindView(R.id.reffer_sub_layout)
    RelativeLayout refferSubLayout;

    @BindView(R.id.ship_charge)
    TextView shipCharge;

    @BindView(R.id.total_item)
    TextView totalItem;

    @BindView(R.id.order_total)
    TextView orderTotal;

    @BindView(R.id.sub_total_price)
    TextView subtotal;

    @BindView(R.id.add_new_btn)
    TextView addNewBtn;

    @BindView(R.id.spinner_reffer)
    Spinner spinnerReffer;

    @BindView(R.id.spinner_sub_reffer)
    SearchableSpinner spinnerSubReffer;

    @BindView(R.id.add_billbtn)
    TextView addNewBillBtn;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.checkbox)
    AppCompatCheckBox checkbox;

    @BindView(R.id.recycler_view_billing)
    RecyclerView recyclerViewBilling;

    @BindView(R.id.entity_name)
    TextView entityName;

    @BindView(R.id.entity_edit)
    EditText entityEdit;

    @BindView(R.id.business_name)
    TextView businessName;

    @BindView(R.id.spin_layout)
    RelativeLayout spinLayoutBusiness;

    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.gst_in_no)
    TextView gstInNoTxt;

    @BindView(R.id.gst_in_edit)
    EditText gstInEdit;

    @BindView(R.id.submit_layout)
    LinearLayout submitLayout;

    @BindView(R.id.submit_btn)
    TextView submitBtn;

    @BindView(R.id.cancel)
    TextView cancelBtn;

    @BindView(R.id.edt_layout)
    LinearLayout editLayout;

    @BindView(R.id.edit_btn)
    TextView editBtn;

    @BindView(R.id.remove)
    TextView removeBtn;

    @BindView(R.id.next_btn)
    TextView nextBtn;
    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.no_data_txt)
    TextView noDataTxt;

    @BindView(R.id.cart_layout)
    RelativeLayout cartLayout;
    @BindView(R.id.gst_txt)
    TextView gstTxt;

    @BindView(R.id.subtotal_txt)
    TextView subtotalTxt;


    @BindView(R.id.price)
    TextView price;

    @BindView(R.id.price_txt)
    TextView priceTxt;
    UserData userData;
    Intent intent;
    GstInfo gstInfo;
    int bill_add_id = 0;
    List<Addresses> billaddressesList = new ArrayList<>();

    public static final String GSTINFORMAT_REGEX = "[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9A-Za-z]{1}[Z]{1}[0-9a-zA-Z]{1}";
    public static final String GSTN_CODEPOINT_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    ShipAddressAdapter shipAddressAdapter;
    ShipAddressAdapter billAddressAdapter;
    List<OrderSource> orderSourceList =new ArrayList<>();
    String source,total_item;

    List<Addresses> addressList = new ArrayList<>();
    ArrayAdapter<String> dataAdapter;
    int add_id;
    String pincode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(CheckOutActivity.this,"user_data",UserData.class);
        getPersonalInfoResponse();
        getBillingAddressResponse();
        getGstInfoResponse();
        getEntityResponse();
        getOrderSourceResponse();
        intent = getIntent();

        if(intent!=null){
            source = intent.getStringExtra("source");
            total_item = intent.getStringExtra("total_item");
            getcartDetailsResponse(source,total_item,null);
        }


        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckOutActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckOutActivity.this, PaymentActivity.class);
                intent.putExtra("source",source);
                if(total_item!=null &&!total_item.equalsIgnoreCase("")){
                    intent.putExtra("total_item",total_item);
                }
                if(shipAddressAdapter!=null && addressList.size()>0){
                    if(add_id==0){
                       add_id = addressList.get(0).getAddressID();
                       pincode = addressList.get(0).getPinCode();
                    }
                    else if( add_id!=0){
                        intent.putExtra("add_id",add_id);
                        intent.putExtra("pin_code",pincode);
                    }
                    else {
                        Toast.makeText(CheckOutActivity.this, "Please add/select shipping address.", Toast.LENGTH_SHORT).show();
                    }
                    if(checkbox.isChecked()){
                        bill_add_id =add_id;
                        intent.putExtra("bill_add_id",bill_add_id);
                        startActivity(intent);
                    }
                    else {
                        if(billAddressAdapter!=null && billaddressesList.size()>0){
                            bill_add_id = billaddressesList.get(0).getAddressID();
                            intent.putExtra("bill_add_id",bill_add_id);
                            startActivity(intent);
                        }
                        else {
                            Toast.makeText(CheckOutActivity.this, "Please add/select billing address.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                else {
                    Toast.makeText(CheckOutActivity.this, "Please add/select shipping address.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entityName.setVisibility(View.GONE);
                businessName.setVisibility(View.GONE);
                gstInNoTxt.setVisibility(View.GONE);
                entityEdit.setVisibility(View.VISIBLE);
                spinLayoutBusiness.setVisibility(View.VISIBLE);
                gstInEdit.setVisibility(View.VISIBLE);
                editLayout.setVisibility(View.GONE);
                submitLayout.setVisibility(View.VISIBLE);
                if(gstInfo!=null){
                    entityEdit.setText(gstInfo.getLegalName());
                    gstInEdit.setText(gstInfo.getGSTINofBusiness());


                    if(dataAdapter!= null){
                        int pos = dataAdapter.getPosition(gstInfo.getTypeOfEntity());
                        spinner.setSelection(pos);
                    }}
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entityName.setVisibility(View.VISIBLE);
                businessName.setVisibility(View.VISIBLE);
                gstInNoTxt.setVisibility(View.VISIBLE);
                entityEdit.setVisibility(View.GONE);
                spinLayoutBusiness.setVisibility(View.GONE);
                gstInEdit.setVisibility(View.GONE);
                editLayout.setVisibility(View.VISIBLE);
                submitLayout.setVisibility(View.GONE);
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(!validGSTIN(gstInEdit.getText().toString())){
                        Toast.makeText(CheckOutActivity.this, "Enter valid GSTIN number", Toast.LENGTH_SHORT).show();
                    }
                    else if(entityEdit.getText().toString().isEmpty()){
                        Toast.makeText(CheckOutActivity.this, "please enter entity name", Toast.LENGTH_SHORT).show();
                    }
                    else if(gstInEdit.getText().toString().isEmpty()){
                        Toast.makeText(CheckOutActivity.this, "please enter GSTIN number", Toast.LENGTH_SHORT).show();
                    }
                    else if(spinner.getSelectedItemPosition()==0){
                        Toast.makeText(CheckOutActivity.this, "please select type of business", Toast.LENGTH_SHORT).show();
                    }

                    else {
                        String legal_name =entityEdit.getText().toString();
                        String business = String.valueOf(spinner.getSelectedItem());
                        String gst_number =gstInEdit.getText().toString();
                        updateGstnDetailsResponse(legal_name,business,gst_number);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        addNewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckOutActivity.this, EditAddressActivity.class);
                startActivity(intent);
            }
        });

        addNewBillBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckOutActivity.this, EditAddressActivity.class);
                intent.putExtra("bill","bill");
                startActivity(intent);
            }
        });

        spinnerReffer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int pos = spinnerReffer.getSelectedItemPosition();

                spinnerSubReffer.setVisibility(View.VISIBLE);
                setSubRefferList(orderSourceList,pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    recyclerViewBilling.setVisibility(View.GONE);
                    addNewBillBtn.setVisibility(View.GONE);
                }
                else {
                    recyclerViewBilling.setVisibility(View.VISIBLE);
                    addNewBillBtn.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getBillingAddressResponse();
        getPersonalInfoResponse();
    }

    public void getBillingAddressResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<BillAddressResponse> call= apiService.getBillingAddressesResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(CheckOutActivity.this,"Please wait");
        call.enqueue(new Callback<BillAddressResponse>() {
            @Override
            public void onResponse(Call<BillAddressResponse> call, Response<BillAddressResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(CheckOutActivity.this);
                int statusCode = response.code();
                BillAddressResponse billAddressResponse=response.body();
                if(billAddressResponse!=null) {
                   billaddressesList = billAddressResponse.getData();
                    if(billaddressesList!=null && billaddressesList.size()>0){
                        addNewBillBtn.setVisibility(View.GONE);
                        billAddressAdapter= new ShipAddressAdapter(CheckOutActivity.this, billaddressesList,"checkout", new ShipAddressAdapter.ButtonClick() {
                            @Override
                            public void clicked(View view, int pos) {
                                if(view.getId()==R.id.remove_btn){
                                    long add_id = billaddressesList.get(pos).getAddressID();
                                     deleteAddressResponse(add_id);
                                     billAddressAdapter.removeAt(pos);
                                        }
                                        else if(view.getId()==R.id.edit_btn){
                                            Intent intent = new Intent(CheckOutActivity.this, EditAddressActivity.class);
                                            intent.putParcelableArrayListExtra("address_info", (ArrayList<? extends Parcelable>) billaddressesList);
                                            intent.putExtra("position",pos);
                                            intent.putExtra("type","edit");
                                            intent.putExtra("bill","bill");
                                            startActivity(intent);
                                        }
                                    }
                                });
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CheckOutActivity.this,LinearLayoutManager.VERTICAL,false);
                                recyclerViewBilling.setLayoutManager(linearLayoutManager);
                                recyclerViewBilling.setAdapter(billAddressAdapter);

                    }
                    else {
                        addNewBillBtn.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<BillAddressResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(CheckOutActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
       public void getPersonalInfoResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<UserInfoResponse> call= apiService.getUserInfoResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(CheckOutActivity.this,"Please wait");
        call.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(CheckOutActivity.this);
                int statusCode = response.code();
                UserInfoResponse userInfoResponse=response.body();
                if(userInfoResponse!=null) {
                    USerInfo userInfo = userInfoResponse.getData();
                    if(userInfo!=null){
                        if(userInfo.getAddress()!=null && userInfo.getAddress().size()>0){
                          addressList = userInfo.getAddress();

                            if(addressList!=null && addressList.size()>0){

                                add_id = addressList.get(0).getAddressID();
                                pincode = addressList.get(0).getPinCode();
                                getcartDetailsResponse(source,total_item, String.valueOf(add_id));

                                shipAddressAdapter= new ShipAddressAdapter(CheckOutActivity.this, addressList,"checkout", new ShipAddressAdapter.ButtonClick() {
                                    @Override
                                    public void clicked(View view, int pos) {
                                        if(view.getId()==R.id.remove_btn){
                                            long add_id = addressList.get(pos).getAddressID();
                                            deleteAddressResponse(add_id);
                                            shipAddressAdapter.removeAt(pos);
                                        }
                                        else if(view.getId()==R.id.edit_btn){
                                            Intent intent = new Intent(CheckOutActivity.this, EditAddressActivity.class);
                                            intent.putParcelableArrayListExtra("address_info", (ArrayList<? extends Parcelable>) addressList);
                                            intent.putExtra("position",pos);
                                            intent.putExtra("type","edit");
                                            startActivity(intent);
                                        }
                                        else if(view.getId()==R.id.tag_radio){
                                             add_id = addressList.get(pos).getAddressID();
                                             pincode = addressList.get(pos).getPinCode();
                                            getcartDetailsResponse(source,total_item, String.valueOf(add_id));

                                        }
                                        else if(view.getId()==R.id.ship_layout){
                                            add_id = addressList.get(pos).getAddressID();
                                            pincode = addressList.get(pos).getPinCode();
                                            getcartDetailsResponse(source,total_item, String.valueOf(add_id));
                                        }
                                        else {
                                            add_id = addressList.get(pos).getAddressID();
                                        }

                                    }
                                });
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CheckOutActivity.this,LinearLayoutManager.VERTICAL,false);
                                recyclerView.setLayoutManager(linearLayoutManager);
                                recyclerView.setAdapter(shipAddressAdapter);
                            }
                            else {
                                getcartDetailsResponse(source,total_item,null);
                            }


                        }
                    }


                }


            }


            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(CheckOutActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void deleteAddressResponse(long add_id){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("addressId",add_id);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.deleteUserAddressResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD=Constants.ShowProgress(CheckOutActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    Toast.makeText(CheckOutActivity.this, response.body().getStatusMsg(), Toast.LENGTH_LONG).show();
                }
                else {
                    kProgressHUD.dismiss();
                    Toast.makeText(CheckOutActivity.this, response.body().getStatusMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(CheckOutActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    public void getEntityResponse() {
        UserData userData = DroidPrefs.get(CheckOutActivity.this, "user_data", UserData.class);
        final RetrofitEasyApi apiService = ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<EntityResponse> call = apiService.getEntityResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD = Constants.ShowProgress(this, "Please wait");
        call.enqueue(new Callback<EntityResponse>() {
            @Override
            public void onResponse(Call<EntityResponse> call, Response<EntityResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(CheckOutActivity.this);
                int statusCode = response.code();
                EntityResponse entityResponse = response.body();
                if (entityResponse != null) {
                    if(entityResponse.getData()!=null && entityResponse.getData().size()>0){
                        List<String> entities= new ArrayList<>();
                        entities.add("select");
                        for (int i = 0; i <entityResponse.getData().size() ; i++) {
                            String entity= entityResponse.getData().get(i).getName();
                            entities.add(entity);
                        }
                        // Creating adapter for spinner
                       dataAdapter = new ArrayAdapter<String>(CheckOutActivity.this, android.R.layout.simple_spinner_item, entities);

                        // Drop down layout style - list view with radio button
                        dataAdapter.setDropDownViewResource(R.layout.spinner_item);

                        // attaching data adapter to spinner
                        spinner.setAdapter(dataAdapter);
                    }
                }
            }
            @Override
            public void onFailure(Call<EntityResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(CheckOutActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void updateGstnDetailsResponse( String legal_name, String enitity, String gstno){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("GSTINofBusiness",gstno);
            jsonObject.put("LegalName",legal_name);
            jsonObject.put("TypeOfEntity",enitity);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.updateGstnDetailsResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(CheckOutActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(CheckOutActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    Toast.makeText(CheckOutActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                    entityName.setVisibility(View.VISIBLE);
                    businessName.setVisibility(View.VISIBLE);
                    gstInNoTxt.setVisibility(View.VISIBLE);

                    entityEdit.setVisibility(View.GONE);
                    spinLayoutBusiness.setVisibility(View.GONE);
                    gstInEdit.setVisibility(View.GONE);
                    editLayout.setVisibility(View.VISIBLE);
                    submitLayout.setVisibility(View.GONE);
                    getGstInfoResponse();
                }
                else {
                    Toast.makeText(CheckOutActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(CheckOutActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void deleteGstnDetailsResponse(String ref_id){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("refId",ref_id);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GeneralResponse> call= apiService.deleteGstnDetailsResponse(res,userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(CheckOutActivity.this,"Please wait");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(CheckOutActivity.this);
                int statusCode = response.code();
                GeneralResponse generalResponse=response.body();
                if(generalResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    Toast.makeText(CheckOutActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                    getGstInfoResponse();
                }
                else {
                    Toast.makeText(CheckOutActivity.this, generalResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(CheckOutActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void getGstInfoResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<UserInfoResponse> call= apiService.getUserInfoResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(CheckOutActivity.this,"Please wait");
        call.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(CheckOutActivity.this);
                int statusCode = response.code();
                UserInfoResponse userInfoResponse=response.body();
                if(userInfoResponse!=null){
                    USerInfo userInfo = userInfoResponse.getData();
                    if(userInfo!=null){
                         gstInfo = userInfo.getGtnInfo();
                        if (gstInfo != null) {
                            if (gstInfo.getLegalName() == null || gstInfo.getLegalName().equalsIgnoreCase("")) {
                                entityName.setText("N/A");
                            } else {
                                entityName.setText(gstInfo.getLegalName());
                            }

                            if (gstInfo.getTypeOfEntity() == null || gstInfo.getTypeOfEntity().equalsIgnoreCase("")) {
                                businessName.setText("N/A");
                            } else {
                                businessName.setText(gstInfo.getTypeOfEntity());
                            }

                            if (gstInfo.getGSTINofBusiness() == null || gstInfo.getGSTINofBusiness().equalsIgnoreCase("0")) {
                                gstInNoTxt.setText("N/A");
                            }
                            else {
                                gstInNoTxt.setText(gstInfo.getGSTINofBusiness());
                            }
                        }
                        else {
                            entityName.setText("N/A");
                            businessName.setText("N/A");
                            gstInNoTxt.setText("N/A");
                        }
                        removeBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                deleteGstnDetailsResponse(gstInfo.getRefId());
                            }
                        });

                    }
                }
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(CheckOutActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    private static boolean validGSTIN(String gstin) throws Exception {
        boolean isValidFormat = false;
        if (checkPattern(gstin, GSTINFORMAT_REGEX)) {
            isValidFormat = verifyCheckDigit(gstin);
        }
        return isValidFormat;

    }
    public static boolean checkPattern(String inputval, String regxpatrn) {
        boolean result = false;
        if ((inputval.trim()).matches(regxpatrn)) {
            result = true;
        }
        return result;
    }
    private static boolean verifyCheckDigit(String gstinWCheckDigit) throws Exception {
        Boolean isCDValid = false;
        String newGstninWCheckDigit = getGSTINWithCheckDigit(
                gstinWCheckDigit.substring(0, gstinWCheckDigit.length() - 1));

        if (gstinWCheckDigit.trim().equals(newGstninWCheckDigit)) {
            isCDValid = true;
        }
        return isCDValid;
    }
    public static String getGSTINWithCheckDigit(String gstinWOCheckDigit) throws Exception {
        int factor = 2;
        int sum = 0;
        int checkCodePoint = 0;
        char[] cpChars;
        char[] inputChars;

        try {
            if (gstinWOCheckDigit == null) {
                throw new Exception("GSTIN supplied for checkdigit calculation is null");
            }
            cpChars = GSTN_CODEPOINT_CHARS.toCharArray();
            inputChars = gstinWOCheckDigit.trim().toUpperCase().toCharArray();

            int mod = cpChars.length;
            for (int i = inputChars.length - 1; i >= 0; i--) {
                int codePoint = -1;
                for (int j = 0; j < cpChars.length; j++) {
                    if (cpChars[j] == inputChars[i]) {
                        codePoint = j;
                    }
                }
                int digit = factor * codePoint;
                factor = (factor == 2) ? 1 : 2;
                digit = (digit / mod) + (digit % mod);
                sum += digit;
            }
            checkCodePoint = (mod - (sum % mod)) % mod;
            return gstinWOCheckDigit + cpChars[checkCodePoint];
        } finally {
            inputChars = null;
            cpChars = null;
        }
    }
    public void getOrderSourceResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<OrderSourceResponse> call= apiService.getOrderSourceResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(CheckOutActivity.this,"Please wait");
        call.enqueue(new Callback<OrderSourceResponse>() {
            @Override
            public void onResponse(Call<OrderSourceResponse> call, Response<OrderSourceResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(CheckOutActivity.this);
                int statusCode = response.code();
                OrderSourceResponse orderSourceResponse=response.body();
                if(orderSourceResponse!=null) {
                orderSourceList = orderSourceResponse.getData();
                    List<String> reffers= new ArrayList<>();
                    for (int i = 0; i <orderSourceList.size() ; i++) {
                        String entity= orderSourceList.get(i).getRefName();
                        reffers.add(entity);
                    }


                    // Creating adapter for spinner
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(CheckOutActivity.this, android.R.layout.simple_spinner_item, reffers);

                    // Drop down layout style - list view with radio button
                    dataAdapter.setDropDownViewResource(R.layout.spinner_item);

                    // attaching data adapter to spinner
                    spinnerReffer.setAdapter(dataAdapter);
                    setSubRefferList(orderSourceList,0);
                }
            }
            @Override
            public void onFailure(Call<OrderSourceResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(CheckOutActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
    public void setSubRefferList(List<OrderSource> orderSourceList, int pos){
        refferSubLayout.setVisibility(View.VISIBLE);
        List<String> sub_reffers= new ArrayList<>();
        List<OrSource> orSourceList= orderSourceList.get(pos).getOrSource();

        for (int i = 0; i <orSourceList.size() ; i++) {
            String name = orSourceList.get(i).getRefName();
            sub_reffers.add(name);
        }

        // Creating adapter for spinner
        ArrayAdapter<String>  dataAdapter1 = new ArrayAdapter<String>(CheckOutActivity.this, android.R.layout.simple_spinner_item, sub_reffers);

        // Drop down layout style - list view with radio button
        dataAdapter1.setDropDownViewResource(R.layout.spinner_item);

        // attaching data adapter to spinner
        spinnerSubReffer.setAdapter(dataAdapter1);
        spinnerSubReffer.setTitle("Select");
        //the value you want the position for

        //  dataAdapter = (ArrayAdapter) spinnerCountry.getAdapter(); //cast to an ArrayAdapter

        if (spinnerReffer.getSelectedItem() != null) {
            String name = String.valueOf(spinnerReffer.getSelectedItem());
            spinnerSubReffer.setSelection(dataAdapter1.getPosition(name));


        }

}
    public void getcartDetailsResponse(String source,String total_item, String ship_id){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("cartSource",source);
            if(ship_id!=null && !ship_id.equalsIgnoreCase("")){
                jsonObject.put("shippingid",ship_id);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GetCartResponse> call= apiService.getCartDetailsResponse(res,userData.getUser_token());
        //final KProgressHUD kProgressHUD= Constants.ShowProgress(CartActivity.this,"Please wait");
        call.enqueue(new Callback<GetCartResponse>() {
            @Override
            public void onResponse(Call<GetCartResponse> call, Response<GetCartResponse> response) {
                // kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(CheckOutActivity.this);
                int statusCode = response.code();
                GetCartResponse getCartResponse=response.body();
                if(getCartResponse.getResponseType().equalsIgnoreCase("SUCCESS")){
                    if(getCartResponse.getStatus().equalsIgnoreCase("NOT_DELIVERABLE")){
                        noDataTxt.setVisibility(View.VISIBLE);
                        cartLayout.setVisibility(View.GONE);
                        nextBtn.setVisibility(View.GONE);
                        noDataTxt.setText(getCartResponse.getStatusMsg());
                    }
                    else {
                        noDataTxt.setVisibility(View.GONE);
                        cartLayout.setVisibility(View.VISIBLE);
                        nextBtn.setVisibility(View.VISIBLE);
                    }
                    if(getCartResponse.getData()!=null && !getCartResponse.getData().contains("")){
                        List<CartData> cartData = getCartResponse.getData();
                        String currency = "";
                        List<CartList> cartList =new ArrayList<>();
                        for (int i = 0; i <cartData.size() ; i++) {
                            cartList=cartData.get(i).getCartList();

                            if(cartList.get(i).getCurrency().equalsIgnoreCase("USD")){
                                currency = "$";
                                shipIndiaTxt.setVisibility(View.GONE);
                            }
                            else {
                                shipIndiaTxt.setVisibility(View.VISIBLE);
                                currency = "₹";
                            }
                            if(cartData.get(i).getShippingCharge()>0){
                                shipCharge.setText(currency+Math.round(cartData.get(i).getShippingCharge()));
                            }
                            else {
                                shipCharge.setText("Free");
                            }

                            if(cartData.get(i).getCartAmt()!= null){
                                shipIndiaTxt.setText("Free shipping in India on order above ₹"+Math.round(cartData.get(i).getCartAmt()));
                            }
                            else {
                                shipIndiaTxt.setVisibility(View.GONE);
                            }
                            if(cartData.get(i).getGst()!=null && !cartData.get(i).getGst().equals(0)){
                                gst.setText(currency+Math.round(cartData.get(i).getGst()));
                            }
                            else {
                                gst.setVisibility(View.GONE);
                            }
                            if(source.equalsIgnoreCase("B")){
                                totalItem.setText("1");
                            }
                            else {
                                totalItem.setText(total_item);
                            }

                            orderTotal.setText(currency+Math.round(cartData.get(i).getTotalAmount()));
                        }
                        double total_gst= 0;
                        double total_subtotal = 0;
                        for (int i = 0; i <cartList.size() ; i++) {
                            if(cartList.get(i).getProdTypeID()==11){
                                gstTxt.setText("GST 5%");
                                subtotalTxt.setText("Amount");
                                priceTxt.setVisibility(View.VISIBLE);
                                price.setVisibility(View.VISIBLE);
                                subtotal.setText(currency+Math.round(cartList.get(i).getUnitPrice()));
                                orderTotal.setText(currency+Math.round(cartList.get(i).getUnitPrice()*cartList.get(i).getQty()));
                                price.setText(currency+Math.round(cartList.get(i).getTotalPrice()));
                            }else {
                                subtotalTxt.setText("Subtotal");
                                gstTxt.setText("GST 18%");
                                priceTxt.setVisibility(View.GONE);
                                price.setVisibility(View.GONE);
                                total_subtotal = total_subtotal+cartList.get(i).getTotalPrice();
                                subtotal.setText(currency+Math.round(total_subtotal));
                            }
                        }
                    }
                }
                else {
                    Toast.makeText(CheckOutActivity.this, getCartResponse.getStatusMsg(), Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<GetCartResponse> call, Throwable t) {
                //kProgressHUD.dismiss();
                 Toast.makeText(CheckOutActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });

    }
}
