package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.BookAdapter;
import com.planet.Taxmann.adapters.OrderItemAdapter;
import com.planet.Taxmann.adapters.ShipmentProgressAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.BookData;
import com.planet.Taxmann.model.GetRecommendedResponse;
import com.planet.Taxmann.model.Item;
import com.planet.Taxmann.model.MyOrder;
import com.planet.Taxmann.model.MyOrderResponse;
import com.planet.Taxmann.model.OrderData;
import com.planet.Taxmann.model.ShipmentTrackActivity;
import com.planet.Taxmann.model.Shippment;
import com.planet.Taxmann.model.TrackShipmentData;
import com.planet.Taxmann.model.TrackShipmentResponse;
import com.planet.Taxmann.model.TrackingData;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.model.Years;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;


import java.util.ArrayList;
import java.util.List;

public class TrackingOrderActivity extends AppCompatActivity {
    @BindView(R.id.order_date)
    TextView orderDate;

    @BindView(R.id.recycler_view_suggest)
    RecyclerView recyclerViewSuggests;

    @BindView(R.id.order_amount)
    TextView orderAmount;

    @BindView(R.id.tracking_id)
    TextView trackingId;

    @BindView(R.id.carrier_name)
    TextView carrierName;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.recycler_view_progress)
    RecyclerView recyclerViewProgress;

    @BindView(R.id.need_help_btn)
    TextView needHelpBtn;

    UserData userData;
    Intent intent;
    String curiur_no, order_date, order_amount;
    List<Shippment> shippmentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking_order);
        ButterKnife.bind(this);
        userData = DroidPrefs.get(TrackingOrderActivity.this,"user_data",UserData.class);

        intent = getIntent();
        if(intent!=null){
            curiur_no = intent.getStringExtra("curiur_no");
        }

        getSuggestedBooksResponse(userData.getUser_token());

if(curiur_no!=null && !curiur_no.equalsIgnoreCase("")){
    getTrackMyOrderResponse(curiur_no);
    getTrackShipmentResponse(curiur_no);
}

        needHelpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(TrackingOrderActivity.this, WebviewActivity.class);
                browserIntent.putExtra("url","http://www.support.taxmann.com/");
                startActivity(browserIntent);
            }
        });

    }
    public void getTrackMyOrderResponse(String curiur_no){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<MyOrderResponse> call= apiService.getTrackMyOrderResponse(curiur_no, userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(TrackingOrderActivity.this,"Please wait");
        call.enqueue(new Callback<MyOrderResponse>() {
            @Override
            public void onResponse(Call<MyOrderResponse> call, Response<MyOrderResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                MyOrderResponse myOrderResponse = response.body();
                if(myOrderResponse!=null) {
                    OrderData orderData = myOrderResponse.getData();
                    if(orderData!=null){
                        List<MyOrder> myOrderList = orderData.getMyOrder();
                        for (int i = 0; i <myOrderList.size() ; i++) {
                            order_date = myOrderList.get(i).getOrderDate();
                            order_amount = myOrderList.get(i).getOrderAmount();
                             shippmentList = myOrderList.get(i).getShippment();
                        }
                        if(order_date!=null && !order_date.equalsIgnoreCase("")){
                            orderDate.setText(order_date);
                        }
                        else {
                            orderDate.setText("");
                        }
                        if(order_amount!=null && !order_amount.equalsIgnoreCase("")){
                            orderAmount.setText("₹ "+order_amount);
                        }
                        else {
                            orderAmount.setText("");
                        }
                        if(shippmentList!=null && shippmentList.size()>0){
                            List<Item> itemList = new ArrayList<>();
                            for (int i = 0; i <shippmentList.size(); i++) {
                                trackingId.setText(shippmentList.get(i).getCuriurNo());
                                carrierName.setText(shippmentList.get(i).getCuriurName());
                                 itemList = shippmentList.get(i).getItems();

                            }
                            OrderItemAdapter orderItemAdapter = new OrderItemAdapter(itemList, TrackingOrderActivity.this);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(TrackingOrderActivity.this,LinearLayoutManager.VERTICAL,false);
                            recyclerView.setLayoutManager(linearLayoutManager);
                            recyclerView.setAdapter(orderItemAdapter);
                        }

                    }
                    else {
                        Toast.makeText(TrackingOrderActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(TrackingOrderActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<MyOrderResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(TrackingOrderActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    public void getTrackShipmentResponse(String curiur_no){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<TrackShipmentResponse> call= apiService.getTrackShipmentResponse(curiur_no, userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(TrackingOrderActivity.this,"Please wait");
        call.enqueue(new Callback<TrackShipmentResponse>() {
            @Override
            public void onResponse(Call<TrackShipmentResponse> call, Response<TrackShipmentResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                TrackShipmentResponse trackShipmentResponse = response.body();
                if(trackShipmentResponse!=null) {
                    TrackShipmentData trackShipmentData = trackShipmentResponse.getData();
                    if(trackShipmentData!=null){
                        TrackingData trackingData = trackShipmentData.getTrackingData();
                        if(trackingData!=null){
                            List<ShipmentTrackActivity> trackingOrderActivityList = trackingData.getShipmentTrackActivities();
                            if(trackingOrderActivityList!=null && trackingOrderActivityList.size()>0){
                                ShipmentProgressAdapter shipmentProgressAdapter = new ShipmentProgressAdapter(trackingOrderActivityList,TrackingOrderActivity.this);
                                LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(TrackingOrderActivity.this,LinearLayoutManager.VERTICAL,false);
                                recyclerViewProgress.setLayoutManager(linearLayoutManager1);
                                recyclerViewProgress.setAdapter(shipmentProgressAdapter);
                            }
                        }
                    }



                }
                else {
                    Toast.makeText(TrackingOrderActivity.this, response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TrackShipmentResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(TrackingOrderActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    public  void getSuggestedBooksResponse(String token){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<GetRecommendedResponse> call= apiSerivce.getHomePageRecommendedProductsResponse(token);
        call.enqueue(new Callback<GetRecommendedResponse>() {
            @Override
            public void onResponse(Call<GetRecommendedResponse> call, Response<GetRecommendedResponse> response) {
                int status_code= response.code();
                GetRecommendedResponse getRecommendedResponse= response.body();
                if(getRecommendedResponse!=null){
                    List<BookData> recommendedBookList =getRecommendedResponse.getData();
                    if(recommendedBookList!=null && recommendedBookList.size()>0){
                       BookAdapter bookAdapter = new BookAdapter(TrackingOrderActivity.this,recommendedBookList, new BookAdapter.BookClicked() {
                            @Override
                            public void Clicked(View view, int position) {
                                Intent intent =new Intent(TrackingOrderActivity.this, BookDetailsActivity.class);
                                intent.putExtra("book_id", recommendedBookList.get(position).getMainProdId());
                                startActivity(intent);
                            }
                        });
                         GridLayoutManager gridLayoutManager=new GridLayoutManager(TrackingOrderActivity.this,2);
                       // LinearLayoutManager linearLayoutManager=new LinearLayoutManager(TrackingOrderActivity.this, LinearLayoutManager.HORIZONTAL,false);
                        recyclerViewSuggests.setLayoutManager(gridLayoutManager);
                        recyclerViewSuggests.setAdapter(bookAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetRecommendedResponse> call, Throwable t) {
                //  Toast.makeText(mainActivity, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
