package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostStateProfessionInterst {
    @SerializedName("state")
    private String state;

    @SerializedName("profession")
    private List<String> profession = null;

    @SerializedName("interest")
    private List<String> interest = null;

    @SerializedName("email")
    private String email;

    @SerializedName("professionOther")
    private String professionOther;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<String> getProfession() {
        return profession;
    }

    public void setProfession(List<String> profession) {
        this.profession = profession;
    }

    public List<String> getInterest() {
        return interest;
    }

    public void setInterest(List<String> interest) {
        this.interest = interest;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfessionOther() {
        return professionOther;
    }

    public void setProfessionOther(String professionOther) {
        this.professionOther = professionOther;
    }
}
