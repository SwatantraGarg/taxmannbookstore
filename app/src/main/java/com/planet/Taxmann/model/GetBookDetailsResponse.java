package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBookDetailsResponse {

    @SerializedName("ResponseType")
    @Expose
    private String responseType;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("StatusMsg")
    @Expose
    private String statusMsg;
    @SerializedName("Data")
    @Expose
    private BookDetails data;

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public BookDetails getData() {
        return data;
    }

    public void setData(BookDetails data) {
        this.data = data;
    }
}
