package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class USerInfo {
    @SerializedName("personalInfo")
    @Expose
    private PersonalInfo personalInfo;
    @SerializedName("gtnInfo")
    @Expose
    private GstInfo gtnInfo;
    @SerializedName("address")
    @Expose
    private List<Addresses> address = null;
    @SerializedName("userType")
    @Expose
    private List<UserType> userType = null;
    @SerializedName("proffessionInterest")
    @Expose
    private ProffessionInterest proffessionInterest;
    @SerializedName("professionOther")
    @Expose
    private String professionOther;
    @SerializedName("designations")
    @Expose
    private List<Designation> designations = null;

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    public GstInfo getGtnInfo() {
        return gtnInfo;
    }

    public void setGtnInfo(GstInfo gstInfo) {
        this.gtnInfo = gstInfo;
    }

    public List<Addresses> getAddress() {
        return address;
    }

    public void setAddress(List<Addresses> address) {
        this.address = address;
    }

    public List<UserType> getUserType() {
        return userType;
    }

    public void setUserType(List<UserType> userType) {
        this.userType = userType;
    }

    public ProffessionInterest getProffessionInterest() {
        return proffessionInterest;
    }

    public void setProffessionInterest(ProffessionInterest proffessionInterest) {
        this.proffessionInterest = proffessionInterest;
    }

    public String getProfessionOther() {
        return professionOther;
    }

    public void setProfessionOther(String professionOther) {
        this.professionOther = professionOther;
    }

    public List<Designation> getDesignations() {
        return designations;
    }

    public void setDesignations(List<Designation> designations) {
        this.designations = designations;
    }

}
