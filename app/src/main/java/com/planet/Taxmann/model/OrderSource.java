package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderSource {

    @SerializedName("refId")
    @Expose
    private String refId;
    @SerializedName("refName")
    @Expose
    private String refName;
    @SerializedName("orSource")
    @Expose
    private List<OrSource> orSource = null;

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getRefName() {
        return refName;
    }

    public void setRefName(String refName) {
        this.refName = refName;
    }

    public List<OrSource> getOrSource() {
        return orSource;
    }

    public void setOrSource(List<OrSource> orSource) {
        this.orSource = orSource;
    }
}
