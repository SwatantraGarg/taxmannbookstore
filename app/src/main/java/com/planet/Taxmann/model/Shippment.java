package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Shippment {
    @SerializedName("CuriurName")
    @Expose
    private String curiurName;
    @SerializedName("CuriurNo")
    @Expose
    private String curiurNo;
    @SerializedName("Items")
    @Expose
    private List<Item> items = null;

    public String getCuriurName() {
        return curiurName;
    }

    public void setCuriurName(String curiurName) {
        this.curiurName = curiurName;
    }

    public String getCuriurNo() {
        return curiurNo;
    }

    public void setCuriurNo(String curiurNo) {
        this.curiurNo = curiurNo;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
