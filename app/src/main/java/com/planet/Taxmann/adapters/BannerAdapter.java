package com.planet.Taxmann.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.planet.Taxmann.R;
import com.planet.Taxmann.activity.BookDetailsActivity;
import com.planet.Taxmann.model.Advertise;

import java.util.ArrayList;
import java.util.List;

public class BannerAdapter extends PagerAdapter {
    private List<Advertise> advertiseList;
    private LayoutInflater inflater;
    private Context context;


    public BannerAdapter(Context context, List<Advertise> advertiseList) {
        this.context = context;
        this.advertiseList = advertiseList;
        if(context!=null){
            inflater = LayoutInflater.from(context);
        }

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return advertiseList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.top_product_layout, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image);
        final TextView heading1 = (TextView)imageLayout.findViewById(R.id.heading_1);
        final TextView heading2 = (TextView)imageLayout.findViewById(R.id.heading_2);
        final TextView heading3 = (TextView)imageLayout.findViewById(R.id.heading_3);
        final TextView button = (TextView)imageLayout.findViewById(R.id.know_more);

        heading1.setText(advertiseList.get(position).getHeading1());
        heading2.setText(advertiseList.get(position).getHeading2());
        heading3.setText(advertiseList.get(position).getHeading3());

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(advertiseList.get(position).getSiteurl());
                Intent intent= new Intent(Intent.ACTION_VIEW,uri);
                context.startActivity(intent);
            }
        });


        Glide.with(context).load(advertiseList.get(position).getImgurl()).into(imageView);


        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}
