package com.planet.Taxmann.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.planet.Taxmann.R;
import com.planet.Taxmann.model.ReviewList;

import java.util.List;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReviewAdapter  extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {
    Context context;
    List<ReviewList> reviewList;

    public ReviewAdapter(Context context, List<ReviewList> reviewList) {
        this.context = context;
        this.reviewList = reviewList;
    }



    @NonNull
    @Override
    public ReviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ReviewAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.review_item,null));
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewAdapter.ViewHolder holder, int position) {
        if(reviewList!=null && reviewList.size()>0){
            holder.userName.setText(reviewList.get(position).getUserName());
            holder.date.setText(reviewList.get(position).getDate());
            holder.bookFormat.setText("Format: "+reviewList.get(position).getFormat());
            holder.reviewText.setText(reviewList.get(position).getText());

            int count=0;
            for (int i = 0; i < reviewList.get(position).getStarRating().size(); i++) {
                if(reviewList.get(position).getStarRating().get(i).equalsIgnoreCase("1")){
                    count++;
                }
            }
            if(count==0){
                holder.ratingBar.setRating(0);
            }
            else {
                holder.ratingBar.setRating(count);
            }
        }
    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.user_name)
        TextView userName;
        @BindView(R.id.book_format)
        TextView bookFormat;
        @BindView(R.id.myRatingBar)
        RatingBar ratingBar;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.review_text)
        TextView reviewText;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
