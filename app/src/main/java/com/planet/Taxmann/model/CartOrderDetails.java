package com.planet.Taxmann.model;
import com.google.gson.annotations.SerializedName;

public class CartOrderDetails {
    @SerializedName("ProdID")
    private Integer ProdID;

    @SerializedName("OrderQty")
    private Integer OrderQty;

    @SerializedName("Price")
    private Integer Price;

    @SerializedName("Amount")
    private Integer Amount;

    @SerializedName("Currency")
    private String Currency;

    @SerializedName("sMonth")
    private String sMonth;

    @SerializedName("sYear")
    private String sYear;

    public Integer getProdID() {
        return ProdID;
    }

    public void setProdID(Integer prodID) {
        ProdID = prodID;
    }

    public Integer getOrderQty() {
        return OrderQty;
    }

    public void setOrderQty(Integer orderQty) {
        OrderQty = orderQty;
    }

    public Integer getPrice() {
        return Price;
    }

    public void setPrice(Integer price) {
        Price = price;
    }

    public Integer getAmount() {
        return Amount;
    }

    public void setAmount(Integer amount) {
        Amount = amount;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }

    public String getsMonth() {
        return sMonth;
    }

    public void setsMonth(String sMonth) {
        this.sMonth = sMonth;
    }

    public String getsYear() {
        return sYear;
    }

    public void setsYear(String sYear) {
        this.sYear = sYear;
    }
}
