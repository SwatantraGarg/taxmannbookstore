package com.planet.Taxmann.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.planet.Taxmann.R;
import com.planet.Taxmann.activity.FinalRegistrationStepActivity;
import com.planet.Taxmann.model.AoiList;
import com.planet.Taxmann.model.ProfessionList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IntrestCheckBoxAdapter extends RecyclerView.Adapter<IntrestCheckBoxAdapter.ViewHolder> {
    List<AoiList> aoiLists;
    public static List<String> intrestCount=new ArrayList<>();
    Context context;
    public  boolean IS_SELECT;
    FinalRegistrationStepActivity finalRegistrationStepActivity;


    public IntrestCheckBoxAdapter(List<AoiList> aoiLists, Context context, Boolean IS_SELECT) {
        this.aoiLists = aoiLists;
        this.context = context;
        this.IS_SELECT=IS_SELECT;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkbox_text_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String s= aoiLists.get(position).getId();
        String text = s.replace("\n", "").replace("\r", "");
        holder.checkBoxTxt.setText(text);

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    intrestCount.add(aoiLists.get(position).getName());
                }
                else {
                    intrestCount.remove(aoiLists.get(position).getName());
                }
            }
        });




        if(IS_SELECT){
            for (int i = 0; i <aoiLists.size() ; i++) {
                holder.checkBox.setChecked(true);
            }
        }
        else {
            for (int i = 0; i <aoiLists.size() ; i++) {
                holder.checkBox.setChecked(false);
            }
        }








    }


    @Override
    public int getItemCount() {
        return aoiLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.checkbox_text)
        TextView checkBoxTxt;
        @BindView(R.id.checkbox)
        CheckBox checkBox;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
