package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BookDetails {
    @SerializedName("ItemType")
    @Expose
    private Integer itemType;
    @SerializedName("books")
    @Expose
    private Books books;
    @SerializedName("Journals")
    @Expose
    private Journals journals;
    @SerializedName("priviousIssues")
    @Expose
    private List<Object> priviousIssues = null;

    public List<String> getRequestedJournals() {
        return requestedJournals;
    }

    public void setRequestedJournals(List<String> requestedJournals) {
        this.requestedJournals = requestedJournals;
    }

    @SerializedName("requestedJournals")
    @Expose
    private List<String> requestedJournals = null;
    @SerializedName("relatedCombos")
    @Expose
    private List<BookData> relatedCombos;
    @SerializedName("customerWhoBoughtThisAlsoBought")
    @Expose
    private List<BookData> bookData = null;

    public Integer getItemType() {
        return itemType;
    }

    public void setItemType(Integer itemType) {
        this.itemType = itemType;
    }

    public Books getBooks() {
        return books;
    }

    public void setBooks(Books books) {
        this.books = books;
    }

    public Journals getJournals() {
        return journals;
    }

    public void setJournals(Journals journals) {
        this.journals = journals;
    }

    public List<Object> getPriviousIssues() {
        return priviousIssues;
    }

    public void setPriviousIssues(List<Object> priviousIssues) {
        this.priviousIssues = priviousIssues;
    }

    public List<BookData> getRelatedCombos() {
        return relatedCombos;
    }

    public void setRelatedCombos(List<BookData> relatedCombos) {
        this.relatedCombos = relatedCombos;
    }

    public List<BookData> getBookData() {
        return bookData;
    }

    public void setBookData(List<BookData> bookData) {
        this.bookData = bookData;
    }



}
