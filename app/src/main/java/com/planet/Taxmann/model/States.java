package com.planet.Taxmann.model;

import com.google.gson.annotations.SerializedName;

public class States {
    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @SerializedName("Id")
    String Id;

    @SerializedName("Name")
    String Name;


}
