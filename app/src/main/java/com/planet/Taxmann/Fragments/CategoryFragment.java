package com.planet.Taxmann.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.activity.MainActivity;
import com.planet.Taxmann.activity.ViewAllBooksActivity;
import com.planet.Taxmann.activity.WebviewActivity;
import com.planet.Taxmann.adapters.CustomExpandableListAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.AggregationsBySubject;
import com.planet.Taxmann.model.AggregationsBySubjectResponse;
import com.planet.Taxmann.model.BookData;

import com.planet.Taxmann.model.CategoryData;
import com.planet.Taxmann.model.CategoryItemList;
import com.planet.Taxmann.model.CategoryResponse;
import com.planet.Taxmann.model.CategorySubjectName;
import com.planet.Taxmann.model.CategorySubjectNameResponse;
import com.planet.Taxmann.model.Datum;

import com.planet.Taxmann.model.PostProductIndex;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CategoryFragment extends Fragment {

    @BindView(R.id.expandableListView)
    ExpandableListView expandableListView;

    @BindView(R.id.authors)
    TextView authorTxt;

    @BindView(R.id.dealers)
    TextView dealers;

    @BindView(R.id.adit)
    TextView aditTxt;
    MainActivity mainActivity;


    List<CategoryItemList> subCategoryList =new ArrayList<>();

    HashMap<String, List<String>> expandableListDetail=new HashMap<>();

    public CategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mainActivity = (MainActivity)context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this,v);
        getCategoryResponse();


        authorTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), WebviewActivity.class);
                intent.putExtra("url",Constants.WEB_URL+"bookstore/author");
                startActivity(intent);
            }
        });

        dealers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(getContext(),WebviewActivity.class);
                browserIntent.putExtra("url",Constants.WEB_URL+"dealer/search");
                startActivity(browserIntent);
            }
        });


        aditTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(getContext(), WebviewActivity.class);
                browserIntent.putExtra("url",Constants.WEB_URL+"bookstore/adit");
                startActivity(browserIntent);
            }
        });
        // Inflate the layout for this fragment
        return v;
    }

    public  void getCategoryResponse(){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);

        Call<CategoryResponse> call= apiSerivce.getCategoryResponse();
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                int status_code= response.code();
               CategoryResponse  categoryResponse= response.body();
                if(categoryResponse!=null){
                    List<CategoryData> data= categoryResponse.getData();
                    List<CategoryItemList> subCategoryList =new ArrayList<>();
                    List<String> expandableListTitle =new ArrayList<>();
                    for (int i = 0; i <data.size() ; i++) {
                        expandableListTitle.add(data.get(i).getCategoryName());
                        if(data.get(i).getCategoryItemList()==null){
                            subCategoryList.clear();
                            }
                        else {
                            subCategoryList.clear();
                            subCategoryList.addAll(categoryResponse.getData().get(i).getCategoryItemList());
                            }
                        List<String> subCategoryListTitle =new ArrayList<>();
                        for (int j = 0; j <subCategoryList.size() ; j++) {
                            subCategoryListTitle.add(subCategoryList.get(j).getItemName());
                        }
                        expandableListDetail.put(data.get(i).getCategoryName(),subCategoryListTitle);
                    }

                    ExpandableListAdapter expandableListAdapter = new CustomExpandableListAdapter(getContext(), expandableListTitle, expandableListDetail);
                    expandableListView.setAdapter(expandableListAdapter);
                    expandableListView.setVerticalScrollBarEnabled(false);
                    expandableListView.setFastScrollEnabled(false);
                    expandableListView.setSmoothScrollbarEnabled(false);
                    //expListView.setOverscrollHeader(null);
                    expandableListView.setFooterDividersEnabled(false);

                    expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                        private int lastExpandedPosition = -1;
                        @Override
                        public void onGroupExpand(int groupPosition) {
                            if (lastExpandedPosition != -1
                                    && groupPosition != lastExpandedPosition) {
                                expandableListView.collapseGroup(lastExpandedPosition);
                            }
                            lastExpandedPosition = groupPosition;
                        }
                            /*Toast.makeText(getContext(),
                                    expandableListTitle.get(groupPosition) + " List Expanded.",
                                    Toast.LENGTH_SHORT).show();*/

                    });

                    expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

                        @Override
                        public void onGroupCollapse(int groupPosition) {
                           /* Toast.makeText(getContext(),
                                    expandableListTitle.get(groupPosition) + " List Collapsed.",
                                    Toast.LENGTH_SHORT).show();*/

                        }
                    });

                    expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                        @Override
                        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                            if(groupPosition==6 && childPosition==0){
                                Intent browserIntent = new Intent(getContext(), WebviewActivity.class);
                                browserIntent.putExtra("url",Constants.WEB_URL+"pricing-combo-plans");
                                startActivity(browserIntent);
                            }
                            else if(groupPosition==6 && childPosition==3){
                                Intent browserIntent = new Intent(getContext(), WebviewActivity.class);
                                browserIntent.putExtra("url",Constants.WEB_URL+"pricing-e-services");
                                startActivity(browserIntent);
                            }
                            else if(groupPosition==6 && childPosition==4){
                                Intent browserIntent = new Intent(getContext(), WebviewActivity.class);
                                browserIntent.putExtra("url","https://exams.taxmann.com/");
                                startActivity(browserIntent);
                            }
                            else if(groupPosition==6 && childPosition==5){
                                Intent browserIntent = new Intent(getContext(), WebviewActivity.class);
                                browserIntent.putExtra("url","https://virtualbooks.taxmann.com/library");
                                startActivity(browserIntent);
                            }
                            else {
                                getSubjectIndexListResponse(String.valueOf(data.get(groupPosition).getCategoryId()), String.valueOf(data.get(groupPosition).getCategoryItemList().get(childPosition).getItemId()),data.get(groupPosition).getCategoryItemList().get(childPosition).getItemName());
                            }

                            return false;
                        }
                    });

                    expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                        @Override
                        public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                            if(groupPosition==5){
                                getCategorySubjectNameResponse( "10","0","Bookmann India");
                            }
                            return false;
                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                Toast.makeText(mainActivity, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getSubjectIndexListResponse(String cat_id,String subject_id, String subject_name){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<AggregationsBySubjectResponse> call= apiSerivce.getSubjectIndexListResponse(cat_id);
        call.enqueue(new Callback<AggregationsBySubjectResponse>() {
            @Override
            public void onResponse(Call<AggregationsBySubjectResponse> call, Response<AggregationsBySubjectResponse> response) {
                int status_code= response.code();
                AggregationsBySubjectResponse aggregationsBySubjectResponse= response.body();
                if(aggregationsBySubjectResponse!=null){
                    List<Datum> datumList=aggregationsBySubjectResponse.getData();
                    if(datumList!=null && datumList.size()>0){
                        getCategorySubjectNameResponse(cat_id,subject_id,subject_name);
                    }

                }
            }

            @Override
            public void onFailure(Call<AggregationsBySubjectResponse> call, Throwable t) {
                Toast.makeText(mainActivity, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void getCategorySubjectNameResponse(String cat_id, String subject_id, String subject_name){
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("subjectId",subject_id);
            jsonObject.put("subjectName",subject_name);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(jsonObject.toString()).getAsJsonObject();
        Call<CategorySubjectNameResponse> call= apiSerivce.getCategorySubjectNameResponse(res);
        call.enqueue(new Callback<CategorySubjectNameResponse>() {
            @Override
            public void onResponse(Call<CategorySubjectNameResponse> call, Response<CategorySubjectNameResponse> response) {
                int status_code= response.code();
                CategorySubjectNameResponse categorySubjectNameResponse= response.body();
                if(categorySubjectNameResponse!=null){
                    List<String> sub_array= new ArrayList<>();
                    if(cat_id.equalsIgnoreCase("10")){
                        sub_array.add("");
                    }
                    else {
                        sub_array.add(subject_name);
                    }
                    List<String> sub_type_array= new ArrayList<>();
                    sub_type_array.add("");
                    List<String> Author_array= new ArrayList<>();
                    Author_array.add("");
                    List<String> YearOfPublication_array= new ArrayList<>();
                    YearOfPublication_array.add("");
                    List<String> Availability_array= new ArrayList<>();
                    Availability_array.add("");
                    List<String> Format_array= new ArrayList<>();
                    Format_array.add("");
                PostProductIndex postProductIndex = new PostProductIndex();
                postProductIndex.setAuthorArray(Author_array);
                postProductIndex.setSubjectTypeArray(sub_type_array);
                postProductIndex.setFormatArray(Format_array);
                postProductIndex.setYearOfPublicationArray(YearOfPublication_array);
                postProductIndex.setAvailabilityArray(Availability_array);
                postProductIndex.setSubjectArray(sub_array);
                postProductIndex.setSearchId(subject_id);
                postProductIndex.setType("sub");
                postProductIndex.setFilterFrom("");
                postProductIndex.setPage(1);
                postProductIndex.setPagesize(200);
                postProductIndex.setCatId(cat_id);
                postProductIndex.setSortBy("sortpublicationdate");
                postProductIndex.setSearchString("");
                getProductIndexResponse(postProductIndex);

                }
            }

            @Override
            public void onFailure(Call<CategorySubjectNameResponse> call, Throwable t) {
                Toast.makeText(mainActivity, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void getProductIndexResponse(PostProductIndex postProductIndex){
        DroidPrefs.apply(getContext(),"cat_post_index",postProductIndex);
        RetrofitEasyApi apiSerivce= ApiClient.getClient().create(RetrofitEasyApi.class);
        final KProgressHUD kProgressHUD= Constants.ShowProgress(getContext(),"Please wait");
        Call<CategorySubjectNameResponse> call= apiSerivce.getProductIndexListResponse(postProductIndex);
        call.enqueue(new Callback<CategorySubjectNameResponse>() {
            @Override
            public void onResponse(Call<CategorySubjectNameResponse> call, Response<CategorySubjectNameResponse> response) {
                kProgressHUD.dismiss();
                int status_code= response.code();
                CategorySubjectNameResponse categorySubjectNameResponse= response.body();
                if(categorySubjectNameResponse!=null){
                    List<CategorySubjectName> categorySubjectNameList=categorySubjectNameResponse.getData();
                    List<BookData> featuredProductList= new ArrayList<>();
                    List<AggregationsBySubject> aggregationsBySubjectList= new ArrayList<>();
                    if(categorySubjectNameList!=null && categorySubjectNameList.size()>0){
                        for (int i = 0; i <categorySubjectNameList.size() ; i++) {
                            featuredProductList=categorySubjectNameList.get(i).getFeaturedProduct();
                            aggregationsBySubjectList=categorySubjectNameList.get(i).getAggregationsBySubject();

                        }
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("book_list", (ArrayList<? extends Parcelable>) categorySubjectNameList);
                        bundle.putString("type","category");
                        ViewAllFragment viewAllFragment = new ViewAllFragment();
                        viewAllFragment.setArguments(bundle);
                        getFragmentManager().beginTransaction().
                                addToBackStack(null)
                                .add(R.id.container, viewAllFragment).commit();
                    }

                }
                else {

                    kProgressHUD.dismiss();
                    Toast.makeText(getContext(), response.body().getStatusMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CategorySubjectNameResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
