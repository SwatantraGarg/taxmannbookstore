package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BookType {
    @SerializedName("featuredProduct")
    @Expose
    private List<BookData> featuredProduct = null;
    @SerializedName("newreleaseProduct")
    @Expose
    private List<BookData> newreleaseProduct = null;

    public List<BookData> getFeaturedProduct() {
        return featuredProduct;
    }

    public void setFeaturedProduct(List<BookData> featuredProduct) {
        this.featuredProduct = featuredProduct;
    }

    public List<BookData> getNewreleaseProduct() {
        return newreleaseProduct;
    }

    public void setNewreleaseProduct(List<BookData> newreleaseProduct) {
        this.newreleaseProduct = newreleaseProduct;
    }
}
