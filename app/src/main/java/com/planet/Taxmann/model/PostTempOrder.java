package com.planet.Taxmann.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostTempOrder {
    @SerializedName("IPAddress")
    @Expose
    private String iPAddress;
    @SerializedName("SessionID")
    @Expose
    private String sessionID;
    @SerializedName("Currency")
    @Expose
    private String currency;
    @SerializedName("PostageAmount")
    @Expose
    private Integer postageAmount;
    @SerializedName("ShippingAddress")
    @Expose
    private Integer shippingAddress;
    @SerializedName("dealertypeid")
    @Expose
    private Integer dealertypeid;
    @SerializedName("dealerid")
    @Expose
    private Integer dealerid;
    @SerializedName("JournalPostageOption")
    @Expose
    private Integer journalPostageOption;
    @SerializedName("PaymentFromDevice")
    @Expose
    private Integer paymentFromDevice;


    @SerializedName("Payment_Mode")
    @Expose
    private Integer Payment_Mode;
    @SerializedName("productorderdetails")
    @Expose
    private List<CartOrderDetails> productorderdetails = null;
    @SerializedName("cartSource")
    @Expose
    private String cartSource;

    public String getIPAddress() {
        return iPAddress;
    }

    public void setIPAddress(String iPAddress) {
        this.iPAddress = iPAddress;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getPostageAmount() {
        return postageAmount;
    }

    public void setPostageAmount(Integer postageAmount) {
        this.postageAmount = postageAmount;
    }

    public Integer getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Integer shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public Integer getDealertypeid() {
        return dealertypeid;
    }

    public void setDealertypeid(Integer dealertypeid) {
        this.dealertypeid = dealertypeid;
    }

    public Integer getDealerid() {
        return dealerid;
    }

    public void setDealerid(Integer dealerid) {
        this.dealerid = dealerid;
    }

    public Integer getJournalPostageOption() {
        return journalPostageOption;
    }

    public void setJournalPostageOption(Integer journalPostageOption) {
        this.journalPostageOption = journalPostageOption;
    }

    public Integer getPaymentFromDevice() {
        return paymentFromDevice;
    }

    public void setPaymentFromDevice(Integer paymentFromDevice) {
        this.paymentFromDevice = paymentFromDevice;
    }
    public Integer getPayment_Mode() {
        return Payment_Mode;
    }

    public void setPayment_Mode(Integer payment_Mode) {
        Payment_Mode = payment_Mode;
    }

    public List<CartOrderDetails> getProductorderdetails() {
        return productorderdetails;
    }

    public void setProductorderdetails(List<CartOrderDetails> productorderdetails) {
        this.productorderdetails = productorderdetails;
    }
    public String getCartSource() {
        return cartSource;
    }

    public void setCartSource(String cartSource) {
        this.cartSource = cartSource;
    }
}
