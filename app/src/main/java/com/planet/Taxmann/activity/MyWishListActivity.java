package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.adapters.BookAdapter;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.BookData;
import com.planet.Taxmann.model.BookDataResponse;
import com.planet.Taxmann.model.BookType;
import com.planet.Taxmann.model.Books;
import com.planet.Taxmann.model.MyOrderResponse;
import com.planet.Taxmann.model.OrderData;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.model.Years;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MyWishListActivity extends AppCompatActivity {
    @BindView(R.id.wish_spin)
    AppCompatSpinner wishSpin;

    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.tool_card)
    CardView toolCard;

    @BindView(R.id.wish_recycler_view)
    RecyclerView wishRecyclerView;

    @BindView(R.id.layout_fill)
    LinearLayout layoutFill;

    @BindView(R.id.layout_empty)
    RelativeLayout layoutEmpty;

    @BindView(R.id.explore_btn)
    TextView exploreBtn;

    UserData userData;
    List<BookData> releaseBookDataList= new ArrayList<>();
    List<BookData> featureBookDataList= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wish_list);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(MyWishListActivity.this,"user_data", UserData.class);
        getWishListResponse();
        toolCard.setVisibility(View.VISIBLE);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyWishListActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });

        exploreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyWishListActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        wishSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(wishSpin.getSelectedItem().toString().equalsIgnoreCase("Price(High to low)")){
                    Collections.sort(featureBookDataList, new Comparator<BookData>() {
                        @Override
                        public int compare(BookData lhs, BookData rhs) {
                            String p_lhs = lhs.getPriceInr();
                            p_lhs =  p_lhs.replaceAll(",","");

                            String p_rhs = rhs.getPriceInr();
                            p_rhs = p_rhs.replaceAll(",","");

                            double price_lhs = Double.parseDouble(p_lhs);
                            double price_rhs = Double.parseDouble(p_rhs);

                               return Double.compare(price_rhs, price_lhs);

                        }
                    });
                }
                else if(wishSpin.getSelectedItem().toString().equalsIgnoreCase("Price(Low to high)")){
                    Collections.sort(featureBookDataList, new Comparator<BookData>() {
                        @Override
                        public int compare(BookData lhs, BookData rhs) {
                            String p_lhs = lhs.getPriceInr();
                            p_lhs =  p_lhs.replaceAll(",","");

                            String p_rhs = rhs.getPriceInr();
                            p_rhs = p_rhs.replaceAll(",","");

                            double price_lhs = Double.parseDouble(p_lhs);
                            double price_rhs = Double.parseDouble(p_rhs);
                            return Double.compare(price_lhs, price_rhs);
                        }

                    });

                }
                else {
                    getWishListResponse();
                }
                setAdapter(featureBookDataList);

 }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getWishListResponse();
    }

    public void getWishListResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<BookDataResponse> call= apiService.getWishListResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(MyWishListActivity.this,"Please wait");
        call.enqueue(new Callback<BookDataResponse>() {
            @Override
            public void onResponse(Call<BookDataResponse> call, Response<BookDataResponse> response) {
                kProgressHUD.dismiss();
                Constants.hideSoftKeyboard(MyWishListActivity.this);
                int statusCode = response.code();
                BookDataResponse bookDataResponse = response.body();
                if(bookDataResponse!=null) {
                    List<BookType> bookTypeList = bookDataResponse.getData();
                    if(bookTypeList!=null && bookTypeList.size()>0){
                        wishRecyclerView.setVisibility(View.VISIBLE);
                        for (int i = 0; i <bookTypeList.size() ; i++) {
                            featureBookDataList =bookTypeList.get(i).getFeaturedProduct();
                        }
                        setAdapter(featureBookDataList);
                    }
                    else {
                        wishRecyclerView.setVisibility(View.GONE);
                        layoutEmpty.setVisibility(View.VISIBLE);
                        layoutFill.setVisibility(View.GONE);
                    }



                }


            }


            @Override
            public void onFailure(Call<BookDataResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(MyWishListActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void setAdapter(List<BookData> bookDataList){
        if(bookDataList!=null && bookDataList.size()>0){
            layoutEmpty.setVisibility(View.GONE);
            layoutFill.setVisibility(View.VISIBLE);
            BookAdapter bookAdapter = new BookAdapter(MyWishListActivity.this,featureBookDataList, new BookAdapter.BookClicked() {
                @Override
                public void Clicked(View view, int position) {
                    Intent intent =new Intent(MyWishListActivity.this,BookDetailsActivity.class);
                    intent.putExtra("book_id",bookDataList.get(position).getMainProdId());
                    startActivity(intent);
                }

            });

            GridLayoutManager gridLayoutManager=new GridLayoutManager(MyWishListActivity.this,2);
            wishRecyclerView.setLayoutManager(gridLayoutManager);
            wishRecyclerView.setAdapter(bookAdapter);
            bookAdapter.notifyDataSetChanged();
        }
        else {
            layoutEmpty.setVisibility(View.VISIBLE);
            layoutFill.setVisibility(View.GONE);
        }
    }
}
