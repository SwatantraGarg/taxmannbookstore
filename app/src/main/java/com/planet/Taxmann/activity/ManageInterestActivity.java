package com.planet.Taxmann.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.flexbox.FlexboxLayout;
import com.google.android.material.snackbar.Snackbar;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.planet.Taxmann.R;
import com.planet.Taxmann.interfaces.ApiClient;
import com.planet.Taxmann.interfaces.RetrofitEasyApi;
import com.planet.Taxmann.model.GstInfo;
import com.planet.Taxmann.model.PersonalInfo;
import com.planet.Taxmann.model.ProffessionInterest;
import com.planet.Taxmann.model.USerInfo;
import com.planet.Taxmann.model.UserData;
import com.planet.Taxmann.model.UserInfoResponse;
import com.planet.Taxmann.model.UserInterest;
import com.planet.Taxmann.model.UserProffession;
import com.planet.Taxmann.utils.Constants;
import com.planet.Taxmann.utils.DroidPrefs;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageInterestActivity extends AppCompatActivity {
    @BindView(R.id.interst)
    TextView interestTxt;

    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.proffesion)
    TextView proffesionTxt;

    @BindView(R.id.add_new_interst_btn)
    TextView changeInterest;

    @BindView(R.id.add_new_proffesion_btn)
    TextView changeProffession;
    USerInfo userInfo;
    UserData userData;
    List<UserInterest> interestList;
    List<UserInterest> proffessionList;
    List<String> intrestValueList = new ArrayList<>();
    List<String> proffesionValueList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_interest);
        ButterKnife.bind(this);

        userData = DroidPrefs.get(ManageInterestActivity.this,"user_data", UserData.class);
        getUserInfoResponse();
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageInterestActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });

        changeInterest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageInterestActivity.this,ChangeInterstActivity.class);
                intent.putParcelableArrayListExtra("intrest_list", (ArrayList<? extends Parcelable>) interestList);
                intent.putStringArrayListExtra("value_list", (ArrayList<String>) proffesionValueList);
                startActivity(intent);
            }
        });

        changeProffession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageInterestActivity.this,ChangeInterstActivity.class);
                intent.putParcelableArrayListExtra("intrest_list", (ArrayList<? extends Parcelable>) proffessionList);
                intent.putStringArrayListExtra("value_list", (ArrayList<String>) intrestValueList);
                intent.putExtra("type","1");
                if(userInfo.getProfessionOther()!=null && !userInfo.getProfessionOther().isEmpty()){
                    intent.putExtra("other_text",userInfo.getProfessionOther());
                }
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserInfoResponse();
    }

    public void getUserInfoResponse(){
        final RetrofitEasyApi apiService= ApiClient.getClient().create(RetrofitEasyApi.class);
        Call<UserInfoResponse> call= apiService.getUserInfoResponse(userData.getUser_token());
        final KProgressHUD kProgressHUD= Constants.ShowProgress(ManageInterestActivity.this,"Please wait");
        call.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                kProgressHUD.dismiss();
                int statusCode = response.code();
                UserInfoResponse userInfoResponse=response.body();
                if(userInfoResponse!=null){
                    userInfo = userInfoResponse.getData();
                    if(userInfo!=null){

                        ProffessionInterest proffessionInterest = userInfo.getProffessionInterest();

                        if(proffessionInterest!=null){

                           interestList = proffessionInterest.getUserInterest();
                            List<String> res = new ArrayList<>();


                            for (int i = 0; i < interestList.size(); i++) {

                                if(interestList.get(i).getSelected()==1){
                                    intrestValueList.add(interestList.get(i).getID());
                                    res.add(interestList.get(i).getValue().trim());
                                }


                            }
                            if(res!=null && res.size()>0){
                                interestTxt.setText(TextUtils.join(", ",res));
                                res.clear();
                                intrestValueList.clear();
                            }


                            proffessionList = proffessionInterest.getUserProffession();
                            List<String> res2= new ArrayList<>();

                            for (int i = 0; i < proffessionList.size(); i++) {

                                if(proffessionList.get(i).getSelected()==1){
                                    proffesionValueList.add(proffessionList.get(i).getID());
                                    res2.add(proffessionList.get(i).getValue().trim());
                                }
                            }

                            if(res2!=null && res2.size()>0){
                                if(userInfo.getProfessionOther()!=null && !userInfo.getProfessionOther().isEmpty()){
                                    proffesionTxt.setText(TextUtils.join(", ",res2)+", "+userInfo.getProfessionOther());
                                    res2.clear();
                                    proffesionValueList.clear();
                                }
                                else {
                                    proffesionTxt.setText(TextUtils.join(", ",res2));
                                    res2.clear();
                                    proffesionValueList.clear();
                                }
                            }
                            else {
                                if(userInfo.getProfessionOther()!=null && !userInfo.getProfessionOther().isEmpty()){
                                    proffesionTxt.setText(userInfo.getProfessionOther());
                            }
                                else {
                                    proffesionTxt.setText("");
                                }
                        }}
                    }
                }
            }
            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                kProgressHUD.dismiss();
                Toast.makeText(ManageInterestActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
